# -*- coding: utf-8 -*-

"""
:synopsis:   'django.wsgi' file for LymPHOS_UB_AC project in Quijost server.

:created:    03/05/2011

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.2 dev'
__UPDATED__ = '2017-07-11'

#==============================================================================
# Change these variables according to server and project
#==============================================================================
_home_folder = "/home/lymphos/"
_web_folder = "/home/lymphos/ptms_html/"
_venv_folder = "venv/"
_poject_name = "LymPHOS_UB_AC"

#==============================================================================
# WSGI application handler definition, environment configuration and debugging
# options: 
#==============================================================================
import os, sys, traceback

try:
    sys.path.append(_web_folder + _poject_name)
    sys.path.insert(1, _web_folder)
    #
    if _venv_folder:
        activate_this = _web_folder + _venv_folder + "bin/activate_this.py" 
        execfile( activate_this, dict(__file__=activate_this) )
#        import site
#        virtual_env_path = _web_folder + _venv_folder + 'lib/python2.7/site-packages'
#        site.addsitedir(virtual_env_path)
#        sys.path.append(virtual_env_path)
    #
    os.environ['DJANGO_SETTINGS_MODULE'] = _poject_name + ".settings"
#     os.environ['MPLCONFIGDIR'] = _web_folder + '.matplotlib'
    os.environ['PYTHON_EGG_CACHE'] = _home_folder + "tmp"
    
    # Django >=1.6 WSGI:
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    
except Exception as e: #Debugging
    err_title = 'Internal Error'
    err_msg = e.message
    err_trace = traceback.format_exc()
    with open(_web_folder + "logs/errors.log", 'a', 0) as io_file:
        io_file.write(err_title + "\n")
        io_file.write("  " + err_msg + "\n")
        io_file.write(err_trace)
    