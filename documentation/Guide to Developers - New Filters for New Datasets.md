
---

**WARNING!**: This is the *Old* source-code repository for TCellXTalk from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for this [same file at the _New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/code/ci/default/tree/documentation/Guide%20to%20Developers%20-%20New%20Filters%20for%20New%20Datasets.md) located at https://sourceforge.net/projects/tcellxtalk.lp-csic-uab.p/**  

---  
  
  
![TCellXTalk](https://bitbucket.org/lp-csic-uab/tcellxtalk/raw/default/static/images/logo.png)


---

**WARNING!**: This is the *Old* source-code repository for TCellXTalk from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for this [same file at the _New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/code/ci/default/tree/documentation/Guide%20to%20Developers%20-%20New%20Filters%20for%20New%20Datasets.md) located at https://sourceforge.net/projects/tcellxtalk.lp-csic-uab.p/**  

---  
  
  
This is the **Guide for Developers** for the web-app at [www.TCellXTalk.org](https://www.TCellXTalk.org), and [its MySQL database](https://www.tcellxtalk.org/download/).

With this guide, other developers can learn how to code new *filters* to import new Datasets of proteomic data into the TCellXTalk Database for these main purposes:

  1. New data from Human T-cell proteomic experiments can be imported into the current TCellXTalk Database, which can be downloaded at [https://www.TCellXTalk.org/download/](https://www.tcellxtalk.org/download/).
  2. New data from other Human cell lines can be integrated on the the current TCellXTalk Database (using the same starting database as above).
  3. New databases can be created from the ground up, for other cell lines or tissues of interest from Humans or other species. In those cases [the provided void database scheme](https://bitbucket.org/lp-csic-uab/tcellxtalk/src/default/TCellXTalk%20DB%20MySQL%20SQL%20schema.sql) will be needed.

**Table Of Contents:**

[TOC]

### Introduction

To import new datasets of proteomic data into the TCellXTalk Database, the information in those datasets has to be put into new records of the database tables, so first of all have a look at the [database structure scheme](https://bitbucket.org/lp-csic-uab/tcellxtalk/raw/default/documentation/TCellXTalkDB%20model%20v2.2.mwb) (you will need [MySQL Workbench](https://www.mysql.com/products/workbench/) to open the linked file) to get familiar with the tables and their relationships.

The import process can be divided into three main steps:

  * [**First Step**](#first-step-filling-ms-data): read the data in the dataset and transform it to meet the requirements of the database tables related to proteomic mass spectrometry data, mainly the _`mspeptides` and `mspepmodifications` tables_. You will also need to create a new _`source` record_ for the dataset.
  * [**Second Step**](#second-step-linking-ms-data-to-proteins): search every Peptide sequence (_`mspeptides` table_, _`seq` field_) against all the protein sequences stored in the database (_`proteins` table_, _`seq` field_), and link the matching ones through their primary keys using the _`proteins_mspeptides` table_.
  * [**Third Step**](#third-step-new-protein-modifications): for every Protein matched with modified Peptides from the new Source, a new set of _`protmodifications` records_ must be created from their corresponding _`mspepmodifications` records_, and get linked to them using the _`protmods_mspepmods` table_.


### First Step (filling MS Data)

The main tables to put the data read from a new dataset are the _`mspeptides` and `mspepmodifications` tables_. And, as you are importing a new dataset, a corresponding new _`sources` record_ should also be created.

#### A new _`sources` record_

The _**`sources` table**_ contains data about each source of information imported into TCellXTalkDB, so when importing a new dataset (a new source of proteomic data) a _new_ _`sources` record_ for this dataset should be created, containing (at least) the following **required** fields:

  * **`name`** : the name of the source/repository of the original information. E.g., `'Nguyen et al. 2016'`.
  * **`version`** : the version of the source/repository, if available (usually if the source is a database or is generated from one); otherwise, the year of publication of the dataset. E.g., `'2016'`.
  * **`ptmid2threshold`** : A [JSON-encoded](https://json.org/) dictionary of key/value, where the _keys_ are the [UniMod](http://www.unimod.org/) IDs for the PTMs whose position probability as been provided in the source dataset, and the _values_ are the thresholds used for each PTM. The special key `"default"` can be specified for a default threshold that applies to the PTM IDs not listed. E.g., `'{"21": 19.0}'`, `'{"121": 0.75}'`. `'{"default": 75.0}'`.

Also, it would be a good idea to fill the _`url` field_ with the URL link to the place where the dataset can be downloaded, or where it's referenced (e.g., `'http://www.sciencedirect.com/science/article/pii/S1874391915301731'`).

#### The imported _`mspeptides` records_

The _**`mspeptides` table**_ contains Experimental (Mass Spectrometry) Peptides. So in this table you have to fill (at least), for each _unique peptide_ found in the dataset, the following **required** fields:

  * **`seq`** : Raw sequence of amino-acids ([1-letter code](https://en.wikipedia.org/wiki/Amino_acid#Table_of_standard_amino_acid_abbreviations_and_properties), including `J`, `Z`, `B` and `X` for ambiguous amino-acids). E.g., `'SAAMJPSKK'`.
  * **`source_id`** : ID (*primary key*) of the _`source` record_ created previously for the dataset from which the MS Peptide was imported/created.
  * **`_db_seq_w_allmodos`** : Amino-acid sequence with **All** UniMod IDs corresponding to PTMs, enclosed in brackets, and after the modified amino-acid. E.g, `'S(1, 21)AAM(35)T(21)JPS(21)K(1, 121)K'`. **All** PTM IDs should be included, be the PTMs significant and of clear physiological origin or not. This fields defines unique MS Peptides on each Source (only one `_db_seq_w_mods` for the same `source_id`).
  * **`_db_seq_w_mods`** : Amino-acid sequence containing **only** UniMod IDs corresponding to PTMs that are _significant_ and from a clear _physiological origin_; the UniMod IDs also enclosed in brackets, and after the modified amino-acid. E.g., `'S(21)AAMTJPS(21)K(1, 121)K'`.
  * **`_db_mod_aas`** : Number of amino-acids containing any PTMs that are _significant_ and from a _physiological origin_. Following the previous examples this would be `3`.

You can also store the protein IDs reported in the source dataset into the record's _field `prots_from_source`_. This field is a JSON-encoded list of the protein IDs (such as [UniProt](https://www.uniprot.org) ACcessions). E.g., `'["P00001", "P00001-2", "Q84751-2"]'`.

#### The linked _`mspepmodifications` records_

Along with each new _`mspeptides` record_ being added to the _`mspeptides` table_ (along with each new MS Peptide) one or more _`mspepmodifications` records_ will be necessary to add to the _**`mspepmodifications` table**_, each one linked to the _`mspeptides` record_, to store the data related to each PTM of the newly created MS Peptide. So, the _`mspepmodifications` table_ is the one that stores all the PTMs of the Experimental MS Peptides. And (at least) the following fields are **required** for this table:

  * **`mspeptide_id`** : the ID (*primary key*) of the Experimental MS Prptide to which the PTM belongs (the aforementioned _`mspeptides` record_).
  * **`mod_type_id`** : UniMod ID corresponding to the PTM Type (e.g., `1`: Acetyl, `121`: GG, `21`: Phospho, `35`: Oxidation, ...). This must be also an ID (*primary key*) of a record already present in the _**`modificationtypes` table**_, which stores data for the different PTM Types as obtained from UniMod; so, if the record for the PTM Type you need is not already stored in this table, you would have to create a new one (currently TCellXTalk DB already contains _`modificationtypes` records_ for Acetilation (ID `1`), Phosphorilation (ID `21`), Ubiquitination (ID `121`) and Oxidation (ID `35`), of which only the last one has not a clear physiological origin and in consequence has its _`only_physio` field_ set to `0`).
  * **`position`** : the amino-acid position, in the MS Peptide sequence, where this PTM is located.
  * **`significant`** : indicates whether the PTM position is statistically significant or not (this is, if the PTM score (if provided in the dataset) is equal or greater than the threshold defined in the dataset or referenced in its original article), and thus whether this PTM should be considered or not as a valid PTM. The values of this field can be only `1` (_significant_) or `0` (_not_ significant).

If the source dataset provides the PTM position score, the value can be stored in the _`ascore` field_.

It's important to create _`mspepmodifications` records_ for **all** the PTMs of the new _`mspeptides` record_, including the *not-significant* ones, because the not-significant PTMs also count to the value of the _`_db_seq_w_allmodos` field_, which defines the *uniqueness* of a MS Peptide for a Source, as seen [before](#the-imported-mspeptides-records).


### Second Step (linking MS Data to Proteins)

*Search* every new MS Peptide sequence (_**`mspeptides` table**_, _`seq` field_) from the new source against all the Protein sequences already stored in the database (_**`proteins` table**_, _`seq` field_), and link the matching ones through their **primary keys** (the _`ac` field_ for _`proteins` records_ and the _`id` field_ for _`mspeptides` records_) using the _**`proteins_mspeptides` table**_; this is, for *every match* of a MS Pepdite sequence with a Protein sequence, create a new _`proteins_mspeptides` record_ with the following fields (**all are required**):

  * **`protein_ac`** : the value of the _`ac` field_ (*primary key*) of the matching Protein; which corresponds to the UniProt ACcession number of the Protein.
  * **`mspeptide_id`** : the value of the _`id` field_ (*primary key*) of the matching Experimental MS Prptide.

When *searching* a MS Peptide sequence into Protein sequences, you have to take into account the following **rules**:

  1. In MS terms the mass of a Leucine (`L`) can *not* be differentiated from the mass of an Isoleucine (`I`). This means that in MS Peptide sequences **`I` = `L`**. So, for example, if you have to search for a MS Peptide with the sequence `SAAMIPSKK`, you will also have to search for the sequence `SAAMLPSKK` (consider it like searching for the [regular expression](https://www.regular-expressions.info/quickstart.html) `SAAM[IL]PSKK`), and link Proteins matching any of both sequences to this same MS Peptide.
  2. A digestion with [Trypsin](https://en.wikipedia.org/wiki/Trypsin) only can generate tryptic MS Peptides with a **C-terminal modified Lysine (`K`)**, if this Lysine was located at the C-terminal of a Protein. So, if your imported MS Peptides come from a tryptic digestion, and have a C-terminal modified Lysine (`K`), can only be located at the C-terminal of Protein sequences to be *valid*. Any match found at any other location within Protein sequences will *not* be valid and *no* _`proteins_mspeptides` record_ should be created for it. And thus, if no match is found at the C-terminal of *at least one* Protein sequence, the MS Peptide will be considered *not valid* and their PTMs will not be "transfered" to any Protein (because it won't be linked to any Protein).


### Third Step (new Protein Modifications)

For every Protein matched with modified Peptides coming from the new Source, a new set of records in the _`protmodifications` table_ must be created from their corresponding _`mspepmodifications` records_ (the information of the PTMs from those MS Peptides should be "transfered" to their corresponding positions within the Protein sequence), and also get linked to them using the _`protmods_mspepmods` table_.

#### The new _`protmodifications` records_

So, for each of these Proteins, every of the matching MS Peptides sequences should be *aligned* to the Protein sequence, and the information of every PTM of these aligned Peptides (data from every _`mspepmodifications` record_ linked to the _`mspeptides` record_) should be **"transfered"** to a record in _**`protmodifications` table**_:

  * **`protein_ac`** : the value of the _`ac` field_ (*primary key*) of the matching Protein to which this PTM will belong.
  * **`mod_type_id`** : UniMod ID corresponding to the PTM Type of the MS Peptide PTM being "transfered". It must be also an ID (*primary key*) of a record already present in the _**`modificationtypes` table**_. It's value can be copied directly from the MS Peptide PTM being "transfered" (the value of the _`mod_type_id` field_ of the _`mspepmodifications` rerecords; see [above](#the-linked-mspepmodifications-records)).
  * **`position`** : the amino-acid position, in the Protein sequence, where this PTM is located. It's value can be calculated from the position of the aligned MS Peptide sequence within the Protein sequence, and the position of the PTM within the MS Peptide sequence.
  * **`source_id`** : ID (*primary key*) of the _`source` record_ created previously for the dataset from which the MS Peptide was imported/created (see [above](#a-new-sources-record)).
  * **`significant`** : indicates whether the PTM position in the Protein sequence can be considered as statistically _significant_ (value `1`) or _not_ (value `0`), and thus whether this PTM should be considered or not as a valid PTM. It's value should be set to `1` only if *at least one* of the _`mspepmodifications` records_ from the same Source (same _`source_id`_) and the same PTM Type (same _`mod_type_id`_) for this Protein position (the calculated Protein PTM _`position` field_ value seen before) is significant (_`significant`_ = `1`). This is, it's value is the greatest _`significant`_ value from the same Source MS Peptide PTMs that define this Protein PTM.

#### Linking _`mspepmodifications` rerecords_ to a _`protmodifications` record_

Every of the new created records in the _`protmodifications` table_ must be linked to their corresponding _`mspepmodifications` records_ using the _**`protmods_mspepmods` table**_. So, for every new _`protmodifications` record_, you have to create as many _`protmods_mspepmods` records_ as _`mspepmodifications` records_ summarized on it. And each of these _`protmods_mspepmods` records_ must have the following fields (**all are required**):

  * **`protmodification_id`** : the value of the _`id` field_ (*primary key*) of the new Protein PTM (the _`protmodifications` record_).
  * **`mspepmodification_id`** : the value of the _`id` field_ (*primary key*) of a MS Prptide PTM (a _`mspepmodifications` record_) used to define the new Protein PTM.


### Questions?

These programs and guidelines are made to be useful. If you have any question regarding this guide, or you want to contribute to this guide or new filters for new datasets, let us know about it at the e-mail below, and we will try to include them in future releases.

![lp . csic <at> uab . cat](https://bitbucket.org/lp-csic-uab/tcellxtalk/raw/default/static/images/contact.png)


---

**WARNING!**: This is the *Old* source-code repository for TCellXTalk from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for this [same file at the _New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/code/ci/default/tree/documentation/Guide%20to%20Developers%20-%20New%20Filters%20for%20New%20Datasets.md) located at https://sourceforge.net/projects/tcellxtalk.lp-csic-uab.p/**  

---  
  

*[MS]: Mass Spectrometry
*[PTM]: Post-Translational Modification
*[PTMs]: Post-Translational Modifications
*[ID]: IDentifier
*[IDs]: IDentifiers
*[X]: any amino-acid
*[J]: a Leucine (L) or an Isoleucine (I)
*[B]: an Asparagine (N) or an Aspartic acid (D)
*[Z]: a Glutamine (Q) or a Glutamic acid (E)
