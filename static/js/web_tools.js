/*
:synopsis: JS basci web functions for TCellXTalk project.

:created:    2015-07-01

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.2
:updated:    2018-05-07
*/

/*
 * Wait Notifications
 */
var please_wait_timeout; //for storing the time out timer reference.

function please_wait(caller, img_id, wait_ms) {
	// Check if caller is a form object or is within one, and avoid the blocking notification if form is not valid:
	if ( !(caller === undefined) ){
		var form=null;
		if (caller.nodeName == 'FORM'){
			form = caller;
		} else if (caller.parentElement.nodeName == 'FORM'){
			form = caller.parentElement
		}
		if ( form && !form.checkValidity() ){
			return false;
		}
	}
	// Check other parameters:
	if (img_id === undefined){ //1 -> Working image, 2 -> Searching image. Defaults to 1.
		img_id = 1;
	}
	if (wait_ms === undefined){ //milliseconds to wait before showing notification. Defaults to 1 second.
		wait_ms = 1000;
	}
	// Show notification:
    document.getElementById('img_overlay_' + img_id).style.display = 'block'; //Make the selected notification image displayable.
    please_wait_timeout = window.setTimeout(show_please_wait, wait_ms); //Sets the time out.
    return true;
}

function show_please_wait() {
	// Shows the overlay notification and the notification image:
    document.getElementById('overlay').style.display = 'block';
    document.getElementById('overlay_img').style.display = 'block';
	return true;
}

function hide_please_wait() {
	window.clearTimeout(please_wait_timeout); //Clears the timeout, to avoid collateral effects.
    //Hides the overlay notification and the notification image:
	document.getElementById('overlay_img').style.display = 'none';
    document.getElementById('overlay').style.display = 'none';
    return true;
}

/*
 * Wait Notification for File Downloads
 */
var please_wait_file_interval; //for storing the interval timer reference.
document.cookie = 'download_ready=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC'; //Expires previous file-download cookies.

function check_file_ready() {
    // Check if the download file is ready for download by checking for a cookie send to the browser when this happens.
    // Original idea to use cookies: http://stackoverflow.com/a/4168965
    var cookies = document.cookie.split(';');
    //Search cookies for a 'download_ready=True' one:
    var cookiesLength = cookies.length;
    for (var i = 0; i < cookiesLength; i++) {
    	var cookie = cookies[i].split('=');
    	var cookie_name = cookie[0].trim();
    	var cookie_value = cookie[1].trim();
    	if (cookie_name == 'download_ready' && cookie_value == 'True'){
    		window.clearInterval(please_wait_file_interval); //Stop checking for the file to be ready.
    		hide_please_wait(); //Hides the overlay notification and the notification image.
    		document.cookie = 'download_ready=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC'; //Expires current file-download cookie.
    		return true;
    	}
    }
    return false;
}

function please_wait_file(img_id, wait_ms) {
    // Inform the user that the server is working, and launch a periodic check for the file to be ready to download.
    please_wait(img_id, wait_ms); //Shows the overlay notification and the notification image.
    please_wait_file_interval = window.setInterval(check_file_ready, 1000); //Sets the time interval to check for the download_ready cookie.
    return true;
}


/*
 * Go-Top Button functions
 * Based on code from https://www.w3schools.com/howto/howto_js_scroll_to_top.asp
 */
function go_top() {
// When the user clicks on the button, scroll to the top of the document
    document.body.scrollTop = 0; //For Safari
    document.documentElement.scrollTop = 0; //For Chrome, Firefox, IE and Opera
}

window.onscroll = win_scroll_check;

function win_scroll_check() {
	// When the user scrolls down 150px from the top of the document, show the button:
    if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
        document.getElementById("go_top_button").style.display = "block";
    } else {
        document.getElementById("go_top_button").style.display = "none";
    }
}


/*
 * Submit form on enter key-press in a text field
 * Based on code from https://stackoverflow.com/a/478239
 */
function submitform_on_enter(kevent) {
	// When key code of the event is 13 (enter/return) and the text field that triggers the event is valid:
	if (kevent.which == 13 && kevent.target.validity.valid) {
		kevent.preventDefault(); //Avoid default behavior of pressing enter key.
		kevent.target.form.submit();
	}
}

