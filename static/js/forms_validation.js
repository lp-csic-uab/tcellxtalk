/*
:synopsis: JS functions to implement some forms validation in LymPHOS-UB-AC (TCellXTalk) project.

:created:    2015-06-15

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2011-2013 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.3
:updated:    2018-06-21
*/

/*
Implementation of min_interval_ctrl and max_interval_ctrl verifications.
*/
function bind_verify_interval_ctrls(min_interval_ctrl_id, max_interval_ctrl_id) {
	var min_interval_ctrl = document.getElementById(min_interval_ctrl_id);
	var max_interval_ctrl = document.getElementById(max_interval_ctrl_id);
	
	var verify_interval_ctrls = function() {
	    var this_value = parseInt(this.value, 10);
	    //Verify current value is within its limits:
	    var this_min = parseInt(this.min, 10);
	    var this_max = parseInt(this.max, 10);
	    if (this_value < this_min) {
	        this.value = this.min;
	        this_value = this_min;
	    } else if (this_value > this_max) {
	        this.value = this.max;
	        this_value = this_max;
	    }
	    //Verify coherence of minimum and maximum values (always minimum <= maximum):
	    if (this.id == min_interval_ctrl_id) {
	        if (this_value > parseInt(max_interval_ctrl.value, 10)) {
	            max_interval_ctrl.value = this_value;
	        }
	    } else if (this.id == max_interval_ctrl_id) {
	        if (this_value < parseInt(min_interval_ctrl.value, 10)) {
	            min_interval_ctrl.value = this_value;
	        }
	    }
	}
	
	//Attach to the change events of the text controls: it's triggered when
	//the user finishes changing their values (when focus leaves text control):
	min_interval_ctrl.onchange = verify_interval_ctrls;
	max_interval_ctrl.onchange = verify_interval_ctrls;
}


/*
Warning on GluC protease selection.
*/
function bind_alertongluc(protease_id) {
	var protease = document.getElementById(protease_id);
	
	function alertongluc() {
		  if (this.value == 3 & !this.alert_shown) { //Selected protease is GluC:
			  window.alert('Caution!\n\n' + 
					       'When GluC protease is selected, Ubiquitinatination can not be checked as a PTM required.');
			  this.alert_shown = true; //Alert already shown
		  }
	}
	
	protease.onchange = alertongluc; //Attach to the change event.
	protease.alert_shown = false; //Alert not shown.
}


