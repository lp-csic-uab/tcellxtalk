# -*- coding: utf-8 -*-

# django.wsgi for LymPHOS_UB_AC project in Quijost server.

'''
:created: 03/05/2011

:authors: OGA at LP CSIC/UAB (lp.csic@uab.cat)

:updated: 2015-06-15
'''

#==============================================================================
# Change these variables according to server and project
#==============================================================================
_home_folder = '/home/lymphos/'
_web_folder = '/home/lymphos/ptms_html/'
_venv_folder = 'venv/'
_poject_name = 'LymPHOS_UB_AC'

#==============================================================================
# WSGI application handler definition, environment configuration and debugging
# options: 
#==============================================================================
import os, sys
# import site
import traceback

try:
    sys.path.append(_web_folder + _poject_name)
    sys.path.insert(1, _web_folder)
    #
    if _venv_folder:
        activate_this = _web_folder + _venv_folder + 'bin/activate_this.py' 
        execfile( activate_this, dict(__file__=activate_this) )
#        virtual_env_path = _web_folder + _venv_folder + 'lib/python2.7/site-packages'
#        site.addsitedir(virtual_env_path)
#        sys.path.append(virtual_env_path)
    #
    os.environ['DJANGO_SETTINGS_MODULE'] = _poject_name + '.settings'
#     os.environ['MPLCONFIGDIR'] = _web_folder + '.matplotlib'
    os.environ['PYTHON_EGG_CACHE'] = _home_folder + 'tmp'
    
    # Old Django WSGI:
    #import django.core.handlers.wsgi
    #application = django.core.handlers.wsgi.WSGIHandler()
    
    # Django >=1.6 WSGI:
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    
    # Use paste module (pythonpaste.org) as debug middleware:
    from django.conf import settings
    if True:
        print >> sys.stderr, "Using Paste error middleware"
        #
        from paste.exceptions.errormiddleware import ErrorMiddleware
        #
        application = ErrorMiddleware(application, debug=True,
                                      show_exceptions_in_wsgi_errors=True)
except Exception as e: #Debugging
    indent = '  '
    err_title = 'Internal Error'
    err_msg = e.message
    err_trace = traceback.format_exc()
    with open(_web_folder + 'logs/errors.log', 'w') as io_file:
        io_file.write(err_title + '\n')
        io_file.write(indent + err_msg + '\n')
        io_file.write(err_trace)
    