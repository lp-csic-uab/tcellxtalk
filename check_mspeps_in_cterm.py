# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis: From a list of C-Term Ubiquitinated peptides, check if they are at the C-Term of some protein sequence from LymPHOS-UB-AC proteins.

:created:    2016/05/31

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.3.1'
__UPDATED__ = '2016-10-24'

#===============================================================================
# Imports
#===============================================================================
import sys
import os
import csv
from multiprocessing.dummy import Pool, cpu_count #Use the threading implementation of multiprocessing Pool, because this script is not CPU intensive to Python but MySQL, so it'a a matter of I/O parallelism.
from datetime import datetime
from time import time
from collections import OrderedDict
from functools import partial
 
from general.basic_func import save_dicts_as_csv

import proteomics.db_models as dbm


#===============================================================================
# Global variables
#===============================================================================
# MySQL default database, if none supplied at the CLI:
DB_URI = 'mysql://root@localhost/lymphosubac'

# Default Input file name, if none supplied at the CLI:
#INPUT_FILEN = '/home/oga/Laboratori de Proteomica CSIC-UAB/LymPHOS-UB-AC/Dades C-Term UB LP-CSIC-UAB - 2016-05-26/ub_ms_peptides_c-term_in_prots_2016-05-31_15558.csv' #Peptides C-Term Ubiquitinated from LP-CSIC/UAB experiments.
#INPUT_FILEN = '/home/oga/Laboratori de Proteomica CSIC-UAB/LymPHOS-UB-AC/Dades C-Term UB Udeshi i Quadroni - 2016-07-06/Ub-site_in_C-term_Quadroni-data' #Peptides C-Term Ubiquitinated from Quadroni article.
INPUT_FILEN = '/home/oga/Laboratori de Proteomica CSIC-UAB/LymPHOS-UB-AC/Dades C-Term UB Udeshi i Quadroni - 2016-07-06/Ub-site_in_C-term_Udeshi-data.csv' #Peptides C-Term Ubiquitinated from Udeshi article. 

# Default Output file name, if none supplied at the CLI:
now = datetime.now()
ofilen = "{0}_ms_peptides_c-term_in_prots_{1}_{2}{3}{4}.tsv".format('Udeshi',
                                                                    now.date(),
                                                                    now.hour,
                                                                    now.minute,
                                                                    now.second)
out_path = os.path.dirname(INPUT_FILEN) or os.getcwd()
OUTPUT_FILEN = os.path.join(out_path, ofilen)


#===============================================================================
# Function definitions
#===============================================================================
def iter_peps4filen(input_filen):
    """
    Get the peptide sequences to check from a CSV file.
    
    :param str input_filen: file name of the CSV file to read the data from.
    
    :return generator : yields peptide sequences.
    """
    with open(input_filen, 'rbU') as iofile:
        csvdr = csv.DictReader(iofile)
        for row in csvdr:
            if row['GlyGly C-terminal?'].upper() == 'SI':
                pep = row['Sequence']
                yield pep


def pep_cterminprots(peptide, db_uri, only_reviwed_prots=True):
    """
    Check if the peptide sequence is found at the C-term of any protein from 
    LymPHOS-UB-AC DataBase.
    
    :param str peptide: a peptide sequence.
    :param str db_uri: a DataBase URI.    
    :param bool only_reviwed_prots: search only on LymPHOS-UB-AC reviewed/swiss-prot(sp)
    proteins (True) or on all LymPHOS-UB-AC proteins (False). Defaults to True.
    
    :return tuple : returned tuple contains the peptide sequence and the
    list of protein ACcessions where this peptide sequence is located at the
    C-term.
    """
    # Create the database connection: #X_NOTE: we need to do it here because a :class:`DataBase` instance cannot be pickled and passed to sub-processes.
    prot_db = dbm.DataBase(db_uri)
    print( "Searching DB for %s at C-term..."%(peptide) ) #DEBUG
    # Open a session and do the search and filter of the result:
    with prot_db.session() as session:
        prot_acs = [ prot.ac for prot in
                     dbm.search_seq_in_prot_db(peptide, session, 
                                               global_tmplt="%{0}") 
                     if (only_reviwed_prots and prot.reviewed) ]
        return peptide, sorted(prot_acs)


def iter_pep_cterminprots2rowdict(pep_cterminprots):
    """
    Pipes the output from :func:`pep_cterminprots` into a valid
    :func:`save_dicts_as_csv` input.

    :param iterable pep_cterminprots: an iterable that contains tuples, each
    one containing a peptide sequence and the list of protein ACcessions where
    this peptide sequence is located at the C-term.

    :return generator : yields :class:`OrderedDict` instances with MS
    Peptides information, suitable to write to a file. A valid
    :func:`save_dicts_as_csv` input.
    """
    for peptide, prot_acs in pep_cterminprots:
        yield OrderedDict(( ('Peptide', peptide), 
                            ( 'C-Term in Prots', '; '.join(prot_acs) ),
                            ))


def main(db_uri, input_filen, output_filen):
    print( "Start MS Peptides Data Export from {0}\nPlease wait...".format(db_uri) )
    # Prepare the multiprocessing pool and data:
    peptides = iter_peps4filen(input_filen)
    map_func = partial(pep_cterminprots, db_uri=db_uri) #X_NOTE: needed because a lambda or function cannot be pickled and passed to sub-processes.
    pool = Pool( processes = max(2, cpu_count()-1) )
    # Process data in parallel:
    results = pool.map(map_func, peptides)
    pool.close()
    pool.join()
    # Save results:
    save_dicts_as_csv(output_filen,
                      iter_pep_cterminprots2rowdict(results),
                      delimiter='\t')
    #
    print( "\nResult Data saved to file:\n  {0}".format(output_filen) )
    #
    return 0



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    # CLI minimal argument parsing:
    cli_argv = sys.argv[1:]
    if cli_argv and ':/' in cli_argv[0]: # First argument is a DB URI
        DB_URI = cli_argv[0]
        cli_argv = cli_argv[1:]
    if cli_argv: # First or next (if first consumed) argument is a filename
        INPUT_FILEN = cli_argv[0]
        cli_argv = cli_argv[1:]
    if cli_argv: # First or next (if first consumed) argument is a filename
        OUTPUT_FILEN = cli_argv[0]
        cli_argv = cli_argv[1:]
    if cli_argv: # Other arguments -> incorrect ones
        sys.stderr.write('Syntax Error!\n\n')
        print('Usage:')
        print( " {0} [DataBase URI] [input file] [output file]".format( sys.argv[0]) )
        print('\nExamples:')
        print( " {0}".format(sys.argv[0]) )
        print( " {0} {1}".format(sys.argv[0], DB_URI) )
        print( " {0} {1} {2}".format(sys.argv[0], DB_URI, INPUT_FILEN) )
        print( " {0} {1} {2} {3}".format(sys.argv[0], DB_URI, INPUT_FILEN, OUTPUT_FILEN) )
        sys.exit(2)
    #
    t0 = time()
    #
    exit_code = main(DB_URI, INPUT_FILEN, OUTPUT_FILEN)
    #
    seconds = time() - t0
    print( "\nProcess took {0:.0f} minutes {0:.0f} seconds.".format(seconds//60, seconds%60) )
    #
    sys.exit(exit_code)
