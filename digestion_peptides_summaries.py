# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis: Create TSV files with results from queries over Digestion Peptides from a Protein Database for LymPHOS-UB-AC.
           Derivates from file 'digestion_peptides_summary.py' at it's version '0.3.2' (2016-08-02).

:created:    2015-02-11

:authors:    Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.11'
__UPDATED__ = '2018-03-26'

#===============================================================================
# Imports
#===============================================================================
import sys
import os
from datetime import datetime
from collections import OrderedDict, defaultdict

from sqlalchemy import desc, func, distinct, and_
from sqlalchemy.orm import aliased, joinedload

import proteomics.db_models as dbm
import proteomics.filters as filters
import proteomics.pepprot_func as ppf
from general.basic_func import save_dicts_as_csv


#===============================================================================
# Global variables/constants  #X-NOTE: :caution: Do not change/modify!!!
#===============================================================================
DB_URI = 'mysql://root@localhost/tcellxtalkdb' #MySQL default database, if none supplied in the CLI

now = datetime.now()
ofilen = "digestion_peptides_summary_{0:%Y-%m-%d_%H%M%S}.tsv".format(now)
OUTPUT_FILEN = os.path.join(os.getcwd(), ofilen) #Default output file name, if none supplied in the CLI

TCELLXTALK_SOURCE_IDS = (7,) # Source IDs corresponding to TCELLXTALK Experiments.

# Digestion Peptides variables:
MISSING_CLEAVAGES = 3 #X_NOTE: the maximum number of missing cleavages of experimental MS Peptides in DataBase was 3 (as of 2017-04-18).
MIN_LENGTH = 6 #X_NOTE: the minimum length of experimental MS Peptides in DataBase was 6 (as of 2017-04-18).
MAX_LENGTH = 65 #X_NOTE: the maximum length of experimental MS Peptides in DataBase was 63 (as of 2017-04-18).
MAX_PTMS = 3
MAX_EQUALPTMS = 2

# MIN_DIF_MODS = 2 #Default minimum number of different modification types a Digestion Peptide should have

# Void constants: #X-NOTE: :caution: Do not change/modify!!!
VOID_STR = ''
VOID_DICT = dict()
VOID_LIST = list()
VOID_SET = set()


#===============================================================================
# Class definitions
#===============================================================================



#===============================================================================
# Function definitions
#===============================================================================
def iter_multiptm_digpep(s, min_dif_mods): #TODO: Add a min_mod_aas=2 argument?
    """
    A Query and Summarizing function: it gets  Digestion Peptides from the
    LymPHOSUBAC DB sorted descending by number of associated modifications, and
    filters them by number of different modification types.
    
    :param Session s: a SQLAlchemy :class:`Session` instance.
    :param int min_dif_mods: minimum number of different modification types a 
    Digestion Peptide should have to be returned.
    
    :return genetator : yields filtered :class:`DigestionPeptide` instances,
    along with the list of different modification types associated with it.
    """  
    print( "Filtering Digestion Peptides with {0} or more different "\
           "Modifications...".format(min_dif_mods) )
    # Do the DB Query:
    query = s.query(dbm.DigestionPeptide)\
             .join(dbm.DigestionPeptide._db_modifications)\
             .filter(dbm.ProtModification.significant==True)\
             .distinct()\
             .group_by(dbm.DigestionPeptide.id)\
             .having(func.count(distinct(dbm.ProtModification.mod_type_id))>=min_dif_mods)\
             .order_by( desc(dbm.DigestionPeptide.mod_aas) )
    print('Getting and Processing Digestion Peptides...')
    for digpep in query:
        # Get the different Modification types associated with this Digestion Peptide: 
        diffmods = sorted( set(mod.mod_type.name 
                               for mod in digpep.modifications) )
        #
        yield digpep, diffmods


def get_query_digpep_with(s, mod_type1, mod_type2):
    """
    A Query-only function: it gets Digestion Peptides from the LymPHOSUBAC DB 
    having AT LEAST the Two provided Significant Modification Types.
    
    :param Session s: a SQLAlchemy :class:`Session` instance.
    :param int mod_type1: a valid Modification Type ID.
    :param int mod_type2: a valid Modification Type ID.
    
    :return Query : returns the resulting query for iterate over results or 
    further modifications/filtering.
    """
    print( "Filtering Digestion Peptides containing Significant PTMs {0}..."\
            .format( ", ".join(map( str, (mod_type1, mod_type2) )) ) )
    ptm_alias = aliased(dbm.ProtModification)
    return s.query(dbm.DigestionPeptide)\
            .join(dbm.DigestionPeptide.protein)\
            .join(dbm.DigestionPeptide._db_modifications)\
            .join(ptm_alias, dbm.DigestionPeptide._db_modifications)\
            .filter(dbm.Protein.reviewed==True)\
            .filter(dbm.ProtModification.significant==True,
                    dbm.ProtModification.mod_type_id==mod_type1)\
            .filter(ptm_alias.significant==True,
                    ptm_alias.mod_type_id==mod_type2)\
            .distinct()
    
def iter_digpep_with(session, mod_type1, mod_type2, filter_identical=False):
    """
    A Query and Summarizing function: it gets Digestion Peptides from the
    LymPHOSUBAC DB having AT LEAST the Two provided Significant Modification
    Types.
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param int mod_type1: a valid Modification Type ID.
    :param int mod_type2: a valid Modification Type ID.
    :param bool filter_identical: True to yield only different Digestion 
    Peptides, otherwise False. Defaults to False.
    
    :return genetator : yields filtered :class:`DigestionPeptide` instances.
    """
    not_filter_identical = not filter_identical
    cached_yield_digpeps = set()
    # Make the complex DB Query:
    query = get_query_digpep_with(session, mod_type1, mod_type2)\
                                 .group_by(dbm.DigestionPeptide.id)\
                                 .order_by(desc( func.count(dbm.ProtModification.id) ))
    #
    print('Getting and Processing Digestion Peptides...')
    #
    for digpep in query:
        if not_filter_identical:
            diffmods = sorted( {mod.mod_type.name for mod in digpep.modifications}  )
            yield digpep, diffmods
        else:
            digpep_fingerprint = (digpep.seq_w_mods, digpep.mod_aas, 
                                  digpep.missing_cleavages)
            if digpep_fingerprint not in cached_yield_digpeps:
                cached_yield_digpeps.add(digpep_fingerprint)
                diffmods = sorted( {mod.mod_type.name for mod in digpep.modifications}  )
                yield digpep, diffmods


def iter_digpep_only_with(s, mod_type1, mod_type2, filter_identical=False):
    """
    A Query and Summarizing function: it gets Digestion Peptides from the
    LymPHOSUBAC DB having ONLY the Two provided Significant Modification Types.
    
    :param Session s: a SQLAlchemy :class:`Session` instance.
    :param int mod_type1: a valid Modification Type ID.
    :param int mod_type2: a valid Modification Type ID.
    :param bool filter_identical: True to yield only different Digestion 
    Peptides, otherwise False. Defaults to False.
    
    :return genetator : yields filtered :class:`DigestionPeptide` instances.
    """
    cached_yield_digpep = set()
    modtype_ids = [mod_type1, mod_type2]
    #
    print( "Filtering Digestion Peptides only with Significant PTMs {0}..."\
            .format( ", ".join(map(str, modtype_ids)) ) )
    # Do the complex DB Query:
    ptm_alias = aliased(dbm.ProtModification)
    query = s.query(dbm.DigestionPeptide)\
             .join(dbm.DigestionPeptide.protein)\
             .join(dbm.DigestionPeptide._db_modifications)\
             .join(ptm_alias, dbm.DigestionPeptide._db_modifications)\
             .filter(dbm.Protein.reviewed==True)\
             .filter(dbm.ProtModification.significant==True,
                     dbm.ProtModification.mod_type_id==mod_type1)\
             .filter(ptm_alias.significant==True,
                     ptm_alias.mod_type_id==mod_type2)\
             .filter( ~dbm.DigestionPeptide._db_modifications.any(
                       and_( dbm.ProtModification.significant==True, 
                             dbm.ProtModification.mod_type_id.notin_(modtype_ids) )
                                                                 ) )\
             .distinct()\
             .order_by( desc(dbm.DigestionPeptide._db_mod_aas) )
#              .group_by(dbm.DigestionPeptide.id)\
#              .order_by(desc( func.count(dbm.ProtModification.id) ))
    #
    diffmods = sorted( mod_name for mod_name, 
                       in s.query(dbm.ModificationType.name)\
                           .filter( dbm.ModificationType.id.in_(modtype_ids) )
                       )
    #
    print('Getting and Processing Digestion Peptides...')
    #
    for digpep in query:
        if not filter_identical:
            yield digpep, diffmods
        else: # Check if digestion peptide has been previously yield:
            digpep_fingerprint = (digpep.seq_w_mods, digpep.mod_aas, 
                                  digpep.missing_cleavages)
            if digpep_fingerprint not in cached_yield_digpep:
                cached_yield_digpep.add(digpep_fingerprint)
                yield digpep, diffmods


def _iter_filter_digpeps(session, consensus_digpeps, mandatory_modids=None, #TO_DO: Update to match improvements in views.ProteinDigestView._filtered_combdigpeps() !!
                         max_miss_clvgs=MISSING_CLEAVAGES, min_length=MIN_LENGTH, 
                         max_length=MAX_LENGTH, max_mods=MAX_PTMS, 
                         max_equalmods=MAX_EQUALPTMS, consensus_peps_cache=None, 
                         redig_peps_cache=None, log_file=None):
    """
    A function to generate Combinatorial Re-Digested Peptides, and Filter them. 
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param iterable consensus_digpeps: an iterable of Consensus Digestion 
    Peptides. Usually a SQLAlchemy :class:`Query` instance.
    :param iterable mandatory_modids: ( (id1 OR id2) AND (id3 OR id4) AND ... ) 
    :param int max_miss_clvgs: maximum number of missing cleavages to 
    allow in the yielded Peptides. Defaults to global constant MISSING_CLEAVAGES.
    :param int min_length: minimum length of the yielded Digestion Peptides.
    Defaults to global constant MIN_LENGTH.
    :param int max_length: maximum length of the yielded Digestion Peptides.
    Defaults to global constant MAX_LENGTH.
    :param int max_mods: maximum number of total PTMs (modified AAs) to 
    allow in the yielded Digestion Peptides. Defaults to global constant 
    MAX_PTMS.
    :param int max_equalmods: maximum number of the same PTM to allow in the
     yielded Digestion Peptides. Defaults to 2 global constant MAX_EQUALPTMS.
    :param set consensus_peps_cache: a cache of retrieved consensus digestion
    peptides sequences with significant modifications. A more global cache set
    can be passed, or (if None, the default) a new one will be created.
    :param set redig_peps_cache: a cache of generated combinatorial re-digested
    peptides sequences with significant modifications. A more global cache set
    can be passed, or (if None, the default) a new one will be created.
    :param file log_file: a file object-like to write the generated peptides 
    to, if supplied. Defaults to None (no file output).
    
    :return generator : yields filtered Re-Digested Combinatorial :class:`DigestionPeptide`
    instances.
    """
    if consensus_peps_cache is None:
        consensus_peps_cache = set()
    if redig_peps_cache is None:
        redig_peps_cache = set()
    if log_file is None:
        save_log = False
    else:
        save_log = True
        # Write Log file headers:
        log_file.write("\t".join( ('Type', 'ID/Mark', 'Sequence with PTMs', 
                                    'Protein AC', 'Protein position (start-end)', 
                                    'Sequence', 'Modified AAs', 
                                    'Missing cleavages') ) + "\n")
    n_consensus_peps = 0
    session_updates = 0 #Used to control when to flush sessions instances to database for freeing some memory.
    VOID_DB_OBJ = dbm.SourceType()
    for cons_digpep in consensus_digpeps:
        # Check for previously processed Consensus Digestion Peptide, and avoid further processing:
        digpep_fingerprint = cons_digpep.seq_w_mods
        if digpep_fingerprint in consensus_peps_cache: continue # Skip this Consensus Digestion Peptide.
        consensus_peps_cache.add(digpep_fingerprint) #Add to cache...
        # Pre-Filter Consensus Digestion Peptides:
        if cons_digpep.length > max_length: # By sequence length:
            continue # Skip this consensus Digestion Peptide.
        n_consensus_peps += 1
        # Save to Log file, if provided:
        if save_log:
            log_file.write('\n')
            log_file.write("\t".join(( 'Consensus', str(cons_digpep.id), 
                                        digpep_fingerprint, cons_digpep.protein.ac, 
                                        "%s - %s"%(cons_digpep.prot_start, 
                                                   cons_digpep.prot_end), 
                                        cons_digpep.seq, str(cons_digpep.mod_aas),
                                        str(cons_digpep.missing_cleavages) )) + "\n")
        print( "Processing Consensus Peptide %d %s"%(n_consensus_peps, digpep_fingerprint) ) #DEBUG
        # Generate combinations of the peptide PTMs (Combinatorial Peptides):
        comb_digpeps = cons_digpep.iter_combinatorial_peptides(mandatory_modids=mandatory_modids) 
        last_combdigpep = VOID_DB_OBJ
        session.add(last_combdigpep)
        for comb_digpep in comb_digpeps:
            session.expunge(last_combdigpep) #Free some session memory.
            last_combdigpep = comb_digpep
            session_updates += 1
            # Save to Log file, if provided:
            if save_log:
                log_file.flush()
                log_file.write('\n')
                log_file.write("\t".join(( '  Combinatorial', "*", 
                                            comb_digpep.seq_w_mods, 
                                            comb_digpep.protein.ac, 
                                            "%s - %s"%(comb_digpep.prot_start, 
                                                       comb_digpep.prot_end), 
                                            comb_digpep.seq, 
                                            str(comb_digpep.mod_aas),
                                            str(comb_digpep.missing_cleavages) )) + "\n")
            # Try to re-digest the Combinatorial Peptide:
            redig_digpeps = dbm.trypsin.digest(comb_digpep, 
                                               max_miss_clvgs, #X-NOTE: Put this to 0 and remove post-filter if we want no missing cleavages except those due to modified K.
                                               min_length, max_length, 
                                               create_digestion_condition=False)
            # Post-Filter Combinatorial Re-digested Peptides:
            last_combredigpep = VOID_DB_OBJ
            session.add(last_combredigpep)
            for redig_digpep in redig_digpeps:
                session.expunge(last_combredigpep) #Free some session memory.
                last_combredigpep = redig_digpep
                session_updates += 1
                # Check for previously seen Re-digested Peptides, and avoid further processing and yielding:
                redigpep_fingerprint = redig_digpep.seq_w_mods
                if redigpep_fingerprint in redig_peps_cache: continue # Skip this Re-digested Peptide.
                redig_peps_cache.add(redigpep_fingerprint) #Add to cache...
                # FIXME: Correct error in missing cleavages count when using the re-digestion trick with Trypsin (X-NOTE: K or R at N-Term is never detected as a cleavage site by trypsin regex):
                if redig_digpep.prot_start > 1: #Peptide is not really at protein N-Term (where a trypsin target site should never be recognized/counted):
                    aa1, aa2 = redig_digpep.seq[:2]
                    if aa1 in ('K', 'R') and aa2 != 'P':
                        redig_digpep.missing_cleavages += 1
                # Save to Log file, if provided:
                if save_log:
                    data4save = ( redigpep_fingerprint, redig_digpep.protein.ac,
                                  "%s - %s"%(redig_digpep.prot_start, 
                                             redig_digpep.prot_end), 
                                  redig_digpep.seq, str(redig_digpep.mod_aas), 
                                  str(redig_digpep.missing_cleavages) )
                    log_file.write("\t".join( ('    Re-digested', "-") + 
                                               data4save ) + "\n")
                # -Filter by total number of missing cleavages:
                if max_miss_clvgs and redig_digpep.missing_cleavages > max_miss_clvgs: #Total number of missing cleavages is greater than the maximum allowed:
                    continue # Skip this Re-digested Peptide.
                # -Filter by presence of mandatory modifications: #X-NOTE: if no mandatory modifications (:param:`mandatory_modids` is None) unmodified peptides are not filtered out!
                modid2positions = redig_digpep.modid2positions
                combredigpep_modids = modid2positions.keys()
                if ( mandatory_modids is not None and #Filter is required...
                     any(map( set(combredigpep_modids).isdisjoint, mandatory_modids )) ): #And any mandatory PTM is not present in the modifications of the Combinatorial Re-digested Peptide:
                    continue # Skip this Re-digested Peptide.
                # -Filter by maximum number of total PTMs:
                combredigpep_nposs = map( len, modid2positions.values() ) or [0] #X-NOTE: [0] will allow unmodified peptides (see note above) to continue passing filters.
                if sum(combredigpep_nposs) > max_mods: #Number of total PTMs (modified AAs) in the combination of modifications is greater than the maximum allowed:
                    continue # Skip this Re-digested Peptide.
                # -Filter by maximum repetitions of the same PTM:
                if max(combredigpep_nposs) > max_equalmods: #Any number of each PTM > maximum allowed.
                    continue # Skip this Re-digested Peptide.
                # Save to Log file, if provided:
                if save_log:
                    log_file.write("\t".join( ('      Yielded', ">") + 
                                               data4save ) + "\n")
                yield redig_digpep, map(str, combredigpep_modids)
            session.expunge(last_combredigpep) #Free some session memory.
            if session_updates > 150000: #Free some session memory flushing instances to database:
                print('Flushing objects from session to database...') #DEBUG
                session.flush()
                session_updates = 0
                print("Memory used in `redig_peps_cache`: %d bytes"%redig_peps_cache.__sizeof__())
                print("Consensus Peptides Processed: %s"%n_consensus_peps) #DEBUG
        session.expunge(last_combdigpep) #Free some session memory.


def iter_digpep_Phos_w_Ub_or_Ac(session, max_miss_clvgs=3, min_length=6, 
                                max_length=50, max_mods=3, max_equalmods=2, *args):
    """
    A Query and Summarizing function: it gets Digestion Peptides from the
    LymPHOSUBAC DB that have AT LEAST One AA phosphorylated and One AA
    ubiquitinated or acetylated.
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param int max_miss_clvgs: maximum number of missing cleavages to 
    allow in the yielded Peptides. Defaults to 3.
    :param int min_length: minimum length of the yielded Digestion Peptides.
    Defaults to 6.
    :param int max_length: maximum length of the yielded Digestion Peptides.
    Defaults to 50 (X_NOTE: maximum value from experimental data was 46 AA, with a mean of 40.7 and a std deviation of 4.3).
    :param int max_mods: maximum number of total PTMs (modified AAs) to 
    allow in the yielded Digestion Peptides. Defaults to 3 (X_NOTE: maximum value from experimental MSPeptides in DataBase was 5, but only for 7 MSPeptides).
    :param int max_equalmods: maximum number of the same PTM to allow in the
     yielded Digestion Peptides. Defaults to 2 (X_NOTE: but maximum value used to search experimental data was 3).
    
    :return generator : yields filtered Combinatorial :class:`DigestionPeptide`
    instances.
    """
    cached_consensus_peps = set() # General caches shared with called functions
    cached_redig_peps = set()     #
    n_yielded_peps = 0
    # Mandatory Modifications:
    modid_types1 = (21,) #Phosphorylation
    modid_types2 = (1, 121) #Ubiquitination or acetylation
    mandatory_modids = (modid_types1, modid_types2) # ( (id1 OR id2) AND (id3 OR id4) AND ... )
    for mod_type1 in modid_types1:
        for mod_type2 in modid_types2:
            digpeps_query = get_query_digpep_with(session, mod_type1, mod_type2)
            digpeps = _iter_filter_digpeps(session, digpeps_query, 
                                           mandatory_modids, max_miss_clvgs, 
                                           min_length, max_length, max_mods, 
                                           max_equalmods, cached_consensus_peps, 
                                           cached_redig_peps)
            for digpep, diffmods in digpeps:
                n_yielded_peps += 1
                yield digpep, diffmods
    print("Total Re-digested Peptides Yielded: %s"%n_yielded_peps) #DEBUG
    del cached_consensus_peps
    del cached_redig_peps


def get_query_digpep_inprots_with(s, protein_acs, mod_type1, mod_type2):
    """
    A Query-only function: it gets Digestion Peptides from the LymPHOSUBAC DB,
    Only for the given Protein ACs, and having AT LEAST the Two provided
    Significant Modification Types.
    
    :param Session s: a SQLAlchemy :class:`Session` instance.
    :param list of str protein_acs: a list containing the protein ACs (without 
    isoform information) to restrict the search to.
    :param int mod_type1: a valid Modification Type ID.
    :param int mod_type2: a valid Modification Type ID.
    
    :return Query : returns the resulting query for iterate over results or 
    further modifications/filtering.
    """
    print( "Filtering Digestion Peptides containing Significant PTMs {0}..."\
            .format( ", ".join(map( str, (mod_type1, mod_type2) )) ) )
    ptm_alias = aliased(dbm.ProtModification)
    return s.query(dbm.DigestionPeptide)\
            .join(dbm.DigestionPeptide.protein)\
            .join(dbm.DigestionPeptide._db_modifications)\
            .join(ptm_alias, dbm.DigestionPeptide._db_modifications)\
            .filter( dbm.Protein.ac_noisoform.in_(protein_acs) )\
            .filter(dbm.ProtModification.significant==True,
                    dbm.ProtModification.mod_type_id==mod_type1)\
            .filter(ptm_alias.significant==True,
                    ptm_alias.mod_type_id==mod_type2)\
            .distinct()


def iter_digpep_Phos_w_Ub_or_Ac4proteins(session, prot_filen, max_miss_clvgs=0, 
                                         min_length=6, max_length=50, 
                                         max_mods=3, max_equalmods=2, *args):
    """
    A Query and Summarizing function: it gets Digestion Peptides from the
    LymPHOSUBAC DB that have AT LEAST One AA phosphorylated and One AA
    ubiquitinated or acetylated.
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param str prot_filen: file name of the data file containing the proteins 
    of interest.
    :param int max_miss_clvgs: maximum number of missing cleavages to 
    allow in the yielded Peptides. Defaults to 3.
    :param int min_length: minimum length of the yielded Digestion Peptides.
    Defaults to 6.
    :param int max_length: maximum length of the yielded Digestion Peptides.
    Defaults to 50 (X_NOTE: maximum value from experimental data was 46 AA, with a mean of 40.7 and a std deviation of 4.3).
    :param int max_mods: maximum number of total PTMs (modified AAs) to 
    allow in the yielded Digestion Peptides. Defaults to 3 (X_NOTE: maximum value from experimental MSPeptides in DataBase was 5, but only for 7 MSPeptides).
    :param int max_equalmods: maximum number of the same PTM to allow in the
     yielded Digestion Peptides. Defaults to 2 (X_NOTE: but maximum value used to search experimental data was 3).
    
    :return generator : yields filtered Combinatorial :class:`DigestionPeptide`
    instances.
    """
    cached_consensus_peps = set() # Generals caches shared with called functions
    cached_redig_peps = set()     #
    n_yielded_peps = 0
    # Mandatory Modifications:
    modid_types1 = (21,) #Phosphorylation
    modid_types2 = (1, 121) #Ubiquitination or acetylation
    mandatory_modids = (modid_types1, modid_types2) # ( (id1 OR id2) AND (id3 OR id4) AND ... )
    # Proteins of interest:
    protein_acs = list()
    with open(prot_filen, 'rU') as in_file:
        in_file.readline() #Discard headers
        for line in in_file:
            prot_ac, _ , _ = line.partition('\t')
            protein_acs.append( prot_ac.strip() )
    with open(OUTPUT_FILEN + ".prediction_log.tsv", 'wb') as log_file:
        for mod_type1 in modid_types1:
            for mod_type2 in modid_types2:
                digpeps_query = get_query_digpep_inprots_with(session, protein_acs, 
                                                              mod_type1, mod_type2)
                digpeps = _iter_filter_digpeps(session, digpeps_query, 
                                               mandatory_modids, max_miss_clvgs, 
                                               min_length, max_length, max_mods, 
                                               max_equalmods, cached_consensus_peps, 
                                               cached_redig_peps, log_file)
                for digpep, diffmods in digpeps:
                    n_yielded_peps += 1
                    yield digpep, diffmods
    print("Total Re-digested Peptides Yielded: %s"%n_yielded_peps) #DEBUG
    del cached_consensus_peps
    del cached_redig_peps


def seq_in_cterm(session, seq, only_reviwed_prots=True):
    """
    Check if the peptide sequence is found at the C-term of any protein from 
    LymPHOS-UB-AC DataBase.
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param str seq: a peptide sequence.
    :param bool only_reviwed_prots: search only on LymPHOS-UB-AC reviewed/swiss-prot(sp)
    proteins (True) or on all LymPHOS-UB-AC proteins (False). Defaults to True.
    
    :return list : a list of protein ACcessions where this peptide sequence is
    located at the C-term.
    """
    print( "Searching DB for %s at proteins C-term..."%(seq) ) #DEBUG
    # Do the Peptide Search in Proteins C-term and filter of the result:
    prot_acs = [ prot.ac for prot in
                 dbm.search_seq_in_prot_db(seq, session, 
                                           global_tmplt="%{0}") 
                 if (only_reviwed_prots and prot.reviewed) ]
    return prot_acs


def iter_digpep_with_Ub_Ac_same_pos(s, source_ids=TCELLXTALK_SOURCE_IDS, 
                                    filter_identical=False):
    """
    A Query and Summarizing function: it gets Digestion Peptides from the
    LymPHOSUBAC DB that can have the same AA Ubiquitinated or Acetylated.
    
    :param Session s: a SQLAlchemy :class:`Session` instance.
    :param iterable source_ids: an iterable of integers, which are the ID of the 
    Source repositories to filter Digestion Peptides with. Defaults to the 
    global TCELLXTALK_SOURCE_IDS.
    :param bool filter_identical: True to yield only different Digestion 
    Peptides, otherwise False. Defaults to False.
    
    :return genetator : yields filtered :class:`DigestionPeptide` instances.
    """
    cached_yield_digpep = set()
    modtype_ids = (1, 121)
    # Do the complex DB Query:
    ptm_alias = aliased(dbm.ProtModification)
    print( "Filtering Digestion Peptides with Significant PTMs {0}..."\
            .format( ", ".join(map(str, modtype_ids)) ) )
    query = s.query(dbm.DigestionPeptide)\
             .join(dbm.DigestionPeptide.protein)\
             .join(dbm.DigestionPeptide._db_modifications)\
             .join(ptm_alias, dbm.DigestionPeptide._db_modifications)\
             .filter(dbm.Protein.reviewed==True)\
             .filter( dbm.ProtModification.significant==True, 
                      dbm.ProtModification.mod_type_id==modtype_ids[0], 
                      dbm.ProtModification.source_id.in_(source_ids) )\
             .filter( ptm_alias.significant==True, 
                      ptm_alias.mod_type_id==modtype_ids[1], 
                      ptm_alias.source_id.in_(source_ids) )\
             .filter(dbm.ProtModification.position==ptm_alias.position)\
             .distinct()\
             .group_by(dbm.DigestionPeptide.id)\
             .order_by(desc( func.count(dbm.ProtModification.id) ))
    #
    diffmods = sorted( mod_name for mod_name, 
                       in s.query(dbm.ModificationType.name)\
                           .filter( dbm.ModificationType.id.in_(modtype_ids) )
                       )
    print('Getting and Processing Digestion Peptides...')
    for digpep in query:
        if not filter_identical:
            yield digpep, diffmods
        else:
            digpep_fingerprint = (digpep.seq_w_mods, digpep.mod_aas, 
                                  digpep.missing_cleavages)
            if digpep_fingerprint not in cached_yield_digpep:
                cached_yield_digpep.add(digpep_fingerprint)
                yield digpep, diffmods


def iter_exppeps4file(session, exp_filen, mandatory_modids=None):
    """
    A Query and Summarizing function: it compares predicted combinations of
    Digestion Peptides with Phosphorylations (Phos) and Acetylations (Ac) or
    Ubiquitinations (Ub) from LymPHOSUBAC DB with experimental found MS
    Peptides with Phos and Ac or Ub from a Zip file with P.D. 1.4 TSV search
    results.
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param str exp_filen: name of the Zip file with P.D. 1.4 TSV search results.
    :param iterable of iterables: mandatory Modification IDs: ( (id1 OR id2) AND (id3 OR id4) AND ... )
    
    :return tuple of lists : the Experimental MS Peptides Also found as
    predicted Digestion Peptides, and the Experimental MS Peptides NOT found as
    predicted Digestion Peptides.
    """
    ub_or_ac = (1, 121) #Ubiquitination or acetylation
    if mandatory_modids is None:
        mandatory_modids = ( (21,), ub_or_ac ) #Phosphorylation and (Ubiquitination or acetylation)
    # Get experimental found MS Peptides with mandatory Modification IDs from
    # file :param:`exp_filen`:
    tsv_fetcher = filters.PDTSVFetcher(exp_filen, cache_limit=None) #PD TSV importer filter to get files data without using a database session, avoiding repeated data by using a internal cache without limit of elements.
    exp_peps = { psm.mspeptide for psm in tsv_fetcher.iter_all_psms() } #Get ALL MS Peptides, allowing them to be updated with all the common PSMs (and their PSM Modifications) in TSV files.
    # Pre-Filter experimental MS Peptides:
    for exp_pep in exp_peps:
        pos2modids = { pos: {mod.mod_type.id for mod in mods} 
                       for pos, mods in exp_pep.pos2modifications.items() }
        # -Filter by presence of required PTMs:
        all_modids = reduce(set.union, pos2modids.values(), VOID_SET)
        if any( map(all_modids.isdisjoint, mandatory_modids) ):
            continue # Skip this experimental MS Peptide.
        # -Filter C-terminal Ub and Ac:
        if ( pos2modids.get(exp_pep.length, VOID_SET).intersection(ub_or_ac) #A C-terminal Ub or Ac...
             and not seq_in_cterm(session, exp_pep.seq) ): #And peptide is not at C-term of any protein:
                continue # Skip this experimental MS Peptide.
        #
        yield exp_pep


def exppep2rowdict(exp_pep, only_significant=False, session=None, if_sep=", "):
    """
    A formating function: it formats the summary data into row dictionaries
    that :func:`save_dicts_as_csv` can use to generate a CSV/TSV output file.
    
    :param MSPeptide exp_pep: a :class:`MSPeptide` instance from 
    experimental data.
    :param bool only_significant: True -> only Significant PTMs are considered,
    False -> all PTMs are considered. Defaults to False.
    :param Session session: a SQLAlchemy :class:`Session` instance. Needed to 
    obtain extra information (similar MS Peptides) from the database.
    :param str if_sep: intra-field separator. The separator to use between 
    multiple values for a single field. Defaults to ", ".
    
    :return OrderedDict : a :class:`OrderedDict` instance with Peptides
    information.
    """
    not_only_significant = not only_significant
    #
    print("Exporting %s"%exp_pep.seq_w_mods) #DEBUG
    # Peptide information:
    # - Different_Modifications:
    diffmods = sorted(set( mod.mod_type.name for mod in exp_pep.modifications
                           if (not_only_significant or mod.significant) ))
    # - Proteins:
    if exp_pep.proteins:
        protein_acs = [prot.ac for prot in exp_pep.proteins]
    elif exp_pep.prots_from_source:
        protein_acs = exp_pep.prots_from_source
    else:
        protein_acs = list()
    # - Experiments:
    experiments = [exp.name for exp in exp_pep.experiments]
    # - Similar MS Peptides in DataBase:
    mspeps = session.query(dbm.MSPeptide)\
                    .options( joinedload('_db_modifications')
                              .joinedload('mod_type'), 
                              joinedload('source') )\
                    .filter(dbm.MSPeptide.seq==exp_pep.seq)\
                    .all()
    frmtd_mspeps = ["{0} [{1}]".format(mspep.seq_w_mods, mspep.source.name) 
                    for mspep in mspeps]
    # - Scans Sources:
    scans = ["{0} {1}-{2}".format(psm.spectrum.raw_file, psm.spectrum.scan_i, 
                                  psm.spectrum.scan_f) for psm in exp_pep.psms]
    # - Build output row dictionary with Peptide information:
    rowdict = OrderedDict((('Peptide_Seq', exp_pep.seq), 
                           ('Peptide_Seq_With_PTMs', exp_pep.seq_w_mods), 
                           ('Total_Modified_AAs', exp_pep.mod_aas),
                           ('Proteins', if_sep.join(protein_acs)), 
                           ('Missing_Cleavages', exp_pep.missing_cleavages),
                           ('Different_Modifications', len(diffmods)), 
                           ('Modification_Types', if_sep.join(diffmods)), 
                           ('Experiments', if_sep.join(experiments)), 
                           ('Similar_MS_Peptides_from_DB', if_sep.join(frmtd_mspeps)), 
                           ('Scans_Sources', if_sep.join(scans)), 
                           ))
    # Associated Modifications information:
    for index, mod in enumerate(sorted(exp_pep.modifications), start=1):
        keyhead = "PTM_{0}_".format(index)
        moddict = OrderedDict(((keyhead+"Type", mod.mod_type.name), 
                               (keyhead+"Position", mod.position), 
                               (keyhead+"AA", mod.aa), 
                               (keyhead+"Ascore", mod.ascore), 
                               (keyhead+"Significant", mod.significant), 
                               ))
        # Add Modification information to output row dictionary:
        rowdict.update(moddict)
    #
    return rowdict


def iter_exppeps2rowdicts(exp_peps, only_significant=False, session=None, 
                          if_sep=", "):
    """
    Iterative wrapper for multiple Experimental Peptides for
    :func:`exppep2rowdict`.
    """
    for exp_pep in exp_peps:
        yield exppep2rowdict(exp_pep, only_significant, session, if_sep)


def comp_pred_and_exp_peps(session, exp_filen):
    """
    A Query and Summarizing function: it compares predicted combinations of
    Digestion Peptides with Phosphorylations (Phos) and Acetylations (Ac) or
    Ubiquitinations (Ub) from LymPHOSUBAC DB with experimental found MS
    Peptides with Phos and Ac or Ub from a Zip file with P.D. 1.4 TSV search
    results.
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param str exp_filen: name of the Zip file with P.D. 1.4 TSV search results.
    
    :return tuple of lists : the Experimental MS Peptides Also found as
    predicted Digestion Peptides, and the Experimental MS Peptides NOT found as
    predicted Digestion Peptides.
    """
    # Mandatory Modifications:
    modid_types1 = (21,) #Phosphorylation
    modid_types2 = (1, 121) #Ubiquitination or acetylation
    mandatory_modids = (modid_types1, modid_types2) # ( (id1 OR id2) AND (id3 OR id4) AND ... )
    # Get experimental found MS Peptides with Phos and Ac or Ub from file
    # :param:`exp_filen`:
    seqwmods2exppeps = defaultdict(set)
    for exp_pep in iter_exppeps4file(session, exp_filen, mandatory_modids):
        seqwmods2exppeps[exp_pep.seq_w_mods].add(exp_pep)
    # Compare predicted combinations of Digestion Peptides and experimental
    # found MS Peptides:
    # - Experimental MS Peptides Also found as predicted Digestion Peptides 
    #   (the common ones):
    with open(OUTPUT_FILEN + ".allpredicted.tsv", 'wb') as io_file:
        # Write headers:
        io_file.write("\t".join( ['Digestion_Peptide', 
                                  'Digestion_Peptide_With_PTMs', 
                                  'Modification_Types'] ) + "\n")
        common_exppep_mspeps = list()
        for pred_digpep, diffmods in iter_digpep_Phos_w_Ub_or_Ac(session):
            # Write each predicted Digestion Peptide to file:
            io_file.write("\t".join( [pred_digpep.seq, pred_digpep.seq_w_mods, 
                                      ", ".join(diffmods)] ) + "\n")
            io_file.flush() #Immediate write to disc.
            # Get and remove from further comparisons experimental peptides 
            # that are equal to the predicted Digestion Peptide, if any:
            exp_peps = seqwmods2exppeps.pop(pred_digpep.seq_w_mods, VOID_SET)
            for exp_pep in exp_peps:
                common_exppep_mspeps.append( exppep2rowdict(exp_pep, session=session) )
    # - Experimental MS Peptides NOT found as predicted Digestion Peptides 
    #   (the differential ones):
    diff_exppep_mspeps = list()
    for exp_peps in seqwmods2exppeps.values():
        for exp_pep in exp_peps:
            diff_exppep_mspeps.append( exppep2rowdict(exp_pep, session=session) )
    #
    return common_exppep_mspeps, diff_exppep_mspeps


def iter_digpep_diffmods2rowdicts(digpeps_diffmods, session=None, if_sep=", "):
    """
    A formating function: it formats the summary data into something 
    :func:`save_dicts_as_csv` can digest to generate a CSV/TSV output file.
    It pipes the output from either :func:`iter_multiptm_digpep`, 
    :func:`iter_digpep_only_with` or :func:`iter_digpep_only_with`into a valid 
    :func:`save_dicts_as_csv` input.
    
    :param iterable digpeps_diffmods: an iterable that contains tuples
    of :class:`DigestionPeptide` instances and the list of different
    modification types associated with it. Usually the output from either
    :func:`iter_multiptm_digpep` or :func:`iter_digpep_only_with`.
    :param Session session: an optional SQLAlchemy :class:`Session` instance. 
    Not needed and not used in this formating function implementation.
    :param str if_sep: intra-field separator. The separator to use between 
    multiple values for a single field. Defaults to ", ".
        
    :return generator : yields :class:`OrderedDict` instances with Digestion
    Peptides information, suitable to write to a file. A valid
    :func:`save_dicts_as_csv` input.
    """
    for digpep, diffmods in digpeps_diffmods:
        sys.stdout.write("Exporting %s        \r"%digpep.seq_w_mods) # DEBUG
        sys.stdout.flush()                                           #
        # Digestion Peptide information:
        rowdict = OrderedDict((('Digestion_Peptide', digpep.seq), 
                               ('Digestion_Peptide_With_PTMs', digpep.seq_w_mods), 
                               ('Total_Modified_AAs', digpep.mod_aas),
                               ('Protein_1st', digpep.protein.ac), 
                               ('Start_Position', digpep.prot_start), 
                               ('End_Position', digpep.prot_end), 
                               ('Missing_Cleavages', digpep.missing_cleavages),
                               ('Different_Modifications', len(diffmods)), 
                               ('Modification_Types', if_sep.join(diffmods)), 
                               ))
        # Associated MS Peptides:
        mspeptides = ", ".join( "%s [%s]"%(mspep.seq_w_mods, mspep.source.name) 
                                for mspep in digpep.mspeptides_similar )
        rowdict['Similar_MS_Peptides'] = mspeptides
        # Associated Modifications information:
        pos_shift = digpep.prot_start - 1
        for index, mod in enumerate(sorted(digpep.modifications), start=1):
            keyhead = "PTM_{0}_".format(index)
            moddict = OrderedDict(((keyhead+"Type", mod.mod_type.name), 
                                   (keyhead+"Source", mod.source.name), 
                                   (keyhead+"Position", mod.position - pos_shift), 
                                   (keyhead+"AA", mod.aa), 
                                   (keyhead+"Ascore", mod.ascore), 
                                   (keyhead+"Significant", mod.significant), 
                                   ))
            rowdict.update(moddict)
        #
        yield rowdict


def iter_digpep_diffmods_extrainfo2rowdicts(digpeps_diffmods, session=None,
                                            if_sep=", "):
    """
    A formating function: it formats the summary data into something 
    :func:`save_dicts_as_csv` can digest to generate a CSV/TSV output file.
    It pipes the `digpeps_diffmods` output from other function into a valid 
    :func:`save_dicts_as_csv` input.
    
    :param iterable digpeps_diffmods: an iterable that contains tuples
    of :class:`DigestionPeptide` instances and the list of different
    modification types associated with it. Usually the output from either
    :func:`iter_multiptm_digpep` or :func:`iter_digpep_only_with`.
    :param Session session: a SQLAlchemy :class:`Session` instance. Needed to 
    obtain extra information from the database.
    :param str if_sep: intra-field separator. The separator to use between 
    multiple values for a single field. Defaults to ", ".
        
    :return generator : yields :class:`OrderedDict` instances with Digestion
    Peptides information, suitable to write to a file. A valid
    :func:`save_dicts_as_csv` input.
    """
    seq2protdata_cache = dict()
    for rowdict in iter_digpep_diffmods2rowdicts(digpeps_diffmods, if_sep):
        # Calculate modified peptide mass:
        digpep_seq = rowdict['Digestion_Peptide']
        digpep_seq_w_mods = rowdict['Digestion_Peptide_With_PTMs']
        pep_mass = ppf.mass.get_mass(digpep_seq) #Mass for the raw sequence
        for unimod in ppf.find_mods(digpep_seq_w_mods): #Add mass for each PTM in seq_w_mods:
            pep_mass += unimod['delta_mass']
        rowdict['Predicted_Mass'] = pep_mass
        # Find matching proteins:
        try:
            # - Try getting Protein ACcessions and no-isoform flag from cache
            #   dict:
            prot_acs, only_isoforms = seq2protdata_cache[digpep_seq]
        except KeyError as _:
            # - Otherwise get them from DataBase search:
            prot_acs = list()
            prot_ac_noisoforms = set()
            for prot in dbm.search_seq_in_prot_db(digpep_seq, session):
                prot_acs.append(prot.ac)
                prot_ac_noisoforms.add(prot.ac_noisoform)
                session.expunge(prot)
            only_isoforms = (len(prot_ac_noisoforms)==1)
            seq2protdata_cache[digpep_seq] = (prot_acs, only_isoforms) #Update cache dict
        rowdict['Matching_Prots'] = if_sep.join( sorted(prot_acs) )
        rowdict['Unique_Pep'] = (len(prot_acs)==1)
        rowdict['Only_Isoforms'] = only_isoforms
        #
        yield rowdict



def main(db_uri, output_filen, summary_func, summary_args, formating_func=None):
    """
    Entry point function.
    
    :param str db_uri:
    :param str output_filen:
    :param func summary_func:
    :param iterable summary_args:
    :param func formating_func:
    
    :return int : exit status code.
    """
    if formating_func is None:
        formating_func = iter_digpep_diffmods2rowdicts
    print( "Start Digestion Peptides Data Summary from {0}".format(db_uri) )
    # Create the database connection:
    prot_db = dbm.DataBase(db_uri)
    # Do Query and Process it, Format results and Write them to a file:
    with prot_db.session_no_autoflush() as session:
        if summary_func is comp_pred_and_exp_peps: #Special case (`comp_pred_and_exp_peps`):
            for ext, rowdicts in zip( (".common.tsv", ".diff.tsv"), 
                                      comp_pred_and_exp_peps(session, *summary_args) ):
                save_dicts_as_csv(output_filen + ext, rowdicts, delimiter='\t')
        else:
            save_dicts_as_csv(output_filen, 
                              formating_func(summary_func(session, *summary_args), 
                                             session=session), 
                              delimiter='\t')
        session.rollback() # Avoid changes to the database.
    print( "\nProcessed Data saved to file:\n  {0}".format(output_filen) )
    #
    return 0



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    # CLI minimal argument parsing:
    itself = lambda x: x
    CLI_ARGUMENTS = OrderedDict((
        # Different Summary Options:
        ( 'multiptm-digpeptides', (None, iter_multiptm_digpep,  1, int) ),
        ( 'only-twoptm-digpeptides', (None, iter_digpep_only_with, 2, int) ),
        ( 'Phos_w_Ub_or_Ac-digpeptides', (None, iter_digpep_Phos_w_Ub_or_Ac, 0, itself) ),
        ( 'Phos_w_Ub_or_Ac-digpeptides-in-prots', (iter_digpep_diffmods_extrainfo2rowdicts, 
                                                   iter_digpep_Phos_w_Ub_or_Ac4proteins, 1, itself) ),
        ( 'Process_Experiment-File', (iter_exppeps2rowdicts, 
                                      iter_exppeps4file, 1, itself) ),
        ( 'Comp_Phos_w_Ub_or_Ac-digpeptides', (None, comp_pred_and_exp_peps, 1, itself) ),
        ( 'excluding_Ub_Ac-digpeptides', ( None, iter_digpep_with_Ub_Ac_same_pos, 1, 
                                           lambda csargs: map(int, csargs.split(',')) ) ),
                                  ))
    #
    formating_func = None
    summary_func = None
    n_summary_args = 0
    summary_args = tuple()
    PROG_NAME = os.path.basename( sys.argv[0] ) # Get script name.
    cli_argv = sys.argv[1:] # Exclude script name from the CLI arguments to process.
#     cli_argv = ['only-twoptm-digpeptides', '21', '121']
#     cli_argv = ['excluding_Ub_Ac-digpeptides', '12']
#     cli_argv = ['Phos_w_Ub_or_Ac-digpeptides']
#     cli_argv = ['Comp_Phos_w_Ub_or_Ac-digpeptides', 
#                 '/home/oga/LymPHOS-UB-AC/Dades cerques UB-Phos i AC-Phos - PD1.4 - 2017-03-27/Dades cerques UB-Phos i AC-Phos - PD1.4 - 2017-03-27.TSV.zip']
    cli_argv = ['Phos_w_Ub_or_Ac-digpeptides-in-prots', 
                '/home/oga/LymPHOS-UB-AC/Dades cerques UB-Phos i AC-Phos - PD1.4 - 2017-03-27/TCR-related proteins_extended_170817.tsv']
#     cli_argv = ['Process_Experiment-File', 
#                 '/home/oga/LymPHOS-UB-AC/Dades cerques UB-Phos i AC-Phos - PD1.4 - 2017-03-27/Dades cerques UB-Phos i AC-Phos - PD1.4 - 2017-03-27.TSV.zip'] #TEST #DEBBUG
    if cli_argv and ':/' in cli_argv[0]: # First argument is a DB URI:
        DB_URI = cli_argv[0]
        cli_argv = cli_argv[1:]
    if cli_argv: # First, or next (if first consumed), argument is a Summary Option:
        try:
            formating_func, summary_func, n_summary_args, args_func = CLI_ARGUMENTS[ cli_argv[0] ]
            summary_args = [ args_func(arg) for arg in cli_argv[1:n_summary_args+1] ]
        except (KeyError, ValueError) as _:
            pass
        cli_argv = cli_argv[n_summary_args+2:]
    if cli_argv: # Last argument, if present, is an output filename:
        OUTPUT_FILEN = cli_argv[0]
        cli_argv = cli_argv[1:]
    if not summary_func or len(summary_args) != n_summary_args or cli_argv: # Other arguments -> incorrect ones:
        sys.stderr.write('\nSyntax Error!\n')
        print('Usage:')
        print( " {0} [DataBase URI] summary summary-options [output file]"\
                .format(PROG_NAME) )
        print('\nExamples:')
        print( " {0} multiptm-digpeptides 2".format(PROG_NAME) )
        print( " {0} twoptm-digpeptides 1 121".format(PROG_NAME) )
        print( " {0} {1} twoptm-digpeptides 121 21".format(PROG_NAME, DB_URI) )
        print( " {0} {1} twoptm-digpeptides 1 21 {2}".format(PROG_NAME, DB_URI, 
                                                             OUTPUT_FILEN) )
        sys.exit(2)
    #
    exit_code = main(DB_URI, OUTPUT_FILEN, summary_func, summary_args, formating_func)
    #
    sys.exit(exit_code)
