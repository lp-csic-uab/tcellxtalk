
---

**WARNING!**: This is the *Old* source-code repository for TCellXTalk from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for this [same file at the _New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/code/ci/default/tree/README_VENV.md) located at https://sourceforge.net/projects/tcellxtalk.lp-csic-uab.p/**  

---  
  
To ensure all third-party dependencies are meet, have a look at the `requirements.txt` file, or just run ` pip install -r requirements.txt ` from your activated [Python virtual environment](https://virtualenv.pypa.io)'s command line.

Some packages in `requirements.txt` may need to be compiled from source, in these cases some dependencies should be satisfied:

  - Python development files (`libpython2.7-dev` and dependencies in Debina/Ubuntu-like distributions) for all cases where compilation is needed during module installation.
  - libmysqlclient development files (`libmysqlclient-dev` in Debina/Ubuntu-like distributions) for **MySQL-python module**.
  - libpng development files (`libpng-dev` in Debina/Ubuntu-like distributions) and  libfreetype development files (`libfreetype6-dev` in Debina/Ubuntu-like distributions) for **matplotlib module**.
  
In some cases (as reported on _Ubuntu 18.04_ and the versions specified for packages in `requirements.txt`) ` pip install -r requirements.txt ` will lead to incompatibilities between the `numpy` binary wheel package installed and the `matplotlib` compiled package. To avoid them, numpy should be also compiled from source. To this end, use the following commands (from inside your activated virtual environment) instead of ` pip install -r requirements.txt ` :

```
(venv)$ pip uninstall wheel
(venv)$ pip install -r requirements.txt --no-binary :all:
```

---

**WARNING!**: This is the *Old* source-code repository for TCellXTalk from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for this [same file at the _New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/code/ci/default/tree/README_VENV.md) located at https://sourceforge.net/projects/tcellxtalk.lp-csic-uab.p/**  

---  
  