
---

**WARNING!**: This is the *Old* source-code repository for the web-app at [www.TCellXTalk.org](https://www.TCellXTalk.org).  
**Please, look for the [_New_ repository](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/**  

---

![TCellXTalk](https://www.TCellXTalk.org/files/images/logo.png)

---

**WARNING!**: This is the *Old* source-code repository for the web-app at [www.TCellXTalk.org](https://www.TCellXTalk.org).  
**Please, look for the [_New_ repository](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/**  

---

**Table Of Contents:**  

[TOC]

#### Description

**TCellXTalk** is a comprehensive database of experimentally detected phosphorylation, ubiquitination and acetylation sites in human T cells.

The web-app at [www.TCellXTalk.org](https://www.TCellXTalk.org) makes **TCellXTalk** accessible from Internet, and enables the _in silico_ prediction of potential co-modified peptides to facilitate their experimental detection, using targeted or directed mass spectrometry, for the study of protein post-translational modification cross-talk.

More detailed information on **TCellXTalk** and the people at the CSIC/UAB Proteomics Laboratory behind it can be obtained at the web-app [About](https://www.TCellXTalk.org/about/) section.


#### Source Code Requirements

##### Main _Third-party_ Dependencies:

  * [Python](https://www.python.org/) 2.7
  * [Django](https://www.djangoproject.com/) 1.7
  * [SQLAlchemy](https://www.sqlalchemy.org/) 1.0
  * [MySQLdb - MySQL for Python](https://pypi.org/project/MySQL-python/) 1.2.5
  * [matplotlib](https://matplotlib.org/) 1.4.3
  * [numpy](https://www.numpy.org/) 1.9.2

(Third-party versions correspond to those used. Newer versions have not been tested, although they may also be fine).

To ensure all third-party dependencies are meet, have a look at the `requirements.txt` file, or just run ` pip install -r requirements.txt ` from your [Python virtual environment](https://virtualenv.pypa.io)'s command line.
If you find some problem installing dependencies using Python `pip` command, maybe the contents of file [`README_VENV.md`](https://bitbucket.org/lp-csic-uab/tcellxtalk/src/master/README_VENV.md) can be of some help.

##### Other _LP-CSIC/UAB_ Dependencies:

  * [LP-CSIC/UAB proteomics package](https://bitbucket.org/lp-csic-uab/proteomics): contains the code for the proteomics models for SQLAlchemy, filters/importers for different proteomics datasets, and other useful proteomics functions.
  * [LP-CSIC/UAB general package](https://bitbucket.org/lp-csic-uab/general): contains useful classes and functions for Python development.
  * [LP-CSIC/UAB math_extras package](https://bitbucket.org/lp-csic-uab/math_extras): contains extra mathematical functions not found in the math package from Python standard lib.


#### Contribute

These programs are made to be useful. If you use them and don't work entirely as you expect, let us know about features you think are needed, and we will try to include them in future releases.

![lp . csic <at> uab . cat](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png)  

---

**WARNING!**: This is the *Old* source-code repository for the web-app at [www.TCellXTalk.org](https://www.TCellXTalk.org).  
**Please, look for the [_New_ repository](https://sourceforge.net/p/lp-csic-uab/tcellxtalk/) located at https://sourceforge.net/p/lp-csic-uab/tcellxtalk/**  

---
