# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis:   Digest Protein and Combine PTMs needed functions for LymPHOS_UB_AC 
             (TCellXTalk) project. Started from code of view 
             :class:`ProteinDigestCombineView` in 'views.py' module v0.6.2 dev 
             (revision 464 (ae3301e834fe), 2018-08-30).

:created:    2015-04-13

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.6.3 dev'
__UPDATED__ = '2018-11-12'

#===============================================================================
# Imports
#===============================================================================
# Python core imports:
from collections import OrderedDict
from datetime import datetime

# Other imports:
from general.basic_func import save_dicts_as_csv
from proteomics import db_models as dbm
from proteomics import pepprot_func as ppf
from sqlalchemy.orm import undefer, joinedload
# #  DEBUG:
# from guppy import hpy


#===============================================================================
# Function definitions
#===============================================================================
class DigestCombineMixin(object):
    def _protein_protease(self, session):
        # Do the query to get the Protein to digest:
        Protein = dbm.Protein
        protein = session.query(Protein)\
                         .options( undefer('seq'),
                                   joinedload('_db_modifications')\
                                   .joinedload('mod_type') )\
                         .get(self.protein_ac)
        if not protein:
            self.errors.put_with_key( 'DataBase Search', 
                                      "Protein '{}' not found in Database".format(self.protein_ac) )
            return None
        # Do the query to get the Protease to use for digestion:
        Protease = dbm.Protease
        protease = session.query(Protease).get(self.protease_id)
        #Avoid MySQL error 1205 ('Lock wait timeout exceeded; try
        #restarting transaction') when more than one user is digesting-
        #combine a protein. This is due to the protease being updated while
        #still linked to a session (changes to its :attr:`target_seq_re` in
        #:method:`_filtered_combdigpeps`) and thus being locked by MySQL
        #avoiding re-reading it from another process/user:
    #             session.expunge(protease)
        if not protease:
            self.errors.put_with_key( 'DataBase Search', 
                                      "Protease ID '{}' not found in Database".format(self.protease_id) )
            return None
        #
        session.expunge_all()
        session.rollback()
        #
        return protein, protease
    
    
    def _consesnsus_digpeps4combination(self, protein, protease):
        # Get basic data for the fully digested peptides from protein:
        fulldig_protodigpeps = list( protease.iter_fulldigest_basicdata(protein) )
        # Generate Digestion Peptides:
        # - Frequently accessed variables (some speed optimization):
        prot_seq = protein.seq
        PepLikeMixin = dbm.PepLikeMixin
        DigPepNoDB = dbm.DigestionPeptideNoDB
        min_length = self.min_pep_length
        mandatory_modids = self.exclusive_ptms
        # - Get basic data for the digested peptides with 1 extra missed 
        #   cleavage (this is two contiguous full digested peptides):
        dpdata = protease.iter_digpepsbasicdata4mssclvglvl(self.missing_cleavage_lvls+1, 
                                                           fulldig_protodigpeps)
        for start_pos, end_pos, mssng_clvgs in dpdata:
            # - Pre-Filter by Minimum Length:
            if (end_pos - start_pos + 1) < min_length:
                continue #Skip this Digestion Peptide.
            # - Pre-Filter by presence of Mandatory PTMs (not exclusively, 
            #   so allows other extra PTMs):
            alldpmods = set( PepLikeMixin._iter_modifications_slice(protein, 
                                                                    start_pos, 
                                                                    end_pos) )
            if not mandatory_modids.issubset( {mod.mod_type_id 
                                               for mod in alldpmods 
                                               if mod.significant} ):
                continue #Skip this Digestion Peptide.
            # - Generate and yield Consensus Digestion Peptide:
            yield DigPepNoDB(protein=protein, #TEST!!
                             prot_start=start_pos, 
                             prot_end=end_pos, 
                             seq=prot_seq[start_pos-1:end_pos], 
                             consensus=True,
                             consensus_peptide=None, 
                             digestion_condition=None,
                             missing_cleavages=mssng_clvgs, 
                             _db_modifications=alldpmods)
    
    
    def _filtered_combdigpeps(self, session, consensus_digpeps, protease, 
                              consensus_peps_cache=None, redig_peps_cache=None, 
                              log_filen=None):
        """
        A method to generate Combinatorial Re-Digested Peptides from Consensus 
        Digestion Peptides, and Filter them.
        X_NOTE: this method derives from :func:`_iter_filter_digpeps` of 
        module `digestion_peptides_summaries.py` version 0.11 (2018-03-21) at 
        revision 311 (cac25ca2f4c1).
                
        :param Session session: a SQLAlchemy :class:`Session` instance.
        :param iterable consensus_digpeps: an iterable of Consensus Digestion 
        Peptides. Usually a SQLAlchemy :class:`Query` instance.
        :parma Protease protease: Protease used to generate the Consensus 
        Digestion Peptides, and needed to re-digest Combinatorial Peptides.
        :param set consensus_peps_cache: a cache of processed Consensus Digestion
        Peptides Sequences. A more global cache set can be passed, or (if None, 
        the default) a new one will be created.
        :param set redig_peps_cache: a cache of generated Combinatorial re-digested
        Peptides Sequences. A more global cache set can be passed, or (if None, 
        the default) a new one will be created.
        :param str log_filen: a file-name to write a log of the generated 
        peptides to, if supplied. Defaults to None (no log-file output).
        
        :return generator : yields filtered Re-Digested Combinatorial 
        :class:`DigestionPeptide` instances.
        """
        # Parameters check:
        if consensus_peps_cache is None:
            consensus_peps_cache = set()
        if redig_peps_cache is None:
            redig_peps_cache = set()
        if log_filen is None:
            save_log = False
            log_file = None
        else:
            save_log = True
            log_file = open(log_filen, "wb")
            # Write Log file headers:
            log_file.write("\t".join( ("Type", "ID/Mark", "Sequence with PTMs", 
                                       "Protein AC", "Protein position (start-end)", 
                                       "Sequence", "Modified AAs", 
                                       "Missing cleavages") ) + "\n")
        # Frequently accessed attributes as variables (some speed optimization):
        missing_cleavage_lvls = self.missing_cleavage_lvls
        min_length = self.min_pep_length
        max_length = self.max_pep_length
    #             max_miss_clvgs = self.max_missing_cleavages
        min_mods = self.min_total_ptms
        max_mods = self.max_total_ptms
        max_equalmods = self.max_equal_ptms
        exclusive_modids = self.exclusive_ptms
        DigPepNoDB = dbm.DigestionPeptideNoDB
        mandatory_modids4comb = { (mod_id,) for mod_id in exclusive_modids } #Format it according to the requirements of :method:``DigestionPeptide.iter_combinatorial_peptides``
        # Check if Protease is unable to cut at N-term or C-term or None, and 
        # generate alternative 'target sequence' regex to allow cutting them at 
        # peptide re-digestion step: 
        orig_targetseq_re = targetseq_re4firstpep = targetseq_re4interpep = targetseq_re4lastpep = protease.target_seq_re
        if "(?<!^)" in orig_targetseq_re: # Protease can NOT cut at N-term:
            targetseq_re4lastpep = orig_targetseq_re.replace("(?<!^)", "") #Modified regex to allow cutting at N-term.
            targetseq_re4interpep = targetseq_re4lastpep                   #
        if "(?!$)" in orig_targetseq_re: # Protease can NOT cut at C-term:
            targetseq_re4firstpep = orig_targetseq_re.replace("(?!$)", "") #Modified regex to allow cutting at C-term.
            targetseq_re4interpep = targetseq_re4lastpep.replace("(?!$)", "") #Modified regex to allow cutting at both N-term and C-term.
        #
    #             session_updates = 0 #Used to control when to flush sessions instances to database for freeing some memory.
    #             VOID_DB_OBJ = dbm.SourceType()
        for cons_digpep in consensus_digpeps:
            # Check for previously processed Consensus Digestion Peptide, and avoid further processing:
            digpep_seqwmods = cons_digpep.seq_w_mods
            if digpep_seqwmods in consensus_peps_cache:
                continue # Skip this Consensus Digestion Peptide.
            consensus_peps_cache.add(digpep_seqwmods) #Add to cache...
            # Save to Log file, if provided:
            if save_log:
                log_file.write('\n')
                log_file.write("\t".join(( "Consensus", str(cons_digpep.id), 
                                            digpep_seqwmods, cons_digpep.protein.ac, 
                                            "%s - %s"%(cons_digpep.prot_start, 
                                                       cons_digpep.prot_end), 
                                            cons_digpep.seq, str(cons_digpep.mod_aas),
                                            str(cons_digpep.missing_cleavages) )) + "\n")
            # Chose Protease N-term/C-term cutting action depending on Consensus Digestion Peptide position inside the Protein:
            if cons_digpep.prot_start == 1: # Consensus Digestion Peptide at Start of the protein:
                protease.target_seq_re = targetseq_re4firstpep
            elif cons_digpep.prot_end == cons_digpep.protein.length: # Consensus Digestion Peptide at End of the protein:
                protease.target_seq_re = targetseq_re4lastpep
            else: # Consensus Digestion Peptide is a Intermediate peptide:
                protease.target_seq_re = targetseq_re4interpep
            # Generate combinations of the peptide PTMs (Combinatorial Peptides):
    #                 last_combdigpep = VOID_DB_OBJ
    #                 session.add(last_combdigpep)
            for comb_digpep in cons_digpep.iter_combinatorial_peptides(mandatory_modids=mandatory_modids4comb, 
                                                                       CombPepCls=DigPepNoDB): #TEST
    #                     session.expunge(last_combdigpep) #Free some session memory.
    #                     last_combdigpep = comb_digpep
    #                     session_updates += 1
                # Save to Log file, if provided:
                if save_log:
                    log_file.flush()
                    log_file.write('\n')
                    log_file.write("\t".join(( "  Combinatorial", "*", 
                                                comb_digpep.seq_w_mods, 
                                                comb_digpep.protein.ac, 
                                                "%s - %s"%(comb_digpep.prot_start, 
                                                           comb_digpep.prot_end), 
                                                comb_digpep.seq, 
                                                str(comb_digpep.mod_aas),
                                                str(comb_digpep.missing_cleavages) )) + "\n")
                # Try to re-digest the Combinatorial Peptide:
                redig_digpeps = protease.digest(comb_digpep, missing_cleavage_lvls, 
                                                min_length, max_length, #Re-digested peptides are filtered here by length...
                                                create_digestion_condition=False, 
                                                DigPepCls=DigPepNoDB) #TEST
                # Post-Filter Combinatorial Re-digested Peptides:
    #                     last_combredigpep = VOID_DB_OBJ
    #                     session.add(last_combredigpep)
                for redig_digpep in redig_digpeps:
    #                         session.expunge(last_combredigpep) #Free some session memory.
    #                         last_combredigpep = redig_digpep
    #                         session_updates += 1
                    # Check for previously seen Re-digested Peptides, and avoid further processing and yielding:
                    redigpep_seqwmods = redig_digpep.seq_w_mods
    #                         redigpep_fingerprint = hash(redigpep_seqwmods) #TEST: Try fixing memory leaks..
                    if redigpep_seqwmods in redig_peps_cache:
                        continue # Skip this Re-digested Peptide.
                    redig_peps_cache.add(redigpep_seqwmods) #Add to cache...
                    # Save to Log file, if provided:
                    if save_log:
                        data4save = ( redigpep_seqwmods, redig_digpep.protein.ac,
                                      "%s - %s"%(redig_digpep.prot_start, 
                                                 redig_digpep.prot_end), 
                                      redig_digpep.seq, str(redig_digpep.mod_aas), 
                                      str(redig_digpep.missing_cleavages) )
                        log_file.write("\t".join( ("    Re-digested", "-") + 
                                                   data4save ) + "\n")
    #                         # -Filter by total number of missing cleavages:
    #                         if redig_digpep.missing_cleavages > max_miss_clvgs: #Total number of missing cleavages is greater than the maximum allowed:
    #                             continue # Skip this Re-digested Peptide.
                    # -Filter by presence of exclusive modifications: #X-NOTE: if no exclusive modifications (`exclusive_modids` is None) unmodified peptides are not filtered out!
                    modid2positions = redig_digpep.modid2positions
                    if exclusive_modids and set( modid2positions.keys() ) != exclusive_modids: #The modifications of the Combinatorial Re-digested Peptide differ from the exclusive PTMs:
                        continue # Skip this Re-digested Peptide.
                    # -Filter by minimum and maximum number of total PTMs:
                    combredigpep_nposs = map( len, modid2positions.values() ) or [0] #X-NOTE: [0] will allow unmodified peptides (see note above) to continue passing filters.
                    if not (min_mods <= sum(combredigpep_nposs) <= max_mods): #Number of total PTMs (modified AAs) in the combination of modifications is out of the allowed limits:
                        continue # Skip this Re-digested Peptide.
                    # -Filter by maximum repetitions of the same PTM:
                    if max(combredigpep_nposs) > max_equalmods: #Any number of each PTM > maximum allowed.
                        continue # Skip this Re-digested Peptide.
                    # Save to Log file, if provided:
                    if save_log:
                        log_file.write("\t".join( ("      Yielded", ">") + 
                                                   data4save ) + "\n")
                    #
                    yield redig_digpep
                    #
    #                     session.expunge(last_combredigpep) #Free some session memory.
    #                     if session_updates > 150000: #Free some session memory flushing instances to database:
    #                         session.flush()
    #                         session_updates = 0
    #                         # Save to Log file, if provided:
    #                         if save_log:
    #                             log_file.write("Database Session Flush()\n")
    #                 session.expunge(last_combdigpep) #Free some session memory.
        #
        if save_log:
            log_file.close()
    
    
    @staticmethod
    def _add_mass2digpeps(dig_peps):
        for dig_pep in dig_peps:
            dig_pep.mass = "{:.5f}".format( ppf.pep_w_mods_mass(dig_pep) )
    #                dig_pep.htmlseq = "".join( add_html_mods2seqlst(list(dig_pep.seq),
    #                                                                dig_pep.pos2modifications) )
            yield dig_pep
    
    
    @staticmethod
    def _combdigpeps2rowdicts(digpeps, if_sep=", "):
        """
        Formats input data into something :func:`save_dicts_as_csv` can use to 
        generate a CSV/TSV output file.
        X_NOTE: this method derives from :func:`iter_digpep_diffmods2rowdicts` 
        of module `digestion_peptides_summaries.py` version 0.11 (2018-03-21) 
        at revision 311 (cac25ca2f4c1).
        
        :param iterable digpeps: an iterable of :class:`DigestionPeptide` 
        instances.
        :param str if_sep: intra-field separator. The separator to use between 
        multiple values for a single field. Defaults to ", ".
            
        :return generator : yields :class:`OrderedDict` instances with
        Digestion Peptides information, suitable to write to a file (a valid
        input for :func:`save_dicts_as_csv`).
        """
        for digpep in digpeps:
            # Get different significant PTM abbreviations:
            diffmods = sorted( {mod.mod_type.extradata['full_name'] 
                                for mod in digpep.sig_modifications} )
    #                 # Get PTM Sources information:
    #                 pos_ptm2sourcenames = defaultdict(set)
    #                 for pos, ptms in digpep.pos2modifications.items():
    #                     for ptm in ptms:
    #                         pos_ptm2sourcenames[pos, ptm.mod_type.name].add(ptm.source.name)
    #                 sources = if_sep.join( "%s-%s: %s sources (%s)"%( ptmname, pos, 
    #                                                                   len(sourcenames), 
    #                                                                   if_sep.join(sorted(sourcenames)) ) 
    #                                        for (pos, ptmname), sourcenames 
    #                                        in sorted( pos_ptm2sourcenames.items() ) )
            # Digestion Peptide information:
            rowdict = OrderedDict((('Digestion_Peptide', digpep.seq), 
                                   ('Digestion_Peptide_With_PTMs', digpep.seq_w_abbreviatedmods), 
                                   ('Digestion_Peptide_ProForma', digpep.as_proforma()), 
                                   ('Total_Modified_AAs', digpep.mod_aas),
                                   ('Protein_1st', digpep.protein.ac), 
                                   ('Start_Position_1st', digpep.prot_start), 
                                   ('End_Position_1st', digpep.prot_end), 
                                   ('Missed_Cleavages', digpep.missing_cleavages),
                                   ('Different_Modifications', len(diffmods)), 
                                   ('Modification_Types', if_sep.join(diffmods)), 
                                   ('Monoisotopic_peptide_mass', 
                                    "{:.5f}".format( ppf.pep_w_mods_mass(digpep) )), 
    #                                        ('PTM_Sources', sources), 
                                   ))
            #
            yield rowdict
    
    def write_digcomb_data2files(self, session, io_file, log_filen=None, **kwargs):
        # Get Protein to digest and Protease to use:
        protein_protease = self._protein_protease(session)
        if protein_protease:
            protein, protease = protein_protease
            # Write information header before data:
            now = datetime.now()
            prot_ac = protein.ac
            io_file.write("Inferred Combinatorial Digestion Peptides File\n"
                          "  Downloaded from TCellXTalk v{} "
                          "(https://www.TCellXTalk.org) on {:%Y-%m-%d_%H:%M:%S}\n"
                          "  (cc) BY-NC-SA ( http://creativecommons.org/"
                          "licenses/by-nc-sa/4.0/ )\n\n"
                          "Digestion parameters:\t"
                          " Protein Digested: {}\t Protease used: {}\t\n"
    #                          " Extra Missed Cleavages Levels: {}\n"
                          "Filter parameters:\t"
                          " Peptide lengths: {} - {} AAs\t"
    #                          " Max. Missed Cleavages: {}\n\t"
                          " PTMs/peptide: {} - {}\t"
                          " Max. PTMs of same type/peptide: {}\t"
                          " PTMs required: {}\n"
                          "\n".format(__VERSION__, now, 
                                      prot_ac, protease.name, 
    #                                      self.missing_cleavage_lvls, 
                                      self.min_pep_length, self.max_pep_length, 
    #                                      self.max_missing_cleavages, 
                                      self.min_total_ptms, self.max_total_ptms, 
                                      self.max_equal_ptms, 
                                      self.exclusiveptm_names)
                                      )
            # Get Consensus Digestion Peptides:
            consensus_digpeps = self._consesnsus_digpeps4combination(protein, protease)
            # Generate, Filter, Format and Save the Combinatorial Digestion 
            # Peptides as TSV inside the :class:`HttpResponse` object:
            save_dicts_as_csv(io_file, #Save
                              self._combdigpeps2rowdicts( #Format
                                  self._filtered_combdigpeps(session, #Generate and Filter
                                                             consensus_digpeps, 
                                                             protease, 
                                                             log_filen=log_filen) ), 
                              delimiter='\t')
        #
        return io_file
    
