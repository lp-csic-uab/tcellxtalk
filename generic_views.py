# -*- coding: utf-8 -*-

from __future__ import print_function

"""
:synopsis:   Basic Generic Views for LymPHOS_UB_AC (TCellXTalk) project.

:created:    2014-09-16

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.2.5 dev'
__UPDATED__ = '2018-11-12'

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.views.generic.base import View, TemplateView
from django.http import HttpResponse #, HttpResponseRedirect # Needed only by Deprecated :class:`SingleFormHtmlPage`.
from django.conf import settings
from django.core.paginator import InvalidPage, EmptyPage
from django.shortcuts import render
from django import forms

# Project imports:
from web_utilities import LoggerCls, SQLAlchemyPaginator

# Python core imports:
import os

# Other imports:
from general.basic_func import str_translate


#===============================================================================
# Views basic classes definitions
#===============================================================================
class BaseTemplateContext(object):
    """
    A minimum Context for a Base Template.
    """
    
    def __init__(self, template='base.html', css_sheet=None, sections=None, 
                 side_bar='side_bar.html', **kwargs):
        """
        - Generic attributes:
        :attr str template: optional Base Template file to use/extends. Defaults 
        to 'base.html'.
        :attr str css_sheet: CSS file to use with the template. Defaults to 
        'default.css' if the view uses a side-bar, or to 'default_nosidebar.css' 
        otherwise.
        
        - Navigation bar related attributes:
        :attr dict sections: { section_name: {label: 'Nav-bar button label', title: 'html title/tooltip'} }. 
        Allows the rendering  of the web's navigation bar.
        
        - Side-bar attributes:
        :attr str side_bar: side-bar template to use. Set to '' to disable the 
        sidebar. Defaults to 'side_bar.html'.
        """
        self.template = template
        if css_sheet is None: # Use a default CSS file depending on the `side_bar`:
            css_sheet = 'default.css' if side_bar else 'default_nosidebar.css'
        self.css_sheet = css_sheet
        self.sections = sections
        self.side_bar = side_bar
        # Extra base context attributes initialization:
        for key, value in kwargs.items():
            setattr(self, key, value)


class SectionView(TemplateView):
    """
    Basic Section web-page View.
    
    - Generic section attributes:
    :attr BaseTemplateContext base_context: a context for the base template to 
    use with this section web-page view.
    :attr str template_name: section template file to use.
    :attr str title: title of the section web page.
    
    - Navigation bar related attributes:
    :attr str section: section name of this section web page. Defaults to 'home'.
    
    - Other:
    :attr dict default_context: optional dictionary with extra context data for
    the template rendering.
    
    - Inherited:
    :method get: from TemplateView super-class.
    :method render_to_response: from TemplateView' TemplateResponseMixin super-class.
    :method get_template_names: from TemplateView' TemplateResponseMixin super-class.
    :method get_context_data: from TemplateView' ContextMixin super-class.
    :classonlymethod as_view: from TemplateView' View super-class.
    :method dispatch: from TemplateView' View super-class.
    :method http_method_not_allowed: from TemplateView' View super-class.
    :method options: from TemplateView' View super-class.
    """
    base_context = None #A :class:`BaseTemplateContext` instance to use with this section view.
    
    template_name = '' #Section template file to use.
    title = '' #Title of the section web page.
    
    section = 'home' #Section name of this section web page.
    
    default_context = None #Can be a dictionary with extra context data for the template rendering
    
    def __init__(self, **kwargs):
        # Overwrites TemplateView' super-class :method:`View.__init__`.
        super(SectionView, self).__init__(**kwargs)
        if not self.title: # Use a default title from the appropriate section in `self.base_context`:
            self.title = self.base_context.sections[self.section]['title']
    
    def get_context_data(self, **kwargs):
        """
        :returns dict : a default basic context dictionary to render the
        template (updated with :attr:`self.default_context` dictionary, and
        provided `kwargs`), containing a 'base' context name-space for the base
        template and a 'view' name-space for the section template.
        """
        # Overwrites TemplateView' super-class :method:`ContextMixin.get_context_data`.
        #
        # Create a basic context dictionary populated with some "globals" and a 
        # 'base' context name-space for use by the base template ('base.html'):
        context = {'APPNAME': settings.APPNAME, #"Globals"
                   'base': self.base_context}   #'base' context name-space for the base template
        # Update with optional ``self.default_context`` dictionary:
        if self.default_context:
            context.update(self.default_context)
        # Update with `kwargs` and a 'view' name-space, from super-class :method:`ContextMixin.get_context_data`:
        context.update( super(SectionView, self).get_context_data(**kwargs) )
        #
        return context


class PaginatorMixin(object):
    """
    Mixin Class to add paginated results to TemplateView (sub)classes.
    Call :method:`paginate_results`.
    """
    
    itemsxpage = 0 #Default items per page for the Paginator object.
    allowed_itemsxpage = tuple() #Values allowed for the :attr:`itemsxpage`.
    page = 1 #Page of results to show.
    
    def paginate_results(self, query_results, special_count_query=False,
                         reenter_form=None):
        """
        Basic functionality for pagination: `query_esults` are formated as a
        django :class:`Paginator.page` instance located at the page number
        indicated by :attr:`page`.
        
        :param iterable query_results: and iterable of result objects. Usually 
        a query instance.
        :param bool or Query special_count_query: False for using the default
        :method:``count`` of the :param:`query_results` object (this usually
        means the slow :method:``count`` of a SQLAlchemy :class:``Query``
        instance.
        True to use an alternative, faster trick (rewriting the SQL query using
        :function:``SQLAlchemy.func.count``, :caution: this can have unexpected
        results with queries with eager loading).
        Or a SQLAlchmey :class:``Query`` object specially designed to get the
        total count of results in a fast way (ex.: session.query(func.count(distinct( ModelCls.id ))), 
        see http://docs.sqlalchemy.org/en/rel_0_9/orm/query.html?highlight=count#sqlalchemy.orm.query.Query.count).
        Defaults to False.
        :param Form or None reenter_form: a :class:``django.forms.Form``
        instance with the data needed for re-entering via POST, or None. 
        Defaults to None (no re-entering form available).
        
        :return object : a :class:`Paginator.page` instance located at the page
        number in :attr:`page`.
        """
        # Get and clean the :attr:`itemsxpage`:
        try:
            itemsxpage = int(self.itemsxpage)
        except ValueError as _:
            itemsxpage = self.__class__.itemsxpage
        if itemsxpage not in self.allowed_itemsxpage:
            itemsxpage = self.__class__.itemsxpage
        self.itemsxpage = itemsxpage
        # Persist the cleaned :attr:`itemsxpage` value between POSTs as a
        # hidden input field in forms:
        if reenter_form is not None:
            # Monkey-patch the re-entering form to add a 'itemsxpage' field
            # with its actual value:
            reenter_form.fields['itemsxpage'] = forms.IntegerField(initial=itemsxpage,
                                                                   required=False)
        # Creates the Paginator object from :attr:``query``:
        paginator = SQLAlchemyPaginator(query_results, itemsxpage, 
                                        special_count_query=special_count_query)
        # Get the required page of results (according to :attr:`page`):
        # - Get and clean the :attr:`page`:
        try:
            page = int(self.page)
        except ValueError as _:
            page = 1
        self.page = page
        # - Try to get the requested page of paginated results:
        try:
            paginated_results = paginator.page(self.page)
        except (EmptyPage, InvalidPage) as _: #If requested page is out of range:
#             paginated_results = paginator.page(paginator.num_pages) #Deliver last page of query
            paginated_results = None #Nothing to paginate
        #
        return paginated_results


class ErrorsMixin(object):
    """
    Mixin Class to add error management to TemplateView (sub)classes. It adds
    a :class:`LoggerCls` instance in :attr:`errors`, so errors should be stored 
    through :class:`LoggerCls` methods.
    Also adds a 'errors' name-space to the context.
    """
    
    def __init__(self, **kwargs):
        super(ErrorsMixin, self).__init__(**kwargs)
        self.errors = LoggerCls()
    
    def get_errors_context(self, errors=None):
        return {'errors': errors or self.errors}
    
    def get_context_data(self, **kwargs):
        """
        Adds a 'errors' name-space to the context.
        """
        # Would Overwrite the following super-class
        # :method:``get_context_data`` according to the sub-class M.R.O.
        context = super(ErrorsMixin, self).get_context_data(**kwargs)
        context.update( self.get_errors_context() )
        return context


class SQLAlchemySectionView(ErrorsMixin, SectionView):
    
    db = None #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do the searches.
    
    strmapping4search = None #Mapping table to use to format search strings for queries.
     
    def formatstr4search(self, search_str, mapping_table=None, 
                         other_char_tmplt="{0}", global_tmplt="%{0}%"):
        """
        Formating of `search_str` string according to `mapping_table`
        dictionary
        """
        if mapping_table is None:
            mapping_table = self.strmapping4search
        #
        return global_tmplt.format( str_translate(search_str, 
                                                  mapping_table, 
                                                  other_char_tmplt) )
    
    def get_context_data(self, session, **kwargs):
        """
        Use a DataBase session to do the corresponding search and format the 
        obtained results as a context dictionary needed to render an HTML 
        response. 
        Overwrite in subclasses.
        
        :param Session session: a SQLAlchemy DataBase session.
        
        :return dict : a context dictionary with the formated results.
        """
        # Overwrites super-class :method:`ErrorsMixin.get_context_data`, witch
        # in turn (according to :class:`SQLAlchemySectionView` M.R.O.) is
        # overwriting super-class :method:`SectionView.get_context_data`.
        return super(SQLAlchemySectionView, self).get_context_data(**kwargs)
    
    def get_db_response(self, autoflush=False, **kwargs):
        """
        Due to SQLAlchemy constrains, call this method in subclasses to
        generate a `HttpResponse` with data from a SQLAlchemy DataBase.
        It generates a DataBase Session and doesn't close the session until an
        `HttpResponse` is generated/rendered against a template, so the
        SQLAlchemy objects are alive and can still get data from the database.
        
        :param bool autoflush: indicates if the Session used should flush data
        to the database automatically (True) or not (False). Defaults to False 
        (by default no data should go to the database automatically).
        
        :returns: a :class:`HttpResponse` object with the results rendered 
        against the view template.
        """
        # X-NOTE: May be, in the future, we can let the session unclosed (using
        # DataBase.new_session() instead of DataBase.session() context manager)
        # to avoid direct rendering of context against template using function 
        # django.shorcuts.render(request, template, context):
        # session = self.db.new_session()
        with self.db.session(autoflush=autoflush) as session:
            # Get `context` data to render against a template:
            context = self.get_context_data(session, **kwargs)
            # Direct rendering of `context` against the template
            # (:attr:`template_name`) to generate a final HttpResponse (no
            # further modifications to the context can be done by django
            # middleware):
            httpresponse = render(self.request, self.template_name, context)
        #
        return httpresponse
    
    def get(self, request, *args, **kwargs):
        """
        Method overwritten to call :method:`get_db_response` that generates a
        `HttpResponse` with data from a SQLAlchemy DataBase.
        """
        # Overwrites super-class :method:``TemplateView.get``.
        return self.get_db_response(**kwargs)


class FormMixin(object):
    """
    Mixin Class to add basic form management to `SQLAlchemySectionView` 
    (sub)classes.
    Overwrite :method:`form_valid` and/or :method:`form_invalid` to do real
    stuff in response to POST request.
    """
    
    form_class = None #The form class to instantiate. Required.
    form_initial_values = None #A dictionary containing the desired initial values for the form fields. Optional.
    
    def form_valid(self, form):
        """
        Overwrite this method to take actions only when Valid form data.
        
        :param Form form: a django :class:`forms.Form` instance.
        
        :returns: a :class:`HttpResponse` object with the :param:`form` as part
        of the context.
        """
        return self.get_db_response(form=form, **self.kwargs)
    
    def form_invalid(self, form, base_key=''):
        """
        Overwrite this method to take actions only when Invalid form data.
        
        :param Form form: a django :class:`forms.Form` instance.
        :param str base_key: the base key-name under which store the error 
        messages.
        
        :returns: a :class:`HttpResponse` object with the :param:`form` as part
        of the context.
        """
        for key, errors in form.errors.items():
            try:
                key = form[key].label #Try to get the field on-screen name associated to the `key`
            except (KeyError, AttributeError) as _:
                pass
            full_key = "{0} ('{1}')".format(base_key, key)
            for error in errors:
                self.errors.put_with_key(full_key, error)
        #
        return self.get_db_response(form=form, **self.kwargs)
    
    def post(self, request, *args, **kwargs):
        """
        Default handler method for POST requests. Creates a :class:`form_class`
        instance with its values from :attr:``request.POST``, check validity of
        those values, and returns from the corresponding method
        (:method:`form_valid` or :method:`form_invalid`).

        :returns: a :class:`HttpResponse` object whose content is a context
        dictionary (from :method:`self.get_context_data`) rendered against a
        template.
        """
        form = self.form_class(request.POST) #Match POST data to form variables
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get(self, request, *args, **kwargs):
        """
        Adds a :class:`form_class` instance (with optional initial values, as
        defined in :attr:`form_initial_values`) to the Context of the View.
        
        :returns: a :class:`HttpResponse` object.
        """
        # Would Overwrite the following super-class :method:``get`` according
        # to the sub-class M.R.O.
        if self.form_initial_values:
            form = self.form_class(initial=self.form_initial_values)
        else:
            form = self.form_class()
        return super(FormMixin, self).get(request, form=form, *args, **kwargs) #X-NOTE: usually will refer to :class:`SQLAlchemySectionView`


class FormSearchListView(FormMixin, PaginatorMixin, SQLAlchemySectionView):
    """
    Generic View Class that allows DataBase Searches from a Form POST, and
    generate a result list, optionally paginated and ordered.
    """
    
    table_headers = None #Iterable of dictionaries containing the table headers to use in included template 'table_result_headers.html'.
    
    form_search_field = 'search' #Form field with the search string to use.
    form_sorttags_field = 'sort_tags' #Form field with the fields to order the query by.
    
    search_value = None #Value to search for.
    
    query_ModelCls = None #A SQLAlchemy ORM declarative model class (Ex. Proteins).
    query_fieldattr = '' #A string corresponding to a SQLAlchemy ORM model class field name (Ex. 'ac').
    
    def __init__(self, **kwargs):
        #
        super(FormSearchListView, self).__init__(**kwargs)
        #
        # Attribute initialization:
        self._orderby_fields = tuple() #Fields to order the search by.
    
    # self.orderby_fields is a read-write property:
    @property
    def orderby_fields(self):
        return self._orderby_fields
    @orderby_fields.setter
    def orderby_fields(self, sort_tags):
        sorttag2orderbytmplt = { header['sort_tag']: header['orderby_tmplt']
                                 for header in self.table_headers 
                                 if header.get('sort_tag', False) }
        sortorder2sortuples = { '': ('ASC', 'DESC'), 'desc': ('DESC', 'ASC') } # '' -> ASC (the default in SQL) ; 'desc' -> DESC
        allowed_sorttags = set( sorttag2orderbytmplt.keys() ) #Tags allowed in forms to be translated to ORDER BY SQL expressions. = set( self.sorttag2orderbytmplt.keys() ) #Tags allowed in forms to be translated to ORDER BY SQL expressions
        allowed_sortorders = set( sortorder2sortuples.keys() )
        # Only accept allowed tags (in ``allowed_sorttags``):
        sql_orderby_exps = list()
        for sorttag_sortorder in sort_tags:
            # Compare if valid field name and valid order keyword:
            sorttag, _, sortorder = sorttag_sortorder.partition(' ')
            if sorttag in allowed_sorttags and sortorder in allowed_sortorders:
                sql_orderby_exps.append( sorttag2orderbytmplt[sorttag].format( *sortorder2sortuples[sortorder] ) )
        self._orderby_fields = tuple(sql_orderby_exps)
    
    def get_results_context(self, session, **kwargs):
        """
        Overwrite this method to customize search query and results formating.
        
        :param Session session: a SQLAlchemy database session.
        
        :return dict : a context dictionary, with the paginated results of the
        query, to update the general context.
        """
        ModelCls = self.query_ModelCls
        field_attr = getattr(ModelCls, self.query_fieldattr)
        results = session.query(ModelCls)\
                         .filter( field_attr.like(self.search_value) )\
                         .order_by(*self.orderby_fields)
        #
        return { 'paginated_results': self.paginate_results( results, 
                                                             reenter_form=kwargs['form'] ) }
    
    def get_context_data(self, session, **kwargs):
        """
        To customize Context Data in sub-classes consider better to overwrite 
        final :method:`get_results_context` instead of this method.
        """
        # Overwrites super-class :method:`SQLAlchemySectionView.get_context_data`.
        context = super(FormSearchListView, self).get_context_data(session, **kwargs)
        # Get results from DataBase:
        if self.search_value is not None: #Only get results if there is something to search for (form was valid)
            context.update( self.get_results_context(session, **kwargs) )
        #
        return context
    
    def form_valid(self, form):
        """
        Define the database search parameters with data from :param:`form`.
        
        :returns: a :class:`HttpResponse` object with the search results rendered against
        the template.
        """
        # Define search parameters from input data:
        self.search_value = self.formatstr4search( form.cleaned_data[self.form_search_field] )
        self.orderby_fields = form.cleaned_data[self.form_sorttags_field].split(',')
        self.page = self.request.POST.get('page', '1') #Make sure requested page exists. Otherwise, deliver first page
        self.itemsxpage = self.request.POST.get('itemsxpage', self.itemsxpage) #Get requested items per page or use the default one
        #
        return super(FormSearchListView, self).form_valid(form)
    
    def form_invalid(self, form):
        """
        Put :param:`form` errors into View (self) errors dictionary.
        
        :returns: a :class:`HttpResponse` object with the form errors rendered
        against the template.
        """
        return super(FormSearchListView, self).form_invalid(form, 
                                                            base_key="Search Form Errors")


class SearchItemView(SQLAlchemySectionView):
    """
    Generic View Class that allows DataBase Searches for individual Items from
    a GET, to display item information.
    """
    
    get_search_field = None #Integer for positional arguments, string for keyword arguments. 
    search_field_validators = (lambda s: None, )
    
    search_value = None #Value to search for.
    
    query_ModelCls = None #A SQLAlchemy ORM declarative model class (Ex. Proteins).
    query_fieldattr = '' #A string corresponding to a SQLAlchemy ORM declarative model class field name (Ex. 'ac').
       
    def formatstr4search(self, search_str, mapping_table=None, 
                         other_char_tmplt="{0}", global_tmplt="{0}"):
        return super(SearchItemView, self).formatstr4search(search_str, 
                                                            mapping_table, 
                                                            other_char_tmplt, 
                                                            global_tmplt)
    
    def get_results_context(self, session, **kwargs):
        """
        Overwrite this method to customize search query and results formating.
        
        :param Session session: a SQLAlchemy database session.
        
        :return dict : a context dictionary to update the general context.
        """
        ModelCls = self.query_ModelCls
        field_attr = getattr(ModelCls, self.query_fieldattr)
        results = session.query(ModelCls)\
                         .filter( field_attr == self.search_value )
        return {'results': results}
    
    def get_context_data(self, session, **kwargs):
        """
        To customize context data consider overwriting
        :method:`get_results_context` instead.
        """
        context = super(SearchItemView, self).get_context_data(session, **kwargs)
        #
        if self.search_value is not None: #Only get results if there is something to search for
            context.update( self.get_results_context(session, **kwargs) )
        #
        return context
    
    def search_field_is_valid(self, search_str):
        """
        Use validators in :attr:`search_field_validators` to validate the search
        string.
        
        :param str search_str: the search string to validate.
        
        :return bool : whether search string is valid or not.
        """
        valid = True
        for validator in self.search_field_validators:
            try:
                validator(search_str)
            except Exception as e:
                valid = False
                self.errors.put_with_key('Search String', e)
        return valid
    
    def get(self, request, *args, **kwargs):
        # Get the search string:
        if isinstance(self.get_search_field, int):
            search_str = args[self.get_search_field]
        else:
            search_str = kwargs[self.get_search_field]
        # Validate the search string:
        if search_str and self.search_field_is_valid(search_str):
            return self.search_field_valid(search_str)
        else:
            return self.search_field_invalid(search_str)
    
    def search_field_valid(self, search_str):
        """
        Do the database search for the `search_str`.
        
        :returns: a :class:`HttpResponse` object with the query_results rendered against
        the template.
        """
        self.search_value = self.formatstr4search(search_str)
        return self.get_db_response(**self.kwargs)
    
    def search_field_invalid(self, search_str):
        """
        :returns: a :class:`HttpResponse` object with the search errors rendered
        against the template.
        """
        return self.get_db_response(**self.kwargs)


class DinamicFile(View):
    """
    Abstract class for generating dynamic files: Database data -> Some file.
    
    - Inherited:
    :classonlymethod as_view: from View super-class.
    :method dispatch: from View super-class.
    :method http_method_not_allowed: from View super-class.
    :method options: from View super-class.
    """

    request = None
    
    db = None #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do the searches.
    
    def __init__(self, *args, **kwargs):
        """
        Simply adds a null request attribute to the instance
        """
        # 
        super(DinamicFile, self).__init__(*args, **kwargs)
        #
        self._name = ''
        self._ext = ''

        self.httpresponse = None
        self.saved = False

    # ``self.filename`` is a read-write property:
    @property
    def filename(self):
        return self._name + self._ext if self._name else ''
    @filename.setter
    def filename(self, filename):
        self._name, self._ext = os.path.splitext(filename) if filename else ('', '')
    
    def get_file_httpresponse(self, session, content_type=None, **kwargs):
        """
        Overwrite in sub-classes to do real stuff.
        """
        return HttpResponse(content_type=content_type)
        
    def get_db_response(self, autoflush=False, **kwargs):
        """
        Due to SQLAlchemy constrains, this method is needed to generate a
        `HttpResponse` with data from a SQLAlchemy DataBase. It generates a
        DataBase Session and doesn't close it until an `HttpResponse` is
        generated and returned by :method:`get_file_http_response`, so this
        method has an active SQLAlchemy Session to get the data it would need.
        
        :param bool autoflush: indicates if the Session used should flush data
        to the database automatically (True) or not (False). Defaults to False 
        (by default no data should go to the database automatically).
        
        :returns: a :class:`HttpResponse` object with a file generated form the
        database.
        """
        # X-NOTE: May be, in the future, we can let the session unclosed (using
        # DataBase.new_session() instead of DataBase.session() context manager):
        # session = self.db.new_session()
        with self.db.session(autoflush=autoflush) as session:
            self.httpresponse = self.get_file_httpresponse(session, **kwargs)
        return self.httpresponse

    def get(self, request, *args, **kwargs):
        """
        Calls :method:`get_db_response` to get a `HttpResponse` with data from
        a SQLAlchemy DataBase.
        """
        return self.get_db_response(**kwargs)

    def post(self, request, *args, **kwargs):
        """
        The real parameters are in ``self.args`` and/or ``self.request.GET`` ->
        redirect to :method:``self.get``.
        So this should only should be called from download forms.
        """
        return self.get(request, *args, **kwargs)

    def save(self, request=None, filename=None):
        if request: self.request = request
        if filename: self.filename = filename
        #
        with open(self.filename, 'wb') as io_file:
            io_file.write(self.httpresponse.content)
        self.saved = True
        return self


# class SingleFormHtmlPage(SectionView):
#     """
#     Basic class for web-page Form views management.
# 
#     It implements the post method that makes use of Django middleware csrf
#     (Cross Site Request Forgery) protection: each POST form in the template
#     should contain the tag {% csrf_token %}
#     """
# 
#     form_class = None
#     initial = dict()
#     form = None
# 
#     succes_url = None
# 
#     def __init__(self, *args, **kwargs):
#         # Overwrites super-class :method:`SectionView.__init__`.
#         super(SingleFormHtmlPage, self).__init__(*args, **kwargs)
#         if not self.form and self.form_class:
#             self.form = self.form_class(initial=self.initial)
#         if not self.form_class and self.form:
#             self.form_class = self.form.__class__
# 
#     def get_context_data(self, **kwargs):
#         # Overwrites super-class :method:`SectionView.get_context_data`.
#         """
#         :returns: a default base context dictionary updated with a form object,
#         and form error information
#         """
#         # Call the base implementation first to get a context:
#         context = super(SingleFormHtmlPage, self).get_context_data(**kwargs)
#         # Update the context with form data:
#         context['form'] = self.form
#         return context
# 
#     def post(self, request, *args, **kwargs):
#         """
#         Default handler method for POST requests.
# 
#         :returns: a :class:`HttpResponse` object whose content is a context
#         dictionary (from :method:`self.get_context_data`) rendered against a
#         template.
#         """
#         self.form = self.form_class(request.POST) #Match POST data to form variables
#         if self.form.is_valid():
#             self.form_valid(*args, **kwargs)
#             if self.succes_url:
#                 return HttpResponseRedirect(self.succes_url)
#         context = self.get_context_data(**kwargs)
#         return self.render_to_response(context)
# 
#     def form_valid(self, *args, **kwargs):
#         """
#         Override this method to process form cleaned data
#         """
#         return
# 
#     def put(self, request, *args, **kwargs):
#         """
#         Default dummy handler method for PUT requests. PUT is a valid HTTP verb
#         for creating (with a known URL) or editing an object, but browsers only
#         support POST for now.
#         """
#         return self.post(request, *args, **kwargs)

