#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis: Create TSV Summaries about the MS Peptides from a Protein Database for LymPHOS-UB-AC.

:created:    2016/04/28

:authors:    Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ =  '0.5'
__UPDATED__ = '2018-11-20'

#===============================================================================
# Imports
#===============================================================================
import sys
import os
import gc
import string
import re

from datetime import datetime
from time import time
from collections import defaultdict, OrderedDict, Counter
from sqlalchemy import and_, or_
from sqlalchemy.orm import undefer, joinedload #, eagerload
from operator import attrgetter

import proteomics.db_models as dbm
import proteomics.pepprot_func as ppf

from general.basic_func import save_dicts_as_csv


#===============================================================================
# Global variables
#===============================================================================
DB_URI = 'mysql://root@localhost/tcellxtalkdb' #MySQL default database, if none supplied in the CLI
now = datetime.now()
FILEN_SUFFIX = "_summary_{0:%Y-%m-%d_%H%M%S}.tsv".format(now) #Default output file-name suffix, if no file-name supplied in the CLI.
OUTPUT_DIR = os.getcwd() #Default output folder, if no file-name supplied in the CLI.

#Source and SourceType ID constants (Caution!: Update them with the Current Database status):
LYMPHOS2_SRC_ID = 4 # :CAUTION: This Source ID may changed if DataBase is out-dated re-created.
TCELLXTALKDB_SRC_ID = 8 # :CAUTION: This Source ID may changed if DataBase is out-dated re-created, and now contains also Acetylations (not only ubiquitinations).
EXTRA_SOURCENAME2PTMIDS = { 'TCellXTalkDB': [1, 121], #ModificationType IDs imported from TCellXTalkDB Source
                            'Mertins et al. 2013': [1, 21, 121] } #ModificationType IDs imported from Mertins2013 Source
MSDATA_SRCTYPE_ID = 2 # SourceType ID corresponding to the Sources containing MS Data.

# Digestion Peptides variables:
MISSING_CLEAVAGES = 2
MIN_LENGTH = 7
MAX_LENGTH = 35

# Number of AAs surrounding a PTM by each side to make a PTM Region like [PTM_pos - AAS_AROUND_PTM, PTM_pos + AAS_AROUND_PTM] :
AAS_AROUND_PTM = 4

# Co-modified (Phos + Ub) Predicted digestion Peptides Sequences (with PTMs):
PREDICTEDPEPS_FILEN = "/home/oga/LymPHOS-UB-AC/Predicted Digestion Peptides Phos plus Ub for experiments - 2018-03-16.txt"
with open(PREDICTEDPEPS_FILEN, "rU") as iofile:
    PREDICTED_PEPS = map( str.strip, iofile.readlines() ) #List of sequences with PTMs 


#===============================================================================
# Class definitions
#===============================================================================



#===============================================================================
# Function definitions
#===============================================================================
def phospeps_vs_ubpeps_direct_fast(session, *args):
    """
    :caution: DEPRECATED!
    
    :param Session session: a SQLAlchemy :class:`Session` instance.
    """
    # Query the database for LymPHOS2 MS Peptides' raw sequences and PD-
    # Ubiquitinated MS Peptides' raw sequences:
    phospeps = set( session.query(dbm.MSPeptide.seq)\
                           .filter(dbm.MSPeptide.source_id==LYMPHOS2_SRC_ID,
                                   dbm.MSPeptide._db_modifications.any(), #The only PTMs in this source are Phosphorylations
                                   dbm.MSPeptide.regulation!=None) )
    ubpeps = set( session.query(dbm.MSPeptide.seq)\
                         .filter( dbm.MSPeptide.source_id==TCELLXTALKDB_SRC_ID, 
                                  dbm.MSPeptide._db_modifications.any(
                                  dbm.MSPepModification.mod_type_id==121) ) ) #Only MS Peptides with Ubiquitinations (this source contains also Oxidations).
    return ubpeps.intersection(phospeps)


def iter_phospeps_vs_ubpeps_in(session, *args):
    """
    Query the LymPHOSUBAC DB for LymPHOS2 MS Peptides and PD-Ubiquitinated MS
    Peptides, and look/filter for those Phosphorylated MS Peptides sequences
    inside Ubiquititanted MS Pepides sequences.

    :param Session session: a SQLAlchemy :class:`Session` instance.

    :return genetator : yields tuples of lists containing :class:`MSPeptide`
    instances phosphorylated, and :class:`MSPeptide` instances ubiquitinated,
    with matching sequences or sub-sequences.
    """
    seq2phospeps = defaultdict(list)
    seq2ubpeps = defaultdict(list)
    # Query the database for LymPHOS2 MS regulated Peptides and PD-
    # Ubiquitinated MS Peptides:
    phospeps = session.query(dbm.MSPeptide)\
                      .filter(dbm.MSPeptide.source_id==LYMPHOS2_SRC_ID, 
                              dbm.MSPeptide._db_modifications.any(), #The only PTMs in this source are Phosphorylations
                              dbm.MSPeptide.regulation!=None).all()
    ubpeps = session.query(dbm.MSPeptide)\
                    .filter( dbm.MSPeptide.source_id==TCELLXTALKDB_SRC_ID, 
                             dbm.MSPeptide._db_modifications.any( 
                             dbm.MSPepModification.mod_type_id==121) ).all() #Only MS Peptides with Ubiquitinations (this source contains also Oxidations).
    # Group results by raw aminoacid sequence: #X_NOTE: this de-duplication step is very important for speed optimization (5X increases in next search step)
    for mspeps, seq2peps in ( (phospeps, seq2phospeps), (ubpeps, seq2ubpeps) ):
        for mspep in mspeps:
            seq2peps[mspep.seq].append(mspep)
    # Search for Phosphorylated MS Peptides sequences inside Ubiquititanted
    # MS Pepides sequences:
    ubpepseqs = sorted( seq2ubpeps.keys() )
    for phospepseq in sorted( seq2phospeps.keys() ):
        for ubpepseq in ubpepseqs:
            if phospepseq in ubpepseq:
                yield seq2phospeps[phospepseq], seq2ubpeps[ubpepseq]


def iter_mspeps2rowdicts(phospeps_ubpeps, session=None, if_sep="; "):
    """
    Pipes the output from :func:`iter_phospeps_vs_ubpeps_in` into a valid
    :func:`save_dicts_as_csv` input.

    :param iterable phospeps_ubpeps: an iterable that contains tuples of lists
    containing :class:`MSPeptide` instances phosphorylated, and
    :class:`MSPeptide` instances ubiquitinated. Usually the output from
    :func:`iter_phospeps_vs_ubpeps_in`.
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param str if_sep: intra-field separator. The separator to use between 
    multiple values for a single field. Defaults to ", ".
    
    :return generator : yields :class:`OrderedDict` instances with MS
    Peptides information, suitable to write to a file. A valid
    :func:`save_dicts_as_csv` input.
    """
    headers = ('Regulated_Phospho_Peptide', 'Ub_Peptide', 'Equal_seqs', 
               'Mixed_Peptide', 'Mixed_Peptide_Modified_AAs', 
               'Distance_between_Modified_AAs', )
    blank_rowdict = OrderedDict( zip( headers, ['']*len(headers) ) )
    for phospeps, ubpeps in phospeps_ubpeps:
        for phospep in phospeps:
            for ubpep in ubpeps:
                sys.stdout.write( 'Exporting %s %s   \r'%(phospep.seq_w_mods, ubpep.seq_w_mods) ) # DEBUG
                sys.stdout.flush()
                # Matching MS Peptides information:
                rowdict = OrderedDict((('Regulated_Phospho_Peptide', phospep.seq_w_mods),
                                       ('Ub_Peptide', ubpep.seq_w_mods),
                                       ('Equal_seqs', ubpep.length==phospep.length),
                                       ))
                # Mixed MS Peptide information:
                mixedpep = dbm.MSPeptide(seq=ubpep.seq) #Use the longer ubiquitinated peptide sequence as base sequence.
                # - Mix modifications:
                phos_posshift = mixedpep.seq.find(phospep.seq) #Position shift for phosphorylations over ubiquitinated sequence.
                mixedmods = set() #Phosphorylations + Ubiquitinations over ubiquitinated sequence.
                for posshift, mods in ( (0, ubpep.modifications),
                                        (phos_posshift, phospep.modifications) ):
                    mixedmods.update(dbm.MSPepModification(position=mod.position+posshift,
                                                           mod_type=mod.mod_type)
                                     for mod in mods)
                mixedpep._db_modifications = mixedmods
                # - Mixed sequence with modifications:
                mixedseq_w_mods = mixedpep.seq2seq_w_mods(mixedpep.seq,
                                                          mixedpep.pos2modifications)
                rowdict['Mixed_Peptide'] = mixedseq_w_mods
                # - Number of modified AAs and distance between modifications:
                mixedmod_pos = sorted(mixedmod.position for mixedmod in mixedmods)
                rowdict['Mixed_Peptide_Modified_AAs'] = len( set(mixedmod_pos) )
                mixedmod_dist = [ str( mixedmod_pos[idx+1] - mixedmod_pos[idx] )
                                  for idx in range(len(mixedmod_pos) - 1) ]
                rowdict['Distance_between_Modified_AAs'] = if_sep.join(mixedmod_dist)
                # Common proteins information:
                # - Get only reviewed (Swiss-Prot) proteins common to both MS peptides:
                common_prots = {prot for prot in phospep.proteins if prot.reviewed}\
                               .intersection(prot for prot in ubpep.proteins if prot.reviewed) #Only Swiss-Prot (reviewed) proteins
                common_prots = sorted( common_prots, key=attrgetter('ac') )
                # - Yield the first protein AC and description with the rest of
                #   row information:
                rowdict['Common_Proteins'] = common_prots[0].ac
                rowdict['Descriptions'] = common_prots[0].name
                yield rowdict
                # - Yield the remaining proteins AC and description as new rows
                #   with the rest of the fields void:
                for prot in common_prots[1:]:
                    rowdict = blank_rowdict.copy()
                    rowdict['Common_Proteins'] = prot.ac
                    rowdict['Descriptions'] = prot.name
                    yield rowdict


def iter_hd_regions_in_reg_prots(session, *args):
    """
    :param Session session: a SQLAlchemy :class:`Session` instance.
    
    :return generator :
     """
    # Query the database for Proteins containing both LymPHOS2 MS regulated
    # Peptides and PD-Ubiquitinated MS Peptides:
    mspep_lymphos_filter = and_( dbm.MSPeptide.source_id==LYMPHOS2_SRC_ID, 
                                 dbm.MSPeptide._db_modifications.any(),
                                 dbm.MSPeptide.regulation!=None )
    ub_filter =  dbm.MSPepModification.mod_type_id==121
    mspep_pd_ub_filter = and_( dbm.MSPeptide.source_id==TCELLXTALKDB_SRC_ID, 
                               dbm.MSPeptide._db_modifications.any(ub_filter) )
    reg_prots = session.query(dbm.Protein)\
                       .filter( dbm.Protein.reviewed==True, #Only Swiss-Prot (reviewed) proteins
                                dbm.Protein.mspeptides.any(mspep_lymphos_filter),
                                dbm.Protein.mspeptides.any(mspep_pd_ub_filter) )\
                       .distinct().all()
    for prot in reg_prots:
        # Filter for only High Density Protein Regions containing both
        # LymPHOS2 phosphorylations and PD-UB ubiquitinations:
        filtered_hdptmregions = list()
        for hd_ptm_region in prot.hd_ptm_regions.values():
            region_mods_type_source = { (ptm.source_id, ptm.mod_type_id)
                                        for ptm in hd_ptm_region.modifications }
            if (LYMPHOS2_SRC_ID, 21) in region_mods_type_source and (TCELLXTALKDB_SRC_ID, 121) in region_mods_type_source: # :CAUTION: Those Source ID have changed in DataBase (are out-dated)
                #
                filtered_hdptmregions.append(hd_ptm_region)
        if filtered_hdptmregions:
            yield prot, filtered_hdptmregions


def iter_hdptmregion2rowdict(prot_filteredhdptmregions, 
                             protease=dbm.create_proteases()[0], session=None, 
                             if_sep="; "):
    """
    Pipes the output from :func:`iter_hd_regions_in_reg_prots` into a valid
    :func:`save_dicts_as_csv` input

    :param iterable prot_filteredhdptmregions: an iterable containing tuples of
    a :class:`Protein` instance and a list of it's High Density Protein Regions
    phosphorylated and ubiquitinated (as :class:`ProtRegion` instances).
    Usually the output from :func:`iter_hd_regions_in_reg_prots`.
    :param Protease protease: a :class:`Protease` instance to use as a digestion
    enzyme for the proteins. Defaults to the 'Trypsin with exceptions' protease
    creted by :func:`dbm.create_proteases`.
    :param Session session: a SQLAlchemy :class:`Session` instance.
    :param str if_sep: intra-field separator. The separator to use between 
    multiple values for a single field. Defaults to ", ".
    
    :return generator : yields :class:`OrderedDict` instances with High Density 
    Protein Region information, suitable to write to a file. A valid
    :func:`save_dicts_as_csv` input.
    """
    for prot, filtered_hdptmregions in prot_filteredhdptmregions:
        prot_ac = prot.ac
        # Get ALL Full digestion positions for the current Protein (`prot`):
        prot_ctermcuts = [ cut_pos for _, cut_pos, _ 
                           in protease.iter_fulldigest_basicdata(prot) ]
        for hd_ptm_region in filtered_hdptmregions:
            hdptmregion_start = hd_ptm_region.prot_start
            hdptmregion_end = hd_ptm_region.prot_end
            sys.stdout.write( 'Exporting %s in %s   \r'%(hd_ptm_region.seq_w_mods, prot_ac) ) # DEBUG
            sys.stdout.flush()
            rowdict = OrderedDict((('Protein', prot_ac),
                                   ('Protein_Region', hd_ptm_region.seq_w_mods),
                                   ('Region_Start', hdptmregion_start),
                                   ('Region_End', hdptmregion_end),
                                   ('Region_Modified_AAs', hd_ptm_region.mod_aas),
                                   ))
            # Get only Full Digestion Peptides Overlapping the current High
            # Density Protein Region (`hd_ptm_region`):
            dig_peps = list() #Full Digestion Peptides Overlapping current High Density Protein Region.
            prev_cut_pos = 0
            overlapping = False
            for cut_pos in prot_ctermcuts:
                if not overlapping and cut_pos >= hdptmregion_start:
                    overlapping = True
                    dig_peps.append( dbm.DigestionPeptide(protein=prot,
                                                          prot_start=prev_cut_pos+1,
                                                          prot_end=cut_pos) )
                elif overlapping:
                    dig_peps.append( dbm.DigestionPeptide(protein=prot,
                                                          prot_start=prev_cut_pos+1,
                                                          prot_end=cut_pos) )
                if cut_pos >= hdptmregion_end:
#                     overlapping = False
                    break
                prev_cut_pos = cut_pos
#             if overlapping or prev_cut_pos < hdptmregion_start: # Get partial or full overlapping digestion peptides at the end of the protein: 
#                 dig_peps.append( dbm.DigestionPeptide(protein=prot,
#                                                       prot_start=prev_cut_pos+1,
#                                                       prot_end=prot.length) )
            #
            rowdict['Overlapping_DigestionPeptides'] = if_sep.join(dig_pep.seq_w_mods 
                                                                   for dig_pep 
                                                                   in dig_peps)
            #
            yield rowdict


def query_prots4sources_modtypes(s, source_id=None, modtype_id=None, 
                                 isoforms=True):
    query = s.query(dbm.Protein)\
             .filter(dbm.Protein.reviewed==True)
    # Optionally Filter-Out protein isoforms:
    if not isoforms:
        query = query.filter(dbm.Protein.ac==dbm.Protein.ac_noisoform)
    # Join and Filter by attributes of significant ProtModification:
    query = query.join(dbm.Protein.modifications)\
                     .filter(dbm.ProtModification.significant==True)
    # -Optionally Filter by ProtModification Source.id:
    if isinstance( source_id, (int, long) ): # :param:`source_id` is an integer (simple or long):
        query = query.filter(dbm.ProtModification.source_id==source_id)
    elif source_id is not None: # Assume :param:`source_id` is an iterable object:
        query = query.filter( dbm.ProtModification.source_id.in_( set(source_id) ) ) #Coerce (and "copy") it to a set
    # -Optionally Filter by ProtModification.mod_type.id: 
    if isinstance( modtype_id, (int, long) ): # :param:`modtype_id` is an integer:
        query = query.filter(dbm.ProtModification.mod_type_id==modtype_id)        
    elif modtype_id is not None: # Assume :param:`modtype_id` is an Iterable object:
        query = query.filter( dbm.ProtModification.mod_type_id.in_( set(modtype_id) ) ) #Coerce (and "copy") it to a set
    #
    return query.order_by(dbm.Protein.ac).distinct()

def query_protmods4ac_sources_modtypes(s, prot_ac, source_id=None, modtype_id=None):
    query = s.query(dbm.ProtModification)\
             .filter(dbm.ProtModification.protein_ac==prot_ac, 
                     dbm.ProtModification.significant==True)
    # -Optionally Filter by ProtModification Source.id:
    if isinstance( source_id, (int, long) ): # :param:`source_id` is an integer (simple or long):
        query = query.filter(dbm.ProtModification.source_id==source_id)
    elif source_id is not None: # Assume :param:`source_id` is an iterable object:
        query = query.filter( dbm.ProtModification.source_id.in_( set(source_id) ) ) #Coerce (and "copy") it to a set
    # -Optionally Filter by ProtModification.mod_type.id: 
    if isinstance( modtype_id, (int, long) ): # :param:`modtype_id` is an integer:
        query = query.filter(dbm.ProtModification.mod_type_id==modtype_id)        
    elif modtype_id is not None: # Assume :param:`modtype_id` is an Iterable object:
        query = query.filter( dbm.ProtModification.mod_type_id.in_( set(modtype_id) ) ) #Coerce (and "copy") it to a set
    #
    return query.order_by(dbm.ProtModification.position)

def query_mspeps4sources_modtypes(s, source_id=None, modtype_id=None, 
                                  only_linked2prots=True):
    query = s.query(dbm.MSPeptide)
    # Optionally Filter by Source.id:
    if isinstance( source_id, (int, long) ): # :param:`source_id` is an integer:
        query = query.filter(dbm.MSPeptide.source_id==source_id)
    elif source_id is not None: # Assume :param:`source_id` is an iterable object:
        query = query.filter( dbm.MSPeptide.source_id.in_( set(source_id) ) )
    # Optionally Filter by MSPepModification.mod_type.id when MSPepModification is 
    # significant and from a physiological type. But for simplicity, it uses 
    # :attr:``seq_w_mods`` because all PTMs in it are always significant and 
    # physiological:
    if isinstance( modtype_id, (int, long) ): # :param:`modtype_id` is an integer:
        query = query.filter( dbm.MSPeptide.seq_w_mods.like("%({})%".format(modtype_id)) )
    elif modtype_id is not None: # Assume :param:`modtype_id` is an iterable object:
        ors = [dbm.MSPeptide.seq_w_mods.like( "%({})%".format(modtype_id) ) 
               for modtype_id in modtype_id]
        query = query.filter( or_(*ors) )
    else:
        # Filter at least by any AA modified:
        query = query.filter(dbm.MSPeptide.mod_aas>0)
    # Optionally filter to discard MS Peptides not linked to any Reviewed
    # Protein (because they have problematic C-Term PTMs or because they were
    # not found in any Reviewed Protein of the Database):
    if only_linked2prots:
        query = query.filter( dbm.MSPeptide.proteins.any(dbm.Protein.reviewed==True) )
    #
    return query

def db_source2ptmids(session, extra_sourcename2ptmids=None):
    # Check parameters:
    if extra_sourcename2ptmids is None:
        extra_sourcename2ptmids = EXTRA_SOURCENAME2PTMIDS
    # Get Sources from Database:
    sources = session.query(dbm.Source)\
                     .join(dbm.Source.source_types)\
                     .filter(dbm.SourceType.id==MSDATA_SRCTYPE_ID).all()
    # Get PTM IDs from :param:`extra_sourcename2ptmids` or from Database:
    return OrderedDict( ( source, 
                          ( extra_sourcename2ptmids.get(source.name, None) or 
                            sorted(source.ptmid2threshold.keys()) ) )
                        for source in sources )  

def db_ptmids(session, only_physio=True):
    # Get all ModificationType.id from Database:
    ptm_ids = session.query(dbm.ModificationType.id)\
                     .filter(dbm.ModificationType.only_physio==only_physio)\
                     .all()
    return sorted( ptm_id for ptm_id, in ptm_ids )

def iter_ptm_source_nprots(session, source2ptmids=None):
    # Check parameters:
    if source2ptmids is None:
        source2ptmids = db_source2ptmids(session)
    #
    for source, ptm_ids in source2ptmids.items():
        for ptm_id in ptm_ids:
            prots_q = query_prots4sources_modtypes(session, source.id, ptm_id)
            #
            yield ptm_id, source.name, prots_q.count()

def ptm_nprots4TCellXTalkDBSource(session):
    """
    :caution: DEPRECATED!!
    """
    tcellxtalk_src = session.query(dbm.Source).get(TCELLXTALKDB_SRC_ID)
    source2ptmids = { tcellxtalk_src: ( 1, 121, [1, 121] ) }
    for ptm_id, _, nprots in iter_ptm_source_nprots(session, source2ptmids):
        yield ptm_id, nprots

def iter_prots2dictrows(session, prots, if_sep="; "): #IMPROVE: still high memory use due to the SQLAlchemy session.
    session_count = 0
    last_prot = dbm.SourceType()
    session.add(last_prot)
    for prot in prots:
        # Free, or mark to be freed, some session memory:
        session.expunge(last_prot)
        del last_prot
        session_count += 1
        # Get protein related data:
        try:
            gene_name = prot.gene.name
        except AttributeError:
            gene_name = ''
        ptmns = sorted( {mod.mod_type.extradata['full_name'] 
                         for mod in prot.sig_modifications} )
        goannotations = sorted( {"{0} ({1})".format(goa.go_id, goa.go.name) 
                                 for goa in prot.goannotations} )
        # Generate the basic new row dictionary:
        prot_row = OrderedDict((
                                ('Protein_AC', prot.ac), 
                                ('AC_No_Isoform', prot.ac_noisoform), 
                                ('Gene', gene_name), 
                                ('Length', prot.length), 
                                ('Modified_AAs', prot.mod_aas), 
                                ('PTMs', if_sep.join(ptmns)), 
                                ('GO_Annotations', if_sep.join(goannotations)), 
                                ))
        #
        yield prot_row
        #
        del prot_row
        last_prot = prot
        # Free up some memory from time to time:
        if session_count > 2000:
            session_count = 0
            session.flush()
            gc.collect()

def iter_protswhdptmrgns2dictrows(session, prots, byref_counter=None, 
                                  if_sep="; "): #IMPROVE: still high memory use due to the SQLAlchemy session.
    session_count = 0
    last_prot = dbm.SourceType()
    session.add(last_prot)
    yields_counter = 0
    for prot in prots:
        # Free, or mark to be freed, some session memory:
        session.expunge(last_prot)
        del last_prot
        session_count += 1
        # Check and Get the number of High Density PTM Regions. And skip
        # proteins without any:
        hdptmregions = len(tuple( prot.finditer_hd_ptm_regions() ))
        if not hdptmregions:
            # Skip this protein:
            last_prot = prot
            continue
        # Get protein related data:
        try:
            gene_name = prot.gene.name
        except AttributeError:
            gene_name = ''
        ptmns = sorted( {mod.mod_type.extradata['full_name'] 
                         for mod in prot.sig_modifications} )
        goannotations = sorted( {"{0} ({1})".format(goa.go_id, goa.go.name) 
                                 for goa in prot.goannotations} )
        # Generate the basic new row dictionary:
        prot_row = OrderedDict((
                                ('Protein_AC', prot.ac), 
                                ('AC_No_Isoform', prot.ac_noisoform), 
                                ('Gene', gene_name), 
                                ('Length', prot.length), 
                                ('Modified_AAs', prot.mod_aas), 
                                ('PTMs', if_sep.join(ptmns)), 
                                ('HD_PTM_Regions', hdptmregions), 
                                ('GO_Annotations', if_sep.join(goannotations)), 
                                ))
        #
        yields_counter += 1
        #
        yield prot_row
        #
        del prot_row
        last_prot = prot
        # Free up some memory from time to time:
        if session_count > 2000:
            session_count = 0
            session.flush()
            gc.collect()
    # Finally pass the yields_counter using the "by reference" byref_counter:
    if byref_counter is not None:
        byref_counter[0] = yields_counter

def nprots_modified(session, output_filen='', if_sep="; "):
    """
    Return the number of Proteins with any PTM from any Source.
    """
    modprots_q = session.query(dbm.Protein)\
                        .options( joinedload('_db_modifications')\
                                  .joinedload('mod_type'),
                                  joinedload('gene') )\
                        .filter(dbm.Protein.reviewed==True, 
                                dbm.Protein.mod_aas>0)
    if output_filen:
        print("Start processing Proteins with PTMs to file...")
        save_dicts_as_csv(output_filen, iter_prots2dictrows(session, modprots_q, 
                                                            if_sep), 
                          delimiter='\t')
        print( "End processing Proteins with PTMs to file.\nSee output file:\n"
               " {}".format(output_filen) )
    return modprots_q.count()
    
def nprots_w_hdptmregions(session, output_filen='', if_sep="; "):
    """
    Return the number of Proteins with High Density PTM Regions.
    """
    modprots_q = session.query(dbm.Protein)\
                        .options( joinedload('_db_modifications')\
                                  .joinedload('mod_type'),
                                  joinedload('gene') )\
                        .filter(dbm.Protein.reviewed==True, 
                                dbm.Protein.mod_aas>0)
    if output_filen:
        counter = [0]
        #
        print("Start processing Proteins with HD PTM Regions to file...")
        save_dicts_as_csv(output_filen, iter_protswhdptmrgns2dictrows(session, 
                                                                        modprots_q, 
                                                                        counter, 
                                                                        if_sep), 
                          delimiter='\t')
        print( "End processing Proteins with HD PTM Regions to file.\nSee output file:\n"
               " {}".format(output_filen) )
        #
        nprots_w_hdptmrgn = counter[0]
    else:
        nprots_w_hdptmrgn = 0
        for prot in modprots_q:
            for discarded in prot.finditer_hd_ptm_regions():
                nprots_w_hdptmrgn += 1
                break
    #
    return nprots_w_hdptmrgn

def nprots4predictedpeps(session, predpeps=PREDICTED_PEPS, output_filen='', if_sep="; "):
    """
    Return the number of Proteins matched by any of the Predicted co-modified
    (Phos + Ub) digestion Peptides.
    """
    prot2predpeps_match = defaultdict(list)
    for seq_w_mods in predpeps:
        # Get the Peptide Sequence and the ModificationType IDs at each position:
        pep_seq, peppos2modids = ppf.split_seq_and_things(seq_w_mods, 
                                                       ('(', ',', ')'), int)
        peppos2modid = { pos: modids[0] for pos, modids in peppos2modids.items() }
        # Get Proteins containing the Peptide Sequence:
        prots4pepseq = dbm.search_seq_in_prot_db(pep_seq, session)
        # Filter-Out Proteins not containing the Peptide PTMs in place:
        pepseq4search = ppf.formatseq4search(pep_seq, ppf.AA2REGEX_PROTEOMICS) #Format peptide Sequence as a regex pattern to search in Protein Sequence
        pepposs_modids = peppos2modid.items()
        pepmodids = set( peppos2modid.values() )
        for prot in prots4pepseq:
            modid2protposs = prot.modid2positions
            # -Filter-Out Proteins not containing the Peptide PTMs:
            if not pepmodids.issubset(modid2protposs):
                continue
            # -Find the Peptide sequence in the Protein sequence
            for match in re.finditer(pepseq4search, prot.seq):
                start = match.start()
                # -Check that the PTMs in the current matching protein region
                #  match the Peptide PTMs in the right positions:
                if all( ( (peppos + start) in modid2protposs[modid] ) 
                        for peppos, modid in pepposs_modids ):
                    prot2predpeps_match[prot].append( (seq_w_mods, start+1) )
                    break
    if output_filen:
        print("Start processing Proteins matched by Predicted Peptides to file...")
        save_dicts_as_csv(output_filen, iter_prots2dictrows(session, 
                                                            prot2predpeps_match.keys(), 
                                                            if_sep), 
                          delimiter='\t')
        print( "End processing Proteins matched by Predicted Peptides to file.\nSee output file:\n"
               " {}".format(output_filen) )
    return len( prot2predpeps_match.keys() )
    
def iter_ptm_source_nmspeps(session, source2ptmids=None):
    # Check parameters:
    if source2ptmids is None:
        source2ptmids = db_source2ptmids(session)
    #
    for source, ptm_ids in source2ptmids.items():
        for ptm_id in ptm_ids:
            mspeps_q = query_mspeps4sources_modtypes(session, source.id, ptm_id)
            # Filter-Out possible duplicated MSPeptides:
            unique_seq_w_mods = {mspep.seq_w_mods for mspep in mspeps_q}
            #
            yield ptm_id, source.name, len(unique_seq_w_mods)

def iter_ptm_nmspeps4ptmids(session, ptm_ids=None):
    # Check parameters:
    if ptm_ids is None:
        # Get all physiological ModificationType.id from Database:
        ptm_ids = db_ptmids(session, only_physio=True)
    #
    for ptm_id in ptm_ids:
        mspeps_q = query_mspeps4sources_modtypes(session, modtype_id=ptm_id)
        # Filter-Out duplicated MSPeptides (same MSPeptide from different sources, ...):
        unique_seq_w_mods = {mspep.seq_w_mods for mspep in mspeps_q}
        #
        yield ptm_id, len(unique_seq_w_mods)

def iter_ptm_source_nptmsites_OLD(session, source2ptmids=None):
    """
    :caution: DEPRECATED! Use new :func:`iter_ptm_source_nptmsites` instead.
    
    Calculate the number of PTM sites from the MS Peptides of a Source and a
    PTM type, for each Source and PTM types/IDs provided (`source2ptmids`), or
    for each of all available Sources and their PTM IDs in Database if none is 
    provided.
    
    It reduces duplicates by identical MS Peptide sequences and by MS Peptide
    sequences contained in other MS Peptide sequences (so it doen't go to the
    protein sequence to match sequence regions of a fixed length centered
    around each PTM).
    
    :param dict source2ptmids: { Source: [ModificationType.id, ...] }
    
    :return generator : yields tuples containing the current PTM ID, Source
    name and number of PTM sites for the Source and the PTM ID.
    """
    # Check parameters:
    if source2ptmids is None:
        source2ptmids = db_source2ptmids(session)
    #
    for source, ptm_ids in source2ptmids.items():
        for ptm_id in ptm_ids:
            mspeps_q = query_mspeps4sources_modtypes(session, source.id, ptm_id)
            # Summarize PTM positions (for the current PTM ID) of the different
            # MSPeptides with the same sequence (different ``seq_w_mods``) 
            seq2ptmposs = defaultdict(set)
            for mspep in mspeps_q:
                seq2ptmposs[mspep.seq].update( mspep.modid2positions[ptm_id] )
            # Check sequences contained into other sequences, to summarize
            # their PTM positions (for the current PTM ID):
            seqs_by_length = sorted(seq2ptmposs.keys(), key=len) #Sort sequences by their length so iterate from shortest to largest, to avoid check for a larger sequence inside a shorter one
            for idx, seq1 in enumerate(seqs_by_length):
                seq1_poss = seq2ptmposs[seq1]
                clean_seq1_poss = False
                for seq2 in seqs_by_length[idx+1:]: #Avoid comparison with previous compared sequences
                    if seq1 in seq2:
                        # -Transfer positions of the contained sequence to the
                        #  larger containing one:
                        pos_shift = seq2.find(seq1)
                        seq2ptmposs[seq2].update(pos+pos_shift 
                                                 for pos in seq1_poss)
                        clean_seq1_poss = True
                # -After checking, if PTM positions have been transfered, clean
                #  own ones (they will be counted into the other sequences):
                if clean_seq1_poss:
                    seq2ptmposs[seq1] = set()
            #
            yield ptm_id, source.name, sum( len(poss) 
                                            for poss in seq2ptmposs.values() )

def ptmsites4prots(session, prots, source_id=None, ptm_id=None, 
                   aas_around_ptm=AAS_AROUND_PTM, ptmseq_frmt=lambda seq: seq, 
                   ptmid2ptmsites2srcs=None):
    """
    :param int aas_around_ptm: number of AAs surrounding a PTM by each side to 
    make a PTM Region like [PTM_pos - aas_around_ptm, PTM_pos + aas_around_ptm].
    Defaults to the value of the global constant AAS_AROUND_PTM.
    
    :return defaultdict : a dictionary of dictionaries where each PTM ID is
    associated with its PTM-sites sequences and each PTM-site sequence with the
    Source IDs where it was found:
        {ModificationType.id: { PTM-site_seq: {Source.id, ...}, ... }, ...}
    """
    if ptmid2ptmsites2srcs is None:
        ptmid2ptmsites2srcs = defaultdict( lambda: defaultdict(set) ) # Ex.: {1: { 'AAAAKAAAA': {6, 12}, ... }, ...}
    spaces = " " * aas_around_ptm
    #
    for prot in prots:
        prot_seq = spaces + prot.seq + spaces # Surround sequence with as many spaces as the number of AAs surrounding a PTM by each side, to avoid caring about extremes.
        protmods_q = query_protmods4ac_sources_modtypes(session, prot.ac, 
                                                        source_id, ptm_id)
        last_protmod_pos = 0
        for protmod in protmods_q:
            protmod_pos = protmod.position
            if protmod_pos != last_protmod_pos:
                start = protmod_pos - 1 # This is really (protmod_pos + aas_around_ptm) - 1 - aas_around_ptm
                end = protmod_pos + aas_around_ptm*2
                ptmsite_seq = ptmseq_frmt( prot_seq[start:end] )
                last_ptmreg_seq = ptmsite_seq
                last_protmod_pos = protmod_pos
            else:
                ptmsite_seq = last_ptmreg_seq
            ptmid2ptmsites2srcs[protmod.mod_type_id][ptmsite_seq].add(protmod.source_id)
    #
    return ptmid2ptmsites2srcs

def iter_ptm_source_nptmsites(session, source2ptmids=None, 
                              aas_around_ptm=AAS_AROUND_PTM):
    """
    Calculate the number of PTM sites from the MS Peptides of a Source and a
    PTM type, for each Source and PTM types/IDs provided (`source2ptmids`), or
    for each of all available Sources and their PTM IDs in Database if none is 
    provided.
    
    It reduces duplicates by going to the protein sequence to match sequence 
    regions of a fixed length centered around each PTM.
    
    :param dict source2ptmids: { Source: [ModificationType.id, ...] }
    :param int aas_around_ptm: number of AAs surrounding a PTM by each side to 
    make a PTM Region like [PTM_pos - aas_around_ptm, PTM_pos + aas_around_ptm].
    Defaults to the value of the global constant AAS_AROUND_PTM.
    
    :return generator : yields tuples containing the current PTM ID, Source
    name, and number of PTM sites for that Source and that PTM ID.
    """
    # Check parameters:
    if source2ptmids is None:
        source2ptmids = db_source2ptmids(session)
    #
    IL2J_transtbl = string.maketrans('IL', 'JJ')
    ptmseq_frmt = lambda seq, transtbl=IL2J_transtbl: seq.translate(transtbl)
    #
    for source, ptm_ids in source2ptmids.items():
        source_id = source.id
        for ptm_id in ptm_ids:
            prots_q = query_prots4sources_modtypes(session, source_id, ptm_id)
            prots_q = prots_q.options( undefer('seq') )
            ptmid2ptmsites2srcs = ptmsites4prots(session, prots_q, source_id, 
                                                 ptm_id, aas_around_ptm, 
                                                 ptmseq_frmt)
            #
            yield ptm_id, source.name, len( ptmid2ptmsites2srcs[ptm_id].keys() )

def iter_ptm_nptmsites4ptmids(session, ptm_ids=None, aas_around_ptm=AAS_AROUND_PTM):
    """
    Calculate the number of PTM sites from the MS Peptides of a Source and a
    PTM type, for each Source and PTM types/IDs provided (`source2ptmids`), or
    for each of all available Sources and their PTM IDs in Database if none is 
    provided.
    
    It reduces duplicates by going to the protein sequence to match sequence 
    regions of a fixed length centered around each PTM.
    
    :param iterable ptmids: an iterable of ModificationType IDs to look for.
    :param int aas_around_ptm: number of AAs surrounding a PTM by each side to 
    make a PTM Region like [PTM_pos - aas_around_ptm, PTM_pos + aas_around_ptm].
    Defaults to the value of the global constant AAS_AROUND_PTM.
    
    :return generator : yields tuples containing the current PTM ID, number of
    PTM sites for that PTM ID, and a Counter .
    """
    # Check parameters:
    if ptm_ids is None:
        # Get all physiological ModificationType.id from Database:
        ptm_ids = db_ptmids(session, only_physio=True)
    #
    IL2J_transtbl = string.maketrans('IL', 'JJ')
    ptmseq_frmt = lambda seq, transtbl=IL2J_transtbl: seq.translate(transtbl)
    # Get modified proteins:
    prots_q = query_prots4sources_modtypes(session, None, ptm_ids)
    prots_q.options( undefer('seq') )
    # Get results:
    ptmid2ptmsites2srcs = ptmsites4prots(session, prots_q, None, ptm_ids, 
                                         aas_around_ptm, ptmseq_frmt)
    # Process results:
    for ptm_id, ptmsites2srcs in ptmid2ptmsites2srcs.iteritems():
        nptmsites = len( ptmsites2srcs.keys() )
        nsources2nptmsites = Counter( len(srcs) for srcs in ptmsites2srcs.values() )
        #
        yield ptm_id, nptmsites, nsources2nptmsites


def summary_tables4paper(db_uri):
    # Create the database connection:
    prot_db = dbm.DataBase(db_uri)
    # Do Query and Process it, Format results and Write them to a file:
    with prot_db.session_no_autoflush() as session:
        source2ptmids = db_source2ptmids(session)
        ptm_ids = db_ptmids(session, only_physio=True)
        # Number of Modified Proteins for each PTM type and each Source:
        print('\nNumber of Modified Proteins for each PTM type and each Source:')
        for data in iter_ptm_source_nprots(session, source2ptmids):
            print(data)
        # Number of MS Peptides for each PTM type and each Source:
        print('\nNumber of MS Peptides for each PTM type and each Source:')
        for data in iter_ptm_source_nmspeps(session, source2ptmids):
            print(data)
        # Number of PTM Sites for each PTM type and each Source:
        print('\nNumber of PTM Sites for each PTM type and each Source:')
        for data in iter_ptm_source_nptmsites(session, source2ptmids):
            print(data)
        # Number of MS Peptides for each PTM type (in all Sources):
        print('\nNumber of MS Peptides for each PTM type (in all Sources):')
        for data in iter_ptm_nmspeps4ptmids(session, ptm_ids):
            print(data)
        # Number of PTM Sites for each PTM type:
        print('\nNumber of PTM Sites for each PTM type:')
        for data in iter_ptm_nptmsites4ptmids(session, ptm_ids):
            print(data)
    with prot_db.session_no_autoflush() as session:
        # Calculate and save to file Modified Proteins:
        print('\nCalculate and save to file ALL Modified Proteins:')
        output_filen = os.path.join(OUTPUT_DIR, 
                                    "Modified_Proteins" + FILEN_SUFFIX)
        nprots_mod = nprots_modified(session, output_filen)
        print("Total number of Modified Proteins in DB: ", nprots_mod)
    with prot_db.session_no_autoflush() as session:
        # Calculate and save to file Proteins with High Density PTM Regions:
        print('\nCalculate and save to file ALL Proteins with High Density PTM Regions:')
        output_filen = os.path.join(OUTPUT_DIR, 
                                    "Proteins_w_hdPTMregions" + FILEN_SUFFIX)
        nprots_hdptmregs = nprots_w_hdptmregions(session, output_filen)
        print("Total number of Proteins with High Density PTM Regions in DB: ", nprots_hdptmregs)    
    with prot_db.session_no_autoflush() as session:
        # Calculate and save to file Proteins matched by co-modified Predicted digestion Peptides:
        print('\nCalculate and save to file ALL Proteins matched by co-modified Predicted digestion Peptides:')
        output_filen = os.path.join(OUTPUT_DIR, 
                                    "Proteins_for_Predicted_Peptides" + FILEN_SUFFIX)
        nprots_predpeps = nprots4predictedpeps(session, PREDICTED_PEPS, output_filen)
        print("Total number of Proteins matched by co-modified Predicted digestion Peptides: ", nprots_predpeps)    
    #
    return 0



def main(db_uri, output_filen, summary_func, summary_args, formating_func=None):
    """
    Entry point function.
    
    :param str db_uri:
    :param str output_filen:
    :param func summary_func:
    :param iterable summary_args:
    :param func formating_func:
    
    :return int : exit status code.
    """
    # Check parameters:
    if formating_func is None:
        formating_func = iter_mspeps2rowdicts
    if not output_filen:
        output_filen = os.path.join(OUTPUT_DIR, summary_func.func_name + FILEN_SUFFIX)
    print( 'Start MS Peptides Data Export from {0}'.format(db_uri) )
    # Create the database connection:
    prot_db = dbm.DataBase(db_uri)
    # Do Query and Process it, Format results and Write them to a file:
    with prot_db.session_no_autoflush() as session:
        save_dicts_as_csv(output_filen, 
                          formating_func(summary_func(session, *summary_args), 
                                         session=session), 
                          delimiter='\t')
        session.rollback() # Avoid changes to the database.
    #
    print( '\nData saved to file:\n  {0}'.format(output_filen) )
    #
    return 0



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    # CLI minimal argument parsing:
    itself = lambda x: x
    CLI_ARGUMENTS = OrderedDict((
        # Different Summary Options:
        ( 'hd_regions_in_reg_prots', (iter_hdptmregion2rowdict, iter_hd_regions_in_reg_prots,  0, itself) ),
        ( 'phospeps_vs_ubpeps_in', (None, iter_phospeps_vs_ubpeps_in, 0, itself) ),
        ( 'paper_summary', (None, summary_tables4paper, 0, itself) ),
                                  ))
    #
    output_filen = ''
    formating_func = None
    summary_func = None
    n_summary_args = 0
    summary_args = tuple()
    PROG_NAME = os.path.basename( sys.argv[0] ) # Get script name.
    cli_argv = sys.argv[1:] # Exclude script name from the CLI arguments to process.
#     cli_argv = ['hd_regions_in_reg_prots']
    cli_argv = ['paper_summary']
    if cli_argv and ':/' in cli_argv[0]: # First argument is a DB URI
        DB_URI = cli_argv[0]
        cli_argv = cli_argv[1:]
    if cli_argv: # First, or next (if first consumed), argument is a Summary Option:
        try:
            formating_func, summary_func, n_summary_args, args_func = CLI_ARGUMENTS[ cli_argv[0] ]
            summary_args = [ args_func(arg) for arg in cli_argv[1:n_summary_args+1] ]
        except (KeyError, ValueError) as _:
            pass
        cli_argv = cli_argv[n_summary_args+1:]
    if cli_argv: # Last argument, if present, is an output filename:
        output_filen = cli_argv[0]
        cli_argv = cli_argv[1:]
    if not summary_func or len(summary_args) != n_summary_args or cli_argv: # Other arguments -> incorrect ones:
        sys.stderr.write('Syntax Error!\n\n')
        print('Usage:')
        print( ' {0} [DataBase URI] [output file]'.format( sys.argv[0]) )
        print('\nExamples:')
        print( ' {0}'.format(sys.argv[0]) )
        print( ' {0} {1}'.format(sys.argv[0], DB_URI) )
        print( ' {0} {1} {2}'.format(sys.argv[0], DB_URI, output_filen) )
        sys.exit(2)
    #
    t0 = time()
    # Generate summary:
    if summary_func == summary_tables4paper:
        # -Special case:
        exit_code = summary_tables4paper(DB_URI)
    else:
        # -Normal cases:
        exit_code = main(DB_URI, output_filen, summary_func, summary_args, formating_func)
    #
    print( "\n(Process took {0} seconds).".format(time() - t0) )
    #
    sys.exit(exit_code)
