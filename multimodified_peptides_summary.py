# -*- coding: utf-8 -*-
"""
:synopsis: :caution: DEPRECATED!! Not useful for finding peptides to monitor.
           Create a TSV Summary about the multiple modified peptides from 
           experimental MS peptides in a Protein Database for LymPHOS-UB-AC.

:created:    2015/05/15

:authors:    Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat

:version:    0.1
:updated:    2015-05-22
"""

#===============================================================================
# Imports
#===============================================================================
from __future__ import division
from __future__ import print_function

import sys
import os
from datetime import datetime
from collections import OrderedDict
from sqlalchemy import func, desc, distinct
from sqlalchemy.orm import undefer, joinedload

from general.basic_func import save_dicts_as_csv

import proteomics.db_models as dbm


#===============================================================================
# Global variables
#===============================================================================
DB_URI = 'mysql://root@localhost/lymphosubac' #MySQL default database

now = datetime.now()
filen = 'multiplemodified_peptides_summary_{0}_{1}{2}{3}.tsv'.format(now.date(), 
                                                                     now.hour, 
                                                                     now.minute, 
                                                                     now.second)
OUTPUT_FILEN = os.path.join(os.getcwd(), filen) #Output filename

MAX_PTM_DIST = 7 #Maximum allowed distance between PTMs in a protein
 
#===============================================================================
# Class definitions
#===============================================================================



#===============================================================================
# Function definitions
#===============================================================================
def iter_multimodified_peptide(prot_db, min_mod_pos=2):
    """
    Makes peptide chunks from overlaping MS peptides that map near
    modifications in a protein.
    """
    with prot_db.session() as session:
        query = session.query(dbm.Protein, 
                              func.count(distinct(dbm.ProtModification.position)).label('mod_aas'))\
                       .options( undefer('seq') )\
                       .join(dbm.ProtModification)\
                       .group_by(dbm.Protein.ac)\
                       .filter(dbm.Protein.ac == 'Q15942')\
                       .filter('mod_aas' >= min_mod_pos) #DEBUG
        for protein, mod_ass in query:
            print(protein, mod_ass) #DEBUG
            # For every modified protein position, get the cur_region delimited by
            # the MS peptides that have defined it:
            #  aaaaaaaPaaaaaaaaaa   Protein ( P: modified position )
            #       aaPaaa          MS peptide 1
            #        aPaaaaa        MS peptide 2
            #      [........]       Region
            pos2region = OrderedDict()
            for pos, mods in sorted( protein.pos2modifications.items() ):
                pep_starts = [pos] #MS peptide starts relative to the protein
                pep_ends = [pos] #MS peptide ends relative to the protein
                for mod in mods:
                    for mspepmod in mod.mspepmodifications:
                        pep_start = pos - mspepmod.position + 1 #Use the MS peptide modification position to get MS peptide start relative to the protein
                        pep_starts.append(pep_start)
                        pep_end = pep_start + len(mspepmod.mspeptide.seq) - 1 #MS peptide end relative to the protein
                        pep_ends.append(pep_end)
                pos2region[pos] = { 'start': min(pep_starts), 
                                     'end': max(pep_ends) }
            # Get overlapping regions:
            last_region = {'n_pos':0 , 'start': 0, 'end': 0}
            for pos, cur_region in pos2region.items():
                if last_region['end'] >= cur_region['start']: #Current cur_region overlaps previous one:
                    # Increase the extend of the previous cur_region with the new
                    # one (mix current and previous regions):
                    last_region['start'] = min( last_region['start'], cur_region['start'] )
                    last_region['end'] = max( last_region['end'], cur_region['end'] )
                    last_region['n_pos'] += 1 #Also indicates that this cur_region includes a new modified position
                else: #No overlapping:
                    # Create and yield a new multimodified_peptide with the
                    # last cur_region, if it has more than one modified position:
                    if last_region['n_pos'] >= min_mod_pos:
                        yield dbm.DigestionPeptide( protein=protein,
                                                    prot_start=last_region['start'],
                                                    prot_end=last_region['end'] )
                    # Current analyzed cur_region becomes the last cur_region:
                    last_region = { 'n_pos': 1, 
                                    'start': cur_region['start'], 
                                    'end': cur_region['end'] }
            # Create and yield a new multimodified_peptide with the
            # last remaining cur_region, if it has more than one modified position:          
            if last_region['n_pos'] >= min_mod_pos:
                yield dbm.DigestionPeptide( protein=protein,
                                            prot_start=last_region['start'],
                                            prot_end=last_region['end'] )
                        
def iter_digpeps2rowdict(multimodified_peps):
    """
    Pipes the output from :func:`iter_multimodified_peptide` into a valid
    :func:`save_dicts_as_csv` input
    
    :param iterable multimodified_peps: an iterable that contains tuples
    of :class:`DigestionPeptide` instances and the list of different
    modification types associated with it. Usually the output from 
    :func:`iter_multimod_digpep`.
    
    :return generator : yields :class:`OrderedDict` instances with Digestion
    Peptides information, suitable to write to a file. A valid
    :func:`save_dicts_as_csv` input.
    """
    for multimod_pep in multimodified_peps:
        sys.stdout.write('Exporting %s    \r'%multimod_pep.seq_w_mods) # DEBUG
        # Digestion Peptide information:
        dif_mods = sorted( set(mod.mod_type.name for mod in multimod_pep.modifications) )
        rowdict = OrderedDict((('Multimodified_Peptide', multimod_pep.seq), 
                               ('Protein', multimod_pep.protein_ac), 
                               ('Start_Position', multimod_pep.prot_start), 
                               ('End_Position', multimod_pep.prot_end), 
                               ('Different_Modifications', len(dif_mods)), 
                               ('Modification Types', ', '.join(dif_mods)), 
                               ('Total_Modifications', len(multimod_pep.modifications)),
                               ))
        # Associated Modifications information:
        for index, (pos, mod) in enumerate( sorted(multimod_pep.pos2modifications.items()) ):
            keyhead = 'Modification_{0}_'.format(index + 1)
            moddict = OrderedDict(((keyhead+'Type', mod.mod_type.name), 
                                   (keyhead+'Position', pos), 
                                   (keyhead+'AA', mod.aa), 
                                   (keyhead+'Ascore', mod.ascore), 
                                   ))
            rowdict.update(moddict)
        #
        yield rowdict


def main(db_uri, output_filen):
    print( 'Start Multimodified Peptides Data Export from {0}'.format(db_uri) )
    # Create the database connection:
    prot_db = dbm.DataBase(db_uri)
#     #DEBUG:
#     for digpep in iter_multimodified_peptide(prot_db, min_mod_pos=2):
#         print('\t'.join(map(str, (digpep.seq_w_mods, digpep.length, digpep.protein.ac, digpep.prot_start, digpep.prot_end))))
    # Write the query to a file:
    save_dicts_as_csv(output_filen, 
                      iter_digpeps2rowdict( iter_multimodified_peptide(prot_db, 
                                                                       min_mod_pos=2) ), 
                      delimiter='\t')
    print( '\nData saved to file:\n  {0}'.format(output_filen) )
    #
    return 0



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    # CLI minimal argument parsing:
    cli_argv = sys.argv[1:]
    if cli_argv and ':/' in cli_argv[0]: #First argument is a DB URI
        DB_URI = cli_argv[0]
        cli_argv = cli_argv[1:]
    if cli_argv: #First or next (if first consumed) argument is a filename
        OUTPUT_FILEN = cli_argv[0]
        cli_argv = cli_argv[1:]
    if cli_argv: #Other arguments -> incorrect ones
        sys.stderr.write('Syntax Error!\n\n')
        print('Usage:')
        print( ' {0} [DataBase URI] [output file]'.format( sys.argv[0]) )
        print('\nExamples:')
        print( ' {0}'.format(sys.argv[0]) )
        print( ' {0} {1}'.format(sys.argv[0], DB_URI) )
        print( ' {0} {1} {2}'.format(sys.argv[0], DB_URI, OUTPUT_FILEN) )
        sys.exit(2)
    #
    exit_code = main(DB_URI, OUTPUT_FILEN)
    #
    sys.exit(exit_code)
