# -*- coding: utf-8 -*-

"""
:synopsis:   Custom Template Tags and Filters for LymPHOS_UB_AC project.

:created:    2017-07-11

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.1 dev'
__UPDATED__ = '2018-08-16'

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django import template

# Python core imports:
from operator import attrgetter

#===============================================================================
# Library object
#===============================================================================
register = template.Library()


#===============================================================================
# Filter Function definitions
#===============================================================================
@register.filter
def ordered_by_attr(objs_sequence, attr):
    """
    Very similar filter to the standard ``dictsort`` filter in django.
    
    :param iterable objs_sequence: a sequence of objects.
    :param str attr: the attribute to use for sorting, in the format accepted
    by :class:`operator.attrgetter`.
    
    :return list : a sorted list of the objects in `objs_sequence`, according
    to the value of attribute `attr`.
    """
    return sorted( objs_sequence, key=attrgetter(attr) )


@register.filter
def ordered(sequence, order='normal'):
    """
    Sorts a sequence in "normal"/"ascending" or "reverse"/"descending" order.
    
    :param iterable sequence: a sequence of sortable objects (strings, numbers).
    :param str order: the sort order to use. It must be one of
    "normal"/"ascending" or "reverse"/"descending". Defaults to 'normal'.
    
    :return list : a sorted list of the objects in `sequence`.
    """
    if order in {'normal', 'ascending'}:
        return sorted(sequence)
    elif order in {'reverse', 'descending'}:
        return sorted(sequence, reverse=True)
    else:
        raise ValueError('`order` parameter must have only one the values: '\
                         '"normal"/"ascending" or "reverse"/"descending".')


@register.filter
def get_type(value):
    # Based on https://stackoverflow.com/a/12028864
    return type(value).__name__

