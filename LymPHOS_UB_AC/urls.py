# -*- coding: utf-8 -*-
"""
:synopsis:   Django URL settings for LymPHOS_UB_AC project.

:created:    2015-01-26

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.2 dev'
__UPDATED__ = '2018-08-24'

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.conf import settings
from django.conf.urls import patterns, url #, include
# from django.contrib import admin

# Application imports:
from generic_views import BaseTemplateContext, SectionView
from views import (SearchSectionView, ProteinSearchListView, ProteinView, 
                   ProteinDigestCombineView, MSPeptideSearchListView, 
                   MSPeptideView, Spectrum2PNG, Spectrum2MGF, DownloadSectionView
#                    DigPepSearchListView # Deprecated View :class:`DigPepSearchListView`
                   )

# Python core imports:
from collections import OrderedDict


#===============================================================================
# Global variables
#===============================================================================
# Dictionary with the web page sections: { section_name: {label: 'Label', title: 'html title/tooltip'} }
# X-NOTE: used an OrderedDict to have a predefined order of the sections in the navigation bar
SECTIONS = OrderedDict((
    ('home', {'label': 'Home', 'title': '{} Home Page'.format(settings.APPNAME)} ),
    ('search', {'label': 'Search', 'title': 'Search {}'.format(settings.APPNAME)} ),
    ('about', {'label': 'About', 'title': 'About {}'.format(settings.APPNAME)} ),
    ('statistics', {'label': 'Statistics', 'title': '{} Database Statistics'.format(settings.APPNAME)} ),
    ('contribute', {'label': 'Contribute', 'title': 'Contribute to {}'.format(settings.APPNAME)} ),
    ('download', {'label': 'Download', 'title': '{} Database Download'.format(settings.APPNAME)} ),
    ('contact', {'label': 'Contact', 'title': 'Contact with us'} ),
    ('help', {'label': 'Help', 'title': '{} Help'.format(settings.APPNAME)} ),
                        ))

# Base Contexts for the Base Template base.html:
basecontext_no_sidebar = BaseTemplateContext(sections=SECTIONS, side_bar='')


#===============================================================================
# URL patterns variable
#===============================================================================
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'LymPHOS_UB_AC.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    # url(r'^admin/', include(admin.site.urls)),

    # Home:
    url(r'^$',
        SectionView.as_view(base_context=basecontext_no_sidebar,
                            template_name='home_section.html',
                            title='Home Page',
                            section='home'),
        name='home'
        ),
    # Search:
    url(r'^search/$',
        SearchSectionView.as_view(base_context=basecontext_no_sidebar),
        name='search'
        ),
    # Protein Search Results:
    url(r'^protein_results/$',
        ProteinSearchListView.as_view(base_context=basecontext_no_sidebar),
        name='protein_results'
        ),
    # Protein Search View:
    url(r'^protein/([\w\-]+)$',
        ProteinView.as_view(base_context=basecontext_no_sidebar),
        name='protein_view'
        ),
    # Protein Digest View:
    url(r'^protein_digest/([\w\-]+)$',
        ProteinDigestCombineView.as_view(base_context=basecontext_no_sidebar),
        name='protein_digest'
        ),
    # DEPRECATED Digestion Peptides Search Results:
#     url(r'^digpep_results/$',
#         DigPepSearchListView.as_view(base_context=basecontext_no_sidebar),
#         name='digpep_results'
#         ),
    # MS Peptide Search Results:
    url(r'^ms_peptide_results/$',
        MSPeptideSearchListView.as_view(base_context=basecontext_no_sidebar),
        name='ms_peptide_results'
        ),
    # MS Peptide View:
    url(r'^ms_peptide/([0-9]+)$',
        MSPeptideView.as_view(base_context=basecontext_no_sidebar),
        name='ms_peptide_view'
        ),
    # Spectrum PNG file:
    url(r'^spectrum_image/([0-9]+\-.+\.png)$',
        Spectrum2PNG.as_view(),
        name='spectrum_image'
        ),
    # Spectrum MGF file:
    url(r'^download_mgf/([0-9]+\-.+\.mgf)$',
        Spectrum2MGF.as_view(),
        name='download_mgf'
        ),
    # About:
    url(r'^about/$',
        SectionView.as_view(base_context=basecontext_no_sidebar,
                            template_name='about_section.html',
                            title='About',
                            section='about'),
        name='about'
        ),
    # Statistics:
    url(r'^statistics/$',
        SectionView.as_view(base_context=basecontext_no_sidebar,
                            template_name='statistics_section.html',
                            title='Statistics',
                            section='statistics'),
        name='statistics'
        ),
    # Contribute:
    url(r'^contribute/$',
        SectionView.as_view(base_context=basecontext_no_sidebar,
                            template_name='contribute_section.html',
                            title='Contribute',
                            section='contribute'),
        name='contribute'
        ),
    # Statistics:
    url(r'^download/$',
        DownloadSectionView.as_view(base_context=basecontext_no_sidebar,
                                    template_name='download_section.html',
                                    title='Download',
                                    section='download'),
        name='download'
        ),
    # Contact:
    url(r'^contact/$',
        SectionView.as_view(base_context=basecontext_no_sidebar,
                            template_name='contact_section.html',
                            title='Contact',
                            section='contact'),
        name='contact'
        ),
    # Help:
    url(r'^help/$',
        SectionView.as_view(base_context=basecontext_no_sidebar,
                            template_name='help_section.html',
                            title='Help',
                            section='help'),
        name='help'
        ),
    # Terms and Conditions:
    url(r'^terms_and_conditions/$',
        SectionView.as_view(base_context=basecontext_no_sidebar,
                            template_name='termsandconditions_section.html',
                            title='Terms and Conditions',
                            section='termsandconditions'),
        name='termsandconditions'
        ),
)
