# -*- coding: utf-8 -*-
"""
:synopsis:   Django settings for LymPHOS_UB_AC (TCellXTalk) project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/
For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/

:created:    2015-01-26

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.6 dev'
__AUTHORS__ = 'Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat ; http://proteomica.uab.cat)'
__UPDATED__ = '2018-06-29'

#===============================================================================
# Imports
#===============================================================================
# Python core imports:
import os

# Other imports:
from settings_private import *
import proteomics.db_models as DBM # SQLAlchemy DataBase Models. See also `PROT_DB` below.

#===============================================================================
# Settings variables
#===============================================================================
# WEB_DEPLOY: See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

# Application and Middleware definition:

DEFAULT_APPS = (
#     'django.contrib.admin',
#     'django.contrib.auth',
    'django.contrib.contenttypes',
#     'django.contrib.sessions', #X-NOTE: needed only by session database backend
#     'django.contrib.messages',
    'django.contrib.staticfiles',
)

THIRD_PARTY_APPS = (
)

LOCAL_APPS = (
    'LymPHOS_UB_AC',
)

INSTALLED_APPS = DEFAULT_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
#     'django.contrib.auth.middleware.AuthenticationMiddleware',
#     'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
#     'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
)


# Templates:

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,'templates'), #General Base Templates
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

ROOT_URLCONF = 'LymPHOS_UB_AC.urls'

WSGI_APPLICATION = 'LymPHOS_UB_AC.wsgi.application'


# Database:
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# IDs (Unimod Accessions) of modifications without a clear physiological origin, 
# so they can be excluded from some operations (`seq_w_mods` and others):
NO_PHYSIO_MDDIDS = DBM.NO_PHYSIO_MDDIDS #It's the same global object as of `db_models` module.
NO_PHYSIO_MDDIDS.update( {4, 35} ) #Update with Cys Carbamidomethylation (4) and Met Oxidation (35).
# DATABASES = {                #X_NOTE: We don't use Django ORM but SQLAlchemy.
# }
PROT_DB = DBM.DataBase(DB_URI) # Connect to a SQLAlchemy database with proteomic data.


# Internationalization:
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images):
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

STATIC_URL = '/files/'


# Session middleware configuration:
# https://docs.djangoproject.com/en/1.7/topics/http/sessions/

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_HTTPONLY = True
# if DEPLOY:                                       # We are in Production Mode
#     SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
# else:                                            # We are in Development Mode
#     SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'
SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies' # Use signed cookies for sessions due to problems with the cache backend in deployment server.
