# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis: Fixes for Proteomics Database LymPHOS-UB-AC. Splitted from create_db4LymPHOSUBAC.py

:created:    2015/01/28

:authors:    Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014-2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.10'
__UPDATED__ = '2018-04-17'

#===============================================================================
# Imports
#===============================================================================
import sys
import os
import re
import csv
from collections import OrderedDict
from operator import attrgetter

from sqlalchemy.orm import joinedload
from sqlalchemy import text, or_

import proteomics.db_models as dbm
import proteomics.filters as filters


#===============================================================================
# Global variables
#===============================================================================
# Bioinfo01 folder for ZIP DB files:
DBS_FOLDER = '/home/oga/Laboratori de Proteomica CSIC-UAB/Bases de Dades/'
# Laptop folder for ZIP DB files:
# DBS_FOLDER = '/home/ali3n/Documents/Laboratorio Proteomica CSIC-UAB/Bases de Dades (reduit)/'

# Bioinfo01 folder for Data Results files:
RESULTS_FOLDER = '/home/oga/Laboratori de Proteomica CSIC-UAB/LymPHOS-UB-AC/'
# Laptop folder for Data Results files:
# RESULTS_FOLDER = '/home/ali3n/Documents/Laboratorio Proteomica CSIC-UAB/LymPHOS-UB-AC/'

# ZIP file where local UniProt XML and FASTA files reside:
UNIPROT_FILE = os.path.join(DBS_FOLDER, 'UniProtKB/Human reference proteome - UP000005640 rev.02-2016 - 2016-04-18/splitted_compressed.zip')
# ZIP file containing Choudhary et al. 2009 supplementary information TSV tables:
CHOUDHARY09_FILE = os.path.join(DBS_FOLDER, 'PTM articles for PHOS UB AC in Jurkat 2015-03-16/Choudhary_09_Acetylations_SupplementaryTableS4.zip')
# ZIP file containing Udeshi et al. 2013 supplementary information TSV tables:
UDESHI13_FILE = os.path.join(DBS_FOLDER, 'PTM articles for PHOS UB AC in Jurkat 2015-03-16/Udeshi_13_Ubiquitinations_SupplementaryTable2.zip')
# ZIP file containing Mayya et al. 2009 supplementary information TSV tables:
MAYYA09_FILE = os.path.join(DBS_FOLDER, 'PTM articles for PHOS UB AC in Jurkat 2015-03-16/Mayya_09_Phosphorilations_TableS1.zip')

# DataBase varaibles:
DB_URI = 'mysql://root@localhost/lymphosubac' #MySQL is faster than SQLite.
# URI for the local MySQL LymPHOS DataBase:
LYMPHOSDB_URI = 'mysql://root@localhost/lymphos_lymphos2allpepd'


#===============================================================================
# Class definitions
#===============================================================================


#===============================================================================
# Function definitions to Fix imported data
#===============================================================================
def fix_uniprot_mods_in_isoforms(prot_db, obofilefetcher=None, **kwargs):
    """
    Fix UniProt PTMs in isoforms deleting those related only to the cannonical
    protein. And re-locating PTMs positions to the isoform rigth positions.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`UniProtRepositoryFetcher` (previous to
    proteomics/filters.py code repository revision 364 of 2015-04-28).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param OBOGOTermFetcher obofilefetcher: an optinonal :class:`OBOGOTermFetcher`
    instance to get required GO Terms.
    """
    print('\nFixing UniProt protein modifications in isoforms:')
    with prot_db.session() as session:
        uprot_converter = filters.UniProtRepositoryFetcher(UNIPROT_FILE,
                                                           db_session=session,
                                                           obofilefetcher=obofilefetcher)
        commit_counter = 0
        total_fixed = 0
        for protein in session.query(dbm.Protein):
            ac = protein.ac
            if '-' not in ac: #Do not fix cannonical proteins:
                continue
            total_fixed += 1
            sys.stdout.write( 'Fixing UniProt PTMs for Protein: %s    %s    (%s)    \r'%(protein.ac, total_fixed, commit_counter) ) #DEBUG
            sys.stdout.flush()                                                                                                #
            # Fix only PTMs from UniProt repository:
            uprot_mods = set()
            other_mods = set()
            for mod in protein.modifications:
                if mod.source == uprot_converter.repository:
                    uprot_mods.add(mod)
                else:
                    other_mods.add(mod)
            prot_xml = uprot_converter._get_prot_xml(ac)
            cleaned_uprot_mods = uprot_converter._clean_isoform_ptms(ac,
                                                                     uprot_mods,
                                                                     prot_xml)
            protein._db_modifications = other_mods.union(cleaned_uprot_mods)
            # session.commit() from time to time:
            commit_counter += 1
            if commit_counter == 2000:
                session.commit()
                commit_counter = 0
#                 break #DEBUG: limit the database to a minimum for debugging
        session.commit()
    print('\nEnd of fixing UniProt protein modifications in isoforms.')
    return uprot_converter.logger.log


def fix_nterm_mspepmods2protmods(prot_db):
    """
    Fix Modifications (:class:`ProtModification`) for N-term MS Peptides
    (:class:`MSPeptide`) and their modifications (:class:`MSPepModification`).
    
    :caution: This function should be used only to repair a database created
    with an old version of :function:`_mspepmods2protmods` (previous to
    this file code repository revision 103 of 2015-06-17).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting fixin of Protein Modifications from N-term MS Peptides:')
    #
    formatseq4search = filters.pepprot_func.formatseq4search
    aa_mapping = filters.pepprot_func.AA2REGEX_PROTEOMICS.copy()
    aa_mapping['0'] = '[MX]{0,1}' #X-NOTE: For N-term processed Met or X (more than 6000 UniProt Human proteins start with X).
    with prot_db.session() as session:
        evidence = dbm.select_or_create(session, dbm.Evidence, type='IDA')[0] #Inferred from Direct Assay
        mspep_query = session.query(dbm.MSPeptide)\
                             .filter( dbm.MSPeptide.seq_w_mods.like("_[1]%") )\
                             .filter( dbm.MSPeptide.source_id.in_([4,5]) )\
                             .distinct()
        commit_counter = 0
        # Get MS modifications information, from related MSPeptides:
        for mspeptide in mspep_query:
            if not mspeptide.is_ntermonly(): continue #Work with N-terminal only peptides.
            sys.stdout.write( 'Processing MS data for %s    \r'%(mspeptide.seq_w_mods) )
            sys.stdout.flush()
            # Format the MS peptide sequence for searching:
            pep_seq = formatseq4search('0' + mspeptide.seq, #Allow the possibility that a N-term MS Peptide has the N-term Met processed
                                       aa_mapping,
                                       global_tmplt='^{0}') #Search only in the N-term of protein sequences.
            source = mspeptide.source #repository source
            for protein in mspeptide.proteins:
                if not protein.seq: continue #Only process proteins with sequences.
                # Search for the MS peptide in the protein sequence:
                match = re.match(pep_seq, protein.seq)
                if not match: #Bad association between MS Peptide and Protein:
                    print( 'Bad association between MS Peptide %s and Protein %s'%(mspeptide.seq_w_mods, protein.ac) ) #DEBUG
                    continue
                pos_shift = len( match.group() ) - len(mspeptide.seq) #Position shift depending if the peptide is found in te protein adding or not a N-term M or X.
                for msmod in mspeptide.modifications:
                    commit_counter += 1
                    pos = match.start() + pos_shift + msmod.position #protein position
                    modtype = msmod.mod_type #modification type
                    ascore = msmod.ascore
                    celllines = {exp.cell_line for exp in mspeptide.experiments}
                    # Put MS modification information into a new or existing
                    # :class:`dbm.ProtModification` instance:
                    modification, created = dbm.select_or_create(session, dbm.ProtModification,
                                                                 protein_ac=protein.ac,
                                                                 mod_type=modtype,
                                                                 position=pos,
                                                                 experimental=True,
                                                                 source=source)
                    if created: #Add extra data for newly created ProtModification:
                        modification.ascore = ascore
                        modification.cell_lines = celllines
                        modification.evidences = {evidence}
                        modification.mspepmodifications = {msmod}
                        session.add(modification)
                    else: #Modify existing ProtModification:
                        modification.ascore = max(ascore, modification.ascore)
                        modification.cell_lines.update(celllines)
                        modification.mspepmodifications.add(msmod)
                protein.mod_aas = len( protein.pos2modifications.keys() ) #Update number of modified aminoacids
            # session.commit() from time to time:
            if commit_counter == 5000:
                session.commit()
                commit_counter = 0
        session.commit()
    #
    print('\nEnd fixin of Protein Modifications from N-term MS Peptides.')

def _seq_w_mods2lymphos_seq(seq_w_mods):
    """
    AAAS[21]AA -> AAAsAA
    """
    lymphos_seq = ''
    seq_parts = seq_w_mods.split('[21]')
    for seq_part in seq_parts[:-1]:
        lymphos_seq += seq_part[:-1] + seq_part[-1].lower()
    lymphos_seq += seq_parts[-1]
    return lymphos_seq

def fill_prots_from_source4lymphos(prot_db, only_no_prot=False):
    """
    Fill :attr:`prots_from_source` in :class:`MSPeptide` instances imported
    from LymPHOS DB.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`LymPHOSFetcher` (previous to
    proteomics/filters.py file at repository revision 382 of 2015-06-19).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Bool only_no_prot: if True only processes :class:`MSPeptide`
    instances without associated proteins. If False processes all instances.
    Defaults to False.
    """
    print('\nStart filling prots_from_source in LymPHOS MS imported data:')
    #
    with prot_db.session() as session:
        ldb_converter = filters.LymPHOSFetcher(lymphosdb_uri=LYMPHOSDB_URI,
                                               db_session=session)
        mspeps = session.query(dbm.MSPeptide)\
                        .filter(dbm.MSPeptide.source == ldb_converter.repository)
        if only_no_prot: #Fill prots_from_source only in LymPHOS imported MS Peptides without associated proteins:
            mspeps = mspeps.filter( ~dbm.MSPeptide.proteins.any() )
        lymphos_seq2mspep = {_seq_w_mods2lymphos_seq(mspep.seq_w_mods): mspep
                             for mspep in mspeps}
        with ldb_converter.ldb_session() as ldb_sesion:
            peptidecache = ldb_converter.ldb_models.lymphos_peptidecache
            ldb_mspeps = ldb_sesion.query(peptidecache)
            if only_no_prot:
                ldb_mspeps = ldb_mspeps.filter( peptidecache.peptide.in_(lymphos_seq2mspep.keys()) )
            for ldb_mspep in ldb_mspeps:
                sys.stdout.write( 'Processing MS data for %s    \r'%(ldb_mspep.peptide) )
                sys.stdout.flush()
                prots_acs = sorted( ac_name.split('^')[0]
                                    for ac_name in ldb_mspep.dbproteins.split('|') )
                lymphos_seq2mspep[ldb_mspep.peptide].prots_from_source = prots_acs
        session.commit()
    #
    print('\nFinished filling prots_from_source in LymPHOS MS imported data.')


def _fill_prots_from_source4tables(prot_db, converter, get_prots_from_row,
                                   only_no_prot=False):
    """
    Fill :attr:`prots_from_source` in :class:`MSPeptide` instances imported
    from supplementary tables in articles.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`TSVMSDataFetcher` (previous to
    proteomics/filters.py file at repository revision 383 of 2015-06-22).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param TSVMSDataFetcher converter: a :subclass:`TSVMSDataFetcher` converter.
    :param function get_prots_from_row: a function to obtain protein ACcession
    numbers from a dict row.
    :param Bool only_no_prot: if True only processes :class:`MSPeptide`
    instances without associated proteins. If False processes all instances.
    Defaults to False.
    """
    with prot_db.session() as session:
        converter.db_session = session
        mspeps = session.query(dbm.MSPeptide)\
                        .filter(dbm.MSPeptide.source == converter.repository)
        if only_no_prot: #Fill prots_from_source only in MS Peptides without associated proteins:
            mspeps = mspeps.filter( ~dbm.MSPeptide.proteins.any() )
        seq_w_mods2mspep = {mspep.seq_w_mods: mspep for mspep in mspeps}
        for tsv_filen in converter.repo_file.namelist():
            # Process the file:
            with converter.repo_file.open(tsv_filen, 'rU') as io_file:
                csvdr = csv.DictReader(io_file, delimiter='\t')
                for row in csvdr:
                    pos2psmmods, seq = converter._get_pos2psmmods4row(row)
                    seq_w_mods = dbm.MSPeptide.seq2seq_w_mods(seq, pos2psmmods)
                    if seq_w_mods2mspep.has_key(seq_w_mods):
                        sys.stdout.write( 'Processing MS data for %s    \r'%(seq_w_mods) )
                        sys.stdout.flush()
                        mspep = seq_w_mods2mspep[seq_w_mods]
                        prots_from_source = get_prots_from_row(row)
                        if not mspep.prots_from_source:
                            mspep.prots_from_source = sorted(prots_from_source)
                        else:
                            mspep.prots_from_source = sorted( set(mspep.prots_from_source).union(prots_from_source) )
            session.commit()

def fill_prots_from_source4Choudhary09(prot_db, **kwargs):
    """
    Fill :attr:`prots_from_source` in :class:`MSPeptide` instances imported
    from Choudhary et al. 2009 supplementary information tables.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`Choudhary09AcFetcher` (previous to
    proteomics/filters.py file at repository revision 383 of 2015-06-22).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Bool only_no_prot: if True only processes :class:`MSPeptide`
    instances without associated proteins. If False processes all instances.
    Defaults to False.
    """
    print('\nStart filling prots_from_source in Choudhary09 MS imported data:')
    #
    converter = filters.Choudhary09AcFetcher(CHOUDHARY09_FILE)
    get_prots_from_row = lambda row: [ac for ac in row['Uniprot'].split(';')
                                      if ac.strip()]
    _fill_prots_from_source4tables(prot_db, converter, get_prots_from_row,
                                   **kwargs)
    #
    print('\nFinished filling prots_from_source in Choudhary09 MS imported data.')
    return converter.logger.log

def fill_prots_from_source4Udeshi13(prot_db, **kwargs):
    """
    Fill :attr:`prots_from_source` in :class:`MSPeptide` instances imported
    from Choudhary et al. 2009 supplementary information tables.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`Udeshi13UbFetcher` (previous to
    proteomics/filters.py file at repository revision 383 of 2015-06-22).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Bool only_no_prot: if True only processes :class:`MSPeptide`
    instances without associated proteins. If False processes all instances.
    Defaults to False.
    """
    print('\nStart filling prots_from_source in Udeshi13 MS imported data:')
    #
    converter = filters.Udeshi13UbFetcher(UDESHI13_FILE)
    get_prots_from_row = lambda row: [ac for ac in row['Proteins'].split(';')
                                      if ac.strip()]
    _fill_prots_from_source4tables(prot_db, converter, get_prots_from_row,
                                   **kwargs)
    #
    print('\nFinished filling prots_from_source in Udeshi13 MS imported data.')
    return converter.logger.log

def fill_prots_from_source4Mayya09(prot_db, **kwargs):
    """
    Fill :attr:`prots_from_source` in :class:`MSPeptide` instances imported
    from Mayya et al. 2009 supplementary information tables.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`Mayya09PhosFetcher` (previous to
    proteomics/filters.py file at repository revision 383 of 2015-06-22).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Bool only_no_prot: if True only processes :class:`MSPeptide`
    instances without associated proteins. If False processes all instances.
    Defaults to False.
    """
    print('\nStart filling prots_from_source in Mayya09 MS imported data:')
    #
    converter = filters.Mayya09PhosFetcher(MAYYA09_FILE)
    get_prots_from_row = lambda row: [ row['Uniprot ID'].upper() ] if row['Uniprot ID'].strip() else []
    _fill_prots_from_source4tables(prot_db, converter, get_prots_from_row,
                                   **kwargs)
    #
    print('\nFinished filling prots_from_source in Mayya09 MS imported data.')
    return converter.logger.log

def fill_prots_from_source4papers(prot_db, **kwargs):
    """
    Fill :attr:`prots_from_source` in :class:`MSPeptide` instances imported
    from supplementary information tables.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`TSVMSDataFetcher` sub-classes (previous to
    proteomics/filters.py file at repository revision 383 of 2015-06-22).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Bool only_no_prot: if True only processes :class:`MSPeptide`
    instances without associated proteins. If False processes all instances.
    Defaults to False.
    """
    fill_prots_from_source4Choudhary09(prot_db, **kwargs)
    fill_prots_from_source4Mayya09(prot_db, **kwargs)
    fill_prots_from_source4Udeshi13(prot_db, **kwargs)


def clear_decoys(prot_db):
    """
    Removes from database all :class:`MSPeptide` instances that point to any
    decoy in his :attr:`prots_from_source`. Also cleans associated data.
    
    :caution: This function should be used with caution, and only to repair a
    database created with an old version of :class:`LymPHOSFetcher` (previous
    to proteomics/filters.py file at repository revision 382 of 2015-06-19).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    with prot_db.session_no_autoflush() as session:
        prots_to_update = set()
        decoys = session.query(dbm.MSPeptide).filter( text('prots_from_source LIKE "%decoy%"') )
        for mspep in decoys:
            prots_to_update.update(mspep.proteins) #Remember affected proteins
            # Delete associated :class:`MSPepModification` instances after
            # cleaning their associated :class:`ProtModification` instances:
            for msmod in reversed(list(mspep.modifications)):
                for protmod in reversed(list(msmod.protmodifications)):
                    protmod.mspepmodifications.remove(msmod)
                    if not protmod.mspepmodifications:
                        session.delete(protmod)
                session.delete(msmod)
            # Clears link between decoy and Proteins in the database:
            mspep.proteins = set()
            # Delete associated :class:`PSM` instances and their associated
            # :class:`Spectrum` instances:
            for psm in reversed(list(mspep.psms)):
                session.delete(psm.spectrum)
                session.delete(psm)
            # Finally delete the decoy MSPeptide:
            session.delete(mspep)
        session.commit()
        # Update cached modification information for affected proteins:
        for protein in prots_to_update:
            protein.mod_aas = len( protein.pos2modifications.keys() )
        session.commit()


def fix_lymphos_msdata_scores(prot_db):
    """
    Fix LymPHOS2 MS data bad scores in :class:`PSM` instances due to a bug in
    :class:`LymPHOSFetcher` when importing float data using SQLAlchemy
    reflection mechanism (see http://stackoverflow.com/q/19387882).
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`LymPHOSFetcher` in ``proteomics/filters.py``
    file (previous to proteomics/filters.py file code repository revision 386
    of 2015-07-01).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nFixing LymPHOS2 MS data scores in database:')
    #
    with prot_db.session() as session:
        ldb_converter = filters.LymPHOSFetcher(lymphosdb_uri=LYMPHOSDB_URI,
                                               db_session=session)
        # Get only LymPOSUBAC MS Peptides imported from LymPHOS2:
        mspeps = session.query(dbm.MSPeptide)\
                        .options( joinedload('psms')\
                                  .joinedload('spectrum') )\
                        .filter(dbm.MSPeptide.source == ldb_converter.repository)
        # Classify LymPHOSUBAC PSMs according to unique values in LymPHOS2 Data
        # records:
        lymphos_data2psm = dict()
        for mspep in mspeps:
            lymphos_seq = _seq_w_mods2lymphos_seq(mspep.seq_w_mods) #AAS[21]AA -> AAsAA (LymPHOS Data record `peptide` field)
            for psm in mspep.psms:
                spectrum = psm.spectrum
                # Regenerate a unique ID for matching the original LymPHOS2 DATA
                # record of the PSM:
                gid = (lymphos_seq, spectrum.raw_file, spectrum.scan_i,
                       spectrum.scan_f)
                lymphos_data2psm[gid] = psm
        with ldb_converter.ldb_session() as ldb_session:
            commit_counter = 0
            # Get all Data records from LymPHOS2:
            data = ldb_converter.ldb_models.lymphos_data
            for ldb_data in ldb_session.query(data):
                # Generate a unique ID for the LymPHOS2 DATA record:
                gid = (ldb_data.peptide, ldb_data.raw_file, ldb_data.scan_i,
                       ldb_data.scan_f)
                # Get a matching LymPHOSUBAC PSM, if available (for ex. decoy LymPHOS2 Data records were deleted from LymPHOSUBAC):
                psm = lymphos_data2psm.get(gid, None)
                if psm:
                    sys.stdout.write( '%s Fixing PSM: %s        \r'%(commit_counter, gid) )
                    sys.stdout.flush()
                    # Renew search engines scores:
                    psm.scores = {'sequest_xcorr': float(ldb_data.xcorr),
                                  'sequest_deltacn': float(ldb_data.deltacn),
                                  'sequest_d': float(ldb_data.d),
                                  'omssa_pvalue': float(ldb_data.omssa),
                                  'phenyx_zscore': float(ldb_data.phenyx),
                                  'easyprot_zscore': float(ldb_data.easyprot),
                                  'peaks_logp': float(ldb_data.peaks),
                                  }
                    commit_counter += 1
                # session.commit() from time to time:
                if commit_counter == 5000:
                    session.commit()
                    commit_counter = 0
            session.commit()
    #
    print('\nEnded fixing LymPHOS2 MS data scores in database.')
    return ldb_converter.logger.log


def add_cache_attr2spectra(prot_db):
    """
    Add the rigth :attr:`has_mzs_intensities` value to :class:`Spectrum`
    instances.
    
    :caution: This function should be used only to update a database created
    with an old version of :class:`Spectrum` in ``proteomics/db_models.py``
    file (previous to proteomics/db_models.py file code repository revision 387
    of 2015-07-06) and an old version of file ``proteomics/filters.py``
    (previous to proteomics/filters.py file code repository revision 388 of
    2015-07-07).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nModifying Spectra cache attribute `has_mzs_intensities` in database:')
    #
    with prot_db.session() as session:
        commit_counter = 0
        spectra = session.query(dbm.Spectrum).filter_by(has_mzs_intensities=None)
        for spectrum in spectra:
            sys.stdout.write('Modifying spectrum %s         \r'%spectrum.id)
            sys.stdout.flush()
            # Set :attr:`has_mzs_intensities` according to the presence (True)
            # or absence (False) of mz information:
            if spectrum.masses:
                spectrum.has_mzs_intensities = True
            else:
                spectrum.has_mzs_intensities = False
            commit_counter += 1
            # session.commit() from time to time:
            if commit_counter == 2000:
                session.commit()
                commit_counter = 0
        session.commit()
    #
    print('\nEnded modifying Spectra cache attribute `has_mzs_intensities` in database.')


def upgrade_lymphos_mspep_regulation(prot_db):
    """
    Upgrade LymPHOS2 MS Peptides :attr:``regulation`` in :class:`MSPeptide` 
    instances, due to a change in :class:`LymPHOSFetcher` to take into account 
    all the statistics used in LymPHOS2 (not only t test 95%).
    
    :caution: This function should be used only to upgrade a database created
    with an old version of :class:`LymPHOSFetcher` in ``proteomics/filters.py``
    file (previous to proteomics/filters.py file code repository revision 474
    of 2016-04-27).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nUpgrading LymPHOS MS Peptides regulation in database:')
    #
    with prot_db.session() as session:
        ldb_converter = filters.LymPHOSFetcher(lymphosdb_uri=LYMPHOSDB_URI,
                                               db_session=session)
        # Get only LymPOSUBAC MS Peptides imported from LymPHOS2:
        mspeps = session.query(dbm.MSPeptide)\
                        .filter(dbm.MSPeptide.source == ldb_converter.repository)
        with ldb_converter.ldb_session() as ldb_session:
            commit_counter = 0
            for mspep in mspeps:
                lymphos_seq = _seq_w_mods2lymphos_seq(mspep.seq_w_mods) #AAS[21]AA -> AAsAA (LymPHOS Data record `peptide` field)
                # Get LymPHOS2 peptidecache peptide corresponding to LymPOSUBAC 
                # MS Peptide:
                peptidecache = ldb_converter.ldb_models.lymphos_peptidecache
                ldb_pep = ldb_session.query(peptidecache)\
                                     .filter(peptidecache.peptide==lymphos_seq)\
                                     .one()
                if not ldb_pep.quantitative: #Only process quantitative MS Peptides:
                    continue
                sys.stdout.write( '(%s) Upgrading MSPeptide: %s        \r'%(commit_counter, mspep.seq_w_mods) )
                sys.stdout.flush()
                # Get related LymPHOS2 supra-experiments:
                data = ldb_converter.ldb_models.lymphos_data
                ldb_expgroups = ldb_session.query(ldb_converter.ldb_models.lymphos_expgroup)\
                                           .join(ldb_converter.ldb_models.lymphos_experiments)\
                                           .join(data)\
                                           .filter(data.peptide==ldb_pep.peptide)\
                                           .distinct().all()
                # Update MS Peptide quantitative information:
                mspep.regulation = ldb_converter._get_mspepregulation4ldbpep(ldb_pep, ldb_expgroups, ldb_session)
                commit_counter += 1
                # session.commit() from time to time:
                if commit_counter == 5000:
                    session.commit()
                    commit_counter = 0
        session.commit()
    #
    print('\nFinished upgrading LymPHOS MS Peptides regulation in database.')
    return ldb_converter.logger.log


def add_reviewed_attr2uniprot_prots(prot_db):
    """
    Add a value for :attr:`reviewed` in :class:`Protein` instances imported
    from UniProt.
    
    :caution: This function should be used only to repair a database created
    with an old version of :class:`UniProtRepositoryFetcher` (previous to
    proteomics/filters.py code repository revision 481 of 2015-05-10).
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print("\nAdding 'reviewed' attribute to UniProt proteins:")
    #
    with prot_db.session() as session:
        uprot_converter = filters.UniProtRepositoryFetcher(UNIPROT_FILE,
                                                           db_session=session,
                                                           obofilefetcher=None)
        commit_counter = 0
        total_fixed = 0
        for protein in session.query(dbm.Protein)\
                              .filter(dbm.Protein.source_repository==uprot_converter.repository):
            ac = protein.ac
            total_fixed += 1
            sys.stdout.write( "Adding 'reviewed' attribute to UniProt Protein: %s  %s (%s)         \r"%(protein.ac, total_fixed, commit_counter) ) #DEBUG
            sys.stdout.flush()                                                                                                                     #
            # Add a value for :attr:`reviewed` (False for TrEMBL or True for Swiss-Prot dataset origin):
            protein.reviewed = uprot_converter.get_dataset(ac) == 'Swiss-Prot'
            # session.commit() from time to time:
            commit_counter += 1
            if commit_counter == 2000:
                session.commit()
                commit_counter = 0
#                 break #DEBUG: limit the database to a minimum for debugging
        session.commit()
    #
    print("\nFinished adding 'reviewed' attribute to UniProt proteins.")
    return uprot_converter.logger.log


def clear_msdata4source(prot_db, source_id): #IMPROVE performance!
    """
    Removes from database all :class:`MSPeptide` instances belonging to a given
    Source. Also cleans associated data.
    
    :CAUTION: This function Permanently Deletes data and thus should be used 
    with extreme caution.
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param int source_id: the ID of the target :class:`Source` instance.
    """
    print("\nDeleting MS Data from Source %s:"%source_id)
    #
    with prot_db.session_no_autoflush() as session:
        prots_to_update = set()
        experiments_to_delete = set()
        source_mspeps = session.query(dbm.MSPeptide)\
                               .filter(dbm.MSPeptide.source_id==source_id)
        commit_counter = 0
        for mspep_deleted, mspep in enumerate(source_mspeps, start=1):
            # Delete associated :class:`MSPepModification` instances after
            # cleaning their associated :class:`ProtModification` instances:
            for msmod in list(mspep.modifications):
                for protmod in list(msmod.protmodifications):
                    protmod.mspepmodifications.remove(msmod)
                    if not protmod.mspepmodifications: #No MSPepModifications supporting this ProtModification:
                        prots_to_update.add(protmod.protein) #Remember affected Proteins.
                        #Clears remaining links between ProtModification and other models:
                        protmod.cell_lines = set()
                        protmod.evidences = set()
                        protmod.digestion_peptides = set()
                        #Delete this ProtModification:
                        session.delete(protmod)
                #Clears remaining links between MSPepModification and other models:
                msmod.evidences = set()
                #Delete this MSPepModification:
                session.delete(msmod)
            # Clears link between MSPeptide and other models:
            mspep.proteins = set()
            experiments_to_delete.update(mspep.experiments) #Remember affected Experiments.
            mspep.experiments = set()
            # Delete associated :class:`PSM` instances and their associated
            # :class:`Spectrum` instances:
            for psm in list(mspep.psms):
                session.delete(psm.spectrum)
                session.delete(psm)
            # Finally delete the MSPeptide:
            session.delete(mspep)
            commit_counter += 1
            sys.stdout.write("%s (%s) MS Peptides and associated data deleted       \r"%(mspep_deleted, commit_counter))
            sys.stdout.flush()
            # session.commit() from time to time:
            if commit_counter == 2000:
                session.commit()
                commit_counter = 0                    
        session.commit()
        # Delete affected experiments:
        sys.stdout.write("Deletting %s associated expetiments ...      \r"%len(experiments_to_delete))
        sys.stdout.flush()
        for experiment in experiments_to_delete:
            session.delete(experiment)
        session.commit()
        # Update cached modification information for affected Proteins:
        for protein in prots_to_update:
            sys.stdout.write("Updating affected protein %s ...      \r"%protein.ac)
            sys.stdout.flush()
            protein.mod_aas = len( protein.pos2modifications.keys() )
        session.commit()
    #
    print("\nFinished deleting MS Data from Source %s."%source_id)


def fix_PTM_significances(prot_db):
    """
    Add a value for :attr:`significant` in all Modification instances.
    
    :caution: This function should be used only to update a database created
    with an old version of :module:`filters` (previous to
    proteomics/filters.py code repository revision 514 of 2016-07-26).
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print("\nAdding 'significant' attribute to different PTMs:")
    #
    with prot_db.session() as session:
        # Create dictionary of ascore thresholds:
        sources = session.query(dbm.Source).all()
        ptm_ids = [ x for x, in session.query(dbm.ModificationType.id).all() ]
        srcid_ptmid2threshold = dbm.generate_srcid_ptmid2threshold(sources, ptm_ids)
        # Fix significances on different kinds of PTMs (different PTM class models):
        commit_counter = 0
        total_fixed = 0
        for PTM_cls, srcid_attr in ( (dbm.PSMModification, 'psm.mspeptide.source_id'), 
                                     (dbm.MSPepModification, 'mspeptide.source_id'), 
                                     (dbm.ProtModification, 'source_id') ):
            get_source_id = attrgetter(srcid_attr) #:class:`attrgetter` instance to get the Source ID from objects of the corresponding PTM class model.
            for ptm in session.query(PTM_cls).filter(PTM_cls.ascore != None): #Only process PTM objects with an 'ascore' value:
                ascore_threshold = srcid_ptmid2threshold[get_source_id(ptm), 
                                                         ptm.mod_type_id]
                if ascore_threshold is not None:
                    ptm.significant = (ptm.ascore >= ascore_threshold)
                    total_fixed += 1
                    commit_counter += 1
                    sys.stdout.write("Significance added to %s (%s) PTMs       \r"%(total_fixed, commit_counter))
                    sys.stdout.flush()
                    # session.commit() from time to time:
                    if commit_counter == 3500:
                        session.commit()
                        commit_counter = 0                    
        session.commit()
    #
    print("\nFinished adding 'significant' attribute to different PTMs.")


def fix_ProtPTM_significances(prot_db):
    """
    Fix ``None`` values for :attr:`significant` in ProtModification instances,
    if needed.
    
    :caution: This function should be used only to update a database created
    with an old version of :module:`db_models` (previous to
    proteomics/db_models.py code repository revision 583 of 2017-03-04).
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print("\nFixing 'significant' attribute in different Protein PTMs:")
    #
    with prot_db.session() as session:
        # Create dictionary of ascore thresholds:
        sources = session.query(dbm.Source).filter(dbm.Source.id > 1) #All sources except UniProt
        ptm_ids = [ x for x, in session.query(dbm.ModificationType.id).all() ] #All Modification IDs
        srcid_ptmid2threshold = dbm.generate_srcid_ptmid2threshold(sources, ptm_ids)
        # Fix significances in ProtModification instances:
        commit_counter = 0
        total_fixed = 0
        for ptm in session.query(dbm.ProtModification).filter(dbm.ProtModification.ascore != None): #Only process ProtModification objects with an 'ascore' value:
            ascore_threshold = srcid_ptmid2threshold[ptm.source_id, ptm.mod_type_id]
            if ascore_threshold is not None:
                ptm.significant = (ptm.ascore >= ascore_threshold)
                total_fixed += 1
                commit_counter += 1
                sys.stdout.write("Significance added to %s (%s) PTMs       \r"%(total_fixed, commit_counter))
                sys.stdout.flush()
                # session.commit() from time to time:
                if commit_counter == 3500:
                    session.commit()
                    commit_counter = 0                    
        session.commit()
    #
    print("\nFinished fixing 'significant' attribute in different Protein PTMs.")


def fix_uniprot_PTMs(prot_db):
    """
    Add Evidences to :attr:`evidences` and a value for :attr:`significant` in 
    ALL UniProt Protein Modification instances.
    
    :caution: This function should be used only to update a database created
    with an old version of :class:`UniProtRepositoryFetcher` (previous to
    proteomics/filters.py code repository revision 518 of 2016-07-29).
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print("\nAdding 'evidences' and 'significant' attributes to UniProt PTMs:")
    #
    with prot_db.session() as session:
        # Create an :class:`UniProtRepositoryFetcher` instance to get fixed data:
        up_converter = filters.UniProtRepositoryFetcher(UNIPROT_FILE, 
                                                        db_session=session, 
                                                        caching=True) #X_NOTE: caching needed for big speed ups, because a lot of cache hits will be produced by Evidences.
        # Prepare variables and functions for fast access:
        up_src_id = up_converter.repository.id
        sort_key4db = attrgetter('position', 'mod_type_id') #X_NOTE: Field :attr:`mod_type_id` will avoid hitting again the database.
        sort_key4tmp = attrgetter('position', 'mod_type.id')
        # Fix significances on UniProt PTMs:
        commit_counter = 0
        total_fixed = 0
        prots_w_uniprot_mods = session.query(dbm.Protein)\
                                      .join(dbm.Protein._db_modifications)\
                                      .filter(dbm.ProtModification.source_id==up_src_id)\
                                      .distinct()
        for prot in prots_w_uniprot_mods:
            # Get old unfixed Database stored Protein Modifications from
            # UniProt for the current Protein:
            db_uprotmods = [prot_mod for prot_mod in prot.modifications
                            if prot_mod.source_id == up_src_id]
            db_uprotmods = sorted(db_uprotmods, key=sort_key4db)
            db_uprotmods_updated = False
            # Get new fixed and cleaned temporal Protein Modifications from
            # :class:`UniProtRepositoryFetcher` instance for the current
            # Protein AC:
            uprot_xml = up_converter._get_prot_xml(prot.ac)
            tmp_uprotmods = up_converter._iter_ptms4protxml(uprot_xml, 
                                                            prot.reviewed)
            tmp_uprotmods = up_converter._clean_isoform_ptms(prot.ac, #Clean/Re-arrange the PTMs according to this isoform definition in the XML.
                                                             tmp_uprotmods, 
                                                             uprot_xml)
            tmp_uprotmods = sorted(tmp_uprotmods, key=sort_key4tmp)
#             assert len(db_uprotmods) == len(tmp_uprotmods) # Early Safety check.
            for db_uprotmod, tmp_uprotmod in zip(db_uprotmods, tmp_uprotmods):
                # Safety checks:
                assert (db_uprotmod.position == tmp_uprotmod.position and
                        db_uprotmod.mod_type_id == tmp_uprotmod.mod_type.id)
                # Update/Fix Database UniProt Protein Modifications:
                if not tmp_uprotmod.evidences and not tmp_uprotmod.significant:
                    continue # Skip unchanged Protein Modifications.
                db_uprotmod.evidences = tmp_uprotmod.evidences
                db_uprotmod.significant = tmp_uprotmod.significant
                tmp_uprotmod.evidences = set() #Break Evidences association with temporal Protein Modification.
                session.delete(tmp_uprotmod) #Avoid temporal Protein Modification to enter the Database (X_NOTE: it was automatically added to the session because their Evidences were added to the session).
                db_uprotmods_updated = True
                total_fixed += 1
                commit_counter += 1
                sys.stdout.write( "UniProt PTMs fixed: %s (%s) (%s)        \r"%(total_fixed, prot.ac, commit_counter) )
                sys.stdout.flush()
            # Update number of Significant modified aminoacids in the Protein:
            if prot.reviewed and db_uprotmods_updated:
                prot.mod_aas = len( {prot_mod.position 
                                     for prot_mod in prot.modifications 
                                     if prot_mod.significant} )
            # session.commit() from time to time:
            if commit_counter >= 3500:
                session.commit()
                commit_counter = 0                    
        session.commit()
    #
    print("\nFinished adding 'evidences' and 'significant' attributes to UniProt PTMs.")


def fill_mod_aas4mspeptides(prot_db):
    """
    Fill :attr:`_db_mod_aas` in :class:`MSPeptide` instances.
    
    :caution: This function should be used only to "repair" a database created
    with an old version of proteomics/filters.py (previous to the file at 
    repository revision 609 of 2017-07-10).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print("\nAdding 'mod_aas' attribute to MS Peptides:")
    #
    with prot_db.session() as session:
        # Fill :attr:`_db_mod_aas` on MS Peptides:
        commit_counter = 0
        total_fixed = 0
        mspeptides = session.query(dbm.MSPeptide)\
                            .options( joinedload('_db_modifications') )
        for mspep in mspeptides:
            total_fixed += 1
            mspep.refresh_mod_aas(only_significant=True)
            commit_counter += 1
            # session.commit() from time to time:
            if commit_counter >= 10000:
                print("%d MS Peptides modified..."%total_fixed)
                session.commit()
                commit_counter = 0                    
        session.commit()
    #
    print("\nFinished adding 'mod_aas' attribute to MS Peptides.")
    print("Total MS Peptides modified: %d"%total_fixed)


def fix_seq_w_mods4mspeps(prot_db):
    """
    Fix format of :attr:`seq_w_mods` in :class:`MSPeptide` instances, replacing
    square brackets with brackets.
    Ex.: from 'AAAS[21]K[1, 121]AA' to 'AAAS(21)K(1, 121)AA'
    
    :caution: This function should be used only to update a database created
    with old versions of :module:`db_models` and :module:`filters` (previous to
    code repository revision 612 of 2017-07-25).
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print("\nFixing 'seq_w_mods' attribute in MS Peptides:")
    #
    with prot_db.session() as session:
        # Fix :attr:`seq_w_mods` in MSPeptide instances:
        commit_counter = 0
        total_fixed = 0
        #Only process MS Peptides with significant modifications:
        mspeptides = session.query(dbm.MSPeptide)\
                            .options( joinedload('_db_modifications')\
                                      .joinedload('mod_type') )\
                            .filter( dbm.MSPeptide.seq_w_mods.like("%[%") )
        for mspep in mspeptides:
            mspep.refresh_seq_w_mods()
            total_fixed += 1
            commit_counter += 1
            sys.stdout.write("%s fixed %s (%s)        \r"%(mspep.seq_w_mods, total_fixed, commit_counter))
            sys.stdout.flush()
            # session.commit() from time to time:
            if commit_counter == 10000:
                session.commit()
                commit_counter = 0                    
        session.commit()
    #
    print("\nFinished fixing 'seq_w_mods' attribute in MS Peptides.")


def disable_small_mspeps(prot_db, min_length=6):
    """
    Mark all PTMs derived from small MS Peptides (shorter than `min_length`) as 
    not significant (:attr:`significant` = False) so they do not count for any 
    significant operation/visualization of the Database. Update :attr:`mod_aas` 
    of Peptide-like objects accordingly.
    X-NOTE: Actually there are only 2 MS Peptides smaller than 6 AAs: DY(21)VR 
    (from Watts) and RDS(21)ER (from LymPHOS2).
    
    :caution: This function should be used only to update a database created
    before 2018-04-17 (with old versions of :module:`filters`; previous to
    code repository revision ??? of 2018-04-??).
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param int min_length: the minimum length a MS Peptide should have (shorter
    MS Peptides will have their PTM significances set to False).
    """
    print( "\nMarking as not significant all PTMs from MS Peptides shorter "
           "than {} AAs:".format(min_length) )
    #
    with prot_db.session() as session:
        commit_counter = 0
        total_disabled = 0
        # Query to get Only the MS Peptides with significant modifications...:  
        ors = [ dbm.MSPeptide.seq.like( "_" * length ) 
                for length in range(1, min_length) ]
        
        mspeptides = session.query(dbm.MSPeptide)\
                            .options( joinedload('_db_modifications')\
                                      .joinedload('protmodifications')\
                                       .joinedload('mspepmodifications') )\
                            .filter(dbm.MSPeptide._db_mod_aas>0)
        # ... And shorter than :param:`min_length` using a filter with multiple
        # 'OR LIKE' SQL stamens (one for each sequence length smaller than
        # `min_length`):
        mspeptides = mspeptides.filter( or_(*ors) )
        # Process MS Peptide PTMs:
        prots_to_refresh = set()
        for mspep in mspeptides:
            sys.stdout.write("Disabling %s (%s)   (%s)        \r"%(mspep.seq_w_mods, total_disabled, commit_counter))
            sys.stdout.flush()
            for mspepmod in mspep.modifications:
                if not mspepmod.significant:
                    continue #Skip this MS Peptide Modification.
                mspepmod.significant = False
                commit_counter += 1
                # -Process also derived Protein Modifications:
                for protmod in mspepmod.protmodifications:
                    if not protmod.significant:
                        continue #Skip this Protein Modification.
                    if len(protmod.mspepmodifications) == 1:
                        # -Protein Modification only derives from current MS Peptide Modification:
                        protmod.significant = False
                        prots_to_refresh.add(protmod.protein)
                    else:
                        # -Protein Modification also derives from other MS Peptide Modifications:
                        protmod.significant = max(protmodmspepmod.significant 
                                                  for protmodmspepmod 
                                                  in protmod.mspepmodifications)
                        if not protmod.significant: #Significance has changed to False:
                            prots_to_refresh.add(protmod.protein)
                    commit_counter += 1
            # Update MS Peptide after their PTMs have been set as not significant:
            mspep._db_seq_w_mods = mspep.seq
            mspep._db_mod_aas = 0
            mspep._db_allphysiomods_signif = False
            total_disabled += 1
            commit_counter += 1
            # session.commit() from time to time:
            if commit_counter == 10000:
                session.commit()
                commit_counter = 0                    
        # Update :attr:`mod_aas` of all Proteins with PTMs disabled:
        for protein in prots_to_refresh:
            old_modaas = protein.mod_aas
            protein.mod_aas = new_modaas = protein.get_mod_aas()
            commit_counter += 1
            sys.stdout.write("Updating %s (mod AAs: %s -> %s)   (%s)        \r"%(protein.ac, old_modaas, new_modaas, commit_counter)) #DEBUG
            sys.stdout.flush()
            # session.commit() from time to time:
            if commit_counter == 10000:
                session.commit()
                commit_counter = 0                    
        session.commit()
    #
    print( "\nFinished marking as not significant all PTMs from MS Peptides "
           "shorter than {} AAs:".format(min_length) )



def main(db_uri, *args, **kwargs):
    args = list(args)
    # Create the database connection:
    prot_db = dbm.DataBase(db_uri)
    #
    for arg in args:
        CLI_ARGUMENTS[arg](prot_db, **kwargs)
    #
    return 0



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    # DEBUG: Warnings as exceptions:
    import warnings
    warnings.filterwarnings("error")

    # CLI minimal argument parsing (X-NOTE: the OrderedDict is only for the help output):
    CLI_ARGUMENTS = OrderedDict(( 
        # Options to repair/bug-fix/upgrade the database:
        ('fix_uniprot_ptms', fix_uniprot_mods_in_isoforms),
        ('fix_nterm_mspep', fix_nterm_mspepmods2protmods),
        ('fill_prots_from_source4lymphos', fill_prots_from_source4lymphos),
        ('fill_prots_from_source4papers', fill_prots_from_source4papers),
        ('clear_decoys', clear_decoys),
        ('fix_lymphos_scores', fix_lymphos_msdata_scores),
        ('add_cache_attr2spectra', add_cache_attr2spectra),
        ('upgrade_lymphos_regulation', upgrade_lymphos_mspep_regulation), 
        ('add_reviewed_attr2uniprot_prots', add_reviewed_attr2uniprot_prots),
        ('fix_ptm_significances', fix_PTM_significances),
        ('fix_protein_ptm_significances', fix_ProtPTM_significances),
        ('fix_uniprot_ptms', fix_uniprot_PTMs),
        ('fill_mspeptides_mod_aas', fill_mod_aas4mspeptides),
        ('fix_seq_w_mods', fix_seq_w_mods4mspeps),
                                  ))
    #
    cli_argv = sys.argv[1:]
    allowed_cli_argv = CLI_ARGUMENTS.keys()
    if cli_argv and ':/' in cli_argv[0]: #First argument is a DB URI
        DB_URI = cli_argv[0]
        cli_argv = cli_argv[1:]
    if not cli_argv: # Used for DEBUG/TEST:
        cli_argv = ['fix_seq_w_mods']
    if ( not cli_argv or len(cli_argv) > len(allowed_cli_argv) or
         set(cli_argv).difference(allowed_cli_argv) ): # No arguments or other arguments but incorrect ones
        sys.stderr.write('\nSyntax Error!\n\n')
        print('\nUsage:')
        print( ' {0} [DataBase URI] {1}'.format( sys.argv[0],
                                                ' '.join('['+argv+']'
                                                         for argv in
                                                         allowed_cli_argv) ) )
        print('\nExamples:')
        print( ' {0}'.format(sys.argv[0]) )
        print( ' {0} fix_uniprot_ptms'.format(sys.argv[0]) )
        print( ' {0} {1}'.format(sys.argv[0], DB_URI) )
        sys.exit(2)
    #
    exit_code = main(DB_URI, *cli_argv)
    #
    sys.exit(exit_code)
