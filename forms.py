# -*- coding: utf-8 -*-

from __future__ import print_function

"""
:synopsis:   Form classes for LymPHOS_UB_AC (TCellXTalk) project.

:created:    2015-04-08

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.3.1 dev'
__UPDATED__ = '2018-08-30'

#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django import forms
from django.core import validators, urlresolvers
from django.conf import settings

# Application imports:

# Python core imports:
import re

#==============================================================================
# Form classes
#==============================================================================
class FormHelpTextInTittle(forms.Form):
    """
    Subclass of `forms.Form` to use the `help_text` of each form field as the
    'title' attribute of the field's widget.
    """
    def __init__(self, *args, **kwargs):
        # Overwrites super-class :method:``BaseForm.__init__``.
        super(FormHelpTextInTittle, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['title'] = field.help_text
            field.help_text = None


class ProteinSearchForm(FormHelpTextInTittle):
    error_css_class = 'form_error'

    template_include = 'protein_search_form.html' #Mini-template to use as an included template for rendering this form inside a web-page.
    post_url = urlresolvers.reverse_lazy('protein_results') #Default URL to post this form's data. To be used as the `action` field of a html `form` tag.

    protein_text = forms.CharField(label='Text',
                                   help_text='Enter the UniProt ACcession Number, the UniProt ID or part of the UniProt Description',
                                   max_length=25,
                                   validators=[ validators.RegexValidator(r"""^[ /\w\.\-\*\?\)\(]+$""") ], 
                                   required=False)
    multiprotein_acs = forms.CharField(label='Multiple UniProt ACs',
                                       help_text='Enter the UniProt ACcession Numbers separated by spaces, commas or semi-colons',
                                       widget=forms.Textarea,
                                       max_length=10240, #Allow 1 MiB of data
                                       validators=[ validators.RegexValidator(r"""^[ /\w\-\,\;\n\r]+$""") ], 
                                       required=False)
    
    search_field_filled = None #Indicates the form search field filled by the user: 'protein_text' or 'multiprotein_acs'

    sort_tags = forms.CharField(required=False)

#     _search_validator = r"""[ /\w\.\-\*\?\)\(]"""
#
#     def clean_protein_text(self):
#         protein_text = self.cleaned_data['protein_text']
#         errors = ', '.join( set(re.sub( self._search_validator, '', protein_text )) )
#         if errors:
#             error_msg = 'You searched for \' {0} \' , but \' {1} \' is/are not '\
#                         'allowed in this search.'.format(prteoin_text, errors)
#             raise forms.ValidationError(error_msg)
#         return protein_text
    
    def clean_multiprotein_acs(self):
        """
        Convert field :attr:`multiprotein_acs` from `str` to `set` of Protein ACs.
        """
        multiprotein_acs = self.cleaned_data['multiprotein_acs'] #Get :attr:`multiprotein_acs` as an already validated Python Unicode string
        multiprotein_acs = set( re.split("[, ;\n\r]+", multiprotein_acs) ) #Split by allowed separators and convert to a set
        multiprotein_acs.discard('') #Discard possible empty string due to a separator at the end, multiple followed separators, ...
        return multiprotein_acs
    
    def clean(self):
        """
        Check whether one of both form input fields contains user data.
        """
        # Overwrites super-class :method:``BaseForm.clean``.
        cleaned_data = super(ProteinSearchForm, self).clean()
        # Get validated user data. Uses :method:`get` to deal with cases where
        # the field has been filled but is not present in the cleaned_data dict
        # due to validation errors:
        protein_text = cleaned_data.get('protein_text', True)
        multiprotein_acs = cleaned_data.get('multiprotein_acs', True)
        # Check whether both input fields are void:
        if (not self.errors and not protein_text and not multiprotein_acs): #X-NOTE: If there is some validation error (:attr:``errors``) it's likely because any of the two fields has been entered but is not valid
            raise forms.ValidationError("At least One field of '{}' or '{}' "
                                        "must be entered to search for..."
                                        "".format(self['protein_text'].label, 
                                                  self['multiprotein_acs'].label), 
                                        code='at_least_one')
        # Check whether both input fields contain user data:
        if (protein_text and  multiprotein_acs):
            raise forms.ValidationError("Only One field of '{}' or '{}' must "
                                        "be entered to search for..."
                                        "".format(self['protein_text'].label, 
                                                  self['multiprotein_acs'].label), 
                                        code='only_one')
        self.search_field_filled = 'protein_text' if protein_text else 'multiprotein_acs'
        return cleaned_data


class ProteinDigestCombineForm(FormHelpTextInTittle):
    error_css_class = 'form_error'

    template_include = 'protein_digest_combine_form.html' #Mini-template to use as an included template for rendering this form inside a web-page.
    post_url = urlresolvers.reverse_lazy('protein_digest') #Default URL to post this form's data. To be used as the `action` field of a html `form` tag.

    db = None #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do searches.

    protein_ac = forms.CharField(label='Protein ACcesion number',
                                 help_text='Protein ACcesion number',
                                 max_length=25,
                                 validators=[validators.RegexValidator(r"""^[ /\w\.\-\*\?\)\(]+$""")])
    protease = forms.ChoiceField(label='Protease',
                                 help_text='Select a digestion enzyme')
#     missing_cleavage_lvls = forms.IntegerField(label='Missed Cleavages',
#                                                help_text='Enter a number between 0 and 3. This indicate the Extra missed cleavages to generate during digestion',
#                                                min_value=0,
#                                                max_value=3,
#                                                initial=0)
    min_pep_length = forms.IntegerField(label='Min. peptide length',
                                        help_text='Enter a number between 5 and 60 to filter resulting peptides by Minimum length',
                                        min_value=5,
                                        max_value=60,
                                        initial=7)
    max_pep_length = forms.IntegerField(label='Max. peptide length',
                                        help_text='Enter a number between 5 and 65 to filter resulting peptides by Maximum length',
                                        min_value=5,
                                        max_value=65,
                                        initial=25)
#     max_missing_cleavages = forms.IntegerField(label='Max. Missed Cleavages',
#                                                help_text='Enter a number between 0 and 4 to filter resulting peptides by Maximum number of missed cleavages',
#                                                min_value=0,
#                                                max_value=4,
#                                                initial=3)
    exclusive_ptms = forms.MultipleChoiceField(label='PTMs required', 
                                               widget=forms.CheckboxSelectMultiple, 
                                               help_text='The inferred peptides must have all the PTMs selected (and only the PTMs selected). If no PTM is selected, the inferred peptides will have all possible PTM combinations that match the other parameters defined.', 
                                               required=False)
    min_total_ptms = forms.IntegerField(label='Min. PTMs/peptide',
                                        help_text='Enter a number between 1 and 4 to filter resulting peptides by Minimum number of total PTMs',
                                        min_value=1,
                                        max_value=4,
                                        initial=1)
    max_total_ptms = forms.IntegerField(label='Max. PTMs/peptide',
                                        help_text='Enter a number between 1 and 5 to filter resulting peptides by Maximum number of total PTMs',
                                        min_value=1,
                                        max_value=5,
                                        initial=3)
    max_equal_ptms = forms.IntegerField(label='Max. PTMs of the same type/peptide',
                                        help_text='Enter a number between 1 and 5 to restrict the Maximum number of each PTM type in resulting peptides',
                                        min_value=1,
                                        max_value=5,
                                        initial=2)

    def __init__(self, *args, **kwargs):
        """
        :caution: the database should contain at least one Protease row.
        """
        # Overwrites super-class :method:``FormHelpTextInTittle.__init__``.
        with self.db.session() as session:
            # Get the available Proteases from the database to create the dynamic
            # `protease` choice field:
            proteases = session.query(settings.DBM.Protease).order_by('id').all()
            self.base_fields['protease'].choices = [ (protease.id, protease.name) #A list of tuples [(value, label), ...]
                                                     for protease in proteases ]
            self.base_fields['protease'].initial = proteases[0].id #Set the first protease as the default (it must be ID=1 -> 'Trypsin with exceptions')
            self.protease2omitpmts = {protease.id: protease.digpep_omited_ptms #Allows avoiding GluC with Ubiquitination later (see :method:`clean` below)
                                      for protease in proteases}
            # Get the available Physiological PTMs from the database to create
            # the dynamic `exclusive_ptms` multiple-choice checkboxes field:
            ModificationType = settings.DBM.ModificationType
            mod_types = session.query(ModificationType.id, ModificationType.extradata)\
                               .filter(ModificationType.only_physio==True)
            self.base_fields['exclusive_ptms'].choices = sorted( ( idx, extradata['abbreviation'].capitalize() ) 
                                                                 for idx, extradata in mod_types )
#             self.base_fields['exclusive_ptms'].initial = [mod_type.id for mod_type in mod_types] #set ALL PTMs as the default
        #
        super(ProteinDigestCombineForm, self).__init__(*args, **kwargs)
        #
        # Set the POST URL to /protein_digest/Protein_AC :
        prot_ac = self['protein_ac'].value()
        if prot_ac:
            self.post_url = urlresolvers.reverse_lazy( 'protein_digest',
                                                       args=(prot_ac,) )
    
    def clean(self):
        """
        Check whether values of related fields are OK (cross-validations 
        between fields).
        """
        # Overwrites super-class :method:``BaseForm.clean``.
        cleaned_data = super(ProteinDigestCombineForm, self).clean()
        # Get validated user data. Uses :method:`get` to deal with cases where
        # the field has been filled but is not present in the cleaned_data dict
        # due to validation errors:
#         # Check that `max_missing_cleavages` field is greater than or equal to `missing_cleavage_lvls`:
#         missing_cleavage_lvls = cleaned_data.get('missing_cleavage_lvls', None)
#         max_missing_cleavages = cleaned_data.get('max_missing_cleavages', None)
#         if (missing_cleavage_lvls is not None and max_missing_cleavages is not None 
#             and missing_cleavage_lvls > max_missing_cleavages):
#             raise forms.ValidationError("The value of 'max_missing_cleavages' "
#                                         "should Not be lower than the value "
#                                         "of 'missing_cleavage_lvls'.", 
#                                         code='max_missing_cleavages')
        # Check that `min_total_ptms` field is lower than or equal to `max_total_ptms`:
        min_total_ptms = cleaned_data.get('min_total_ptms', None)
        max_total_ptms = cleaned_data.get('max_total_ptms', None)
        if (min_total_ptms is not None and max_total_ptms is not None 
            and min_total_ptms > max_total_ptms):
            raise forms.ValidationError("The value of '{}' must Not be greater "
                                        "than the value of '{}'."
                                        "".format(self['min_total_ptms'].label, 
                                                  self['max_total_ptms'].label), 
                                        code='min_total_ptms')
        # Check that `exclusive_ptms` do not contain any PTM omitted in
        # Digestion Peptides by the selected Protease:
        protease_id = int( cleaned_data.get('protease', 0) )
        exclusive_ptm_ids = set(map( int, cleaned_data.get( 'exclusive_ptms', list() ) ))
        if ( protease_id and 
             exclusive_ptm_ids.intersection(self.protease2omitpmts[protease_id]) ):
            raise forms.ValidationError("Only trypsin proteases can generate "
                                        "peptides containing Ubiquitinations "
                                        "(K-GlyGly).", 
                                        code='exclusive_ptms')
        # TO_DO: Implement more cross-validations between fields!
        return cleaned_data

# DEPRECATED:
#
# class DigPepSearchForm(FormHelpTextInTittle):
#     error_css_class = 'form_error'
# 
#     template_include = 'digpep_search_form.html' #Mini-template to use as an included template for rendering this form inside a web-page.
#     post_url = urlresolvers.reverse_lazy('digpep_results') #Default URL to post this form's data. To be used as the `action` field of a html `form` tag.
# 
#     db = None #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do searches.
# 
#     digpep_seq = forms.CharField(label='Sequence',
#                                  help_text='Enter an amino-acid sequence to search for Digestion Peptides',
#                                  max_length=25,
#                                  validators=[validators.RegexValidator(r"""^[\w\*\?]+$""")])
#     min_mod_aas = forms.IntegerField(label='Minimum number of modified AAs',
#                                      help_text='Enter a number between 1 and 12',
#                                      min_value=1,
#                                      max_value=12,
#                                      initial=2)
#     max_mod_aas = forms.IntegerField(label='Maximum number of modified AAs',
#                                      help_text='Enter a number between 1 and 12',
#                                      min_value=1,
#                                      max_value=12,
#                                      initial=12)
#     allowed_ptms = forms.MultipleChoiceField(label='Allowed PTMs',
#                                              help_text='Select the PTMs that the Digestion Peptides can have. Use Control key to select/de-select multiple modifications.')
# 
#     sort_tags = forms.CharField(required=False)
# 
#     def __init__(self, *args, **kwargs):
#         # Overwrites super-class :method:``FormHelpTextInTittle.__init__``.
#         #
#         # Get the available PTMs from the database to create the dynamic
#         # `allowed_ptms` multiple-choice field:
#         with self.db.session() as session:
#             ModificationType = settings.DBM.ModificationType
#             mod_types = session.query(ModificationType.id, ModificationType.name).all() #a list of tuples
#             self.base_fields['allowed_ptms'].choices = mod_types
#             self.base_fields['allowed_ptms'].initial = [mod_type.id for mod_type in mod_types] #set ALL PTMs as the default
#         #
#         super(DigPepSearchForm, self).__init__(*args, **kwargs)


class MSPepSearchForm(FormHelpTextInTittle):
    error_css_class = 'form_error'

    template_include = 'mspep_search_form.html' #Mini-template to use as an included template for rendering this form inside a web-page.
    post_url = urlresolvers.reverse_lazy('ms_peptide_results') #Default URL to post this form's data. To be used as the `action` field of a html `form` tag.

    db = None #An instance of :class:`db_models.DataBase`, needed to obtain a DataBase session to do searches.

    mspep_seq = forms.CharField(label='Sequence',
                                help_text='Enter an amino acid sequence using 1-letter code',
                                max_length=25,
                                validators=[validators.RegexValidator(r"""^[A-Za-z\*\?]+$""")])
#     min_mod_aas = forms.IntegerField(label='Minimum number of modified AAs',
#                                      help_text='Enter a number between 1 and 12',
#                                      min_value=1,
#                                      max_value=12,
#                                      initial=2)
#     max_mod_aas = forms.IntegerField(label='Maximum number of modified AAs',
#                                      help_text='Enter a number between 1 and 12',
#                                      min_value=1,
#                                      max_value=12,
#                                      initial=12)
#     allowed_ptms = forms.MultipleChoiceField(label='Allowed PTMs',
#                                              help_text='Select the PTMs that the Digestion Peptides can have. Use Control key to select/de-select multiple modifications.')
    sources = forms.MultipleChoiceField(label='Source repository',
                                        help_text='Select the source repositories for the peptide search. Use the Control key to (de)select multiple sources')

    sort_tags = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        # Overwrites super-class :method:``FormHelpTextInTittle.__init__``.
        with self.db.session() as session:
#             # Get the available PTMs from the database to create the dynamic
#             # `allowed_ptms` multiple-choice field:
#             ModificationType = settings.DBM.ModificationType
#             mod_types = session.query(ModificationType.id, ModificationType.name).all() #a list of tuples
#             self.base_fields['allowed_ptms'].choices = mod_types
#             self.base_fields['allowed_ptms'].initial = (mod_type.id for mod_type in mod_types) #set ALL PTMs as the default
            # Get the available Sources from the database to create the dynamic
            # `sources` multiple-choice field:
            Source = settings.DBM.Source
            SourceType = settings.DBM.SourceType
#            ms_data_source = session.query(SourceType)\
#                                    .filter(SourceType.name=='MS Data')\
#                                    .first()
#            sources = session.query(Source.id, Source.name)\
#                             .filter( Source.source_types.any(SourceType.id==ms_data_source.id) )\
#                             .all() #a list of tuples with source repositories that contain MS data
            sources = session.query(Source.id, Source.name)\
                             .filter( Source.source_types.any(SourceType.name=='MS Data') )\
                             .order_by(Source.name)\
                             .all() #a list of tuples with source repositories that contain MS data
            self.base_fields['sources'].choices = sources
            self.base_fields['sources'].initial = [source.id for source in sources] #set ALL Sources as the default
        #
        super(MSPepSearchForm, self).__init__(*args, **kwargs)

