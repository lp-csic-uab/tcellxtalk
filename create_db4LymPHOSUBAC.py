# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis: Create a Protein Database for LymPHOS-UB-AC (TCellXTalk) Proteomics.

:created:    2015/01/28

:authors:    Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.16.2'
__UPDATED__ = '2018-11-20'

#===============================================================================
# Imports
#===============================================================================
import sys
import os
import re
import functools
from collections import OrderedDict, defaultdict, Counter
from datetime import datetime
from multiprocessing.dummy import Pool, cpu_count

import sqlalchemy
from sqlalchemy.sql import select
from sqlalchemy.orm import joinedload, contains_eager, undefer

import proteomics.db_models as dbm
import proteomics.filters as filters


#===============================================================================
# Global variables/constants  #X-NOTE: :caution: Do not change/modify!!!
#===============================================================================
# Folder containing ZIP DB files:
DBS_FOLDER = '/home/oga/Laboratori de Proteomica CSIC-UAB/Bases de Dades/'
# Folder containing ZIP TSV paper files:
PAPERS_FOLDER = 'PTM articles for PHOS UB AC in Jurkat 2015-03-16'
# Folder containing MS Data Results files to be imported:
RESULTS_FOLDER = '/home/oga/Laboratori de Proteomica CSIC-UAB/LymPHOS-UB-AC/'

# ZIP file where local UniProt XML and FASTA files reside:
UNIPROT_FILE = os.path.join(DBS_FOLDER, 'UniProtKB/Human reference proteome - UP000005640 rev.02-2016 - 2016-04-18/splitted_compressed.zip')
# ZIP file containing a "Gene Ontology Terms" OBO file:
OBO_GO_FILE = os.path.join(DBS_FOLDER, 'GOTerms/go-basic-obo.zip')
# ZIP file containing an "Evidence and Conclusions Ontology" OBO file:
OBO_ECO_FILE = os.path.join(DBS_FOLDER, 'Evidence and Conclusions Ontology/eco-obo.zip')
# ZIP file containing an UniProt gaf-version 2.0 "gene_association.goa_<species>" file:
UNIPROTGOA_FILE = os.path.join(DBS_FOLDER, 'UniProt-GOA/gene_association.goa_human.zip')
# ZIP file containing Experiments and SupraExperiments information TSV tables:
EXPERIMENTS_FILE = os.path.join(DBS_FOLDER, 'Experiments/crosstalk_experiments_worksheet.zip')
# ZIP file containing Choudhary et al. 2009 supplementary information TSV tables:
CHOUDHARY09_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Choudhary_09_Acetylations_SupplementaryTableS4.zip')
# ZIP file containing Udeshi et al. 2013 supplementary information TSV tables:
UDESHI13_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Udeshi_13_Ubiquitinations_SupplementaryTable2.zip')
# ZIP file containing Mayya et al. 2009 supplementary information TSV tables:
MAYYA09_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Mayya_09_Phosphorylations_TableS1.zip')
# ZIP file containing Svinkina et al. 2015 supplementary information TSV tables:
SVINKINA15_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Svinkina_14_Acetylations_3-All_Jurkat_SAHA_KacPeptides.zip')
# ZIP file containing Nguyen et al. 2016 supplementary information TSV tables:
NGUYEN16_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Nguyen_16_Phosphorylations_Jurkat_antiCD3CD28.zip')
# ZIP file containing Watts et al. 1994 TSV tables:
WATTS94_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Watts 1994 ZAP-70 pTyr.zip')
# ZIP file containing Salomon et al. 2003 TSV tables:
SALOMON03_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Salomon 2003 Table 1 pTyr.zip')
# ZIP file containing Ficarro et al. 2005 TSV tables:
FICARRO05_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Ficarro 2005 Table 3 pTyr.zip')
# ZIP file containing Tao et al. 2005 TSV tables:
TAO05_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Tao 2005 Supplementary Table 1 pTyr.zip')
# ZIP file containing Mertins et al. 2013 TSV tables:
MERTINS13_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Mertins_13_SupplementaryTable1.zip')
# ZIP file containing Jouy et al. 2015 TSV tables:
JOUY17_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Jouy_15_Phosphorylations_toplists_SupplementaryTables1and2.zip')
# ZIP file containing Beltejar et al. 2017 TSV dataset:
BELTEJAR17_FILE = os.path.join(DBS_FOLDER, PAPERS_FOLDER, 'Beltejar_17_Phosphorylations_SupplementaryDataset1_RAW.zip')

# ZIP file containing Proteome Discoverer 1.4 PSMs as TSV tables:
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB Fracionament bRP - PD1.4 - 2016-04-10/IAP_bRP_30_F1-F5_PSMs.tsv.zip') #Preliminar Ub only data from a fractionation experiment.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP - PD1.4 - 2016-07-14/Dades_UB_and_AC_PD1.4_2016-07-14.zip') #Ub and Ac Data from fractionation experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP - PD1.4 - 2016-10-17/Dades UB i AC Fracionament bRP - PD1.4 - 2016-10-17.zip') #Ub and Ac Data from fractionation experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Exp. 5 Reports - PD1.4 - 2016-10-17/Dades UB i AC Exp. 5 Reports - PD1.4 - 2016-10-17.zip') #Ub and Ac Data from 5 Reports experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2016-10-17/Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2016-10-17.zip') #TEST: Ub and Ac Data from fractionation and 5 Reports experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Test 2016-10-26/Test 2016-10-26.zip') #DEBUG: Ub and Ac Data from fractionation and 5 Reports experiments.
#PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2017-11-24/Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - Search Engine Rank 1 - 2017-11-24.TSV.zip') #Ub and Ac Data from fractionation AND 5 Reports experiments.
PD_TSV_FILE = os.path.join(RESULTS_FOLDER, 'Dades UB i AC Fracionament bRP and Exp. 5 Reports - PD1.4 - 2017-11-24/Dades UB i AC Fracionament bRP ONLY (acE17 and RPL310) - PD1.4 - Search Engine Rank 1 - 2017-11-24.TSV.zip') #Ub and Ac Data from fractionation experiments ONLY.

# URI for the local MySQL TCellXTalk DataBase:
DB_URI = 'mysql://root@localhost/tcellxtalkdb'

# URI for the local MySQL LymPHOS2 DataBase:
LYMPHOSDB_URI = 'mysql://root@localhost/lymphos_lymphos2allpepd' #Development
# LYMPHOSDB_URI = 'mysql://root@localhost/lymphos_lymphos2allpep' #Production

# Digestion Peptides variables:
MISSING_CLEAVAGES = 0 #X_NOTE: the maximum number of missing cleavages of experimental MS Peptides in DataBase was 3 (as of 2017-04-18).
MIN_LENGTH = 6 #X_NOTE: the minimum length of experimental MS Peptides in DataBase was 6 (as of 2017-04-18).
MAX_LENGTH = 65 #X_NOTE: the maximum length of experimental MS Peptides in DataBase was 63 (as of 2017-04-18).

# Amino acids and PTM IDs that are problematic at the C-term of a MSPeptide: 
# the modified AA can only be at a MSPeptide C-term if the MSPeptide is at the 
# Protein C-term (otherwise they would have avoided Trypsin to digest, ...).
CTERMAAS_W_PROBLEMS = {'K'}
CTERMPTMIDS_W_PROBLEMS = {1, 121}

# Search sequence substitution variables:
AA2GRP_PROTEOMICS_NTERM = dbm.AA2GROUP_PROTEOMICS.copy()
AA2GRP_PROTEOMICS_NTERM['0'] = ('M', '', 'X') #X-NOTE: For N-term processed Met or X (more than 6000 UniProt Human proteins start with X).
AA2REGEX_PROTEOMICS_NTERM = filters.ppf.AA2REGEX_PROTEOMICS.copy()
AA2REGEX_PROTEOMICS_NTERM['0'] = "[MX]{0,1}" #X-NOTE: For N-term processed Met or X (more than 6000 UniProt Human proteins start with X).

# Void constants: #X-NOTE: :caution: Do not change/modify!!!
VOID_STR = ''
VOID_DICT = dict()
VOID_LIST = list()
VOID_SET = set()


#===============================================================================
# Class definitions
#===============================================================================



#===============================================================================
# Function definitions to Import Data
#===============================================================================
def populate_goterms(prot_db, obofilefetcher=None, **kwargs):
    """
    Populate the database with ALL the GOTerms in ``GO_OBO_FILE``.
    Usually there is no need to call this function, due to import filters 
    UniProtRepositoryFetcher and UniProtGOAFetcher already importing the GOTerms 
    needed by their imported GOAnnotations.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param OBOGOTermFetcher obofilefetcher: an optional :class:`OBOGOTermFetcher`
    instance to get required GO Terms.
    """
    if not obofilefetcher:
        obofilefetcher = filters.OBOGOTermFetcher(OBO_GO_FILE)
    #
    print('\nStarting the database population with OBO file GO Terms:')
    with prot_db.session() as session:
        total_imports = 0
        for goterm in obofilefetcher.iter_repofile_goterms():
            total_imports += 1
            sys.stdout.write( "Importing GO Term: %s    %s    \r"%(goterm, total_imports) ) #DEBUG
            sys.stdout.flush()                                                              #
            session.merge(goterm)
        session.commit()
    print('\nEnd of the database population with OBO file GO Terms.')
    return obofilefetcher.logger.log


def populate_proteins(prot_db, obofilefetcher=None, only_reviewed=False, **kwargs):
    """
    Populate the database with all the UniProt proteins in ``UNIPROT_FILE``,
    adding information from LymPHOS DB.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param OBOGOTermFetcher obofilefetcher: an optional :class:`OBOGOTermFetcher`
    instance to get required GO Terms.
    :param bool only_reviewed: sets whether Only reviewed Proteins must be
    imported (True) or All proteins must (False). Defaults to False (import all
    proteins, reviewed or not).
    """
    print('\nStarting the database population with UniProt proteins:')
    with prot_db.session() as session:
        uprot_fetcher = filters.UniProtRepositoryFetcher(UNIPROT_FILE, 
                                                         db_session=session, 
                                                         obofilefetcher=obofilefetcher, 
                                                         only_reviewed=only_reviewed)
        commit_counter = 0
        total_imports = 0
        for protein in uprot_fetcher.iter_all_proteins():
            total_imports += 1
            sys.stdout.write( "Importing UniProt Protein: %s    %s    (%s)    \r"%(protein.ac, total_imports, commit_counter) ) #DEBUG
            sys.stdout.flush()                                                                                                  #
            # Add the protein to the database session:
            session.add(protein)
            # session.commit() from time to time:
            commit_counter += 1
            if commit_counter == 2000:
                session.commit()
                commit_counter = 0
        session.commit()
    print('\nEnd of the database population with UniProt proteins.')
    return uprot_fetcher.logger.log


def import_extra_goannotations(prot_db, obofilefetcher=None, **kwargs):
    """
    Populate the database with all the human GOAnnotations from UniProtGOA.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param OBOGOTermFetcher obofilefetcher: an optional :class:`OBOGOTermFetcher`
    instance to get required GO Terms.
    """
    print('\nStarting the database population with human GOAnnotations from UniProtGOA:')
    with prot_db.session() as session:
        uprotgoa_fetcher = filters.UniProtGOAFetcher(UNIPROTGOA_FILE,
                                                     db_session=session,
                                                     obofilefetcher=obofilefetcher)
        commit_counter = 0
        total_imports = 0
        for goann in uprotgoa_fetcher.iter_all_goannotations():
            # Ensure Go Annotation's Protein is in the Database:
            if not session.query(dbm.Protein).get(goann.protein_ac):
#                 goann.go = None #Workaround for problems with GOTerms already in the session putting these GO Annotation also in the sessioin trought its goannotations Many-to-One relationship
                continue
            #
            total_imports += 1
            sys.stdout.write( "Importing %s  (%s)  %s    \r"%(total_imports, commit_counter, goann) )
            sys.stdout.flush()
            session.add(goann)
            # session.commit() from time to time:
            commit_counter += 1
            if commit_counter == 5000:
                session.commit()
                commit_counter = 0
#                 break #DEBUG: limit the database to a minimum for debugging
        session.commit()
    print('\nEnd of the database population with human GOAnnotations from UniProtGOA.')
    return uprotgoa_fetcher.logger.log


def digest_proteins_orm(prot_db, **kwargs):
    """
    Add tryptic digestion peptides to all the proteins in the database, using
    SQLAlchemy ORM.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database proteins digestion (using SQLAlchemy ORM):')
    with prot_db.session() as session:
        trypsin = session.query(dbm.Protease).get(1) #Trypsin with exceptions
        commit_counter = 0
        total_imports = 0
        for protein in session.query(dbm.Protein).all():
            total_imports += 1
            sys.stdout.write( "Digesting protein: %s    %s    (%s)    \r"%(protein.ac, total_imports, commit_counter) ) #DEBUG
            sys.stdout.flush()                                                                                          #
            # Digest protein with Trypsin:
            protein.digestion_peptides = protein.digest_with(trypsin,
                                                             missing_cleavages=MISSING_CLEAVAGES,
                                                             min_length=MIN_LENGTH,
                                                             max_length=MAX_LENGTH)
            # Associate the protein modifications to each digestion peptide:
            for dig_pep in tuple(protein.digestion_peptides): #A copy of the `protein.digestion_peptides` set to iterate over without problems while removing peptides from the set itself.
                dig_pep.refresh_mod_aas() #Update the number of modified AAs
                # Remove Digestion Peptides with no modifications:
                if dig_pep.mod_aas == 0:
                    protein.digestion_peptides.remove(dig_pep)
                    del dig_pep
            # Call session.commit() from time to time:
            commit_counter += 1
            if commit_counter == 500:
                session.commit()
                commit_counter = 0
        session.commit()
    print('\nEnd of the database proteins digestion (using SQLAlchemy ORM).')
    return

def _cleaving_type_factory(modrows, protease):
    """
    :param list of dict modrows: the Modifications corresponding to the `protein` 
    (see dictionary structure below, at :func:`_digest`).
    :param dict protease: the Protease enzyme to use (see dictionary structure 
    below, at :func:`digest_proteins_core`).
    
    :return function : function to determine cleaving type of the `protease`:
    0 -> Don't cut; 1 -> Full cleavage; 2 -> Ambiguous cleavage.
    """
    # Get protease attributes:
    aa2ptmexcepts = protease['target_ptm_exceptions']
    check_ptmexcepts = bool(aa2ptmexcepts)
    ptm_exception_aas = set( aa2ptmexcepts.keys() )
    context_rules4cuttype = protease['context_rules4cuttype']
    check_context = bool(context_rules4cuttype)
    # Summarize protein modifications and positions:
    pos2mod_ids = defaultdict(set)
    for modrow in modrows:
        pos2mod_ids[ modrow['position'] ].add( modrow['mod_type_id'] )
    mod_poss = set( pos2mod_ids.keys() )
    # Function to determine cut exception due to PTMs:
    def is_ptm_blckd(aa, aa_pos): 
        """
        Determine if the AA (`aa`) at the protein position (`aa_pos`) is 
        modified with a PTM not compatible with the protease cut.
        """
        return ( aa in ptm_exception_aas and aa_pos in mod_poss and 
                 pos2mod_ids[aa_pos].intersection( aa2ptmexcepts[aa] ) )
    #
    # Function to search for cut exceptions due to PTMs and context rules:
    def cleaving_type_func(match_start, match_end, target_aas, nt_context, ct_context):
        """
        Function to determine cleaving type and action of the Protease on a
        found target sequence, depending on cut exceptions due to PTMs and
        ambiguity cut rules for the N-Term and C-Term context of the target
        sequence.
        
        :param int match_start: python start position of the target cut
        sequence inside the peplike sequence.            
        :param int match_end: python end position of the target cut sequence 
        inside the peplike sequence.            
        :param str target_aas: amino acids in the target cut sequence.
        :param str nt_context: amino acids at N-Term of the target cut sequence.
        :param str ct_context: amino acids at C-Term of the target cut sequence.        
        
        :return integer : Cut type: 0 -> Don't cut; 1 -> Full cleavage; 
        2 -> Ambiguous cleavage, wait for the next...
        """
        # Search PTMs that prevent the Protease to cut the target AAs sequence:
        if check_ptmexcepts:
            for index, target_aa in enumerate(target_aas, start=1): #start=1 to transform Python str position -> Biological position
                target_aa_pos = match_start + index
                if is_ptm_blckd(target_aa, target_aa_pos):
                        return 0 # -Don't cut.
        # Check the surrounding AA according to the context rules to detect 
        # special cutting position:
        if check_context:
            if check_ptmexcepts:
                last_nt_aa = nt_context[-1:]
                last_nt_pos = match_start
                not_nt_ptm_blckd = not is_ptm_blckd(last_nt_aa, last_nt_pos)
                first_ct_aa = ct_context[:1]
                first_ct_pos = match_end + 1
                not_ct_ptm_blckd = not is_ptm_blckd(first_ct_aa, first_ct_pos)
            else:
                not_nt_ptm_blckd = not_ct_ptm_blckd = False
            #X_NOTE: Boolean AND relations between each pair of context rules to determine the special cleavage: (nterm_rule1 AND cterm_rule1) -> cut_type
            for nt_rule, ct_rule, cut_type in context_rules4cuttype:
                if (( not nt_rule or (not_nt_ptm_blckd and re.search(nt_rule, nt_context)) )
                    and 
                    ( not ct_rule or (not_ct_ptm_blckd and re.search(ct_rule, ct_context)) )):
                    return cut_type # 0 -> Don't cut; 1 -> Full cleavage; 2 -> Ambiguous cleavage (usually 2)
        return 1 # -Full cleavage.
    #
    return cleaving_type_func

def _iter_digest(protein, modrows, protease, missing_cleavages=0): #TO_DO: Update to match improvements in db_models.Protease.iter_fulldigest_basicdata() !!
    """
    Virtually digest iteratively the `protein` sequence using a `protease`
    enzyme.

    :param dict protein: the Protein to be digested (see dictionary structure
    below, at :func:`_digest`).    
    :param list of dict modrows: the Modifications corresponding to the `protein` 
    (see dictionary structure below, at :func:`_digest`).
    :param dict protease: the Protease enzyme to use (see dictionary structure 
    below, at :func:`digest_proteins_core`).
    :param int missing_cleavages: the maximum missing cleavages degree allowed. 
    Defaults to 0.

    :return generator : yields Proto-Digestion Peptides as tuples of basic data:
        ( biological start position in Protein sequence,
          biological end position in Protein sequence,
          number of missing cleavages of the digestion peptide )
    """
    # Frequently accessed variable keys as variables (some speed optimization):
    peplike_seq = protein['seq']
    peplike_length = protein['length']
    relative_cut_pos = protease['relative_cut_pos']
    contextlen = protease['context_length']
    max_contiguousambiguous = protease['max_contiguousambiguous']
    always_cleaving = lambda match_start, match_end, target_aas, nt_context, ct_context: 1 #Always cut the target cut sequence (no exceptions)
    # Get a function to detect exceptions (missing cleavages) due to PTMs in
    # target sequence AAs, and ambiguous cuts due to surrounding AAs:
    if protease['target_ptm_exceptions'] or protease['context_rules4cuttype']:
        cleaving_type = _cleaving_type_factory(modrows, protease)
    else:
        cleaving_type = always_cleaving
    #
    # First, yield and store ALL the 'Fully' Digested Peptides (except for the 
    # Trypsin standard missed or ambiguous cleavages):
    last_clearcut_pos = 0 #Position of the last clear cut (not ambiguous).
    startpos2fulldig_protodigpeps = defaultdict(list) #Fully digested peptides basic data indexed by start_pos: { start_pos: [ (start_pos, end_pos, missing_cuts), ... ] }
    yielded_protodigpeps = set() # { (start_pos, end_pos, missing_cuts), ... }
    ptm_misscut_poss = list() #Accumulated positions with missed cleavages due to PTMs, from last clear cut.
    ambiguous_pos_grps = list() #A list of ambiguous position groups (each one another list of ambiguous positions: only one can be cut at a time). Ex.: [ [0], [9,10], [15], [19] ]
    iter_cut_matches = re.finditer( protease['target_seq_re'], peplike_seq )
    while True:
        try:
            # Get a new probable cutting match:
            match = iter_cut_matches.next()
        except StopIteration as _:
            if last_clearcut_pos < peplike_length:
                # Generate a 'virtual' match at C-Term of target sequence 
                # to allow processing the remaining C-Terminal Peptide/s:
                match = re.search(".$", peplike_seq) #The last AA
                cleaving_type = always_cleaving
            else:
                # End the processing loop: all 'fully' digested peptides 
                # have been generated:
                break
        # Get data about the new probable cutting position and its surrounding
        # context:
        match_start = match.start()
        cutting_pos = match_start + relative_cut_pos #Current probable cutting position.
        match_end = match.end()
        nt_context = peplike_seq[max(0, match_start-contextlen):match_start] #N-Term context: amino acids at N-Term of the target cut sequence
        ct_context = peplike_seq[ match_end:min(match_end+contextlen, peplike_length) ] #C-Term context: amino acids at C-Term of the target cut sequence
        # Analyze the data to determine how the protease can cut the found
        # position:
        cleaving = cleaving_type(match_start, match_end, match.group(0), 
                                 nt_context, ct_context)
        if cleaving == 1:
            # - UNAMBIGUOUS protease Cutting Position:
            if not ambiguous_pos_grps:
                start_pos_bio = last_clearcut_pos + 1 #From python str position to biological position.
                proto_digpep = ( start_pos_bio, cutting_pos, len(ptm_misscut_poss) ) 
                startpos2fulldig_protodigpeps[start_pos_bio].append(proto_digpep) #Add proto-digestion peptide to caches for later
                yielded_protodigpeps.add(proto_digpep)                            #
                #
                yield proto_digpep
            else:
                ambiguous_pos_grps.append( [cutting_pos] ) #Add new clear/unambiguous cutting position as the ending group of ambiguous positions.
                # Generate all possible combination of digestion peptides due
                # to the intermediate ambiguous cutting positions:
                start_ambigrp = ambiguous_pos_grps[0]
                start_ambigrp_len = len(start_ambigrp)
                for end_ambigrp in ambiguous_pos_grps[1:]:
                    # Product combinations between positions in 'start'
                    # ambiguous group and positions in 'end' ambiguous group:
                    #   Ex.: [1, 2] x [4, 5] -> (1, 4), (1, 5), (2, 4), (2, 5)
                    #   Ex.: [1, 2] x [4] -> (1, 4), (2, 4)
                    for start_idx, start_pos in enumerate(start_ambigrp, start=1):
                        start_pos_bio = start_pos + 1 #From python str position to biological position.
                        base_missing_cuts = start_ambigrp_len - start_idx #The remaining ambiguous positions in the start group would be missing cleavages.
                        for end_idx, end_pos in enumerate(end_ambigrp, start=0):
                            missing_cuts = base_missing_cuts + end_idx #The previous ambiguous positions in this end group would also be missing cleavages.
                            missing_cuts += len( [misscut_pos for misscut_pos 
                                                  in ptm_misscut_poss if 
                                                  start_pos < misscut_pos < end_pos] )
                            proto_digpep = (start_pos_bio, end_pos, missing_cuts)
                            startpos2fulldig_protodigpeps[start_pos_bio].append(proto_digpep) #Add proto-digestion peptide to caches for later
                            yielded_protodigpeps.add(proto_digpep)                            #
                            #
                            yield proto_digpep
                    # Prepare for the next group of ambiguous positions:
                    start_ambigrp = end_ambigrp
                    start_ambigrp_len = len(end_ambigrp)
            # Clean for next cutting position match:
            last_clearcut_pos = cutting_pos
            ambiguous_pos_grps = list()
            ptm_misscut_poss = list()
        elif cleaving == 2:
            # - AMBIGUOUS protease Cutting Position:
            if not ambiguous_pos_grps:
                ambiguous_pos_grps.append( [last_clearcut_pos] )
            ambiguous_grp = ambiguous_pos_grps[-1]
            if ( ( cutting_pos - ambiguous_grp[-1] ) > contextlen #We are Far enough from the previous ambiguous group last position...
                  or len(ambiguous_grp) >= max_contiguousambiguous ): #Or have reached maximum number of contiguous ambiguous cut positions allowed by protease
                #Add ambiguous position to a New group:
                ambiguous_grp = [cutting_pos]
                ambiguous_pos_grps.append(ambiguous_grp)
            else:
                #Add ambiguous position to the Previous ambiguous group:
                ambiguous_grp.append(cutting_pos)
        else:
            # - MISSED Cleavage due to a PTM:
            ptm_misscut_poss.append(cutting_pos)
    #
    # Then, yield the resulting digestion peptides of the different missing
    # cleavages degrees (..., 2 missing cleavages, 1 missing cleavage):
    while missing_cleavages > 0:
        for start_pos_bio in sorted(startpos2fulldig_protodigpeps)[:-missing_cleavages]:
            elongating_peps = startpos2fulldig_protodigpeps[start_pos_bio]
            for _ in range(missing_cleavages):
                elongated_peps = set()
                for _, prev_end_pos, prev_misscuts in elongating_peps:
                    for _, next_end_pos, next_misscuts in startpos2fulldig_protodigpeps[prev_end_pos + 1]: #X_NOTE: ``startpos2fulldig_protodigpeps`` it's a defaultdict(list) so it returns empty lists for keys/positions that doesn't exist.
                        elongated_peps.add( (start_pos_bio, next_end_pos, 
                                             prev_misscuts + next_misscuts) )
                elongating_peps = elongated_peps #Switch
            for _, last_end_pos, all_misscuts in elongating_peps:
                proto_digpep = (start_pos_bio, last_end_pos, 
                                missing_cleavages + all_misscuts)
                if proto_digpep not in yielded_protodigpeps:
                    yielded_protodigpeps.add(proto_digpep)
                    yield proto_digpep
        missing_cleavages -= 1
                
def _digest(protein, modrows, protease, missing_cleavages=0, min_length=None,
            max_length=None):
    """
    Virtually _digest the `protein` sequence using a `protease` enzyme,
    optionally filtering by length the resulting peptides.

    :param dict protein : the Protein to be digested:
        {'ac': Protein ACcession number string,
         'seq': Protein amino-acid sequence string}
    :param list of dict modrows: the Modifications corresponding to the `protein`:
        [ {'id': Protein Modification ID in the Database,
           'position': biological position in the Protein sequence,
           'mod_type_id: ID of the Modification Type (Ex. 1 -> Acetylation),
           'significant': significance of this Protein Modification}, ... ]
    :param dict protease: the Protease enzyme to use (see dictionary structure 
    below, at :func:`digest_proteins_core`).
    :param int missing_cleavages : the missing cleavages degree. Defaults
    to 0.
    :param int min_length: the minimum amino-acid length of the Digestion
    Peptides to be returned. None, or values lower than 2 are equivalent mean 
    "no minimum length". Defaults to None; but it's better to set it at least 
    to 5.
    :param int max_length: the maximum amino-acid length of the Digestion
    Peptides to be returned. Defaults to None -> no maximum length.

    :return list : filtered (or un-filtered) list of Digestion Peptide dictionaries:
        [ {'protein_ac': original Protein ACcession number string,
           'prot_start': biological start position in Protein sequence,
           'prot_end': biological end position in Protein sequence,
           'seq': Digestion Peptide amino-acid sequence string,
           'consensus': True/False,
           'missing_cleavages': number of missing cleavages}, ... ]
    """
    if (min_length and max_length) and (max_length < min_length):
        raise ValueError("'max_length' should be >= 'min_length'")
    # Frequently accessed variable keys as variables (some speed optimization):
    check_minlength = not (min_length is None or min_length < 2)
    check_maxlength = not (max_length is None)
    protein_ac = protein['ac']
    protein_seq = protein['seq']
    # Generate list of Digestion Peptides dictionaries:
    dig_peps4prot = list()
    for prot_start, prot_end, missing_cuts in _iter_digest(protein, modrows, 
                                                           protease, 
                                                           missing_cleavages):
        # - Check Digestion Peptide length, if necessary:
        dp_length = prot_end - prot_start + 1
        if ( (check_minlength and dp_length < min_length) or 
             (check_maxlength and dp_length > max_length) ):
            continue #Skip this Digestion Peptide.
        # - Generate Digestion Peptide dictionary:
        dig_peps4prot.append( {'protein_ac': protein_ac, 
                               'prot_start': prot_start, 
                               'prot_end': prot_end, 
                               'seq': protein_seq[prot_start-1:prot_end], 
                               'consensus': True, 
                               'missing_cleavages': missing_cuts} )
    #
    return dig_peps4prot

def _create_trypsin(prot_db, db_conn=None):
    if db_conn is None:
        db_conn = prot_db.get_db_connection() #Get a new connection to the database.
        close_db_conn = True
    else:
        close_db_conn = False
    # Create the ``Trypsin`` Protease:
    trypsin = {'id': 1, 'name': 'Trypsin with exceptions', 
               'target_seq_re': "(?<!^)([RK])(?!P)(?!$)", 'relative_cut_pos': 1, 
               'target_ptm_exceptions': { 'K': [1, 121] }, 
               'context_length': 1,
               'context_rules4cuttype': [ ['[RK]$', '', 2], ['', '^[RK]', 2] ], 
               'max_contiguousambiguous': 2}
    # Store the ``Trypsin`` if it isn't already in the database:
    ins_protease_stmt = prot_db.tables['proteases'].insert()
    try:
        ins_result = db_conn.execute(ins_protease_stmt, **trypsin)
    except sqlalchemy.exc.IntegrityError as _: #``Trypsin`` is already in the Database.
        pass
    else:
        ins_result.close()
    #
    if close_db_conn:
        db_conn.close()
    #
    return trypsin

def _create_digest_condition(prot_db, name, protease, missing_cleavages, min_length, 
                             max_length, extradata=None, db_conn=None):
    if db_conn is None:
        db_conn = prot_db.get_db_connection() #Get a connection to the database.
        close_db_conn = True
    else:
        close_db_conn = False
    if extradata:
        extradata.setdefault( 'digestion_method', 'standard' )
        extradata.setdefault( 'date_time', datetime.now().isoformat() )
    else:
        extradata = { 'digestion_method': 'standard', 
                      'date_time': datetime.now().isoformat() }
    # Create the Digestion Condition:
    digestion_condition = { 'name': name, 
                            'extradata': extradata, 
                            'max_missing_cleavages': missing_cleavages, 
                            'min_digpep_length': min_length, 
                            'max_digpep_length': max_length, 
                            'digestion_enzyme_id': protease['id'] }
    # Store new Digestion Condition and get its new Primary Key (ID):
    ins_digcond_stmt = prot_db.tables['digestionconditions'].insert()
    ins_result = db_conn.execute(ins_digcond_stmt, **digestion_condition)
    digestion_condition['id'] = ins_result.inserted_primary_key[0]
    ins_result.close()
    #
    if close_db_conn:
        db_conn.close()
    #
    return digestion_condition

def digest_proteins_core(prot_db, protease=None, 
                         missing_cleavages=MISSING_CLEAVAGES, 
                         min_length=MIN_LENGTH, max_length=MAX_LENGTH, 
                         **kwargs):
    """
    Add tryptic Digestion Peptides to all the proteins in the database, using
    SQLAlchemy Core (it's really faster).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param dict protease: the Protease enzyme to use:
        {'id': Protease ID number in database,
         'target_seq_re': regular expression string to match the target site,
         'relative_cut_pos': the cutting position relative to the beginning of 
                             the target site (0), 
         'target_ptm_exceptions': { 'target site AA': [list of modifications
                                                       IDs that prevent the
                                                       Protease from cutting
                                                       his target site] }, 
         'context_length': number of AA before and after the target sequence 
                           to use as a context for it, 
         'context_rules4cuttype': [ ('rule1_for_Nterm_context', 
                                     'rule1_for_Cterm_context', cut_type), ....], 
         'max_contiguousambiguous': maximum number of contiguous ambiguous cut 
                                    positions allowed by protease }
    :param int missing_cleavages : the missing cleavages degree. Defaults
    to the global configuration constant MISSING_CLEAVAGES.
    :param int min_length: the minimum amino-acid length of the Digestion
    Peptides. Defaults to the global configuration constant MIN_LENGTH.
    :param int max_length: the maximum amino-acid length of the Digestion
    Peptides to be returned. Defaults to the global configuration constant MAX_LENGTH.
    """
    print('\nStarting the Database Proteins Digestion (using SQLAlchemy Core):')
    # Get used tables from prot_db metadata:
    proteins = prot_db.tables['proteins']
    protmodifications = prot_db.tables['protmodifications']
    digestionpeptides = prot_db.tables['digestionpeptides']
    digpep_protmod = prot_db.tables['digestionpeptides_protmodifications']
    # Get a connection to the database:
    conn = prot_db.get_db_connection()
    # If no protease supplied, get the ``Trypsin`` as the default one:
    if protease is None:
        protease = _create_trypsin(prot_db, conn)
    # Create a new Digestion Condition and get it's ID:
    dc_name = protease['name'] + " full digestion"
    digestion_condition = _create_digest_condition(prot_db, dc_name, protease, 
                                                   missing_cleavages, 
                                                   min_length, max_length, 
                                                   db_conn=conn)
    digestioncondition_id = digestion_condition['id']
    # Excluded no clear physiological modifications:
    excl_modids = filters.NO_PHYSIO_MDDIDS
    # Query for Proteins to digest:
    sel_prots_stmt = select( [proteins.c.ac, proteins.c.seq, proteins.c.length] )
    protrows = conn.execute(sel_prots_stmt)
    for n_prots, protrow in enumerate(protrows, start=1):
        protrow_ac = protrow['ac']
        # Skip Proteins with NO sequence:
        if not protrow['seq']:
            print( "Protein skipped (NO Sequence found): %s  (%d)\t\t"%(protrow_ac, n_prots) ) #DEBUG
            continue
        #
        sys.stdout.write( "Digesting protein: %s  (%d)"%(protrow_ac, n_prots) ) #DEBUG
        # Get ALL Protein Modifications as (id, position, mod_type_id,
        # significant) tuples:
        sel_mods_stmt = select( [protmodifications.c.id,
                                 protmodifications.c.position,
                                 protmodifications.c.mod_type_id,
                                 protmodifications.c.significant] )\
                              .where(protmodifications.c.protein_ac==protrow_ac)
        prot_allmodrows = conn.execute(sel_mods_stmt).fetchall()
        # Filter and get ONLY Significant Protein Modifications:
        sel_sigmods_stmt = sel_mods_stmt.where(protmodifications.c.significant==True)
        prot_sigmodrows = conn.execute(sel_sigmods_stmt).fetchall()
        # Get and store Digestion Peptides and associate them to the
        # corresponding Protein Modifications:
        with conn.begin() as _: # Begins a Database Transaction:
            n_digpepsxprot = 0 #Number of generated modified digestion peptides for this protein.
            # Obtain Digestion Peptides, but ONLY using their Significant Protein 
            # Modifications (``prot_sigmodrows``) for the PTM missing cut exceptions:
            for dig_pep in _digest(protrow, prot_sigmodrows, protease,
                                   missing_cleavages=missing_cleavages,
                                   min_length=min_length, max_length=max_length):
                # Get ALL (significant or not) Modifications for the current
                # Digestion Peptide (the `dig_pep_mods`):
                dp_protstart = dig_pep['prot_start']
                dp_protend = dig_pep['prot_end']
                dp_allmods = [modrow for modrow in prot_allmodrows
                              if dp_protstart <= modrow['position'] <= dp_protend]
                # Only process and store Digestion Peptides with some
                # Modification (significant or not):
                if dp_allmods:
                    # Add current digestion peptide `_db_mod_aas` (Number of
                    # Significantly Modified Amino Acids) cache attribute:
                    dp_sigmods = {modrow['position'] for modrow in dp_allmods
                                  if modrow['significant'] and 
                                     modrow['mod_type_id'] not in excl_modids}
                    dig_pep['_db_mod_aas'] = len(dp_sigmods)
                    # Add reference to the current Digestion Condition:
                    dig_pep['digestion_condition_id'] = digestioncondition_id
                    # Store new Digestion Peptide, and get its new Primary Key (ID):
                    ins_digpep_stmt = digestionpeptides.insert()
                    ins_result = conn.execute(ins_digpep_stmt, **dig_pep)
                    dig_pep_id = ins_result.inserted_primary_key[0]
                    ins_result.close()
                    # Store Associations between current Digestion Peptide and 
                    # all corresponding Protein Modifications:
                    digpep_protmodrows = [ {'digestionpeptide_id': dig_pep_id,
                                            'protmodification_id': modrow['id']}
                                           for modrow in dp_allmods ]
                    ins_dp2pm_stmt = digpep_protmod.insert()
                    ins_result = conn.execute(ins_dp2pm_stmt, digpep_protmodrows)
                    ins_result.close()
                    n_digpepsxprot += 1
        #
        sys.stdout.write(" -> Dig.Peps. with PTMs:%d     \r"%n_digpepsxprot) #DEBUG
        sys.stdout.flush() #DEBUG
    protrows.close()
    print('\nFinished the Database Proteins Digestion.')
    return


def import_experiments(prot_db, experiments=EXPERIMENTS_FILE, **kwargs):
    """
    Import Experiments and SupraExperiments information from TSV files in
    ``EXPERIMENTS_FILE`` into the database.
    Usually there is no need to call this function, due to each import filter 
    already importing their related Experiments and SupraExperiments data.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param object experiments: a :class:`ExperimentFetcher` instance, or a
    either a string containing the path to a file, or a file-like object, 
    of a zip file containing Experiments and SupraExperiments information 
    as TSV files. Defaults to global ``EXPERIMENTS_FILE``.
    """
    print('\nStarting the database population with file Experiments and SupraExperiments:')
    with prot_db.session() as session:
        #Get a valid :class:`ExperimentFetcher` instance:
        if not isinstance(experiments, filters.ExperimentsFetcher):
            experiments = filters.ExperimentsFetcher(experiments, 
                                                     db_session=session)
        experimentsfetcher = experiments
        #
        total_imports = 0
        for exp in experimentsfetcher.iter_repofile_experiments():
            total_imports += 1
            sys.stdout.write( "Importing Experiment: %s    %s    \r"%(exp, total_imports) ) #DEBUG
            sys.stdout.flush()                                                              #
            session.merge(exp)
        session.commit()
    print('\nEnd of the database population with file Experiments and SupraExperiments.')
    return experimentsfetcher.logger.log


def _mspeps2prots_worker(mspepseq_mspepids, db_uri=DB_URI,
                         aa_mapping=AA2GRP_PROTEOMICS_NTERM):
    """
    Processing function used by each thread/process Worker in the Pool created
    by :func:`_mspeps2prots_mp`, to search MS Peptides sequences in Protein
    sequences and link the matching ones.

    :param tuple mspepseq_mspepids: MS Peptides unmodified sequence string and
    list of MS Peptides that share this unmodified sequence.
    :param str db_uri: the Database URI to connect to. Defaults to global 
    `DB_URI`.
    :param dict aa_mapping: a dictionary that maps amino acids to groups of
    amino acid variants. Ex.: { aa: (aa, aa1, aa2, ... ), ... }. Defaults to 
    global dictionary `AA2GRP_PROTEOMICS_NTERM`.
    """
    mspep_seq, mspepids = mspepseq_mspepids #Unpack data to process.
    # N-term, C-term, or Normal search:
    if mspep_seq[0] == '0':
        search_tmplt = "{0}%" #Search Only at the N-term of Protein sequences.
    elif mspep_seq[-1] == '_':
        mspep_seq = mspep_seq[:-1]
        search_tmplt = "%{0}" #Search Only at the C-term of Protein sequences.
    else:
        search_tmplt = "%{0}%" #Search Everywhere in the Protein sequences.
    # Do the Protein Search and Linking:
    prot_db = dbm.DataBase(db_uri)
    with prot_db.session() as session:
        # Search MS Peptides sequences in Protein sequences:
        prots = dbm.search_seq_in_prot_db(mspep_seq, session, aa_mapping, search_tmplt)
        # Get all the MS Peptides that share the same sequence:
        mspeps = session.query(dbm.MSPeptide)\
                        .filter( dbm.MSPeptide.id.in_(mspepids) )
        # Link MS Peptides with matching Proteins:
        for mspep in mspeps:
            sys.stdout.write( "Linking MS peptide %s        \r"%(mspep.seq_w_mods) )
            sys.stdout.flush()
            mspep.proteins = prots
        # Commit to Database before ending process/thread worker run.
        session.commit()
    # Cleaning:
    prot_db.close() #Close All pooled connections to the database management system (DBMS), to avoid 'Too many connections' error from DBMS.
    del prot_db #Mark for garbage collection.

def _mspeps2prots_mp(prot_db, source=None, experiments=None, 
                     aa_mapping=AA2GRP_PROTEOMICS_NTERM, 
                     ctermaas_w_problems=CTERMAAS_W_PROBLEMS, 
                     ctermptmids_w_problems=CTERMPTMIDS_W_PROBLEMS):
    """
    Link MS Peptides in Database to their corresponding Protein, based in
    sequence matchings.
    This is the Multi-thread/process version. It uses the 
    :func:`_mspeps2prots_worker` for each process/thread worker in a Pool of 
    workers.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Source source: an optional :class:`Source` instance, to filter MS
    Peptides with.
    :param iterable experiments: an optional iterable of :class:`Experiment` 
    instances, to filter MS Peptides with.
    :caution: Only provide ONE of :param:`source` or :param:`experiments`, but
    not both of them.
    :param dict aa_mapping: a dictionary that maps amino acids to groups of
    amino acid variants. Ex.: { aa: (aa, aa1, aa2, ... ), ... }. Defaults to 
    global dictionary `AA2GRP_PROTEOMICS_NTERM`.
    :param iterable ctermaas_w_problems: Amino acids that, if modified, are
    problematic at the C-term of a MSPeptide. Defaults to global
    `CTERMAAS_W_PROBLEMS`.
    :param iterable ctermptmids_w_problems: PTM IDs that are problematic at the
    C-term of a MSPeptide. Defaults to global `CTERMPTMIDS_W_PROBLEMS`.
    """
    print('\nStarting the database linking of MS Peptides to Proteins:')
    # Get and prepare the data to be processed from the DataBase:
    with prot_db.session() as session:
        if source and not experiments: # Only MS Peptides for the supplied Source:
            mspep_query = session.query(dbm.MSPeptide)\
                                 .filter(dbm.MSPeptide.source_id==source.id)
        elif experiments and not source: # Only MS Peptides for the supplied experiments:
            experimentids = {experiment.id for experiment in experiments}
            mspep_query = session.query(dbm.MSPeptide)\
                                 .join(dbm.MSPeptide.experiments)\
                                 .filter( dbm.Experiment.id.in_(experimentids) )\
                                 .distinct()            
        elif not source and not experiments: # All MS Peptides:
            mspep_query = session.query(dbm.MSPeptide)
        else: # Error:
            raise TypeError("Only provide ONE of 'source' or 'experiments', "
                            "but not both of them.")
        # Get needed data from MS Peptides:
        mspepseq2mspepids = defaultdict(list)
        for mspep in mspep_query:
            # Get MS Peptide sequence:
            if not mspep.is_ntermonly(): # Not N-terminal peptides (most frequent ones):
                mspep_seq = mspep.seq #Normal peptides.
                if mspep_seq[-1] in ctermaas_w_problems:
                    pos2allmods = mspep._build_pos2modifications(only_significant=False) #Get All PTMs, significant and non-significant, so PTM C-Term restrictions apply regardless of PTM significance.
                    cterm_mods = pos2allmods.get( len(mspep_seq), set() )
                    for cterm_mod in cterm_mods:
                        if cterm_mod.mod_type_id in ctermptmids_w_problems:
                            mspep_seq += "_" #Mark the MS Peptide sequence as having some C-term PTM that can Only be possible at the Protein C-term.
                            break
            else: # N-terminal only peptides:
                mspep_seq = "0" + mspep.seq #Allow the possibility that a N-term MS Peptide has the N-term Met processed
            # Group MS Peptide IDs with the same unmodified sequence:
            mspepseq2mspepids[mspep_seq].append(mspep.id)
    # Sort MS Peptides by descending length, so the Pool workers will start
    # processing longer peptides, with most probabilities to have multiple
    # variants (and so slower to find in Database):
    mspepseqs_mspepids = sorted(mspepseq2mspepids.iteritems(),
                                key=lambda x: len( x[0] ), reverse=True)
    # Prepare the multiprocessing Pool of workers:
    if _mspeps2prots_worker.func_defaults == (prot_db.db_uri, aa_mapping):
        map_func = _mspeps2prots_worker
    else: #X_NOTE: :class:`functools.partial` needed because a lambda cannot be pickled and passed to sub-processes:
        map_func = functools.partial(_mspeps2prots_worker, 
                                     db_uri=prot_db.db_uri, 
                                     aa_mapping=aa_mapping)
    pool = Pool( processes = max(2, cpu_count()-2) )
    # Process data in parallel:
    pool.map(map_func, mspepseqs_mspepids)
    pool.close() #Avoid more jobs send to this pool.
    pool.join() #Wait for all the workers to finish.
    #
    print('\nEnd of the database linking of MS Peptides to Proteins.')

def _mspeps2prots(prot_db, source=None, aa_mapping=AA2GRP_PROTEOMICS_NTERM, 
                  ctermaas_w_problems=CTERMAAS_W_PROBLEMS, 
                  ctermptmids_w_problems=CTERMPTMIDS_W_PROBLEMS):
    """
    Link MS Peptides in Database to their corresponding Proteins, based in
    sequence matchings.
    This is the Single thread/process version.
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Source source: an optional :class:`Source` instance to filter MS
    Peptides with.
    :param dict aa_mapping: a dictionary that maps amino acids to groups of
    amino acid variants. Ex.: { aa: (aa, aa1, aa2, ... ), ... }. Defaults to 
    global dictionary `AA2GRP_PROTEOMICS_NTERM`.
    :param iterable ctermaas_w_problems: Amino acids that, if modified, are
    problematic at the C-term of a MSPeptide. Defaults to global
    `CTERMAAS_W_PROBLEMS`.
    :param iterable ctermptmids_w_problems: PTM IDs that are problematic at the
    C-term of a MSPeptide. Defaults to global `CTERMPTMIDS_W_PROBLEMS`.
    """
    print('\nStarting the database linking of MS peptides to proteins:')
    #
    with prot_db.session() as session:
        if not source: #All MS Peptides:
            mspep_query = session.query(dbm.MSPeptide)
        else: #Only MS Peptides for the supplied Source:
            mspep_query = session.query(dbm.MSPeptide)\
                                 .filter(dbm.MSPeptide.source_id==source.id)
        mspepseq2cachedprots = dict()
        commit_counter = 0
        for mspep in mspep_query:
            sys.stdout.write( "Linking MS peptide %s        \r"%(mspep.seq_w_mods) )
            sys.stdout.flush()
            # Search the MS Peptide sequence for all matching Protein sequences
            # in the database:
            if not mspep.is_ntermonly(): #Normal and C-terminal peptides:
                mspep_seq = cache_mspep_seq = mspep.seq
                search_tmplt="%{0}%"
                if mspep_seq[-1] in ctermaas_w_problems:
                    cterm_mods = mspep.pos2modifications.get( len(mspep_seq), set() )
                    for cterm_mod in cterm_mods:
                        if cterm_mod.mod_type_id in ctermptmids_w_problems:
                            cache_mspep_seq += "_" #Mark the MS Peptide sequence as having some C-term PTM that can only be possible at the Protein C-term.
                            search_tmplt="%{0}" #Search only in the C-term of Protein sequences.
                            break
            else: #N-terminal only peptides:
                mspep_seq = cache_mspep_seq = "0" + mspep.seq #Allow the possibility that a N-term MS Peptide has the N-term Met processed
                search_tmplt="{0}%" #Search only in the N-term of Protein sequences.
            # Caching of matching proteins:
            if mspepseq2cachedprots.has_key(cache_mspep_seq):
                prots = mspepseq2cachedprots[cache_mspep_seq]
            else:
                prots = dbm.search_seq_in_prot_db(mspep_seq, session,
                                                  aa_mapping, search_tmplt)
                mspepseq2cachedprots[cache_mspep_seq] = prots
            # Link MS Peptide with found Proteins:
            mspep.proteins = prots
            commit_counter += 1
            # session.commit() from time to time:
            if commit_counter == 5000:
                session.commit()
                commit_counter = 0
        session.commit()
    #
    print('\nEnd of the database linking of MS peptides to proteins.')

def _mspepmods2protmods(prot_db, source=None,
                        aa_mapping=AA2REGEX_PROTEOMICS_NTERM, 
                        ctermaas_w_problems=CTERMAAS_W_PROBLEMS, 
                        ctermptmids_w_problems=CTERMPTMIDS_W_PROBLEMS):
    """
    Generate new Modifications (:class:`ProtModification`) for each database
    Protein, from the associated MS Peptides (:class:`MSPeptide`) and their
    Modifications (:class:`MSPepModification`).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Source source: an optional :class:`Source` instance to filter MS
    Peptides with.
    :param dict aa_mapping: a dictionary that maps amino acids to groups of
    amino acid variants. Ex.: { aa: (aa, aa1, aa2, ... ), ... }. Defaults to 
    global dictionary AA2GRP_PROTEOMICS_NTERM.
    :param iterable ctermaas_w_problems: Amino acids that, if modified, are
    problematic at the C-term of a MSPeptide. Defaults to global
    `CTERMAAS_W_PROBLEMS`.
    :param iterable ctermptmids_w_problems: PTM IDs that are problematic at the
    C-term of a MSPeptide. Defaults to global `CTERMPTMIDS_W_PROBLEMS`.
    """
    print('\nStarting collection of Protein Modifications from database MS data:')
    #
    formatseq4search = filters.ppf.formatseq4search
    with prot_db.session() as session:
        evidence = dbm.select_or_create(session, dbm.Evidence, type='IDA')[0] #'Inferred from Direct Assay'.
        if source:
            # Query ONLY Proteins containing MS Peptides from the supplied Source:
            prot_query = session.query(dbm.Protein)\
                                .join(dbm.Protein.mspeptides)\
                                .options( undefer(dbm.Protein.seq), 
                                          contains_eager(dbm.Protein.mspeptides)\
                                          .joinedload(dbm.MSPeptide.modifications) )\
                                .filter(dbm.Protein.seq!=None)\
                                .filter(dbm.MSPeptide.source_id==source.id)\
                                .distinct()
            # Define filter to avoid processing non-modified MS Peptides and MS Peptides not from specified Source:
            mspeps_filter = lambda mspep: ( bool(mspep.modifications) and 
                                            (mspep.source_id == source.id) )
            # Values needed to create dictionary of ascore thresholds:
            sources = [source]
        else:
            # Query ALL Proteins:
            prot_query = session.query(dbm.Protein)\
                                .options( undefer(dbm.Protein.seq), 
                                          joinedload(dbm.Protein.mspeptides)\
                                          .joinedload(dbm.MSPeptide.modifications) )\
                                .filter(dbm.Protein.seq!=None)
            # Define filter to avoid processing non-modified MS Peptides:
            mspeps_filter = lambda mspep: bool(mspep.modifications)
            # Values needed to create dictionary of ascore thresholds:
            sources = session.query(dbm.Source).all()
        # Create dictionary of ascore thresholds (`srcid_ptmid2threshold`):
        ptm_ids = [ x for x, in session.query(dbm.ModificationType.id).all() ] #Query list of tuples -> flat list of IDs.
        srcid_ptmid2threshold = dbm.generate_srcid_ptmid2threshold(sources,
                                                                   ptm_ids)
        # Process MS Modifications data for each associated Protein:
        commit_counter = 0
        for protein in prot_query:
            protein_ac = protein.ac
            sys.stdout.write( "Processing MS data for %s     \r"%(protein_ac) )
            sys.stdout.flush()
            # Get MS Modifications information, from related MS Peptides,
            # grouped by protein Position, Modification Type and Source:
            protmod2msmodsdata = defaultdict( lambda: {'ascores': list(), 
                                                       'celllines': set(), 
                                                       'msmods': set(), 
                                                       'significances': list()} )
            mspeptides = filter(mspeps_filter, protein.mspeptides) #Filter the MS Peptides to those that would participate in Protein Modification creation
            for mspeptide in mspeptides:
                nterm = mspeptide.is_ntermonly()
                # Format the MS Peptide sequence for searching:
                mspep_seq = mspeptide.seq
                if not nterm: # Normal or C-terminal peptides:
                    # - Normal peptide: search Everywhere in the protein
                    #   sequence:
                    search_tmplt = "(?=({0}))"
                    # - C-terminal peptide: search Only at the C-term of
                    #   protein sequence:
                    if mspep_seq[-1] in ctermaas_w_problems:
                        pos2allmods = mspeptide.pos2allmodifications #Get All PTMs, significant and non-significant, so PTM C-Term restrictions apply regardless of PTM significance.
                        cterm_mods = pos2allmods.get( len(mspep_seq), set() )
                        for cterm_mod in cterm_mods:
                            if cterm_mod.mod_type_id in ctermptmids_w_problems:
                                search_tmplt = "{0}$"
                                break
                else: # N-terminal Only peptides:
                    mspep_seq = "0" + mspep_seq #Allow the possibility that a N-term MS Peptide has the N-term Met processed
                    search_tmplt = "^{0}" #Search only in the N-term of protein sequence.
                search_pep_seq = formatseq4search(mspep_seq, aa_mapping, 
                                                  global_tmplt=search_tmplt)
                # Search for the MS Peptide in the protein sequence:
                for match in re.finditer(search_pep_seq, protein.seq):
                    if not nterm: # Normal or C-terminal peptides:
                        pos_shift = 0
                    else: # N-terminal only peptides:
                        pos_shift = len( match.group() ) - len(mspeptide.seq) #Position shift depending if the peptide is found in the protein adding or not a N-term M or X.
                    # Collect MS Modifications information:
                    for msmod in mspeptide.modifications:
                        gid = (match.start() + pos_shift + msmod.position, #Group by protein position,
                               msmod.mod_type, mspeptide.source)           #modification type and repository source
                        msmodsdata = protmod2msmodsdata[gid]
                        msmodsdata['ascores'].append(msmod.ascore)
                        msmodsdata['significances'].append(msmod.significant)
                        msmodsdata['celllines'].update(
                            exp.supraexp.cell_line for exp in mspeptide.experiments
                                                       )
                        msmodsdata['msmods'].add(msmod)
            # Summarize collected MS Modifications information into new
            # :class:`dbm.ProtModification` instances: #TO_DO?: should it check for an already existing PTM and update it, or continue creating always a new one?
            for (pos, modtype, source), msmodsdata in protmod2msmodsdata.items():
                maxascore = max( msmodsdata['ascores'] ) #Get maximum ascore.
                significant = max( msmodsdata['significances'] ) #Get maximum significance (True > False > None).
                # -Determine if ascore is significant for the Protein
                #  Modification, according to its Modification Type and Source
                #  (using the `srcid_ptmid2threshold` dictionary):
                #  X_NOTE: Now, this is probably redundant, because of significant = max( msmodsdata['significances'] ) (see above)
                if significant is None and maxascore is not None:
                    ascore_threshold = srcid_ptmid2threshold[source.id, modtype.id]
                    if ascore_threshold is not None:
                        significant = (maxascore >= ascore_threshold)
                # -Create and store a New Protein Modification:
                modification = dbm.ProtModification(protein_ac=protein_ac,
                                                    mod_type=modtype,
                                                    position=pos,
                                                    ascore=maxascore,
                                                    significant=significant,
                                                    experimental=True,
                                                    cell_lines=msmodsdata['celllines'],
                                                    evidences={evidence},
                                                    source=source,
                                                    mspepmodifications=msmodsdata['msmods'])
                session.add(modification)
                commit_counter += 1
            protein.mod_aas = protein.get_mod_aas() #Update number of modified amino acids
            # session.commit() from time to time:
            if commit_counter >= 5000:
                session.commit()
                commit_counter = 0
        session.commit()
    #
    print('\nEnd collection of Protein Modifications from database MS data.')

def relink_msdata2proteins(prot_db, source=None):
    """
    Link the imported MS Peptides to matching database Proteins
    (:func:`_mspeps2prots_mp`) and create summarized :class:`ProtModification`
    objects linked to these Proteins(:func:`_mspepmods2protmods`).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param Source source: an optional :class:`Source` instance to filter MS
    Peptides with.
    """
#     _mspeps2prots(prot_db, source) # Single thread/process version.
    _mspeps2prots_mp(prot_db, source) # Multi-thread/process version.
    _mspepmods2protmods(prot_db, source)


def import_lymphos2_msdata(prot_db, link_msdata=True, **kwargs):
    """
    Populate the database with LymPHOS2 Mass Spectrometry data.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param bool or None link_msdata: if True, :func:`relink_msdata2proteins`
    will be called after data importation to link the imported MS Peptides to
    matching database Proteins and create summarized :class:`ProtModification`
    objects linked to these Proteins. If False, only :func:`_mspepmods2protmods`
    will be called to create new summarized :class:`ProtModification` objects
    linked to already linked Proteins. And if None, no linking or extra
    processing will be done.
    """
    print('\nStarting the database population with LymPHOS MS data:')
    #
    with prot_db.session() as session:
        ldb_converter = filters.LymPHOSFetcher(lymphosdb_uri = LYMPHOSDB_URI,
                                               db_session = session)
        commit_counter = 0
        total_imports = 0
        link2ldb_prots = False if link_msdata or link_msdata is None else True #Decide if the converter should link MS peptides to Proteins
        for psm in ldb_converter.iter_all_psms(link2ldb_prots=link2ldb_prots):
            total_imports += 1
            sys.stdout.write( "Importing PSM: %s    (%s)    %s        \r"%(total_imports, commit_counter, psm.mspeptide.seq_w_allmods) )
            sys.stdout.flush()
            session.add(psm)
            # session.commit() from time to time:
            commit_counter += 1
            if commit_counter == 2500:
                session.commit()
                commit_counter = 0
        session.commit()
        session.refresh(ldb_converter.repository) #Force refresh of converter repository to avoid having it in a expired state ( because of a previous Session.commit() ) before the Session is closed
    #
    if link_msdata:
        relink_msdata2proteins(prot_db, ldb_converter.repository)
    elif link_msdata is not None:
        _mspepmods2protmods(prot_db, ldb_converter.repository)
    #
    print('\nEnd of the database population with LymPHOS MS data.')
    return ldb_converter.logger.log


def _import_TSV_tables(prot_db, tsv_fetcher, repo_file=None, commit_limit=2000,
                       link_msdata=True):
    """
    Basic function to import MS data using converters that are instances
    of :class:`TSVMSDataFetcher` subclasses.

    :param object tsv_fetcher: an instance of a :class:`TSVMSDataFetcher` subclass.
    :param object repo_file: zip file containing the repository data files. It
    should be either a string containing the path to the file, or a file-
    like object.
    :param int commit_limit: when imported number of PSMs added to the session
    reaches this limit, a commit to the database is done. Defaults to 2000.
    :param bool or None link_msdata: if True, :func:`relink_msdata2proteins`
    will be called after data importation to link the imported MS Peptides to
    matching database Proteins and create summarized :class:`ProtModification`
    objects linked to these Proteins. If False, only :func:`_mspepmods2protmods`
    will be called to create new summarized :class:`ProtModification` objects
    linked to already linked Proteins. And if None, no linking or extra
    processing will be done.
    """
    # First stage: import MS data using the specific importer object passed in 
    # :param:`tsv_fetcher`:
    with prot_db.session() as session:
        if repo_file:
            tsv_fetcher.repo_file = repo_file
        tsv_fetcher.db_session = session
        commit_counter = 0
        total_imports = 0
        for psm in tsv_fetcher.iter_all_psms():
            total_imports += 1
            sys.stdout.write( "Importing PSM: %s  (%s)  %s        \r"%(total_imports, commit_counter, psm.mspeptide.seq_w_mods) ) #DEBUG
            sys.stdout.flush()
            session.add(psm)
            # session.commit() from time to time:
            commit_counter += 1
            if commit_counter == commit_limit:
                session.commit()
                commit_counter = 0
        print("Total imports: %s"%total_imports) #DEBUG
        session.commit() #Final commit()
        #Force refresh of `tsv_fetcher` :attr:``repository`` to avoid having it
        #in a expired state ( because of a previous Session.commit() ) before
        #the Session is closed, so it can be used in the linking stage:
        source = tsv_fetcher.repository
        session.refresh(source)
        session.expunge(source)
    # Second stage: link imported MS Peptides to matching database Proteins and
    # create summarized Protein Modifications, as requested by :param:`link_msdata`:
    if link_msdata:
        relink_msdata2proteins(prot_db, source)
    elif link_msdata is not None:
        _mspepmods2protmods(prot_db, source)


def _TEST_import_TSV_tables(prot_db, tsv_fetcher, repo_file=None,  #TEST, DEBUG
                            commit_limit=2000, link_msdata=True):
    """
    Basic 'moc' function to Test importing MS data using converters that are
    instances of :class:`TSVMSDataFetcher` subclasses.

    :param object tsv_fetcher: an instance of a :class:`TSVMSDataFetcher` subclass.
    :param object repo_file: zip file containing the repository data files. It
    should be either a string containing the path to the file, or a file-
    like object.
    :param int commit_limit: when imported number of PSMs added to the session
    reaches this limit, a commit to the database is done. Defaults to 2000.
    :param bool or None link_msdata: if True, :func:`relink_msdata2proteins`
    will be called after data importation to link the imported MS Peptides to
    matching database Proteins and create summarized :class:`ProtModification`
    objects linked to these Proteins. If False, only :func:`_mspepmods2protmods`
    will be called to create new summarized :class:`ProtModification` objects
    linked to already linked Proteins. And if None, no linking or extra
    processing will be done.
    """
#     with prot_db.session_no_autoflush() as session: #Uncomment to DEBUG
    with prot_db.session() as session: #Comment to DEBUG
        if repo_file:
            tsv_fetcher.repo_file = repo_file
        tsv_fetcher.db_session = session
        commit_counter = 0
        total_imports = 0
        for psm in tsv_fetcher.iter_all_psms():
            total_imports += 1
            print( "Importing PSM: %s  (%s)  %s        \r"%(total_imports, commit_counter, psm.mspeptide.seq_w_mods) )
            session.add(psm)
            # session.commit() from time to time:
            commit_counter += 1
            if commit_counter == commit_limit:
                print( 'session.commit() here, but instead flush()...' )
                session.flush() #Instead of committing to the DB we flush() data temporally...
                commit_counter = 0
        print( 'session.commit() here, but instead roll-back()...' )
#         # Uncomment the following block to DEBUG:
#         session_new = sorted(session.new)
#         objcounter = Counter()
#         for obj in session_new:
#             objcounter[obj.__class__] += 1
#             print(obj)
#         print('\n- Object counts in ``session.new``:')
#         for Cls, number in objcounter.items():
#             print(str(Cls)+" :  ", number)
#         raise Exception('DEBUG')
#         # /End of DEBUG block.
        session.refresh(tsv_fetcher.repository) #Force refresh of `tsv_fetcher` :attr:``repository`` to avoid having it in a expired state ( because of a previous Session.rollback() ) before the Session is closed.
        session.expunge(tsv_fetcher.repository) #Detach of `tsv_fetcher` :attr:``repository`` from the current session.
        session.rollback() # Ensure RollBack of any possible changes.
    #
    if link_msdata:
        print( "Link ALL MS data would be done here...\n {0}\n {1}".format(prot_db.db_uri, tsv_fetcher.repository.name) )
    elif link_msdata is not None:
        print( "Link Only MSPepModificacions would be done here...\n {0}\n {1}".format(prot_db.db_uri, tsv_fetcher.repository.name) )


def import_Choudhary09_acetylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and acetylation information from
    zipped Choudhary et al. 2009 supplementary information tables.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and acetylation information from Choudhary et al. 2009:')
    #
    tsv_fetcher = filters.Choudhary09AcFetcher(CHOUDHARY09_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and acetylation information from Choudhary et al. 2009.')
    return tsv_fetcher.logger.log


def import_Svinkina15_acetylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and acetylation information from
    zipped Svinkina et al. 2015 supplementary information tables.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and acetylation information from Svinkina et al. 2015:')
    #
    tsv_fetcher = filters.Svinkina15AcFetcher(SVINKINA15_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and acetylation information from Svinkina et al. 2015.')
    return tsv_fetcher.logger.log


def import_Udeshi13_ubiquitinations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and ubiquitination information from
    zipped Udeshi et al. 2013 supplementary information tables.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and ubiquitination information from Udeshi et al. 2013:')
    #
    tsv_fetcher = filters.Udeshi13UbFetcher(UDESHI13_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and ubiquitination information from Udeshi et al. 2013.')
    return tsv_fetcher.logger.log


def import_Watts94_phosphorylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and tyrosin phosphorylation
    information from zipped Watts et al. 1994 table 1.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and phosphorylation information from Watts et al. 1994:')
    #
    tsv_fetcher = filters.Watts94PhosFetcher(WATTS94_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and phosphorylation information from Watts et al. 1994.')
    return tsv_fetcher.logger.log


def import_Salomon03_phosphorylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and tyrosin phosphorylation
    information from zipped Salomon et al. 2003 table 1.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and phosphorylation information from Salomon et al. 2003:')
    #
    tsv_fetcher = filters.Salomon03PhosFetcher(SALOMON03_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and phosphorylation information from Salomon et al. 2003.')
    return tsv_fetcher.logger.log


def import_Ficarro05_phosphorylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and tyrosin phosphorylation
    information from zipped Ficarro et al. 2005 table 3.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and phosphorylation information from Ficarro et al. 2005:')
    #
    tsv_fetcher = filters.Ficarro05PhosFetcher(FICARRO05_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and phosphorylation information Ficarro et al. 2005.')
    return tsv_fetcher.logger.log


def import_Tao05_phosphorylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and tyrosin phosphorylation
    information from zipped Tao et al. 2005 supplementary information table 1.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and phosphorylation information from Tao et al. 2005:')
    #
    tsv_fetcher = filters.Tao05PhosFetcher(TAO05_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and phosphorylation information Tao et al. 2005.')
    return tsv_fetcher.logger.log


def import_Mayya09_phosphorylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and phosphorylation information from
    zipped Mayya et al. 2009 supplementary information tables.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and phosphorylation information from Mayya et al. 2009:')
    #
    tsv_fetcher = filters.Mayya09PhosFetcher(MAYYA09_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and phosphorylation information from Mayya et al. 2009.')
    return tsv_fetcher.logger.log


def import_Nguyen16_phosphorylations(prot_db, **kwargs):
    """
    Populate the database with MS peptides and phosphorylations information from
    zipped Nguyen et al. 2016 supplementary information tables.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and phosphorylation information from Nguyen et al. 2016:')
    #
    tsv_fetcher = filters.Nguyen16PhosFetcher(NGUYEN16_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and phosphorylation information from Nguyen et al. 2016.')
    return tsv_fetcher.logger.log


def import_Mertins13_data(prot_db, **kwargs):
    """
    Populate the database with MS peptides and PTM information from
    zipped Mertins et al. 2013 supplementary table 1.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and PTM information from Mertins et al. 2013:')
    #
    tsv_fetcher = filters.Mertins13Fetcher(MERTINS13_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and PTM information from Mertins et al. 2013.')
    return tsv_fetcher.logger.log


def import_Jouy15_data(prot_db, **kwargs):
    """
    Populate the database with MS Peptides and PTM information from
    zipped Jouy et al. 2015 supplementary tables 1 and 2.

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and PTM information from Jouy et al. 2015:')
    #
    tsv_fetcher = filters.Jouy15PhosFetcher(JOUY17_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and PTM information from Jouy et al. 2015.')
    return tsv_fetcher.logger.log


def import_Beltejar17_data(prot_db, **kwargs):
    """
    Populate the database with MS Peptides and PTM information from
    zipped Beltejar et al. 2017 supplementary dataset (raw sheet).

    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    print('\nStarting the database importation of MS peptides and PTM information from Beltejar et al. 2017:')
    #
    tsv_fetcher = filters.Beltejar17PhosFetcher(BELTEJAR17_FILE)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides and PTM information from Beltejar et al. 2017.')
    return tsv_fetcher.logger.log


def import_PD_data(prot_db, filen=PD_TSV_FILE, **kwargs):
    """
    Populate the database with MS peptides and ubiquitination information from
    zipped Proteome Discoverer 1.4 exported TSV files.
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    :param object filen: zip file containing the PD1.4 TSV data files. It
    should be either a string containing the path to the file, or a file-
    like object.
    """
    print('\nStarting the database importation of MS peptides from PD1.4 files in %s:'%filen)
    #
    tsv_fetcher = filters.PDTSVFetcher(filen)
    _import_TSV_tables(prot_db, tsv_fetcher, **kwargs)
    #
    print('\nEnd of the database importation of MS peptides from PD1.4 files in %s .'%filen)
    return tsv_fetcher.logger.log


def populate_proteases(prot_db, **kwargs):
    """
    Populate the database with Protease instances from module `db_models`.
    
    :param DataBase prot_db: a target :class:`DataBase` instance.
    """
    with prot_db.session() as session:
        session.add_all( dbm.create_proteases() )
        session.commit()



def main(db_uri, arg2func, *args, **kwargs):
    args = list(args)
    # Create the database connection:
    prot_db = dbm.DataBase(db_uri)
    # If populating the database with proteins is solicited, recreate the
    # database and do it First of All:
    populate_prots = False
    if 'proteins' in args:
        populate_prots = 'proteins'
    elif 'reviewed-proteins' in args:
        populate_prots = 'reviewed-proteins'
    if populate_prots:
        # -Drop the existing database tables, and re-create them again:
        prot_db.create_tables(force_recreate=True)
        # -Populate the database with Proteases:
        populate_proteases(prot_db, **kwargs)
        # -Populate the database with all the UniProt Proteins in ``UNIPROT_FILE``:
        arg2func[populate_prots](prot_db, **kwargs)
        args.remove(populate_prots)
    #
    for arg in args:
        arg2func[arg](prot_db, **kwargs)
    #
    return 0



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
#     # DEBUG: Warnings as exceptions:
#     import warnings
#     warnings.filterwarnings("error")

    # Common OBO file Fetcher for functions that use it:
    COMMON_OBOFILEFETCHER = filters.OBOGOTermFetcher(OBO_GO_FILE)
    # CLI minimal argument parsing (X-NOTE: the OrderedDict is only for the help output):
    CLI_ARGUMENTS = OrderedDict((
        # Options to fill the database with data:
        ('proteins', functools.partial(populate_proteins,
                                       obofilefetcher=COMMON_OBOFILEFETCHER)),
        ('reviewed-proteins', functools.partial(populate_proteins,
                                                obofilefetcher=COMMON_OBOFILEFETCHER, 
                                                only_reviewed=True)),
        ('go-terms', functools.partial(populate_goterms,
                                       obofilefetcher=COMMON_OBOFILEFETCHER)),
        ('go-annotations', functools.partial(import_extra_goannotations,
                                             obofilefetcher=COMMON_OBOFILEFETCHER)),
        ('experiments', import_experiments),
        ('lymphos-ms-data', import_lymphos2_msdata),
        ('choudhary-acetylations', import_Choudhary09_acetylations),
        ('udeshi-ubiquitinations', import_Udeshi13_ubiquitinations),
        ('mayya-phosphorylations', import_Mayya09_phosphorylations),
        ('svinkina-acetylations', import_Svinkina15_acetylations),
        ('nguyen-phosphorylations', import_Nguyen16_phosphorylations),
        ('watts-phosphorylations', import_Watts94_phosphorylations),
        ('salomon-phosphorylations', import_Salomon03_phosphorylations),
        ('ficarro-phosphorylations', import_Ficarro05_phosphorylations),
        ('tao-phosphorylations', import_Tao05_phosphorylations),
        ('mertins-data', import_Mertins13_data),
        ('jouy-data', import_Jouy15_data),
        ('beltejar-data', import_Beltejar17_data),
        ('pd-data', import_PD_data),
        ('relink-msdata2proteins', relink_msdata2proteins),
        ('digestion-peptides', digest_proteins_core),
        ('digestion-peptides-ORM', digest_proteins_orm),
                                  ))
    allowed_cliargv = CLI_ARGUMENTS.keys()
    #
    PROG_NAME = os.path.basename( sys.argv[0] ) # Get script name.
    cli_argv = sys.argv[1:] # Exclude script name from the CLI arguments to process.
    if cli_argv and ':/' in cli_argv[0]: # First argument is a DB URI:
        DB_URI = cli_argv[0]
        cli_argv = cli_argv[1:]
    if not cli_argv: # No other arguments -> apply default arguments:
#         # Steps to re-create TCellXTalkDB from source data files:
#         cli_argv = ['reviewed-proteins', 'go-annotations'] #1st step in recreate DB.
#         cli_argv = ['proteins', 'go-annotations'] #Alternative 1st step in recreate DB.
#         cli_argv = ['lymphos-ms-data'] #2nd step.
#         cli_argv = ['choudhary-acetylations'] #3rd step.
#         cli_argv = ['svinkina-acetylations'] #4th step.
#         cli_argv = ['udeshi-ubiquitinations'] #5th step.
#         cli_argv = ['mayya-phosphorylations'] #6th step.
#         cli_argv = ['nguyen-phosphorylations'] #7th step.
#         cli_argv = ['watts-phosphorylations', 'salomon-phosphorylations', 
#                     'ficarro-phosphorylations', 'tao-phosphorylations'] #8th step.
#         cli_argv = ['mertins-data'] #9th step.
#        cli_argv = ['jouy-data'] #10th step.
        cli_argv = ['beltejar-data'] #11th step.
#         cli_argv = ['pd-data'] #12th step.
#         cli_argv = ['digestion-peptides'] #Optional 13th step.
#
#         cli_argv = [''] #TEST #DEBUG
    elif ( len(cli_argv) > len(allowed_cliargv) or
           set(cli_argv).difference(allowed_cliargv) ): # Other arguments but incorrect ones:
        sys.stderr.write('\nSyntax Error!\n\n')
        print('\nUsage:')
        print( " {0} [DataBase URI] {1}".format(
                            PROG_NAME,
                            " ".join('['+argv+']' for argv in allowed_cliargv)
                                                 ))
        print('\nExamples:')
        print( " {0}".format(PROG_NAME) )
        print( " {0} proteins".format(PROG_NAME) )
        print( " {0} {1}".format(PROG_NAME, DB_URI) )
        print( " {0} {1} proteins digestion-peptides".format(PROG_NAME, DB_URI) )
        sys.exit(2)
    #
    sys.exit( main(DB_URI, CLI_ARGUMENTS, *cli_argv) )
