# -*- coding: utf-8 -*-

from __future__ import division
from __future__ import print_function

"""
:synopsis:   Views for LymPHOS_UB_AC (TCellXTalk) project.

:created:    2015-04-13

:author:     Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2015-2017 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.6.2 dev'
__UPDATED__ = '2018-11-12'

#===============================================================================
# Imports
#===============================================================================
# Django imports:
# import django.views.static
from django.conf import settings
from django.http import (HttpResponse, HttpResponseRedirect, Http404, 
                         HttpResponseForbidden, 
                         StreamingHttpResponse, HttpResponseNotModified)
from django.core import urlresolvers
from django.views.static import (was_modified_since, http_date)

# Application imports:
from generic_views import (SectionView, SQLAlchemySectionView, 
                           FormSearchListView, SearchItemView, FormMixin, 
                           PaginatorMixin, ErrorsMixin, DinamicFile)
from forms import (ProteinSearchForm, MSPepSearchForm, ProteinDigestCombineForm, 
#                    DigPepSearchForm # Needed only by Deprecated :class:`DigPepSearchListView`
                   forms)
from digest_combine_func import DigestCombineMixin
#  JSONVisor imports:
from jvisor_fragmentor import get_ion_dict

# Python core imports:
import math
import mimetypes
import os
import stat
from collections import OrderedDict, defaultdict, Counter
from datetime import datetime

# Other imports:
import proteomics.pepprot_func as ppf
from sqlalchemy import or_ #, and_, func, distinct # Needed only by Deprecated :class:`DigPepSearchListView`
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import undefer, joinedload
from general.basic_func import chunker, save_dicts_as_csv
#  matplotlib imports:
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
# #  DEBUG:
# from guppy import hpy


#===============================================================================
# Function definitions
#===============================================================================
def add_html_mods2seqlst( seq_lst, pos2modifications,
                          mod_html_tmplt=('<span class="modtype_{id}" '
                                          'title="{name}">{aa}</span>') ):
    for biopos, modifications in pos2modifications.items():
        mod_types = {mod.mod_type for mod in modifications}
        if len(mod_types) == 1:
            mod_type = mod_types.pop()
            mod_id = mod_type.id
            mod_name = mod_type.extradata['full_name']
        else:
            mod_id = 0
            mod_name = ", ".join(mod_type.extradata['full_name'] 
                                 for mod_type in mod_types)
        pypos = biopos - 1 #Biological position -> Python position.
        seq_lst[pypos] = mod_html_tmplt.format( id=mod_id,
                                                name=mod_name,
                                                position=biopos,
                                                aa=seq_lst[pypos] )
    return seq_lst

# Monkey-patch ``settings.DBM.PepLikeMixin`` to add new properties 
# :property:`htmlseq` and :property:`sightmlseq`:
# - Add :func:`add_html_mods2seqlst` as new static method :method:`add_html_mods2seqlst`:
settings.DBM.PepLikeMixin.add_html_mods2seqlst = staticmethod(add_html_mods2seqlst)
# - New function to be used as a property getter:
def get_htmlseq(self):
    if not self._htmlseq:
        self._htmlseq = "".join( self.add_html_mods2seqlst(list(self.seq), 
                                                           self.pos2modifications) )
    return self._htmlseq
# - New function to be used as a property setter:
def set_htmlseq(self, htmlseq):
    self._htmlseq = htmlseq
# - Add attribute cache :attr:`_htmlseq`:
settings.DBM.PepLikeMixin._htmlseq = None
# - Add :func:`htmlseq` as new propwery :property:`htmlseq`:
settings.DBM.PepLikeMixin.htmlseq = property(get_htmlseq, set_htmlseq)
# - New function to be used as a property getter:
def get_sightmlseq(self):
    if not self._sightmlseq:
        self._sightmlseq = "".join( self.add_html_mods2seqlst(list(self.seq), 
                                                              self.pos2sigmodifications) )
    return self._sightmlseq
# - New function to be used as a property setter:
def set_sightmlseq(self, sightmlseq):
    self._sightmlseq = sightmlseq
# - Add attribute cache :attr:`_sightmlseq`:
settings.DBM.PepLikeMixin._sightmlseq = None
# - Add :func:`htmlseq` as new propwery :property:`sightmlseq`:
settings.DBM.PepLikeMixin.sightmlseq = property(get_sightmlseq, set_sightmlseq)


#===============================================================================
# From classes "re-definitions" do add Database to :attr:`db`
#===============================================================================
class ProtDBProteinDigestCombineForm(ProteinDigestCombineForm):
    db = settings.PROT_DB


# class ProtDBDigPepSearchForm(DigPepSearchForm): # Needed only by Deprecated :class:`DigPepSearchListView`
#     db = settings.PROT_DB


class ProtDBMSPepSearchForm(MSPepSearchForm):
    db = settings.PROT_DB


#===============================================================================
# View classes definitions
#===============================================================================
class SearchSectionView(SectionView):
    """
    Search Section web-page View with multiple forms.
    
    :attr iterable form_classes: the form classes to put in the view.
    """
    template_name = 'search_section.html' #Section template file to use.
    title = 'Search' #Title of the section web page.
    section = 'search' #Section name of this section web page.
    
#     form_classes = (ProteinSearchForm, ProtDBDigPepSearchForm, 
#                     ProtDBMSPepSearchForm)
    form_classes = (ProteinSearchForm, ProtDBMSPepSearchForm)
    
    def get_context_data(self, **kwargs):
        """
        :returns dict : a context dictionary to render the template, containing
        a 'forms' name-space.
        """
        context = super(SearchSectionView, self).get_context_data(**kwargs)
        #
        context['forms'] = (FormCls() for FormCls in self.form_classes)
        #
        return context


class ProteinSearchListView(FormSearchListView):
    """
    Searches DataBase for Proteins, and generate a result list paginated and
    ordered.
    """
    template_name = 'protein_results.html' #Section template file to use.
    title = 'Protein Search Results' #Title of the section web page.
    section = 'search' #Section name of this section web page.
    
    form_class = ProteinSearchForm #The form class to instantiate.
    form_search_field = None #Form field with the search string to use: 'protein_text' or 'multiprotein_acs' 
    form_sorttags_field = 'sort_tags' #Form field with the fields to order the query by.
    
    table_headers = ( {'html': 'Proteins',
                       'title': 'Protein Accession Number',
                       'width': '8%',
                       'sort_tag': 'ac',
                       'orderby_tmplt': "ac {0}"},
                      {'html': 'Protein Names',
                       'title': 'Different Protein Names found',
                       'width': '54%',
                       'sort_tag': 'prots',
                       'orderby_tmplt': "proteins.name {0}"},
                      {'html': 'Chr.',
                       'title': 'Chromosome where the gene for this protein is',
                       'width': '4%',
                       'sort_tag': 'chr',
                       'orderby_tmplt': "CAST(genes.chr AS UNSIGNED) {0}, genes.chr {0}"}, #X_NOTE: http://www.mysqltutorial.org/mysql-natural-sorting/#crayon-5b87cc8488208830533627
                      {'html': 'Modified AAs',
                       'title': 'Number of modified amino acids',
                       'width': '8%',
                       'sort_tag': 'mod_aas',
                       'orderby_tmplt': "mod_aas {0}"},
                      {'html': 'Length',
                       'title': 'Sequence length',
                       'width': '6%',
                       'sort_tag': 'length',
                       'orderby_tmplt': "length {0}"},
                     )
    
    itemsxpage = 50 #Default items per page for the Paginator object.
    allowed_itemsxpage = (25, 50, 100, 200) #Values allowed for the :attr:`itemsxpage`.
    
    db = settings.PROT_DB #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do te searches.
    
    strmapping4search = {'*': '%', '?': '_'} #Mapping table to use to format the search string for queries.
    
    def _get_results_context4protein_text(self, session, **kwargs):
        """
        Do the query for Proteins, but only for reviewed/curated ones with at 
        least one modified AA.
        
        By now (2017-07-05) no decoy proteins are in the Database, so it's 
        still not necessary to filter them ( .filter(Protein.is_decoy==False) ). 
        
        :param Session session: a SQLAlchemy DataBase session.
        """
        # Can be user to replace :method:`get_results_context`.
        protein_text = self.search_value
        Protein = settings.DBM.Protein
        results = session.query(Protein).join(Protein.gene)\
                         .filter(Protein.reviewed == True, 
                                 Protein.mod_aas > 0)\
                         .filter( or_(Protein.ac.like(protein_text),
                                      Protein.name.like(protein_text),
                                      Protein.id_in_source_repository.like(protein_text)) )\
                         .order_by(*self.orderby_fields)
        #
        return { 'paginated_results': self.paginate_results( results,
                                                             reenter_form=kwargs['form'] ) }
    
    def _get_results_context4multiprotein_acs(self, session, **kwargs):
        """
        Do the query for Proteins in the Database contained in user supplied
        list of ACcession numbers.
        
        :param Session session: a SQLAlchemy DataBase session.
        """
        # Can be user to replace :method:`get_results_context`.
        multiprotein_acs = self.search_value
        Protein = settings.DBM.Protein
        results = session.query(Protein).join(Protein.gene)\
                         .filter( Protein.ac.in_(multiprotein_acs) )\
                         .order_by(*self.orderby_fields)
        #
        return { 'paginated_results': self.paginate_results( results,
                                                             reenter_form=kwargs['form'] ) }
    
    def get_results_context(self, session, **kwargs):
        """
        Avoids default behavior of :super-class:`FormSearchListView` method.
        Should be set to :method:`_get_results_context4protein_text` or 
        to :method:`_get_results_context4multiprotein_acs`
        """
        raise NotImplementedError()
    
    def form_valid(self, form):
        """
        Deal with different search options ('protein_text' search and
        'multiprotein_acs' search).
        """
        # Overwrites super-class :method:`FormSearchListView.form_valid`.
        form_search_field = self.form_search_field = form.search_field_filled
        #
        if form_search_field == 'protein_text':
            # Sets :method:`get_results_context`:
            self.get_results_context = self._get_results_context4protein_text
            # Define search parameters from input data:
            return super(ProteinSearchListView, self).form_valid(form)
        #
        if form_search_field == 'multiprotein_acs':
            # Sets :method:`get_results_context`:
            self.get_results_context = self._get_results_context4multiprotein_acs
            # Define search parameters from input data:
            self.search_value = form.cleaned_data[form_search_field]
            self.orderby_fields = form.cleaned_data[self.form_sorttags_field].split(',')
            self.page = self.request.POST.get('page', '1') #Make sure requested page exists. Otherwise, deliver first page
            self.itemsxpage = self.request.POST.get('itemsxpage', self.itemsxpage) #Get requested items per page or use the default one
            return self.get_db_response(form=form, **self.kwargs) #Direct call to method, bypassing :method:`form_valid` from super :class:`FormSearchListView`
    
    def get(self, request, *args, **kwargs):
        """
        Redirects to the 'search' page.
        """
        # Overwrites super-class :method:`FormMixim.get`.
        return HttpResponseRedirect( urlresolvers.reverse_lazy('search') )


class ProteinView(SearchItemView):
    """
    Searches DataBase for a Protein, and shows Protein information.
    """
    template_name = 'protein_view.html' #Section template file to use.
    title = 'Protein View' #Title of the section web page.
    section = 'search' #Section name of this section web page.
    
    db = settings.PROT_DB #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do the searches.
    
    get_search_field = 0 #Integer for positional arguments, string for keyword arguments.
    
    @staticmethod
    def add_hdptmregions2seqlst(seq_lst, iter_hd_ptm_regions):
        for start, end in iter_hd_ptm_regions:
            pystart = start - 1
            pyend = end - 1
            seq_lst[pystart] = '<span class="hd_ptm_region">' + seq_lst[pystart]
            seq_lst[pyend] = seq_lst[pyend] + '</span>'
        #
        return seq_lst
    
    @staticmethod
    def seq2html_blocks(seq_lst, aa_x_block=10, blocks_x_line=8):
        """
        Formats an iterable of aminoacids (AA) as an html string composed of
        numbered lines of AA grouped in blocks.

        list('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') ->
        01   AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA AAAAAAAAAA
        41   AAAAAAAAAA

        :param iterable seq_lst: an iterable of strings. Usually a list of
        single letter aminoacids.
        :param int aa_x_block: number of elements of each line block.
        :param int blocks_x_line: number of blocks composing each line.

        :return tuple : an html string composed of lines of AA grouped in
        blocks, and a html formated string with the numbers corresponding to
        the positions of the first AA of each line.
        """
        aa_x_line = blocks_x_line * aa_x_block
        # Get an html sequence composed of lines of `aa_x_line` AA grouped in
        # blocks of `aa_x_block` AA:
        lines = list()
        for row in chunker(seq_lst, aa_x_line):
            blocks_row = "&nbsp;".join( "".join(block)
                                        for block in chunker(row, aa_x_block) )
            lines.append( blocks_row )
        htmlseq = "<br/>".join(lines)
        # Get the numbers of the first AA of each row formated as html:
        seq_length = len(seq_lst)
        seqnums = range(1, seq_length + 1, aa_x_line)
        zeros = int( math.log10(seq_length) ) + 1
        html_seqnums = "<br/>".join("{1:0>{0}d}".format(zeros, d) for d in seqnums)
        #
        return htmlseq, html_seqnums
    
    def get_groupedpos2ptms(self, pos2ptms, hd_ptm_regions, length):
        # Fill intervals between High Density PTM regions:
        all_regions = OrderedDict()
        new_start = 1
        for next_interval, hd_ptm_region in sorted( hd_ptm_regions.items() ):
            new_end = next_interval[0] - 1 #new interval end position = next hd_ptm interval start position - 1
            if new_start <= new_end: #New interval is a real interval of at least 1 position:
                all_regions[ (new_start, new_end) ] = None #Add interval with no region associated
#            htmlseq_lst = add_html_mods2seqlst(list(hd_ptm_region.seq),
#                                               hd_ptm_region.pos2modifications)
#            hd_ptm_region.htmlseq = "".join(htmlseq_lst)
            all_regions[next_interval] = hd_ptm_region #Add next hd_ptm interval with its hd_ptm_region
            new_start = next_interval[1] + 1 #next new interval start position = hd_ptm interval end position + 1
        if new_start <= length: #Add a last interval to the end of the protein if previous hd_ptm interval doen't get to the end:
            all_regions[ (new_start, length) ] = None
        # Group pos2ptms in regions:
        groupedpos2ptms = defaultdict( lambda: {'region': '', 
                                                'pos2ptms': list(), 
                                                'html_class': ''} )
        for pos, ptms in pos2ptms.items():
            for (start, end), region in all_regions.items():
                if start <= pos <= end:
                    groupedpos2ptms[ (start, end) ]['region'] = region
                    groupedpos2ptms[ (start, end) ]['pos2ptms'].append( (pos, ptms) )
                    if region is not None:
                        groupedpos2ptms[ (start, end) ]['html_class'] = 'hd_ptm_bckgrnd'
                    break
        #
        return OrderedDict(sorted( groupedpos2ptms.items() ))
    
    def get_results_context(self, session, **kwargs):
        # Do the query to get a protein:
        Protein = settings.DBM.Protein
        try:
            protein = session.query(Protein)\
                             .options( undefer('seq'),
                                       joinedload('_db_modifications')\
                                       .joinedload('mspepmodifications')\
                                        .joinedload('mspeptide')
                                         .joinedload('source'), 
                                       joinedload('gene') )\
                             .filter(Protein.ac == self.search_value)\
                             .one()
        except NoResultFound as e:
            self.errors.put_with_key('DataBase Search', e)
            return {'protein': None}
        #
        # Fix for django template problems when iterating a defaultdict:
        protein.pos2ptms = pos2ptms = OrderedDict(sorted( protein.pos2modifications.items() ))
        # Format protein sequence as HTML in a new :attr:`htmlseq` added to the
        # protein:
        seq_lst = list(protein.seq)
        #1.-Format PTMs
        mod_html_tmplt = '<span class="modtype_{id}" title="{position} {name}">'\
                         '<a class="mod" href="#pos_{position}">{aa}</a></span>'
        seq_lst = add_html_mods2seqlst(seq_lst, pos2ptms, mod_html_tmplt)
        #2.-Format High Density PTM regions
        hd_ptm_regions = protein.hd_ptm_regions
        seq_lst = self.add_hdptmregions2seqlst( seq_lst, hd_ptm_regions.keys() )
        #3.-Format sequence in blocks and lines
        protein.htmlseq, protein.html_seqnums = self.seq2html_blocks(seq_lst)
        # Format supporting MS Peptides sequences as HTML in a new
        # :attr:`htmlseq` added to each MS Peptide; and collect some data about
        # Protein PTMs and matching co-modified MS Peptides:
        ptmname2count = Counter() # PTM name -> number of that PTM.
        comodmspepscount = 0 # Number of experimental co-modified MS Peptides matching this Protein.
        for ptms in pos2ptms.values():
            ptmnames4pos = set() # PTM full names for this position.
            for ptm in ptms:
                ptmnames4pos.add(ptm.mod_type.full_name)
                ptm.mspeps_cache = mspeps_cache = list() #Stores the MS Peptides associated to this ProtModification through the MSModifications associated to this later one. X_NOTE: It helps to speed up template rendering.
                for msmod in sorted(ptm.mspepmodifications, key=lambda m: m.mspeptide.id):
                    if not msmod.significant: continue # Avoid processing not significant MS Modifications.
                    mspep = msmod.mspeptide
                    mspeps_cache.append(mspep)
                    if not hasattr(mspep, 'otherprots'): # Avoid re-processing already processed MS Peptides:
#                        htmlseq_lst = add_html_mods2seqlst(list(mspep.seq),
#                                                           mspep.pos2sigmodifications)
#                        mspep.htmlseq = "".join(htmlseq_lst)
                        # Check if the MS Peptide matches other Proteins, and
                        # store in new :attr:`otherprots` (number of other
                        # matching proteins, isoforms or not) and
                        # :attr:`otherprotsonlyiso` (are those other proteins
                        # only isoforms of current protein?):
                        otherprots_count = len( {prot.ac 
                                                 for prot in mspep.proteins} ) - 1
                        otherprotsnoiso_count = len( {prot.ac_noisoform 
                                                      for prot in mspep.proteins} ) - 1
                        mspep.otherprots = 'yes' if otherprots_count else 'no'
                        mspep.otherprotsonlyiso = 'yes' if (otherprots_count and not otherprotsnoiso_count) else 'no'
                        # Check if this MS Peptide is an experimental co-modified
                        # Peptide matching the protein:
                        if mspep.is_comodiffied(): #The MS Peptide has more than 1 PTM (of the same or different type):
                            comodmspepscount += 1 #Count it as co-modified for this Protein
            ptmname2count.update(ptmnames4pos) # Collect the count of PTM names in this position.
        # Add PTM name -> number of that PTM, and total number of PTMs to the
        # Protein as new :attr:`ptmname_count` and new :attr:`totalptmcount`:
        protein.ptmname_count = ptmname2count.items()
        protein.totalptmcount = sum( ptmname2count.values() )
        # Add the number of co-modified MS Peptides (with 2 or more PTMs)
        # matching this protein, as a new :attr:`comodmspepscount`:
        protein.comodmspepscount = comodmspepscount
        # Create a data structure with `pos2ptms` grouped by high density PTM
        # regions:
        groupedpos2ptms = self.get_groupedpos2ptms(pos2ptms, hd_ptm_regions, 
                                                   protein.length)
        return { 'protein': protein, 
                 'groupedpos2ptms': groupedpos2ptms.values() }
    
    def get_context_data(self, session, **kwargs):
        context = super(ProteinView, self).get_context_data(session, **kwargs)
        # Adds the form for protein digestion to the context:
        if context['protein']: #Only if there is some protein to digest:
            initial_values = {'protein_ac': context['protein'].ac}
            context['digest_form'] = ProtDBProteinDigestCombineForm(initial=initial_values)
        return context


class ProteinDigestCombineView(DigestCombineMixin, FormMixin, SQLAlchemySectionView):
    """
    Shows Protein Digestion Peptides information. And allows to download 
    Combinatorial Digestion Peptides as a TSV file.
    """
    template_name = 'protein_digest.html' #Section template file to use.
    title = 'Protein Digestion View' #Title of the section web page.
    section = 'search' #Section name of this section web page.
    
    form_class = ProtDBProteinDigestCombineForm #The form class to instantiate.
    
    db = settings.PROT_DB #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do the searches.
    
    get_search_field = 0 #Integer for positional arguments, string for keyword arguments.
    
    def __init__(self, **kwargs):
        self.protein_ac = None #ACcesion number of the protein to digest
        # Digestion attributes Default values (keep in sync with initial values 
        # of the fields in forms.ProteinDigestCombineForm):
        self.protease_id = 1 #ID of the protease to use (ID=1 -> 'Trypsin with exceptions')
        self.missing_cleavage_lvls = 0 #Maximum extra missed cleavages level to generate
        self.min_pep_length = 7 #Minimum digestion peptide length
        self.max_pep_length = 25 #Maximum digestion peptide length
#         self.max_missing_cleavages = 3 #Number of allowed missed cleavages
        self.exclusive_ptms = set() #Mandatory PTM IDs
        self.exclusiveptm_names = 'Any' #Exclusive PTM full names
        self.min_total_ptms = 1 #Minimum number of PTMs/peptide
        self.max_total_ptms = 3 #Maximum number of PTMs/peptide
        self.max_equal_ptms = 2 #Maximum number of PTMs of the same type/peptide
        # Default action:
#         self.action = 'digest' #Only Digest the protein
        self.action = 'digest_combine' #Digest the protein and Combine the PTMs
        #
        super(ProteinDigestCombineView, self).__init__(**kwargs)
    
    def _protein_protease(self, session):
        # Do the query to get the Protein to digest:
        Protein = settings.DBM.Protein
        protein = session.query(Protein)\
                         .options( undefer('seq'),
                                   joinedload('_db_modifications')\
                                   .joinedload('mod_type') )\
                         .get(self.protein_ac)
        if not protein:
            self.errors.put_with_key( 'DataBase Search', 
                                      "Protein '{}' not found in Database".format(self.protein_ac) )
            return None
        # Do the query to get the Protease to use for digestion:
        Protease = settings.DBM.Protease
        protease = session.query(Protease).get(self.protease_id)
        #Avoid MySQL error 1205 ('Lock wait timeout exceeded; try
        #restarting transaction') when more than one user is digesting-
        #combine a protein. This is due to the protease being updated while
        #still linked to a session (changes to its :attr:`target_seq_re` in
        #:method:`_filtered_combdigpeps`) and thus being locked by MySQL
        #avoiding re-reading it from another process/user:
#         session.expunge(protease)
        if not protease:
            self.errors.put_with_key( 'DataBase Search', 
                                      "Protease ID '{}' not found in Database".format(self.protease_id) )
            return None
        #
        session.expunge_all()
        session.rollback()
        #
        return protein, protease
    
    def _consesnsus_digpeps4combination(self, protein, protease):
        # Get basic data for the fully digested peptides from protein:
        fulldig_protodigpeps = list( protease.iter_fulldigest_basicdata(protein) )
        # Generate Digestion Peptides:
        # - Frequently accessed variables (some speed optimization):
        prot_seq = protein.seq
        PepLikeMixin = settings.DBM.PepLikeMixin
        DigPepNoDB = settings.DBM.DigestionPeptideNoDB
        min_length = self.min_pep_length
        mandatory_modids = self.exclusive_ptms
        # - Get basic data for the digested peptides with 1 extra missed 
        #   cleavage (this is two contiguous full digested peptides):
        dpdata = protease.iter_digpepsbasicdata4mssclvglvl(self.missing_cleavage_lvls+1, 
                                                           fulldig_protodigpeps)
        for start_pos, end_pos, mssng_clvgs in dpdata:
            # - Pre-Filter by Minimum Length:
            if (end_pos - start_pos + 1) < min_length:
                continue #Skip this Digestion Peptide.
            # - Pre-Filter by presence of Mandatory PTMs (not exclusively, 
            #   so allows other extra PTMs):
            alldpmods = set( PepLikeMixin._iter_modifications_slice(protein, 
                                                                    start_pos, 
                                                                    end_pos) )
            if not mandatory_modids.issubset( {mod.mod_type_id 
                                               for mod in alldpmods 
                                               if mod.significant} ):
                continue #Skip this Digestion Peptide.
            # - Generate and yield Consensus Digestion Peptide:
            yield DigPepNoDB(protein=protein, #TEST!!
                             prot_start=start_pos, 
                             prot_end=end_pos, 
                             seq=prot_seq[start_pos-1:end_pos], 
                             consensus=True,
                             consensus_peptide=None, 
                             digestion_condition=None,
                             missing_cleavages=mssng_clvgs, 
                             _db_modifications=alldpmods)
    
    def _filtered_combdigpeps(self, session, consensus_digpeps, protease, 
                              consensus_peps_cache=None, redig_peps_cache=None, 
                              log_filen=None):
        """
        A method to generate Combinatorial Re-Digested Peptides from Consensus 
        Digestion Peptides, and Filter them.
        X_NOTE: this method derives from :func:`_iter_filter_digpeps` of 
        module `digestion_peptides_summaries.py` version 0.11 (2018-03-21) at 
        revision 311 (cac25ca2f4c1).
                
        :param Session session: a SQLAlchemy :class:`Session` instance.
        :param iterable consensus_digpeps: an iterable of Consensus Digestion 
        Peptides. Usually a SQLAlchemy :class:`Query` instance.
        :parma Protease protease: Protease used to generate the Consensus 
        Digestion Peptides, and needed to re-digest Combinatorial Peptides.
        :param set consensus_peps_cache: a cache of processed Consensus Digestion
        Peptides Sequences. A more global cache set can be passed, or (if None, 
        the default) a new one will be created.
        :param set redig_peps_cache: a cache of generated Combinatorial re-digested
        Peptides Sequences. A more global cache set can be passed, or (if None, 
        the default) a new one will be created.
        :param str log_filen: a file-name to write a log of the generated 
        peptides to, if supplied. Defaults to None (no log-file output).
        
        :return generator : yields filtered Re-Digested Combinatorial 
        :class:`DigestionPeptide` instances.
        """
        # Parameters check:
        if consensus_peps_cache is None:
            consensus_peps_cache = set()
        if redig_peps_cache is None:
            redig_peps_cache = set()
        if log_filen is None:
            save_log = False
            log_file = None
        else:
            save_log = True
            log_file = open(log_filen, "wb")
            # Write Log file headers:
            log_file.write("\t".join( ("Type", "ID/Mark", "Sequence with PTMs", 
                                       "Protein AC", "Protein position (start-end)", 
                                       "Sequence", "Modified AAs", 
                                       "Missing cleavages") ) + "\n")
        # Frequently accessed attributes as variables (some speed optimization):
        missing_cleavage_lvls = self.missing_cleavage_lvls
        min_length = self.min_pep_length
        max_length = self.max_pep_length
#         max_miss_clvgs = self.max_missing_cleavages
        min_mods = self.min_total_ptms
        max_mods = self.max_total_ptms
        max_equalmods = self.max_equal_ptms
        exclusive_modids = self.exclusive_ptms
        DigPepNoDB = settings.DBM.DigestionPeptideNoDB
        mandatory_modids4comb = { (mod_id,) for mod_id in exclusive_modids } #Format it according to the requirements of :method:``DigestionPeptide.iter_combinatorial_peptides``
        # Check if Protease is unable to cut at N-term or C-term or None, and 
        # generate alternative 'target sequence' regex to allow cutting them at 
        # peptide re-digestion step: 
        orig_targetseq_re = targetseq_re4firstpep = targetseq_re4interpep = targetseq_re4lastpep = protease.target_seq_re
        if "(?<!^)" in orig_targetseq_re: # Protease can NOT cut at N-term:
            targetseq_re4lastpep = orig_targetseq_re.replace("(?<!^)", "") #Modified regex to allow cutting at N-term.
            targetseq_re4interpep = targetseq_re4lastpep                   #
        if "(?!$)" in orig_targetseq_re: # Protease can NOT cut at C-term:
            targetseq_re4firstpep = orig_targetseq_re.replace("(?!$)", "") #Modified regex to allow cutting at C-term.
            targetseq_re4interpep = targetseq_re4lastpep.replace("(?!$)", "") #Modified regex to allow cutting at both N-term and C-term.
        #
#         session_updates = 0 #Used to control when to flush sessions instances to database for freeing some memory.
#         VOID_DB_OBJ = settings.DBM.SourceType()
        for cons_digpep in consensus_digpeps:
            # Check for previously processed Consensus Digestion Peptide, and avoid further processing:
            digpep_seqwmods = cons_digpep.seq_w_mods
            if digpep_seqwmods in consensus_peps_cache:
                continue # Skip this Consensus Digestion Peptide.
            consensus_peps_cache.add(digpep_seqwmods) #Add to cache...
            # Save to Log file, if provided:
            if save_log:
                log_file.write('\n')
                log_file.write("\t".join(( "Consensus", str(cons_digpep.id), 
                                            digpep_seqwmods, cons_digpep.protein.ac, 
                                            "%s - %s"%(cons_digpep.prot_start, 
                                                       cons_digpep.prot_end), 
                                            cons_digpep.seq, str(cons_digpep.mod_aas),
                                            str(cons_digpep.missing_cleavages) )) + "\n")
            # Chose Protease N-term/C-term cutting action depending on Consensus Digestion Peptide position inside the Protein:
            if cons_digpep.prot_start == 1: # Consensus Digestion Peptide at Start of the protein:
                protease.target_seq_re = targetseq_re4firstpep
            elif cons_digpep.prot_end == cons_digpep.protein.length: # Consensus Digestion Peptide at End of the protein:
                protease.target_seq_re = targetseq_re4lastpep
            else: # Consensus Digestion Peptide is a Intermediate peptide:
                protease.target_seq_re = targetseq_re4interpep
            # Generate combinations of the peptide PTMs (Combinatorial Peptides):
#             last_combdigpep = VOID_DB_OBJ
#             session.add(last_combdigpep)
            for comb_digpep in cons_digpep.iter_combinatorial_peptides(mandatory_modids=mandatory_modids4comb, 
                                                                       CombPepCls=DigPepNoDB): #TEST
#                 session.expunge(last_combdigpep) #Free some session memory.
#                 last_combdigpep = comb_digpep
#                 session_updates += 1
                # Save to Log file, if provided:
                if save_log:
                    log_file.flush()
                    log_file.write('\n')
                    log_file.write("\t".join(( "  Combinatorial", "*", 
                                                comb_digpep.seq_w_mods, 
                                                comb_digpep.protein.ac, 
                                                "%s - %s"%(comb_digpep.prot_start, 
                                                           comb_digpep.prot_end), 
                                                comb_digpep.seq, 
                                                str(comb_digpep.mod_aas),
                                                str(comb_digpep.missing_cleavages) )) + "\n")
                # Try to re-digest the Combinatorial Peptide:
                redig_digpeps = protease.digest(comb_digpep, missing_cleavage_lvls, 
                                                min_length, max_length, #Re-digested peptides are filtered here by length...
                                                create_digestion_condition=False, 
                                                DigPepCls=DigPepNoDB) #TEST
                # Post-Filter Combinatorial Re-digested Peptides:
#                 last_combredigpep = VOID_DB_OBJ
#                 session.add(last_combredigpep)
                for redig_digpep in redig_digpeps:
#                     session.expunge(last_combredigpep) #Free some session memory.
#                     last_combredigpep = redig_digpep
#                     session_updates += 1
                    # Check for previously seen Re-digested Peptides, and avoid further processing and yielding:
                    redigpep_seqwmods = redig_digpep.seq_w_mods
#                     redigpep_fingerprint = hash(redigpep_seqwmods) #TEST: Try fixing memory leaks..
                    if redigpep_seqwmods in redig_peps_cache:
                        continue # Skip this Re-digested Peptide.
                    redig_peps_cache.add(redigpep_seqwmods) #Add to cache...
                    # Save to Log file, if provided:
                    if save_log:
                        data4save = ( redigpep_seqwmods, redig_digpep.protein.ac,
                                      "%s - %s"%(redig_digpep.prot_start, 
                                                 redig_digpep.prot_end), 
                                      redig_digpep.seq, str(redig_digpep.mod_aas), 
                                      str(redig_digpep.missing_cleavages) )
                        log_file.write("\t".join( ("    Re-digested", "-") + 
                                                   data4save ) + "\n")
#                     # -Filter by total number of missing cleavages:
#                     if redig_digpep.missing_cleavages > max_miss_clvgs: #Total number of missing cleavages is greater than the maximum allowed:
#                         continue # Skip this Re-digested Peptide.
                    # -Filter by presence of exclusive modifications: #X-NOTE: if no exclusive modifications (`exclusive_modids` is None) unmodified peptides are not filtered out!
                    modid2positions = redig_digpep.modid2positions
                    if exclusive_modids and set( modid2positions.keys() ) != exclusive_modids: #The modifications of the Combinatorial Re-digested Peptide differ from the exclusive PTMs:
                        continue # Skip this Re-digested Peptide.
                    # -Filter by minimum and maximum number of total PTMs:
                    combredigpep_nposs = map( len, modid2positions.values() ) or [0] #X-NOTE: [0] will allow unmodified peptides (see note above) to continue passing filters.
                    if not (min_mods <= sum(combredigpep_nposs) <= max_mods): #Number of total PTMs (modified AAs) in the combination of modifications is out of the allowed limits:
                        continue # Skip this Re-digested Peptide.
                    # -Filter by maximum repetitions of the same PTM:
                    if max(combredigpep_nposs) > max_equalmods: #Any number of each PTM > maximum allowed.
                        continue # Skip this Re-digested Peptide.
                    # Save to Log file, if provided:
                    if save_log:
                        log_file.write("\t".join( ("      Yielded", ">") + 
                                                   data4save ) + "\n")
                    #
                    yield redig_digpep
                    #
#                 session.expunge(last_combredigpep) #Free some session memory.
#                 if session_updates > 150000: #Free some session memory flushing instances to database:
#                     session.flush()
#                     session_updates = 0
#                     # Save to Log file, if provided:
#                     if save_log:
#                         log_file.write("Database Session Flush()\n")
#             session.expunge(last_combdigpep) #Free some session memory.
        #
        if save_log:
            log_file.close()
    
    def get_digest_resultscontext(self, session, **kwargs):
        # DEPRECATED!!
        # Get Protein to digest and Protease to use:
        protein_protease = self._protein_protease(session)
        if not protein_protease:
            return dict()
        protein, protease = protein_protease
        # Get Consensus Digestion Peptides (pre-filtered by size):
        dig_peps = protease.digest(protein, self.missing_cleavage_lvls,
                                   min_length=self.min_pep_length,
                                   max_length=self.max_pep_length, 
                                   create_digestion_condition=False)
        # Filter Consensus Digestion Peptides: 
#         max_miss_clvgs = self.max_missing_cleavages
        exclusive_modids = self.exclusive_ptms
        min_mods = self.min_total_ptms
        max_mods = self.max_total_ptms
        max_equalmods = self.max_equal_ptms
        filtered_digpeps = list()
        for dig_pep in dig_peps:
#             # -By maximum number of missing cleavages:
#             if dig_pep.missing_cleavages > max_miss_clvgs:
#                 continue # Skip this Digestion Peptide.
            # -By presence only of the exclusive PTMs:
            modid2positions = dig_pep.modid2positions
            if exclusive_modids and set( dig_pep.modid2positions.keys() ) != exclusive_modids: #The modifications of the Digested Peptide do not exactly match the exclusive PTMs:
                continue # Skip this Digestion Peptide.
            # -By minimum and maximum number of total PTMs:
            digpep_nposs = map( len, modid2positions.values() ) or [0] #X-NOTE: [0] will allow unmodified peptides to continue passing filters.
            if not (min_mods <= sum(digpep_nposs) <= max_mods): #Number of total PTMs (modified AAs) in the combination of modifications is out of allowed limits:
                continue # Skip this Digestion Peptide.
            # -By maximum repetitions of the same PTM:
            if max(digpep_nposs) > max_equalmods: #Any number of each PTM > maximum allowed.
                continue # Skip this Digestion Peptide.
            filtered_digpeps.append(dig_pep)
        dig_peps = filtered_digpeps
        # Sort digestion peptides by start position in protein:
        dig_peps = sorted( dig_peps, 
                           key=lambda dig_pep: (dig_pep.prot_start, dig_pep.prot_end) )
        # Get matching MS peptides, and add html formated sequence to the digestion peptides:
        MSPeptide = settings.DBM.MSPeptide
        dig_pep2mspeps = OrderedDict()
        for dig_pep in dig_peps:
#            dig_pep.htmlseq = "".join( add_html_mods2seqlst(list(dig_pep.seq),
#                                                            dig_pep.pos2modifications) )
            mspeps = session.query(MSPeptide).filter(MSPeptide.seq==dig_pep.seq,  #Get matching (with same sequence) MS peptides...
                                                     MSPeptide.mod_aas > 0).all() #but only with any significant and clear physiological PTM 
#            for mspep in mspeps:
#                mspep.htmlseq = "".join( add_html_mods2seqlst(list(mspep.seq),
#                                                              mspep.pos2modifications) )
            dig_pep2mspeps[dig_pep] = mspeps
        #
        return {'protein': protein, 'dig_pep2mspeps': dig_pep2mspeps,
                'protease': protease}
    
    @staticmethod
    def _add_mass2digpeps(dig_peps):
        for dig_pep in dig_peps:
            dig_pep.mass = "{:.5f}".format( ppf.pep_w_mods_mass(dig_pep) )
#            dig_pep.htmlseq = "".join( add_html_mods2seqlst(list(dig_pep.seq),
#                                                            dig_pep.pos2modifications) )
            yield dig_pep
        
    def get_digestcombine_resultscontext(self, session, **kwargs):
        # Get Protein to digest and Protease to use:
        protein_protease = self._protein_protease(session)
        if not protein_protease:
            return dict()
        protein, protease = protein_protease
        # Get Consensus Digestion Peptides:
        consensus_digpeps = self._consesnsus_digpeps4combination(protein, protease)
        # Generate, Filter and Format the Combinatorial Digestion Peptides:
        filen = "digestion_peptides_results_{}_{:%Y-%m-%d_%H%M%S}"\
                ".html.log".format( protein.ac, datetime.now() )
        log_filen = os.path.join(settings.BASE_DIR, 'logs', filen) #DEBUG log file-name
        dig_peps = self._add_mass2digpeps( #Format
                       self._filtered_combdigpeps(session, consensus_digpeps, #Generate and Filter
                                                  protease, log_filen=log_filen) 
                                             )
        #
        return {'protein': protein, 'dig_peps': dig_peps, 'protease': protease}
    
    def get_context_data(self, session, **kwargs):
        # Context data for an HTML response:
        # Overwrites super-class :method:`SQLAlchemySectionView.get_context_data`.
        context = super(ProteinDigestCombineView, self).get_context_data(session, **kwargs)
        # Get results:
        if not self.errors: #Only if a valid form:
            if self.action == 'digest': # Only Digest protein:
                results = self.get_digest_resultscontext(session, **kwargs)
            elif self.action == 'digest_combine': # Digest protein and Combine PTMs:
                results = self.get_digestcombine_resultscontext(session, **kwargs)
            context.update(results)
        #
        return context
    
    @staticmethod
    def _combdigpeps2rowdicts(digpeps, if_sep=", "):
        """
        Formats input data into something :func:`save_dicts_as_csv` can use to 
        generate a CSV/TSV output file.
        X_NOTE: this method derives from :func:`iter_digpep_diffmods2rowdicts` 
        of module `digestion_peptides_summaries.py` version 0.11 (2018-03-21) 
        at revision 311 (cac25ca2f4c1).
        
        :param iterable digpeps: an iterable of :class:`DigestionPeptide` 
        instances.
        :param str if_sep: intra-field separator. The separator to use between 
        multiple values for a single field. Defaults to ", ".
            
        :return generator : yields :class:`OrderedDict` instances with
        Digestion Peptides information, suitable to write to a file (a valid
        input for :func:`save_dicts_as_csv`).
        """
        for digpep in digpeps:
            # Get different significant PTM abbreviations:
            diffmods = sorted( {mod.mod_type.extradata['full_name'] 
                                for mod in digpep.sig_modifications} )
#             # Get PTM Sources information:
#             pos_ptm2sourcenames = defaultdict(set)
#             for pos, ptms in digpep.pos2modifications.items():
#                 for ptm in ptms:
#                     pos_ptm2sourcenames[pos, ptm.mod_type.name].add(ptm.source.name)
#             sources = if_sep.join( "%s-%s: %s sources (%s)"%( ptmname, pos, 
#                                                               len(sourcenames), 
#                                                               if_sep.join(sorted(sourcenames)) ) 
#                                    for (pos, ptmname), sourcenames 
#                                    in sorted( pos_ptm2sourcenames.items() ) )
            # Digestion Peptide information:
            rowdict = OrderedDict((('Digestion_Peptide', digpep.seq), 
                                   ('Digestion_Peptide_With_PTMs', digpep.seq_w_abbreviatedmods), 
                                   ('Digestion_Peptide_ProForma', digpep.as_proforma()), 
                                   ('Total_Modified_AAs', digpep.mod_aas),
                                   ('Protein_1st', digpep.protein.ac), 
                                   ('Start_Position_1st', digpep.prot_start), 
                                   ('End_Position_1st', digpep.prot_end), 
                                   ('Missed_Cleavages', digpep.missing_cleavages),
                                   ('Different_Modifications', len(diffmods)), 
                                   ('Modification_Types', if_sep.join(diffmods)), 
                                   ('Monoisotopic_peptide_mass', 
                                    "{:.5f}".format( ppf.pep_w_mods_mass(digpep) )), 
#                                    ('PTM_Sources', sources), 
                                   ))
            #
            yield rowdict
    
    def get_file_httpresponse(self, session, **kwargs):
        httpresponse = HttpResponse(content_type='text/tab-separated-values')
        # Get Protein to digest and Protease to use:
        protein_protease = self._protein_protease(session)
        if protein_protease:
            protein, protease = protein_protease
            # Set the file name:
            now = datetime.now()
            prot_ac = protein.ac
            filen = "inferred_peptides_results_{}_{:%Y-%m-%d_%H%M%S}.tsv".format(prot_ac, now)
            httpresponse['Content-Disposition'] = 'attachment; filename="{0}"'.format(filen)
            # Write information header before data:
            httpresponse.write("Inferred Combinatorial Digestion Peptides File\n"
                               "  Downloaded from TCellXTalk v{} "
                               "(https://www.TCellXTalk.org) on {:%Y-%m-%d_%H:%M:%S}\n"
                               "  (cc) BY-NC-SA ( http://creativecommons.org/"
                               "licenses/by-nc-sa/4.0/ )\n\n"
                               "Digestion parameters:\t"
                               " Protein Digested: {}\t Protease used: {}\t\n"
#                                " Extra Missed Cleavages Levels: {}\n"
                               "Filter parameters:\t"
                               " Peptide lengths: {} - {} AAs\t"
#                                " Max. Missed Cleavages: {}\n\t"
                               " PTMs/peptide: {} - {}\t"
                               " Max. PTMs of same type/peptide: {}\t"
                               " PTMs required: {}\n"
                               "\n".format(settings.__VERSION__, now, 
                                           prot_ac, protease.name, 
#                                            self.missing_cleavage_lvls, 
                                           self.min_pep_length, self.max_pep_length, 
#                                            self.max_missing_cleavages, 
                                           self.min_total_ptms, self.max_total_ptms, 
                                           self.max_equal_ptms, 
                                           self.exclusiveptm_names)
                               )
            # Get Consensus Digestion Peptides:
            consensus_digpeps = self._consesnsus_digpeps4combination(protein, protease)
            # Generate, Filter, Format and Save the Combinatorial Digestion 
            # Peptides as TSV inside the :class:`HttpResponse` object:
            log_filen = os.path.join(settings.BASE_DIR, 'logs', filen+".log") #DEBUG log file-name
            save_dicts_as_csv(httpresponse, #Save
                              self._combdigpeps2rowdicts( #Format
                                  self._filtered_combdigpeps(session, #Generate and Filter
                                                             consensus_digpeps, 
                                                             protease, 
                                                             log_filen=log_filen) ), 
                              delimiter='\t')
        #
        # Set a cookie to tell web-page JavaScrip to stop the "Please Wait..."
        # notification (X-NOTE: http://stackoverflow.com/a/4168965):
        httpresponse.set_cookie('download_ready', 'True')
        return httpresponse
    
    def get_db_response(self, **kwargs):
        """
        Overwrites :method:``SQLAlchemySectionView.get_db_response`` to return 
        a download file as a :class:`HttpResponse` instance when needed.
        """
        if self.action == 'digest_combine_dwnld': # Digest protein, Combine PTMs and Download as a TSV file:
            with self.db.session_no_autoflush() as session:
                httpresponse = self.get_file_httpresponse(session, **kwargs)
            return httpresponse
        else: # View as HTML:
            return super(ProteinDigestCombineView, self).get_db_response(**kwargs)
    
    def form_valid(self, form):
        """
        Prepare data from :param:`form` for the desired user action, and get
        this action.
        
        :returns: a :class:`HttpResponse` object with the search results rendered against
        the template.
        """
        search_params = form.cleaned_data
        # Define digestion parameters from input data:
        self.protein_ac = search_params['protein_ac'] #ACcesion number of the protein to digest
        self.protease_id = int( search_params['protease'] ) #ID of the protease to use
#         self.missing_cleavage_lvls = search_params['missing_cleavage_lvls'] #Maximum extra missed cleavages level to generate
        self.min_pep_length = search_params['min_pep_length'] #Minimum digestion peptide length
        self.max_pep_length = search_params['max_pep_length'] #Maximum digestion peptide length
#         self.max_missing_cleavages = search_params['max_missing_cleavages'] #Number of allowed missed cleavages
        self.exclusive_ptms = exclusive_ptms = set(map( int, search_params['exclusive_ptms'] )) #Exclusive PTM IDs
        self.min_total_ptms = search_params['min_total_ptms'] #Minimum number of PTMs/peptide
        self.max_total_ptms = search_params['max_total_ptms'] #Maximum number of PTMs/peptide
        self.max_equal_ptms = search_params['max_equal_ptms'] #Maximum number of PTMs of the same type/peptide
        # User defined action (digest / digest+combine+download):
        self.action = self.request.POST.get('digest_combine_dwnld', 'digest_combine') #Defaults to Digest the protein and Combine the PTMs
        # Get exclusive PTM full names, and format them for viewing (as plain text or HTML):
        if exclusive_ptms:
            with self.db.session_no_autoflush() as session:
                ModificationType = settings.DBM.ModificationType
                extradatas = session.query(ModificationType.extradata)\
                                    .filter( ModificationType.id.in_(exclusive_ptms) )\
                                    .all()
                ptm_names = sorted(extradata['full_name']+"s" 
                                   for extradata, in extradatas)
            self.exclusiveptm_names = "Only " + " and ".join(ptm_names)
        else:
            self.exclusiveptm_names = "None"
        #
        return super(ProteinDigestCombineView, self).form_valid(form)
    
    def form_invalid(self, form):
        """
        Put :param:`form` errors into View (self) errors dictionary.
        
        :returns: a :class:`HttpResponse` object with the form errors rendered
        against the template.
        """
        return super(ProteinDigestCombineView, self).form_invalid(form, 
                                                                  base_key="Digestion Form Errors")
    
    def get(self, request, *args, **kwargs):
        self.protein_ac = protein_ac = args[self.get_search_field] #:attr:`protein_ac` set to do a default digestion when View accessed through http get method
        self.form_initial_values = {'protein_ac': protein_ac}
        return super(ProteinDigestCombineView, self).get(request, *args, **kwargs)


# DEPRECATED:
#
# class DigPepSearchListView(FormMixin, PaginatorMixin, SQLAlchemySectionView):
#     """
#     Searches DataBase for predicted Digestion Peptides, and generate a result
#     list paginated and ordered, or a downloadable TSV file.
#     """
#     template_name = 'digpep_results.html' #Section template file to use.
#     title = 'Predicted Digestion Peptides Results' #Title of the section web page.
#     section = 'search' #Section name of this section web page.
#     
#     table_headers = ( {'html': 'Digestion Peptide',
#                        'title': 'Digestion Peptide Sequence',
#                        'width': '30%',
#                        'sort_tag': 'seq'},
#                       {'html': 'Protein',
#                        'title': 'Original Protein for the Digestion Peptide',
#                        'width': '6%',
#                        'sort_tag': 'protein_ac'},
#                       {'html': 'Missing cleavages',
#                        'title': 'Number of trypsing missing cleavages in Digestion Peptide',
#                        'width': '6%',
#                        'sort_tag': 'missing_cleavages'},
#                       {'html': 'Number of modified AAs',
#                        'title': 'Number of aminoacids with modifications',
#                        'width': '10%',
#                        'sort_tag': '_db_mod_aas'},
#                       {'html': 'Experimental matching MS peptides',
#                        'title': 'MS peptides in database with the same sequence as the Digestion Peptide',
#                        'width': '30%',
#                        'sort_tag': ''},
#                      )
#     
#     itemsxpage = 50 #Items per page for the Paginator object in abstract method search
#     
#     form_class = ProtDBDigPepSearchForm #The form class to instantiate.
#     
#     allowed_orderby_fields = ('seq', 
#                               'protein_ac',
#                               'missing_cleavages',
#                               '_db_mod_aas') #Fields allowed to order the search by.
#     
#     db = settings.PROT_DB #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do the searches.
#     
#     mapping4search = {'*': '%', '?': '_', 'X': '_'} #Mapping table to use to format the search string for queries.
#     
#     def __init__(self, **kwargs):
#         # Attribute initialization:
#         self.digpep_seq = '' #Digestion Peptide Sequence to search for.
#         self.response_format = 'html'
#         #
#         super(DigPepSearchListView, self).__init__(**kwargs)
#     
#     def get_results_query(self, session, **kwargs):
#         """
#         Only Digestion Peptides from reviewed Proteins.
#         
#         By now (2017-07-05) no decoy proteins are in the Database, so it's 
#         still not necessary to filter them ( .filter(Protein.is_decoy==False) ).
#          
#         :param Session session: a SQLAlchemy DataBase session.
# 
#         :return tuple : a tuple of 4 SQLAlchemy queries: a Digestion Peptides
#         getting query, a MS Peptides getting query, a Digestion Peptides 
#         counting query, and a MS Peptides counting query.
#         """
#         # Get ORM model Classes:
#         DigestionPeptide = settings.DBM.DigestionPeptide
#         ProtModification = settings.DBM.ProtModification
#         Protein = settings.DBM.Protein
#         MSPeptide = settings.DBM.MSPeptide
#         MSPepModification = settings.DBM.MSPepModification
#         # Define base queries for in-silico tryptic Digestion Peptides in Database:
#         #Query for getting in-silico tryptic Digestion Peptides in database:
#         query_dp = session.query(DigestionPeptide)\
#                           .options( joinedload('_db_modifications')
#                                     .joinedload('mod_type') ) 
#         #Query for counting in-silico tryptic Digestion Peptides in database:
#         querycount_dp = session.query( func.count( distinct(DigestionPeptide.id) ) )
#         #Filter queries for in-silico tryptic Digestion Peptides in database:
#         queries_dp = [ q.join(DigestionPeptide.protein)\
#                         .filter(Protein.reviewed==True)\
#                         .filter( DigestionPeptide.seq.like(self.digpep_seq) )\
#                         .filter(DigestionPeptide.mod_aas>=self.min_mod_aas)\
#                         .filter(DigestionPeptide.mod_aas<=self.max_mod_aas) 
#                        for q in (query_dp, querycount_dp) ]
#         # Define base queries for experimental MS Peptides in Database:
#         #Query for getting experimental MS Peptides in database:
#         query_ms = session.query(MSPeptide)\
#                           .options( joinedload('_db_modifications')
#                                     .joinedload('mod_type'), 
#                                     joinedload('proteins'), 
#                                     joinedload('source') )
#         #Query for counting experimental MS Peptides in database:
#         querycount_ms = session.query( func.count( distinct(MSPeptide.id) ) )
#         #Sub-query with in-silico tryptic Digestion Peptides Sequences in database:
#         squery_dp_seq = session.query(DigestionPeptide.seq)\
#                                .join(DigestionPeptide.protein)\
#                                .filter(Protein.reviewed==True)\
#                                .filter( DigestionPeptide.seq.like(self.digpep_seq) )\
#                                .filter(DigestionPeptide.mod_aas>=self.min_mod_aas)\
#                                .filter(DigestionPeptide.mod_aas<=self.max_mod_aas)\
#                                .subquery()
#         #Filter queries for experimental MS Peptides in database:
#         queries_ms = [ q.filter( MSPeptide.seq.like(self.digpep_seq) )\
#                         .filter( MSPeptide.seq.notin_(squery_dp_seq) )\
#                         .filter(MSPeptide.mod_aas>=self.min_mod_aas)\
#                         .filter(MSPeptide.mod_aas<=self.max_mod_aas) 
#                        for q in (query_ms, querycount_ms) ]
#         # If needed, modify queries to exclude not allowed PTMs
#         # (:attr:`not_allowed_ptms`):
#         if self.not_allowed_ptms:
#             not_allowed_ptms = self.not_allowed_ptms
#             queries_dp = [q.filter( ~DigestionPeptide._db_modifications.any( 
#                                      and_(ProtModification.significant==True, 
#                                           ProtModification.mod_type_id.in_(not_allowed_ptms)) ) ) 
#                           for q in queries_dp]
#             queries_ms = [q.filter( ~MSPeptide._db_modifications.any( 
#                                      and_(MSPepModification.significant==True,
#                                           MSPepModification.mod_type_id.in_(not_allowed_ptms)) ) )
#                           for q in queries_ms]
#         # "Close" queries, if needed:
#         query_dp, querycount_dp = queries_dp
#         query_ms, querycount_ms = queries_ms
#         #Prepare :method:``order_by`` parameters:
#         oparams = [getattr( DigestionPeptide, field.split(' ')[0] ).desc()
#                    if 'desc' in field else 
#                    getattr(DigestionPeptide, field)
#                    for field in self.orderby_fields]
#         #"Close" getting queries:
#         query_dp = query_dp.distinct().order_by(*oparams)
#         query_ms = query_ms.distinct()
#         #
#         return query_dp, query_ms, querycount_dp, querycount_ms
#     
#     def get_context_data(self, session, **kwargs):
#         """
#         Format the query results.
#         """
#         context = super(DigPepSearchListView, self).get_context_data(session, **kwargs)
#         #
#         if self.digpep_seq: #Only get results if there is something to search for
#             # Get the queries:
#             query_dp, query_ms, querycount_dp, querycount_ms = self.get_results_query(session, **kwargs)
#             # Paginate/limit the query:
#             paginated_results = self.paginate_results(query_dp,
#                                                       special_count_query=querycount_dp)
#             other_mspeps_paginated = self.paginate_results(query_ms, 
#                                                            special_count_query=querycount_ms)
#             # Add HTML formated sequence to the Digestion Peptides and similar MS
#             # peptides as a new :attr:`htmlseq`:
#             for dig_pep in paginated_results.object_list:
#                 dig_pep.htmlseq = "".join( add_html_mods2seqlst(list(dig_pep.seq),
#                                                                 dig_pep.pos2modifications) )
#                 for mspep in dig_pep.mspeptides_similar:
#                     mspep.htmlseq = "".join( add_html_mods2seqlst(list(mspep.seq),
#                                                                   mspep.pos2modifications) )
#             # Add HTML formated sequence to the Other MS Peptides as a new :attr:`htmlseq`:
#             if other_mspeps_paginated:
#                 for mspep in other_mspeps_paginated.object_list:
#                     mspep.htmlseq = "".join( add_html_mods2seqlst(list(mspep.seq),
#                                                                   mspep.pos2modifications) )
#             #
#             context['paginated_results'] = paginated_results
#             context['other_mspeps_paginated'] = other_mspeps_paginated
#         #
#         return context
#     
#     def _iter_digpepquery2rowdict(self, digpep_query):
#         """
#         Generates a valid :func:`save_dicts_as_csv` input from a Digestion
#         Peptides Query.
# 
#         :param Query digpep_query: a query to get :class:`DigestionPeptide`
#         instances from the DataBase.
# 
#         :return generator : yields :class:`OrderedDict` instances with
#         Digestion Peptides information, suitable to write to a file by
#         :func:`save_dicts_as_csv`.
#         """
#         for digpep in digpep_query:
#             dif_mods = sorted( set(mod.mod_type.name for mod in digpep.modifications) )
#             # Digestion Peptide information:
#             rowdict = OrderedDict((('Digestion_Peptide', digpep.seq),
#                                    ('Digestion_Peptide_With_PTMs', digpep.seq_w_mods),
#                                    ('Total_Modified_AAs', digpep.mod_aas),
#                                    ('Protein', digpep.protein_ac),
#                                    ('Start_Position', digpep.prot_start),
#                                    ('End_Position', digpep.prot_end),
#                                    ('Missing_Cleavages', digpep.missing_cleavages),
#                                    ('Different_Modifications', len(dif_mods)),
#                                    ('Modification_Types', ", ".join(dif_mods)),
#                                    ))
#             # Associated MS Peptides:
#             mspeptides = ", ".join( mspep.seq_w_mods + " (" + mspep.source.name + ")"
#                                     for mspep in digpep.mspeptides_similar )
#             rowdict['Similar_MS_Peptides'] = mspeptides
#             # Associated Modifications information:
#             pos_shift = digpep.prot_start - 1
#             for index, mod in enumerate(sorted(digpep.modifications)):
#                 keyhead = "Modification_{0}_".format(index + 1)
#                 moddict = OrderedDict(((keyhead+"Type", mod.mod_type.name),
#                                        (keyhead+"Position", mod.position - pos_shift),
#                                        (keyhead+"AA", mod.aa),
#                                        (keyhead+"Ascore", mod.ascore),
#                                        ))
#                 rowdict.update(moddict)
#             #
#             yield rowdict
#     
#     def _iter_msquery2rowdict(self, mspep_query):
#         """
#         Generates a valid :func:`save_dicts_as_csv` input from a Other MS
#         Peptides Query.
# 
#         :param Query mspep_query: an iterable query that contains
#         :class:`MSPeptide` instances.
# 
#         :return generator : yields :class:`OrderedDict` instances with Other MS
#         Peptides information, suitable to write to a file. A valid
#         :func:`save_dicts_as_csv` input.
#         """
#         for mspep in mspep_query:
#             dif_mods = sorted( set(mod.mod_type.name for mod in mspep.modifications) )
#             # Digestion Peptide information:
#             rowdict = OrderedDict((('MS_Peptide_Source', mspep.source.name),
#                                    ('MS_Peptide', mspep.seq),
#                                    ('MS_Peptide_With_PTMs', mspep.seq_w_mods),
#                                    ('Total_Modified_AAs', len( mspep.pos2modifications.keys() )),
#                                    ('Protein/s', ', '.join(prot.ac for prot in mspep.proteins) if mspep.proteins else ', '.join(mspep.prots_from_source) + ' *'),
#                                    ('Different_Modifications', len(dif_mods)),
#                                    ('Modification_Types', ', '.join(dif_mods)),
#                                    ))
#             # Associated Modifications information:
#             for index, mod in enumerate(sorted(mspep.modifications)):
#                 keyhead = 'Modification_{0}_'.format(index + 1)
#                 moddict = OrderedDict(((keyhead+'Type', mod.mod_type.name),
#                                        (keyhead+'Position', mod.position),
#                                        (keyhead+'AA', mod.aa),
#                                        (keyhead+'Ascore', mod.ascore),
#                                        ))
#                 rowdict.update(moddict)
#             #
#             yield rowdict
#     
#     def get_file_httpresponse(self, session, **kwargs):
#         now = datetime.now()
#         filen = "digestion_peptides_results_{0:%Y-%m-%d_%H%M%S}.tsv".format(now)
#         #
#         httpresponse = HttpResponse()
#         httpresponse['Content-Type'] = 'text/tsv'
#         httpresponse['Content-Disposition'] = 'attachment; filename="{0}"'.format(filen)
#         # Get the queries:
#         query_dp, query_ms, _, _ = self.get_results_query(session, **kwargs)
#         # Format and save the Digestion Peptides query as TSV in the
#         # :class:`HttpResponse` object:
#         httpresponse.write( "Predicted Digestion Peptides Results ({0}):\n\n".format( query_dp.count() ) )
#         save_dicts_as_csv(httpresponse,
#                           self._iter_digpepquery2rowdict(query_dp),
#                           delimiter='\t')
#         httpresponse.write('\n\n')
#         # Format and save the Other MS Peptides query as TSV in the
#         # :class:`HttpResponse` object:
#         httpresponse.write( "Other experimental MS Peptides Found ({0}):\n\n".format( query_ms.count() ) )
#         save_dicts_as_csv(httpresponse,
#                           self._iter_msquery2rowdict(query_ms),
#                           delimiter='\t')
#         httpresponse.write('\n\n * Proteins found in the original source, but not in the current UniPort Human database used in this web (2015/02)')
#         # Set a cookie to tell web-page JavaScrip to stop the "Please Wait..."
#         # notification (X-NOTE: http://stackoverflow.com/a/4168965):
#         httpresponse.set_cookie('download_ready', 'True')
#         return httpresponse
#     
#     def get_db_response(self, **kwargs):
#         if self.response_format == 'html': # As HTML:
#             return super(DigPepSearchListView, self).get_db_response(**kwargs)
#         elif self.response_format == 'tsv': # As a TSV download file:
#             with self.db.session_no_autoflush() as session:
#                 httpresponse = self.get_file_httpresponse(session, **kwargs)
#             return httpresponse
#     
#     def form_valid(self, form):
#         """
#         Prepare data from :param:`form` for the database search.
# 
#         :returns: a :class:`HttpResponse` object with the search results
#         rendered against the template.
#         """
#         search_params = form.cleaned_data
#         # Define search parameters from input data:
#         self.digpep_seq = self.formatstr4search( search_params['digpep_seq'] )
#         self.min_mod_aas = search_params['min_mod_aas']
#         self.max_mod_aas = search_params['max_mod_aas']
#         allowed_ptms = map( int, search_params['allowed_ptms'] )
#         allowed_ptm_names = list()
#         not_allowed_ptms = list()
#         for cid, cname in form.fields['allowed_ptms'].choices:
#             if cid in allowed_ptms:
#                 allowed_ptm_names.append(cname)
#             else:
#                 not_allowed_ptms.append(cid)
#         self.allowed_ptms = allowed_ptms
#         self.allowed_ptm_names = allowed_ptm_names
#         self.not_allowed_ptms = not_allowed_ptms
#         self.orderby_fields = search_params['order_by'].split(',')
#         # Pagination of results:
#         self.page = int( self.request.POST.get('page', '1') ) #Make sure requested page exists; otherwise, deliver first page.
#         # Results format (file/html):
#         self.response_format = self.request.POST.get('download', 'html') #Defaults to html
#         #
#         return super(DigPepSearchListView, self).form_valid(form)
#     
#     def form_invalid(self, form):
#         """
#         :returns: a :class:`HttpResponse` object with the form errors rendered
#         against the template.
#         """
#         self.errors.put_with_key('Search', form.errors)
#         #
#         return super(DigPepSearchListView, self).form_invalid(form)
#     
#     def get(self, request, *args, **kwargs):
#         """
#         Redirects to the 'search' page.
#         """
#         return HttpResponseRedirect( urlresolvers.reverse_lazy('search') )


class MSPeptideSearchListView(FormSearchListView):
    """
    Searches DataBase for MS Peptides, and generate a result list paginated and
    ordered.
    """
    template_name = 'ms_petide_results.html' #Section template file to use.
    title = 'MS Peptide Search Results' #Title of the section web page.
    section = 'search' #Section name of this section web page.
    
    form_class = ProtDBMSPepSearchForm #The form class to instantiate.
    form_search_field = 'mspep_seq' #Form field with the search string to use.
    form_sorttags_field = 'sort_tags' #Form field with the fields to order the query by.
    
    table_headers = ( {'html': 'Peptide',
                       'title': 'MS Peptide Sequence',
                       'width': '30%',
                       'sort_tag': 'seq', 
                       'orderby_tmplt': "seq {0}"},
                      {'html': 'Source',
                       'title': 'Source Repository/Paper',
                       'width': '20%',
                       'sort_tag': 'source', 
                       'orderby_tmplt': "sources.name {0}"},
                      {'html': 'Protein/s',
                       'title': 'Protein/s containing the Peptide Sequence',
                       'width': '40%',
                       'sort_tag': False},
                      {'html': 'Modified AAs', 
                       'title': 'Number of amino acids with modifications', 
                       'width': '10%', 
                       'sort_tag': 'mod_aas', 
                       'orderby_tmplt': "_db_mod_aas {0}"}, 
#                       {'html': 'Spectrogram',
#                        'title': 'Has spectrogram representation',
#                        'width': '6%',
#                        'sort_tag': False},
                     )
    
    itemsxpage = 50 #Default items per page for the Paginator object.
    allowed_itemsxpage = (25, 50, 100, 200) #Values allowed for the :attr:`itemsxpage`.
    
    db = settings.PROT_DB #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do te searches.
    
    strmapping4search = {'*': '%', '?': '_'} #Mapping table to use to format the search string for queries.
    
    def get_results_context(self, session, **kwargs):
        """
        Do the query for MS Peptides, but only for those whose sources are in
        :attr:`allowed_sources`, and have at least one (any) significant PTM
        (MSPepModification).
        """
        # :caution: To avoid sorting problems, this query has not been 
        # optimized with options for eager loading, but it's still fast enough.
        MSPeptide = settings.DBM.MSPeptide
        results = session.query(MSPeptide).join(MSPeptide.source)\
                         .filter( MSPeptide.seq.like(self.search_value), 
                                  MSPeptide.source_id.in_(self.allowed_sources), 
                                  MSPeptide.mod_aas > 0, 
                                  MSPeptide.proteins.any() )\
                         .order_by(*self.orderby_fields)
        #
        paginated_results = self.paginate_results( results, reenter_form=kwargs['form'] )
#        # Add the HTML formated sequence to the MS Peptides as a new :attr:`htmlseq`:
#        for mspep in paginated_results.object_list:
#            mspep.htmlseq = "".join( add_html_mods2seqlst(list(mspep.seq),
#                                                          mspep.pos2sigmodifications) )
        return {'paginated_results': paginated_results}

    def form_valid(self, form):
        self.raw_search_value = form.cleaned_data[self.form_search_field]
        allowed_sources = map( int, form.cleaned_data['sources'] )
        allowed_sources_names = list()
#         not_allowed_sources = list()
        for cid, cname in form.fields['sources'].choices:
            if cid in allowed_sources:
                allowed_sources_names.append(cname)
#             else:
#                 not_allowed_sources.append(cid)
        self.allowed_sources = allowed_sources
        self.allowed_sources_names = allowed_sources_names
#         self.not_allowed_sources = not_allowed_sources
        #
        return super(MSPeptideSearchListView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        """
        Redirects to the 'search' page.
        """
        return HttpResponseRedirect( urlresolvers.reverse_lazy('search') )


class MSPeptideView(PaginatorMixin, SearchItemView):
    """
    Searches DataBase for a MS Peptide, and shows MS Peptide information.
    """
    
    template_name = 'ms_peptide_view.html' #Section template file to use.
    title = 'MS Peptide View' #Title of the section web page.
    section = 'search' #Section name of this section web page.

    db = settings.PROT_DB #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do te searches.

    get_search_field = 0 #Integer for positional arguments, string for keyword arguments.

    itemsxpage = 5 #Items per page for the Paginator object.
    allowed_itemsxpage = (5, 10, 20, 50) #Values allowed for the :attr:`itemsxpage`.

    spectrum_viewer = 'lorikeet' # 'image', or 'lorikeet' JavaScript Spectrum Viewer (http://uwpr.github.io/Lorikeet/).

    def formatstr4search(self, search_str):
        return int(search_str)

    def get_results_context(self, session, **kwargs):
        # Do the query to get a MS Peptide:
        MSPeptide = settings.DBM.MSPeptide
        mspep = session.query(MSPeptide)\
                       .options( joinedload('_db_modifications')\
                                 .joinedload('mod_type'), 
                                 joinedload('proteins'), 
                                 joinedload('source') )\
                       .get(self.search_value)
        if mspep is None:
            self.errors.put_with_key('DataBase Search', 'Not found')
            return {'mspeptide': None}
#        # Format MS Peptide sequence as HTML in new :attr:`htmlseq`:
#        mspep.htmlseq = ''.join( add_html_mods2seqlst(list(mspep.seq),
#                                                      mspep.pos2sigmodifications) )
        # Do the query to get PSMs:
        PSM = settings.DBM.PSM
        psms = session.query(PSM)\
                      .options( joinedload('spectrum'),
                                joinedload('_db_modifications')\
                                .joinedload('mod_type') )\
                      .filter(PSM.mspeptide == mspep)
        reenter_form = forms.Form() #A void :class:`django.forms.Form` instance to re-enter the selected :attr:`itemsxpage`.
        paginated_psms = self.paginate_results(psms, reenter_form=reenter_form) #Paginate/limit PSM results.
        # Sorted PSM positions and ALL PTMs (significant or not) as a list of
        # tuples [ ( position, {PTMs, ...} ), ... ] in new :attr:`pos2ptms`:
        for psm in paginated_psms.object_list:
            psm.pos2ptms = sorted( psm.pos2allmodifications.items() )
        # Format Spectrum data for Lorikeet JavaScript Spectrum Viewer
        # (http://uwpr.github.io/Lorikeet/):
        if self.spectrum_viewer == 'lorikeet':
            var_mods = list() #Variable modifications needed by 'lorikeet' JavaScript Spectrum Viewer
            for pos, mods in mspep.pos2sigmodifications.items():
                for mod in mods:
                    var_mods.append( {'index': int(pos),
                                      'modMass': mod.mod_type.extradata.get('delta_mass', 79.966331), #DEBUG
                                      'aminoAcid': mod.aa,
                                      } )
            for psm in paginated_psms.object_list:
                spectrum = psm.spectrum
                if spectrum.has_mzs_intensities:
                    spectrum.seq = psm.seq
                    spectrum.var_mods = var_mods
                    spectrum.peaks = map( list, zip(spectrum.masses,
                                                    spectrum.intensities) )
                    spectrum.precursor_mz = spectrum.parent_mass / spectrum.charge
        #
        return {'mspeptide': mspep, 'paginated_psms': paginated_psms, 
                'form': reenter_form}

    def post(self, request, *args, **kwargs):
        """
        The form :class:`ProteinDigestCombineForm` needs a session to be initialized,
        so all the :method:`post` stuff should be moved to a method called from
        :method:`get_db_response` (that generates a session).
        """
        # Get data for the pagination of results:
        self.page = self.request.POST.get('page', '1') #Make sure requested page exists. Otherwise, deliver first page
        self.itemsxpage = self.request.POST.get('itemsxpage', self.itemsxpage) #Get requested items per page or use the default one
        #
        return self.get(request, *args, **kwargs)


class Spectrum2File(DinamicFile):

    file_type = ''

    db = settings.PROT_DB #An instance of :class:`db_models.DataBase` needed to obtain a DataBase session to do the searches.

    def __init__(self, spec_id=0, *args, **kwargs):
        super(Spectrum2File, self).__init__(*args, **kwargs)
        #
        self.spec_id = spec_id
        self.peptide = ''
        #
        self.x_mass = list()
        self.y_intens = list()
        self.peptide = ''
        self.ms_n = 0
        self.charge = 1
        self.mass = 0

    def _set_data4spectrum(self, spectrum):
        """
        Set internal data from a :class:`Spectrum` instance.
        """
        self.spec_id = spectrum.id
        self.x_mass = spectrum.masses
        self.y_intens = spectrum.intensities
        self.ms_n = spectrum.ms_n
        self.charge = spectrum.charge
        self.mass = spectrum.parent_mass

    def get_spectrum(self, session):
        """
        Get a :class:`Spectrum` instance from a DataBase `session`.
        """
        # Search for the spectrum ID:
        Spectrum =  settings.DBM.Spectrum
        spectrum = session.query(Spectrum)\
                          .options( undefer('masses'),
                                    undefer('intensities') )\
                          .get(self.spec_id)
        if spectrum is None: #No spectrum found:
            raise Http404('Error 404 - No Spectrum Found with ID={0}'.format(self.spec_id))
        self._set_data4spectrum(spectrum)
        return spectrum

    def get(self, request, *args, **kwargs):
        self.filename = args[0] #A filename of the type 1234-AAAAA.ext
        #
        # Get the spectra ID and peptide seq from the file name:
        if self._name:
            self.spec_id, self.peptide = self._name.split('-')
        #
        return super(Spectrum2File, self).get(request, *args, **kwargs)


class Spectrum2PNG(Spectrum2File):

    file_type = 'PNG'

    def _identify_ions(self):
        """
        efms = expected fragment masses
        imass = ion mass
        fmass = expected fragment mass
        """
        PRECISSION = 0.5

        ion_dict = get_ion_dict(self.peptide, self.charge)

        efms = sorted(ion_dict.keys())
        nions = len(self.x_mass)
        colors = ['k'] * nions
        annotations = [''] * nions
        #
        last_annot = 0
        for iidx, imass in enumerate(self.x_mass):   #ions
            if (imass - last_annot) < 1: continue
            fragment = ''
            for fmass in efms:                        #expected fragments
                if abs(imass-fmass) < PRECISSION:
                    fragment += ' %s' % ion_dict[fmass]
                    last_annot = imass
            #
            if fragment:
                #quito solo un fragmento. no es eficiente...
                efms.pop(0)
                #
                if 'y' in fragment:
                    color = 'red'
                elif 'b' in fragment:
                    color = 'blue'
                elif 'M' in fragment:
                    color = 'green'
                #
                annotations[iidx] = fragment
                colors[iidx] = color
        return (colors, annotations)

    def _mark_ion(self, axe, colors, annotations):
        """
        Annotate the ion type in spectra.
        """
        # Uses axes.annotate(s, xy, xytext, rotation) because axes.text(s,x,y)
        # doesn't work very well with zoom:
        annotate = axe.annotate

        for idx, annotation in enumerate(annotations):
            if annotation:
                annotate(annotation, xy=(self.x_mass[idx], self.y_intens[idx]),
                                     color=colors[idx], xytext=None,
                                     va ='bottom', rotation='vertical')

    def get_file_httpresponse(self, session, **kwargs):
        """
        Generates a PNG image with the spectrum data selected by spec_id,
        using matplotlib, and returns it for in-line insertion.
        """
        httpresponse = super(Spectrum2PNG, self).get_file_httpresponse(session, 
                                                                       'image/png',
                                                                       **kwargs)
        # Get spectrum data:
        self.get_spectrum(session)
        # Draw the spectrum:
        fig = Figure(figsize=(10,4), dpi=75, frameon=False)
        fig.add_subplot(1, 1, 1)
        #fig.axes[0].set_ylim(bottom=0, top=max(self.y_intens))
        fig.axes[0].set_xlim(left=140, right=1800)

        zeros = [0] * len(self.x_mass)

        colors, annotations = self._identify_ions()
        fig.axes[0].vlines(self.x_mass, zeros, self.y_intens, color = colors,
                           label='False')
        self._mark_ion(fig.axes[0], colors, annotations)

        canvas = FigureCanvasAgg(fig)
        canvas.print_png(httpresponse)
        #
        return httpresponse


class Spectrum2MGF(Spectrum2File):

    file_type = 'MGF'

    def write_mgf(self, io_file):
        """
        Write the spectrum data to the supplied `io_file`

        :param object io_file: a file-like object.
        """
        io_file.write("#Downloaded from TCellXTalk v{0} on {1:%Y-%m-%d_%H:%M:%S}\n"
                      "# http://www.TCellXTalk.org\n"
                      "# (cc) BY-NC-SA ( http://creativecommons.org/licenses/by-nc-sa/4.0/ )\n"
                      "\n"
                      "BEGIN IONS\n"
                      "TITLE=Spectrum MS{2} Id {3} of Peptide {4} from TCellXTalk v{0}\n"
                      "PEPMASS={5:.3f}\n"
                      "CHARGE={6}\n".format(
                          settings.__VERSION__,
                          datetime.now(),
                          self.ms_n, self.spec_id, self.peptide,
                          self.mass,
                          str(abs(self.charge))+'-' if self.charge < 0 else
                          str(self.charge)+'+' )
                      )
        for mass, intensity in zip(self.x_mass, self.y_intens):
            io_file.write('{0:.3f} {1:.2f}\n'.format(mass, intensity))
        io_file.write('END IONS')
        return io_file

    def get_file_httpresponse(self, session, **kwargs):
        """
        Generates a MGF file with the spectrum data selected by spec_id, using
        specifications in http://www.matrixscience.com/help/data_file_help.html
        and returns it for download.
        """
        httpresponse = super(Spectrum2MGF, self).get_file_httpresponse(session,
                                                                       'text/mgf', 
                                                                       **kwargs)
        # Prepare the HttpResponse to contain attachment data for the MGF:
        httpresponse['Content-Disposition'] = 'attachment; filename={0}'.format(self.filename)
        # Get spectrum data:
        self.get_spectrum(session)
        # Write the spectrum data to the MGF:
        self.write_mgf(httpresponse)
        #
        return httpresponse

    def get(self, request, *args, **kwargs):
        # Don't allow MGF downloads via GET method to avoid mass automatic
        # downloads:
        return HttpResponseForbidden('Error 403 - Forbidden: Access is Denied')

    def post(self, request, *args, **kwargs):
        return super(Spectrum2MGF, self).get(request, *args, **kwargs)


class FileResponse(StreamingHttpResponse):
    """
    A streaming HTTP response class optimized for files.
    """
    #X-NOTE: Back-ported from django 1.8.7.
    block_size = 8192

    def _set_streaming_content(self, value):
        if hasattr(value, 'read'):
            self.file_to_stream = value
            filelike = value
            if hasattr(filelike, 'close'):
                self._closable_objects.append(filelike)
            value = iter(lambda: filelike.read(self.block_size), b'')
        else:
            self.file_to_stream = None
        super(FileResponse, self)._set_streaming_content(value)


class DownloadSectionView(ErrorsMixin, SectionView):
    """
    Download Section web-page View with multiple "steps".
    
    :attr str download_step: the "step" name within the download procedure.
    It's also the name of the step's template file (without the ".html").
    """
    template_name = 'download_section.html' #Section template file to use.
    title = 'Download Data' #Title of the section web page.
    section = 'download' #Section name of this section web page.
    
    download_step = "download_step_start" #The "step" name within the download procedure.
    filename = 'TCellXTalkDB.7z' #The file-name to download, without path.
    base_dir = settings.SPECIALFILES_DIR #The path.
    filesize_mib = False #File-size in IEC Mebibytes (X_NOTE: MiB, binary units erroneously displayed by browsers as MB).
    compressed = True #Is file already compressed? (to avoid gzi middleware).
    
    def serve_file(self, request, fullfilename=None):
        # X_NOTE: Partially based on code from django.views.static.serve() v1.7.
        #
        # Check :param:`fullfilename`:
        if not fullfilename:
            base_dir, filename = self.base_dir, self.filename
            fullfilename = os.path.join(base_dir, filename)
        else:
            base_dir, filename = os.path.split(fullfilename)
        #
        statobj = os.stat(fullfilename)
        # Respect the If-Modified-Since header.
        if not was_modified_since(request.META.get('HTTP_IF_MODIFIED_SINCE'),
                                  statobj.st_mtime, statobj.st_size):
            return HttpResponseNotModified()
        # Get file content type:
        content_type, encoding = mimetypes.guess_type(fullfilename)
        content_type = content_type or 'application/octet-stream'
        # Get an HttResponse:
        if settings.DEPLOY:
            # Use Apache2 mod_xsendfile (https://tn123.org/mod_xsendfile/) to
            # speed things up (X_NOTE: based on https://stackoverflow.com/a/1158750):
            httpresponse = HttpResponse(content_type=content_type)
            httpresponse['X-Sendfile'] = fullfilename
        else:
#                 return django.views.static.serve(request, path=self.filename, 
#                                                  document_root=self.base_dir)
            # Stream the file (avoids loading the full big file in RAM) using django:
            httpresponse = FileResponse(open(fullfilename, 'rb'),
                                        content_type=content_type)
        httpresponse['Content-Disposition'] = 'attachment; filename="{0}"'.format(filename)
        httpresponse["Last-Modified"] = http_date(statobj.st_mtime)
        if stat.S_ISREG(statobj.st_mode):
            httpresponse["Content-Length"] = statobj.st_size
        if encoding:
            httpresponse["Content-Encoding"] = encoding 
        elif self.compressed:
            httpresponse["Content-Encoding"] = 'identity' #Avoid gzip middleware (if enabled in ``settings``) for already compressed files, also the 'Content-Length' will not be removed by gzip middleware and so the file size will be transmitted to the user's browser. X_NOTE: see https://docs.djangoproject.com/en/2.0/ref/middleware/ and https://en.wikipedia.org/wiki/HTTP_compression
        #
        return httpresponse
    
    def post(self, request, *args, **kwargs):
        step = request.POST.get('step', 'bad')
        if step == 'download_step_start':
            accept_conds = request.POST.get('accept_conds', 'off')
            if accept_conds == 'on':
                self.download_step = "download_step_end"
                statobj = os.stat( os.path.join(self.base_dir, self.filename) )
                if stat.S_ISREG(statobj.st_mode):
                    self.filesize_mib = statobj.st_size / (1024 ** 2) #File-size in Mebibytes (IEC MiB)            
            else:
                self.errors.put_with_key('Checkbox', 
                                         'You must accept the Terms and Conditions'
                                         '(mark the check-box) to proceed to '
                                         'download.')
        elif step == 'download_step_end':
            return self.serve_file(request)
        return super(DownloadSectionView, self).get(request)


