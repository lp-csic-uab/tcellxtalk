# -*- coding: utf-8 -*-
"""
:synopsis:   Utility classes for LymPHOS_UB_AC (TCellXTalk) project.

:created:    2014-09-16

:author:     Óscar Gallardo (ogallard@gmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2014 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.2 dev'
__UPDATED__ = '2018-08-02'


#===============================================================================
# Imports
#===============================================================================
# Django imports:
from django.core.paginator import Paginator

# Application imports:

# Python core imports:
from collections import defaultdict

# Other imports:
from sqlalchemy.orm.query import Query
from sqlalchemy import func


#==============================================================================
# Accessory classes
#==============================================================================
class SQLAlchemyPaginator(Paginator):
    """
    Subclass of Django :class:`Paginator` to deal with slow performance of
    :method:`count` in SQLAlchemy :class:`Query`.
    """
    def __init__(self, object_list, per_page, orphans=0,
                 allow_empty_first_page=True, special_count_query=False):
        """
        :param bool or Query special_count_query: False for using the default
        :method:``count`` of the :param:`object_list` object (this usually means
        the slow :method:``count`` of a SQLAlchemy :class:``Query`` instance.
        True to use an alternative, faster trick (rewriting the SQL query using
        :function:``SQLAlchemy.func.count``, :caution: this can have unexpected
        results with queries with eager loading).
        Or a SQLAlchmey :class:``Query`` object specially designed to get the
        total count of results in a fast way (ex.: session.query(func.count(distinct( ModelCls.id ))), 
        see http://docs.sqlalchemy.org/en/rel_0_9/orm/query.html?highlight=count#sqlalchemy.orm.query.Query.count).
        Defaults to False.
        """
        self.special_count_query = special_count_query
        super(SQLAlchemyPaginator, self).__init__(object_list, per_page, orphans, 
                                                  allow_empty_first_page)
    
    def _get_count(self):
        """
        Returns the total number of objects, across all pages.
        """
        # Overwrites super-class :method:``Paginator._get_count``.
        if self._count is None:
            special_count_query = self.special_count_query
            if special_count_query is True and isinstance(self.object_list, Query):
                # Faster alternative to :method:``count`` of SQLAlchemy
                # :class:``Query``, as explained in
                # https://stackoverflow.com/a/12942437
                #
                # :CAUTION: this approach doesn't count correctly (using
                # distinct) when joinedload() (OUTHER JOIN) is used in queries!
                q = self.object_list
                qstatement = q.statement.with_only_columns( [func.count()] )\
                                        .order_by(None)
                self._count = q.session.execute(qstatement).scalar()
            elif isinstance(special_count_query, Query):
                # :attr:`special_count_query` is a SQLAlchmey :class:``Query``
                # object specially designed to get the total count of results
                # in a fast way (ex.: session.query(func.count(distinct( ModelCls.id ))), 
                # see http://docs.sqlalchemy.org/en/rel_0_9/orm/query.html?highlight=count#sqlalchemy.orm.query.Query.count).
                self._count = special_count_query.scalar()
            else:
                # Default Django :class:`Paginator` behavior of using
                # :method:``count`` of :attr:`object_list`, if available:
                super(SQLAlchemyPaginator, self)._get_count()
        return self._count
    count = property(_get_count)


class NoPaginator(object):
    """
    This class simulates a Django Paginator Page class, so a page view can't be
    paginated but still exposes the same interface as if it was (useful for
    changing a view from paginated to no paginated without a lot of changes in 
    views and templates).
    """
    def __init__(self, iterable, *args, **kwargs):
        self.object_list = iterable
        self.has_previous = False
        self.has_next = False
        self.paginator = self
        self.number = 1
        self.num_pages = 1
        self.start_index = 1
        self.end_index = len(self.object_list)
        self.count = self.end_index
        self.next_page_number = 1
        self.previous_page_number = 1
        
    def page(self, *args):
        return self


class LoggerCls(object):
    def __init__(self, key_func=None, *func_args, **func_kwargs):
        self._log = defaultdict(list)
        self._crono_keys = list() #List of keys in self._log, in chronological order
        
        if not key_func:
            key_func = self.crono_index
        
        self.key_func = key_func
        self.func_args = func_args
        self.func_kwargs = func_kwargs
        
#     def __call__(self, key=None):
#         if key:
#             return list( self._log.get(key, []) )
#         else:
#             return self.log
    
    def __nonzero__(self):
        return bool(self._crono_keys)
    
    def __iter__(self):
        return self.log.iteritems()
    
    @property
    def log(self):
        return self._log.copy()
    @log.setter
    def log(self, value):
        self.put(value)
    @log.deleter
    def log(self):
        self._log.clear()
        self._crono_keys = list()
    
    def put(self, value):
        key = self.key_func(*self.func_args, **self.func_kwargs)
        self.put_with_key(key, value)
        return key
        
    def put_with_key(self, key, value):
        self._log[key].append(value)
        self._crono_keys.append(key)
        
    def keys(self):
        return list(self._crono_keys)
    
    def crono_index(self, start=0):
        return len(self._crono_keys) + start

