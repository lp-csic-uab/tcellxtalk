#!/usr/bin/python
# coding: utf-8

from __future__ import division
from __future__ import print_function

"""
:synopsis:   Simple Task Queue based on file-system files with the task information.

:created:    2018-10-11

:authors:    Óscar Gallardo (ogallardo@protonmail.com) at LP-CSIC/UAB (lp.csic@uab.cat)
:copyright:  2016 LP-CSIC/UAB (http://proteomica.uab.cat). Some rights reserved.
:license:    GPLv3 (http://www.gnu.org/licenses/gpl-3.0.html)

:contact:    lp.csic@uab.cat
"""

__VERSION__ = '0.1.6 alpha'
__UPDATED__ = '2018-11-15'

#===============================================================================
# Imports
#===============================================================================
import json
import os
import psutil
import sys
import threading
import time #DEBUG

from digest_combine_func import DigestCombineMixin


#===============================================================================
# Constants
#===============================================================================
DFLT_NAMEDPIPE = os.path.expanduser("~/tmp/simple_taskqueue_tests/simple_taskqueue_waiting")
DFLT_TASK_PATH = os.path.expanduser("~/tmp/simple_taskqueue_tests/")

DEBUG = True


#===============================================================================
# Task Class definitions
#===============================================================================
class SimpleTask(object):
    def __init__(self, task_dict, task_func=None, pass_self=True):
        for key, value in task_dict.items():
            setattr(self, key, value)
        self.result = None
        if task_func is not None:
            if not pass_self:
                self.run = task_func
            else:
                self.run = lambda *args, **kwargs: task_func(self, *args, **kwargs)
        else:
            self.run = lambda *args, **kwargs:None
    
    def start(self, *args, **kwargs):
        self.result =  self.run(*args, **kwargs)
        return self


class DigestCombineMail(DigestCombineMixin, SimpleTask): # IMPLEMENT!
    def __init__(self, task_dict):
        super(self, DigestCombineMail).__init__(task_dict, task_func=self.run, 
                                                pass_self=False)
    
    def run(self, *args, **kwargs):
        pass # IMPLEMENT!


#===============================================================================
# Thread Function definitions
#===============================================================================
def sentinel_func(named_pipe=DFLT_NAMEDPIPE, stop_event=threading.Event()):
    self = threading.current_thread()
    while not stop_event.is_set():
        if not os.path.exists(named_pipe):
            os.mkfifo(named_pipe)
        print('Waiting...')
        with open(named_pipe, 'rb') as fifo:
            rcvd_data = fifo.read()
            if 'STOP' in rcvd_data.upper():
                print('\nStop signal received...\n')
                stop_event.set()
                break
            print( rcvd_data )
    if os.path.exists(named_pipe):
        os.remove(named_pipe)


#===============================================================================
# Function definitions
#===============================================================================
def other_running_instance(prog_name=None, self_pid=None):
    # Check parameters:
    if not prog_name:
        prog_name = sys.argv[0] or __file__
    prog_name = os.path.basename(prog_name)
    if self_pid is None:
        self_pid = os.getpid()
    # Search for similar process instances:
    invalid_states = (psutil.STATUS_DEAD, psutil.STATUS_ZOMBIE, psutil.STATUS_STOPPED)
    other = False #By default no other instances
    for proc in psutil.process_iter():
        if any(prog_name in cmdline_tkn for cmdline_tkn in proc.cmdline()):
            if self_pid != proc.pid and proc.status() not in invalid_states:
                other = True #Other similar running instance found.
                break
    #
    return other


def log_errors(task2cause):
    for taskffn, errno_cause in task2cause.iteritems():
        print(taskffn, errno_cause) #IMPROVE!


def iter_remainig_taskffns(path, yielded_tasks):
    for filen in os.listdir(path):
        if filen.upper().endswith('.JTSK'):
            fullfilen = os.path.join(path, filen)
            if os.path.isfile(fullfilen) and fullfilen not in yielded_tasks:
                yield fullfilen

def iter_taskffns(path):
    yielded_tasks = set()
    remaining_tasks = sorted( iter_remainig_taskffns(path, yielded_tasks) )
    while remaining_tasks:
        next_task = remaining_tasks[0]
        yield next_task
        yielded_tasks.add(next_task)
        remaining_tasks = sorted( iter_remainig_taskffns(path, yielded_tasks) )
    
def iter_tasks_queue(path=DFLT_TASK_PATH, attemps=1):
    failedtask2cause = dict() 
    for task_fullfilen in iter_taskffns(path):
        loadtask_attemps = attemps
        while loadtask_attemps:
            errno_cause = tuple()
            try:
                iofile = open(task_fullfilen, 'rb')
            except IOError as e:
                errno_cause = (e.errno, e.strerror)
            else:
                with iofile:
                    try:
                        yield json.load(iofile)
                    except IOError as e: #File reading error:
                        errno_cause = (e.errno, e.strerror)
                    except ValueError as e: #Error in JSON decoding:
                        errno_cause = (-1, e.message)
                    else:
                        break
            if DEBUG: print(loadtask_attemps, errno_cause) #DEBUG
            loadtask_attemps -= 1
        if errno_cause:
            failedtask2cause[task_fullfilen] = errno_cause
    #
    if failedtask2cause:
        log_errors(failedtask2cause)



def main():
    stop_event = threading.Event()
    sentinel = threading.Thread( name="sentinel_func", target=sentinel_func, 
                                  args=(DFLT_NAMEDPIPE, stop_event) )
    sentinel.daemon = True
    sentinel.start()
    #
    completed_tasks = dict()
    undonetask2cause = dict()
    for task in iter_tasks_queue(DFLT_TASK_PATH, attemps=2):
        if not stop_event.wait(0.1):
            #Do the job
            if DEBUG: print( 'Working on: ', task['id'] ) #DEBUG
            time.sleep(10) #IMPLEMENT!
            #
            if DEBUG: print( task['id'], (0, 'Completed OK') ) #DEBUG
            completed_tasks[task['id']] = (0, 'Completed OK')
        else:
            if DEBUG: print( task['id'], (-2, 'Not processed: simpletask_queue process stopped') ) #DEBUG
            undonetask2cause[task['id']] = (-2, 'Not processed: simpletask_queue process stopped')
#    time.sleep(30) #DEBUG
    #
    if not stop_event.is_set(): #Job finished without interruptions:
        with open(DFLT_NAMEDPIPE, 'wb') as fifo:
            fifo.write('STOP\n')
    sentinel.join(5)
    #
    print('...stopping...')
    return 0



#===============================================================================
# Start script execution from CLI
#===============================================================================
if __name__ == '__main__':
    if not other_running_instance():
        sys.exit( main() )
    else:
        if os.path.exists(DFLT_NAMEDPIPE):
            with open(DFLT_NAMEDPIPE, 'wb') as fifo:
                fifo.write('Other instance launched, but shutting down.\n')
        sys.exit('Another instance is already running.')
